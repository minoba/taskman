/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumVariant;

/**
 * @author Cellingo
 *
 */
public class CurriculumvariantCreationFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private JComboBox<String> curriculumvariantTypeCombo;
	private JComboBox<String> curriculumCombo;
	private String[] curriculumTypeIds;

	public CurriculumvariantCreationFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 750, 400);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Nieuw curriculum");
		/*creates the form/content*/
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "maak een nieuw curriculum aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);
		
		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;
		
		formPanel.add(GuiComponentFactory.getJLabel( "een curriculum hoort bij een opleiding en bestaat uit 1 of meerdere jaren", GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		c.gridwidth = 1;
		c.gridy++;
		//c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "opleiding", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;

		List<Curriculum> currs = controller.getDataCollection().getCurricula();
		String[] currIds = new String[ currs.size()+1 ];
		String[] currNames = new String[ currs.size()+1 ];
		currIds[0] = "kies";
		currNames[0] = "kies";
		for( int i=1; i<=currs.size(); i++ ){
			currIds[i] = currs.get(i-1).getCurriculumID();
			currNames[i] = currs.get(i-1).getName();
		}
		
		curriculumCombo = new JComboBox<String>( currIds );
		formPanel.add(curriculumCombo, c);
		
		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 1;

		formPanel.add( GuiComponentFactory.getJLabel( "curriculum type", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;

		List<String> currTypes = Curriculum.getCurriculumTypes();//controller.getDataCollection( controller.getCurrentYear() ).getCurricula();
		curriculumTypeIds = new String[ currTypes.size()+1 ];
		String[] curriculumTypeNames = new String[ currTypes.size()+1 ];
		curriculumTypeIds[0] = "kies";
		curriculumTypeNames[0] = "kies";
		for( int i=1; i<=currTypes.size(); i++ ){
			curriculumTypeIds[i] = currTypes.get(i-1);
			curriculumTypeNames[i] = Curriculum.getCurriculumTypeDescription(currTypes.get(i-1));
		}

		curriculumvariantTypeCombo = new JComboBox<String>( curriculumTypeNames );
		formPanel.add(curriculumvariantTypeCombo, c);
		
		
		c.gridx = 0;
		c.gridy++;

		JButton ok = new JButton("Bewaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("curriculumvariantcreation");
				if( dataOk ) dispose();
			}
		});
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("curriculumvariantcreation");
					if( dataOk ) dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( ok, c );
		
		c.gridx = 1;
		c.gridwidth = 3;

		JButton okNew = new JButton("Bewaar en maak nog een curriculum");
		okNew.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("curriculumvariantcreation");
				if( dataOk ) clearData();
			}
		});
		okNew.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("curriculumvariantcreation");
					if( dataOk ) clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( okNew, c );
		
		c.gridx = 4;
		c.gridwidth = 1;
		
		JButton cancel = new JButton("Annuleren");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( cancel, c );
	}
	
	/**
	 * verifies the data and processes them if they are OK
	 * @return dataOk
	 */
	private boolean verifyAndProcess(){
		
		String currIdStr = curriculumCombo.getSelectedItem().toString();
		Curriculum curriculum = controller.getDataCollection().getCurriculum(currIdStr);
		if( curriculum == null ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een opleiding geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumCombo.requestFocus();
			return false;
		}
		
		String currVarTypeStr = this.curriculumTypeIds[ curriculumvariantTypeCombo.getSelectedIndex() ];
		if( currVarTypeStr.equals("kies") ){
			JOptionPane.showMessageDialog(getContentPane(), "curriculum moet een type hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumvariantTypeCombo.requestFocus();
			return false;
		}
		if( curriculum.containsCurriculumType(currVarTypeStr) ){
			JOptionPane.showMessageDialog(getContentPane(), "deze opleiding bevat al een curriculum van dit type", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumvariantTypeCombo.requestFocus();
			return false;
		}

		CurriculumVariant currVariant = new CurriculumVariant(curriculum, currVarTypeStr);
		curriculum.addCurriculumVariant(currVariant);
		
		return true;
	}
	
	/**
	 * clears the form for reuse
	 */
	private void clearData(){
		curriculumvariantTypeCombo.setSelectedIndex(0);
		curriculumCombo.setSelectedIndex(0);
	}
}
