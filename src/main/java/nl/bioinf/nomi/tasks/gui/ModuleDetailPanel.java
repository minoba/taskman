/**
 *
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.*;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.PlanningRules.WeekRules;

/**
 * @author Cellingo
 *
 */
public class ModuleDetailPanel extends JPanel {

    /**
     * the curriculumModule being modeled here.
     */
    private final CurriculumModule curriculumModule;
    /**
     * the controller.
     */
    private final TaskManagerController controller;
    /**
     * the array of course method IDs.
     */
    private String[] methodsIdArr;
    /**
     * name.
     */
    private JTextField nameField;
    /**
     * JTextfield for credits.
     */
    private JTextField creditsField;
    /**
     * the course methods.
     */
    private JComboBox<String> courseMethods;
    /**
     * the number of attending students for this curriculumModule.
     */
    private final int attendingStudents;
    /**
     * the school year.
     */
    private final int year;
    /**
     * action to be selected.
     */
    private JComboBox<String> actionSelectionComboBox;
    /**
     * check for fixed number of student groups.
     */
    private JCheckBox fixedGroupNumberCheck;
    /**
     * spinner for number of student groups.
     */
    private JSpinner groupNumberSpinner;
    /**
     * confirm button.
     */
    private AbstractButton setFixedGroups;
    /**
     * requirements textfield for planning rules.
     */
    private JTextField planningRequirementsField;
    /**
     * comments for planning rules.
     */
    private Component planningCommentsField;
    /**
     * error label used for manual group size adjustments.
     */
    private JLabel errorLabel;
    /**
     * error label part 1.
     */
    private final String errorLabelBaseTextOne = "Totaal studenten: ";
    /**
     * error label part 2.
     */
    private final String errorLabelBaseTextTwo = "; ingedeeld: ";
    /**
     * the assigned counts.
     * Stored before really assigned to groups
     */
    private final List<Integer> assignedCounts = new ArrayList<>();
    /**
     * save new distribution.
     */
    private JButton saveStudentDistribution;

    /**
     *
     * @param curriculumModule the curriculumModule
     * @param controller the controller object
     * @param year the school year
     */
    public ModuleDetailPanel(final CurriculumModule curriculumModule, final TaskManagerController controller, final int year) {
        this.curriculumModule = curriculumModule;
        this.controller = controller;
        this.year = year;
        this.attendingStudents = curriculumModule.getAttendingStudents();
        for (StudentGroup sg : curriculumModule.getStudentGroups()) {
            assignedCounts.add(sg.getStudentNumber());
        }
        initialize();
    }

    /**
     * init code.
     */
    private void initialize() {
        /*create action selection combo boxes*/
        String[] actions = {"annuleer", "verwijder groep"};
        this.actionSelectionComboBox = new JComboBox<String>(actions);

        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 4;
        c.insets = GuiComponentFactory.getLayoutInsets();
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        /*add curriculumModule editing form*/
        this.add(createModuleEditingForm(), c);

        c.gridy++;
        this.add(GuiComponentFactory.getJLabel(
                "Klassen die deze curriculumModule volgen (hier niet te veranderen)",
                GuiComponentStylingFactory.DETAIL_COMPONENT), c);

        c.gridy++;

        /*add classes table*/
        this.add(getClassesTablePane(), c);

        c.gridy++;

        /*add group info panel*/
        this.add(createGroupsPanel(), c);

        c.gridy++;

        JButton editPlanningRulesButton = new JButton();
        editPlanningRulesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                editSchedulingRules(curriculumModule);
            }
        });
        /*add scheduling information*/
        if (curriculumModule.getPlanningRules() != null) {
            editPlanningRulesButton.setText("Bewerk planningsregels");
            c.gridy++;
            this.add(createSchedulingRules(editPlanningRulesButton), c);

        } else {
            this.add(GuiComponentFactory.getJLabel(
                    "Deze curriculumModule heeft geen planningregels", GuiComponentStylingFactory.DETAIL_COMPONENT), c);

            c.gridy++;
            editPlanningRulesButton.setText("Maak planningsregels");

            this.add(editPlanningRulesButton, c);
            /*add planning rules*/
        }
    }

    /**
     * editing and creation of planning rules.
     *
     * @param curriculumModule the curriculumModule
     */
    private void editSchedulingRules(final CurriculumModule curriculumModule) {
        PlanningRulesEditingFrame f = new PlanningRulesEditingFrame(curriculumModule, controller);
        f.toFront();
        f.requestFocus();
        f.setVisible(true);
    }

    /**
     * creates the basic curriculumModule info form.
     *
     * @return moduleEditingForm
     */
    private JPanel createModuleEditingForm() {
        JPanel mePanel = new JPanel();
        GuiComponentStylingFactory.stylizeComponent(mePanel, GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout mel = new GridBagLayout();
        mePanel.setLayout(mel);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 5;
        c.gridheight = 1;
        c.insets = GuiComponentFactory.getLayoutInsets();
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = curriculumModule.getName()
                + " (" + curriculumModule.getModuleId()
                + ")       kwartaal " + curriculumModule.getTimeframe().getQuarter();
        mePanel.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 1;

        mePanel.add(GuiComponentFactory.getJLabel("modulenaam", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridwidth = 3;
        c.gridx = 1;

        nameField = new JTextField(curriculumModule.getName(), 40);
        mePanel.add(nameField, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy++;

        mePanel.add(GuiComponentFactory.getJLabel("credits", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        creditsField = new JTextField("" + curriculumModule.getCredits(), 5);
        mePanel.add(creditsField, c);

        c.gridx = 2;

        mePanel.add(GuiComponentFactory.getJLabel("werkvorm", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 3;

        CourseMethod ccm = curriculumModule.getCourseMethod();
        List<CourseMethod> methods = Curriculum.getCourseMethods();
        String[] methodsNameArr = new String[methods.size()];
        methodsIdArr = new String[methods.size()];
        int selectIndex = 0;
        for (int i = 0; i < methods.size(); i++) {
            if (ccm.equals(methods.get(i))) {
                selectIndex = i;
            }
            methodsNameArr[i] = methods.get(i).getName();
            methodsIdArr[i] = methods.get(i).getId();
        }
        courseMethods = new JComboBox<String>(methodsNameArr);
        courseMethods.setSelectedIndex(selectIndex);
        mePanel.add(courseMethods, c);

//        c.gridx = 0;
//        c.gridy++;
//        this.add(GuiComponentFactory.getJLabel("start kwartaal", GuiComponentStylingFactory.FORM_COMPONENT), c);
//        c.gridx = 1;
//        Integer[] numbers = {1,2,3,4};
//        startQuarters = new JComboBox(numbers);
//        this.add( startQuarters, c );
//        c.gridx = 2;
//        this.add(GuiComponentFactory.getJLabel("duur (kwartalen)", GuiComponentStylingFactory.FORM_COMPONENT), c);
//        c.gridx = 3;
//        durationQuarters = new JComboBox(numbers);
//        this.add( durationQuarters, c );
        c.gridx = 0;
        c.gridy++;

        JButton ok = new JButton("bewaar wijzigingen");
        //GuiComponentStylingFactory.stylizeComponent(ok, GuiComponentStylingFactory.FORM_COMPONENT);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                saveChanges();
            }
        });
        ok.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    saveChanges();
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) {
            }

            @Override
            public void keyTyped(final KeyEvent e) {
            }
        });
        mePanel.add(ok, c);

        c.gridx = 1;

        JButton delete = new JButton("verwijder curriculumModule");
        delete.setBackground(Color.ORANGE);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                int n = JOptionPane.showConfirmDialog(
                        null,
                        "Weet u zeker dat u deze curriculumModule wilt verwijderen?",
                        "Bevestig verwijderen",
                        JOptionPane.OK_CANCEL_OPTION);
                if (n == JOptionPane.OK_OPTION) {
                    controller.deleteModule(curriculumModule, year);
                }
            }
        });
        delete.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    int n = JOptionPane.showConfirmDialog(
                            null,
                            "Weet u zeker dat u deze curriculumModule wilt verwijderen?",
                            "Bevestig verwijderen",
                            JOptionPane.OK_CANCEL_OPTION);
                    if (n == JOptionPane.OK_OPTION) {
                        controller.deleteModule(curriculumModule, year);
                    }
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) {
            }

            @Override
            public void keyTyped(final KeyEvent e) {
            }
        });
        mePanel.add(delete, c);

        return mePanel;
    }

    /**
     * creates the groups dividing panel.
     *
     * @return groupsPanel
     */
    private JPanel createGroupsPanel() {
        JPanel groupsPanel = new JPanel();
        GuiComponentStylingFactory.stylizeComponent(groupsPanel, GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout gpl = new GridBagLayout();
        groupsPanel.setLayout(gpl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 6;
        gbc.gridheight = 1;
        gbc.insets = GuiComponentFactory.getLayoutInsets();
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;

        groupsPanel.add(GuiComponentFactory.getJLabel("Groepen die deze voor deze curriculumModule gevormd zijn",
                GuiComponentStylingFactory.DETAIL_COMPONENT), gbc);

        gbc.gridy++;

        groupsPanel.add(
                GuiComponentFactory.getJLabel("(groepen zijn hier te verwijderen en studenten her te verdelen;"
                        + " wijzigingen worden pas doorgevoerd wanneer ",
                        GuiComponentStylingFactory.FORM_COMPONENT), gbc);
        gbc.gridy++;

        groupsPanel.add(
                GuiComponentFactory.getJLabel("op \"bewaar\" geklikt wordt en het totaal "
                        + "aantal correct is voor alle groepen samen)",
                        GuiComponentStylingFactory.FORM_COMPONENT), gbc);

        gbc.gridy++;
        gbc.gridwidth = 3;
        gbc.gridheight = 5;

        groupsPanel.add(getGroupsTablePane(), gbc);

        gbc.gridwidth = 2;
        gbc.gridheight = 1;
        gbc.gridx = 3;

        groupsPanel.add(GuiComponentFactory.getJLabel("gebruik vast aantal groepen",
                GuiComponentStylingFactory.DETAIL_COMPONENT), gbc);

        gbc.gridx = 5;

        this.fixedGroupNumberCheck = new JCheckBox();
        fixedGroupNumberCheck.setSelected(curriculumModule.isGroupNumberLocked());

        fixedGroupNumberCheck.addActionListener(
            new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent arg0) {
                    groupNumberSpinner.setEnabled(fixedGroupNumberCheck.isSelected());

                    if (fixedGroupNumberCheck.isSelected()) {
                        setFixedGroups.setText("Zet groepen vast        ");
                    } else {
                        setFixedGroups.setText("Bepaal groepen dynamisch");
                    }
                    setFixedGroups.setEnabled(true);
                }
            }
        );
        groupsPanel.add(fixedGroupNumberCheck, gbc);

        gbc.gridy++;
        gbc.gridx = 3;
        gbc.gridwidth = 1;

        int initValue = this.curriculumModule.getStudentGroups().size();
        SpinnerModel m = new SpinnerNumberModel(initValue, 0, 30, 1);
        this.groupNumberSpinner = new JSpinner(m);
        groupNumberSpinner.setEnabled(fixedGroupNumberCheck.isSelected());

        groupsPanel.add(groupNumberSpinner, gbc);

//        gbc.gridy++;
        gbc.gridx = 4;
        gbc.gridwidth = 2;

        this.setFixedGroups = new JButton("");
        if (fixedGroupNumberCheck.isSelected()) {
            setFixedGroups.setText("Zet groepen vast        ");
        } else {
            setFixedGroups.setText("Bepaal groepen dynamisch");
            this.setFixedGroups.setEnabled(false);
        }

        setFixedGroups.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                boolean fixedGroups = fixedGroupNumberCheck.isSelected();
                curriculumModule.setGroupNumberLocked(fixedGroups);
                RequiredAction ra;
                if (fixedGroups) {
                    int groupNumber = Integer.parseInt(groupNumberSpinner.getModel().getValue().toString());
                    ra = curriculumModule.setLockedGroupNumber(groupNumber);
                } else {
                    ra = curriculumModule.classSizeChanged();
                }
                controller.doModuleAction(ra, curriculumModule);
            }
        });
        groupsPanel.add(setFixedGroups, gbc);

        gbc.gridx = 3;
        gbc.gridy++;
        gbc.gridwidth = 3;

        //must be declared here to prevent NPE
        this.saveStudentDistribution = new JButton("Bewaar verdeling");

        this.errorLabel = GuiComponentFactory.getJLabel("",
                GuiComponentStylingFactory.DETAIL_COMPONENT);
        groupsPanel.add(errorLabel, gbc);
        setErrorLabelText();

        gbc.gridy++;
        gbc.gridwidth = 2;

        //set disabled until valid and required
        this.saveStudentDistribution.setEnabled(false);
        groupsPanel.add(saveStudentDistribution, gbc);

        saveStudentDistribution.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                //final check
                if (attendingStudents != getAssignedTotal()) {
                    setErrorLabelText();
                    return;
                }
                //loop the student groups and assign the new student count, then update views
                List<StudentGroup> studentGroups = ModuleDetailPanel.this.curriculumModule.getStudentGroups();
                for (int i = 0; i < studentGroups.size(); i++) {
                    studentGroups.get(i).setStudentNumber(assignedCounts.get(i));
                }
                controller.updateViews("moduleoverview");
            }
        });
        return groupsPanel;
    }

    /**
     * sets the error label text.
     */
    private void setErrorLabelText() {
        int assignedCount = getAssignedTotal();
        String text = this.errorLabelBaseTextOne
                + this.attendingStudents
                + this.errorLabelBaseTextTwo
                + getAssignedTotal();
        errorLabel.setText(text);
        //if OK: green
        Color c = new Color(0, 75, 5);
        if (this.attendingStudents == assignedCount) {
            saveStudentDistribution.setEnabled(true);
        } else {
            c = new Color(255, 140, 140);
            saveStudentDistribution.setEnabled(false);
        }
        errorLabel.setForeground(c); //(new Color(175, 175, 50));
    }

    /**
     * returns the assigned total of this table.
     *
     * @return assignedTotal the assigned total
     */
    private int getAssignedTotal() {
        int total = 0;
        for (Integer i : assignedCounts) {
            total += i;
        }
        return total;
    }

    /**
     * adds the scheduling rules form.
     *
     * @param editPlanningRulesButton the button
     * @return the panel
     */
    private JPanel createSchedulingRules(final JButton editPlanningRulesButton) {
        JPanel schedulingRulesPanel = new JPanel();
        GuiComponentStylingFactory.stylizeComponent(schedulingRulesPanel, GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout srl = new GridBagLayout();
        schedulingRulesPanel.setLayout(srl);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 3;
        c.gridheight = 1;
        c.insets = GuiComponentFactory.getLayoutInsets();
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        PlanningRules planningRules = curriculumModule.getPlanningRules();

        StringBuilder titleSb = new StringBuilder("Planningregels voor deze curriculumModule");
        if (planningRules.isDefaultPlanningRules()) {
            titleSb.append("; dit is een standaard plannings regelset genaamd \"")
                    .append(planningRules.getName())
                    .append("\"");
        }
        schedulingRulesPanel.add(
                GuiComponentFactory.getJLabel(titleSb.toString(), GuiComponentStylingFactory.DETAIL_COMPONENT), c);

        c.gridwidth = 1;
        c.gridy++;

        schedulingRulesPanel.add(editPlanningRulesButton, c);

        c.gridy++;

        schedulingRulesPanel.add(
                GuiComponentFactory.getJLabel("Vereisten", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridwidth = 2;
        c.gridx = 1;

        this.planningRequirementsField = new JTextField(planningRules.getRequirements(), 40);
        schedulingRulesPanel.add(planningRequirementsField, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy++;

        schedulingRulesPanel.add(
                GuiComponentFactory.getJLabel("Commentaar", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridwidth = 2;
        c.gridx = 1;

        this.planningCommentsField = new JTextField(planningRules.getComments(), 40);
        schedulingRulesPanel.add(planningCommentsField, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy++;

        schedulingRulesPanel.add(
                GuiComponentFactory.getJLabel("Weekregels", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridy++;

        int weekNumber = 0;
        int lessonCount = 0;

        for (WeekRules weekRule : planningRules.getRules()) {
            StringBuilder sb;
            weekNumber++;
            c.gridx = 0;

            schedulingRulesPanel.add(
                    GuiComponentFactory.getJLabel(("Week " + weekNumber),
                            GuiComponentStylingFactory.FORM_COMPONENT),
                    c);

            int lessonWeekCount = 0;
            for (Lesson lesson : weekRule.getLessons()) {
                lessonCount++;
                lessonWeekCount++;
                c.gridx = 1;

                sb = new StringBuilder("Les ");
                sb.append(lessonCount);
                sb.append(", type=");
                sb.append(lesson.getType());
                sb.append(", duur=");
                sb.append(lesson.getDuration());
                sb.append(", commentaar=");
                sb.append(lesson.getComments());

                schedulingRulesPanel.add(
                        GuiComponentFactory.getJLabel(sb.toString(),
                                GuiComponentStylingFactory.FORM_COMPONENT),
                        c);

                c.gridy++;
            }
            if (lessonWeekCount == 0) {
                c.gridy++;
            }
        }

        return schedulingRulesPanel;
    }

    /**
     * saves changes to curriculumModule data.
     */
    private void saveChanges() {
        String name = nameField.getText();

        if (name != null && (!name.equals(""))) {
            curriculumModule.setName(name);
        }
        try {
            double credits = Double.parseDouble(creditsField.getText());
            curriculumModule.setCredits(credits);
        } catch (Exception ex) {
        }

        try {
            String cmId = methodsIdArr[courseMethods.getSelectedIndex()];
            CourseMethod cm = Curriculum.getCourseMethod(cmId);
            if (cm != null) {
                RequiredAction reqAction = curriculumModule.setMethod(cm);
                if (reqAction != RequiredAction.NONE) {
                    /*let the controller deal with necessary changes*/
                    controller.doModuleAction(reqAction, curriculumModule);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

//        int startQuarter = startQuarters.getSelectedIndex() + 1;
//        int duration = durationQuarters.getSelectedIndex() + 1;
        /*let the controller update the views*/
        controller.updateViews("moduleoverview");
    }

    /**
     * returns a JTable of the attending classes for a curriculumModule.
     *
     * @return jTable
     */
    private JScrollPane getGroupsTablePane() {
        JTable table = new JTable(new GroupsTable());

        TableColumn column;
        for (int i = 0; i < table.getColumnCount(); i++) {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) { //nummer
                column.setPreferredWidth(50);
            } else if (i == 1) { //student aantal
                column.setPreferredWidth(120);
            } else if (i == 2) { //docent
                column.setPreferredWidth(50);
            }
        }
        table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(actionSelectionComboBox));
        table.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(actionSelectionComboBox));

        table.setColumnSelectionAllowed(true);
        table.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(table);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(300, 180));
        return pane;
    }

    /**
     * returns a JTable of the attending classes for a curriculumModule.
     *
     * @return jTable
     */
    private JScrollPane getClassesTablePane() {
        JTable table = new JTable(new ClassesTable());

        TableColumn column;
        for (int i = 0; i < table.getColumnCount(); i++) {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) { //klas
                column.setPreferredWidth(120);
            } else if (i == 1) { //jaar
                column.setPreferredWidth(50);
            } else if (i == 2) { //student aantal
                column.setPreferredWidth(50);
            }
        }
        table.setColumnSelectionAllowed(true);
        table.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(table);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(300, 120));
        return pane;
    }

    /**
     * models the task list of the employee.
     */
    private class GroupsTable extends AbstractTableModel {

        /**
         * the column names.
         */
        private final String[] columnNames = {"Groep", "Docent", "Aantal"};
        /**
         * the student groups.
         */
        private final List<StudentGroup> groups;
        /**
         * default constructor.
         */
        public GroupsTable() {
            groups = curriculumModule.getStudentGroups();
        }

        @Override
        public Class getColumnClass(final int col) {
            if (col == 2) {
                return Integer.class;
            } else {
                return String.class;
            }
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return groups.size();
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            switch (c) {
                case 0:
                    return groups.get(r).getNumber();
                case 1:
                    return groups.get(r).getEmployee().getEmployeeId();
                case 2:
                    return assignedCounts.get(r);
                default:
                    return "";
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }

        @Override
        public boolean isCellEditable(final int row, final int col) {
            return true;
        }

        @Override
        public void setValueAt(final Object value, final int row, final int col) {
            if (groups.size() > 1) {
                /*can only reallocate or remove if there are at least two groups*/

                if (col == 2) {
                    int newStudentNumber = Integer.parseInt(value.toString());
                    assignedCounts.set(row, newStudentNumber);
                    //int assignedStudents = getAssignedTotal();
                    //System.out.println("assignedCounts = " + assignedCounts);
                    //System.out.println("changing value at r:c " + row + ":" + col + " to " + newStudentNumber);

                    setErrorLabelText();
                    fireTableDataChanged();
                    /*redistribute the students*/
//                    int numRows = getRowCount();
//                    int remainingStudents = attendingStudents - newStudentNumber;
//                    boolean correctLastGroup = true;
//                    if (remainingStudents % (numRows - 1) == 0) {
//                        correctLastGroup = false;
//                    }
//                    int studentsPerRow = (int) Math.floor((double) remainingStudents / (numRows - 1));
//
//                    /*TODO what if the group size is exceeded?*/
//                    for (int i = 0; i < numRows; i++) {
//                        if (i == row) {
//                            groups.get(i).setStudentNumber(newStudentNumber);
//                        } else {
//                            groups.get(i).setStudentNumber(studentsPerRow);
//                        }
//                        if (i == numRows - 1 && correctLastGroup) {
//                            groups.get(i).setStudentNumber(studentsPerRow + 1);
//                        }
//                        /*generate the new view of the table*/
//                        fireTableCellUpdated(i, col);
//                    }
                } else if (col < 2) {
                    String action = value.toString();
                    if (action.equals("verwijder groep")) {
                        if (row < groups.size()) {
                            StudentGroup remove = groups.get(row);
                            curriculumModule.removeStudentGroup(remove.getGroupId());
                            assignedCounts.remove(row);
                            fireTableDataChanged();
                        }
                    }
                }
            }
        }
    }

    /**
     * models the attending groups of a curriculumModule.
     */
    private class ClassesTable extends AbstractTableModel {

        /**
         * column names.
         */
        private final String[] columnNames = {"Klas", "Jaar", "Studentaantal"};
        /**
         * the classes.
         */
        private final List<CurriculumYear> classList;

        /**
         * default constructor.
         */
        public ClassesTable() {
            classList = curriculumModule.getAttendingClasses();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return classList.size() + 1;
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            if (r == classList.size()) {
                switch (c) {
                    case 0:
                        return "";
                    case 1:
                        return "Totaal";
                    case 2:
                        return attendingStudents;
                    default:
                        return "";
                }
            } else {
                switch (c) {
                    case 0:
                        return classList.get(r).getCurriculumVariant().getName();
                    case 1:
                        return classList.get(r).getYear();
                    case 2:
                        return classList.get(r).getStudentNumber(curriculumModule.getTimeframe().getQuarter());
                    default:
                        return "";
                }
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }
    }

}
