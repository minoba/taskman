/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CurriculumVariant;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class CurriculumVariantDetailPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private CurriculumVariant curriculumVariant;
	private int schoolYear;
	private TaskManagerController controller;
	
	public CurriculumVariantDetailPanel( TaskManagerController taskManagerController, CurriculumVariant curriculumVariant, int schoolYear ){
		this.controller = taskManagerController;
		this.curriculumVariant = curriculumVariant;
		this.schoolYear = schoolYear;
		initialize();
	}
	
	private void initialize(){
		GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

		/*laying out the panel*/
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 3;
		//c.weightx = 1;
		c.ipadx = 4;
		c.ipady = 4;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		String title = curriculumVariant.getName(); 
		JLabel titleLabel = new JLabel( title + "          " + this.schoolYear + "-" + (this.schoolYear+1) );
		GuiComponentStylingFactory.stylizeComponent(titleLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
		this.add( titleLabel, c );

		c.gridwidth = 1;
		c.gridx = 3;
		
		JButton delete = new JButton("verwijder curriculum uit opleiding");
		delete.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
    			int n = JOptionPane.showConfirmDialog(null, "Weet u zeker dat u dit curriculum wilt verwijderen?", "Bevestig verwijderen", JOptionPane.OK_CANCEL_OPTION);
    			if( n == JOptionPane.OK_OPTION ) controller.deleteCurriculumVariant( curriculumVariant, schoolYear );
			}
		});
		delete.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
	    			int n = JOptionPane.showConfirmDialog(null, "Weet u zeker dat u dit curriculum wilt verwijderen?", "Bevestig verwijderen", JOptionPane.OK_CANCEL_OPTION);
	    			if( n == JOptionPane.OK_OPTION ) controller.deleteCurriculumVariant( curriculumVariant, schoolYear );
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		this.add( delete, c );

		
		c.gridx = 0;
		c.gridy++;
		c.gridy++;

	}
	
}
