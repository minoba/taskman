package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.Project;

/**
 * This class shows the overview of all Projects
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class EmployeesOverviewPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private TaskManagerController controller;
    private List<Employee> employees;

    /**
     * construct with the controller
     *
     * @param controller
     */
    public EmployeesOverviewPanel(TaskManagerController controller) {
        this.controller = controller;
        this.employees = controller.getDataCollection().getEmployees();
        initialize();
    }

    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);

        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "medewerker overzicht van jaar " + controller.getCurrentYear() + " - " + (controller.getCurrentYear() + 1);
        this.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.gridx = 0;
        c.gridy++;

        this.add(GuiComponentFactory.getJLabel("overzicht van medewerkers", GuiComponentStylingFactory.DETAIL_COMPONENT), c);

        c.gridy++;

        this.add(getEmployeesSummaryPane(), c);
    }

    /**
     * returns a JTable of the projects overview
     *
     * @return jTable
     */
    private JScrollPane getEmployeesSummaryPane() {
        MyTableCellRenderer renderer = new MyTableCellRenderer();
        JTable employeesSummaryTable = new JTable(new EmployeesOverviewPanel.EmployeesSummaryTable());
        employeesSummaryTable.setAutoCreateRowSorter(true);
        TableColumn column = null;
        for (int i = 0; i < employeesSummaryTable.getColumnCount(); i++) {
            column = employeesSummaryTable.getColumnModel().getColumn(i);
            if (i == 0) {//naam
                column.setPreferredWidth(200);
                
            } else if (i == 3 || i == 4) {//omschrijving
                column.setPreferredWidth(100);
            } else if (i == 9 || i == 10) {//omschrijving
                column.setPreferredWidth(100);
            }else {
                column.setPreferredWidth(80);
            }
        }
        //employeesSummaryTable.getColumnModel().getColumn(10).setCellRenderer(renderer);
        
        employeesSummaryTable.setColumnSelectionAllowed(true);
        employeesSummaryTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(employeesSummaryTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(1100, 750));
        return pane;
    }
    class MyTableCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            EmployeesSummaryTable model = (EmployeesSummaryTable) table.getModel();
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            int vacantHours = employees.get(row).getVacantHours();
            if( vacantHours < -50 ){
                c.setBackground(Color.RED);
            }
//            else if( vacantHours >= -50 && vacantHours < -10 ){
//                c.setBackground(Color.ORANGE);
//            }
            else if( vacantHours > 20 ){
                c.setBackground(Color.GREEN);
            }
            else{
                c.setBackground(Color.WHITE);
            }
            return c;
        }
    }
    /**
     * models the task list of the employee
     */
    private class EmployeesSummaryTable extends AbstractTableModel {
        private NumberFormat numberFormatter;
        private static final long serialVersionUID = 1L;
        private String[] columnNames = {
                                "Medewerker", "4-letter", "Team", "Aanstelling (fte)", "Aanstelling (uren)", 
                                "Taken kw 1", "Taken kw 2", "Taken kw 3", "Taken kw 4", 
                                "Taakuren jaar", "Balans"};

//        List<Color> rowColours = Arrays.asList( Color.RED, Color.GREEN, Color.LIGHT_GRAY);
        
        public EmployeesSummaryTable() {
            numberFormatter = NumberFormat.getNumberInstance(Locale.ENGLISH);
            numberFormatter.setMaximumFractionDigits(1);
        }

//        public void setRowColour(int row, Color c) {
//            rowColours.set(row, c);
//            fireTableRowsUpdated(row, row);
//        }
        
        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return employees.size();
        }

        @Override
        public Object getValueAt(int r, int c) {
            Employee emp = employees.get(r);
            int[] quarterAndOverallTotals = emp.getQuarterAndOverallTotals();
            switch (c) {
                case 0:
                    return emp.getFullName();
                case 1:
                    return emp.getEmployeeId();
                case 2:
                    return emp.getTeam();
                case 3:
                    return emp.getFte();
                    
                case 4:
                    return emp.getFte() * Employee.getFteSize();//numberFormatter.format(emp.getFte() * Employee.getFteSize() );//employee.getFte()*Employee.getFteSize()
                case 5:
                    return quarterAndOverallTotals[0];
                case 6:
                    return quarterAndOverallTotals[1];
                case 7:
                    return quarterAndOverallTotals[2];
                case 8:
                    return quarterAndOverallTotals[3];
                case 9:
                    return quarterAndOverallTotals[4];
                case 10:
                    return emp.getVacantHours();
                default:
                    return "";
            }
        }

        public String getColumnName(int c) {
            return columnNames[c];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if( columnIndex == 3 || columnIndex == 4 ) return Double.class;
            else if(columnIndex < 6 ) return String.class;
            else return Integer.class;
        }

        
    }
}
