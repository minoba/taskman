package nl.bioinf.nomi.tasks.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.Project;
import nl.bioinf.nomi.tasks.datamodel.ProjectTask;
import nl.bioinf.nomi.tasks.datamodel.TaskLabel;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * This class shows the details of a given Project.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class ProjectDetailPanel extends JPanel {
    /**
     * the controller.
     */
    private final TaskManagerController controller;
    /**
     * the project.
     */
    private final Project project;
    /**
     * the description JTextField.
     */
    private JTextField descField;
    /**
     * the total hours textfield.
     */
    private JTextField totalHoursField;
    /**
     * the employee scrollpane.
     */
    private JScrollPane empSp;
    /**
     * the school year.
     */
    private final int year;
    /**
     * the teacher selection combo.
     */
    private JComboBox<String> teacherSelectionComboBox;
    /**
     * the task labels combo.
     */
    private JComboBox<TaskLabel> taskLabelSelectionComboBox;

    /**
     * construct with the controller.
     *
     * @param controller the controller
     * @param project the project
     */
    public ProjectDetailPanel(final TaskManagerController controller, final Project project) {
        this.controller = controller;
        this.year = controller.getCurrentYear();
        this.project = project;
        initialize();
    }

    /**
     * init code.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);
        /*create teacher selection combo box*/
        List<Employee> eList = controller.getDataCollection().getEmployees(new Comparator<Employee>() {
            @Override
            public int compare(final Employee emp1, final Employee emp2) {
                return emp1.getEmployeeId().compareTo(emp2.getEmployeeId());
            }
        });
        String[] teachers = new String[eList.size() + 1];
        teachers[0] = "kies";
        for (int i = 0; i < eList.size(); i++) {
            teachers[i + 1] = eList.get(i).getEmployeeId() + "@" + eList.get(i).getVacantHours();
        }

        this.teacherSelectionComboBox = new JComboBox<>(teachers);
        teacherSelectionComboBox.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));

        /*create task label selection combo*/
        TaskLabel[] tlArr = TaskLabel.values();
        this.taskLabelSelectionComboBox = new JComboBox<>(tlArr);

        this.setLayout(new BorderLayout());
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        JPanel projectDataPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
        projectDataPanel.setLayout(gbl);
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 3;
        //c.fill = GridBagConstraints.NONE;
        c.ipadx = 4;
        c.ipady = 4;
        c.weightx = 0;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "project " + project.getName() + " van jaar " + year + " - " + (year + 1);
        JLabel titleLabel = GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT);
        GuiComponentStylingFactory.stylizeComponent(titleLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        projectDataPanel.add(titleLabel, c);

        c.gridwidth = 1;
        c.gridx = 3;

        JButton export = new JButton("exporteer als pdf");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                controller.writeProjectToPdf(project, year);
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    controller.writeProjectToPdf(project, year);
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        projectDataPanel.add(export, c);

        c.gridx = 4;

        JButton delete = new JButton("verwijder project");
        delete.setBackground(Color.ORANGE);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                int n = JOptionPane.showConfirmDialog(
                        null,
                        "Weet u zeker dat u dit project wilt verwijderen?",
                        "Bevestig verwijderen",
                        JOptionPane.OK_CANCEL_OPTION);
                if (n == JOptionPane.OK_OPTION) {
                    controller.deleteProject(project);
                }
            }
        });
        delete.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    int n = JOptionPane.showConfirmDialog(
                            null,
                            "Weet u zeker dat u dit project wilt verwijderen?",
                            "Bevestig verwijderen",
                            JOptionPane.OK_CANCEL_OPTION);
                    if (n == JOptionPane.OK_OPTION) {
                        controller.deleteProject(project);
                    }
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        projectDataPanel.add(delete, c);

        c.gridx = 0;
        c.gridy++;

        JLabel descLabel = new JLabel("omschrijving");
        GuiComponentStylingFactory.stylizeComponent(descLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(descLabel, c);

        c.gridx = 1;

        descField = new JTextField(project.getDescription());
        descField.setColumns(25);
        projectDataPanel.add(descField, c);

        c.gridx = 0;
        c.gridy++;
        
        JLabel totalHoursLabel = new JLabel("beschikbaar aantal uren");
        GuiComponentStylingFactory.stylizeComponent(totalHoursLabel, GuiComponentStylingFactory.LIST_COMPONENT);
        projectDataPanel.add(totalHoursLabel, c);

        c.gridx = 1;

        totalHoursField = new JTextField("" + project.getTotalHours());
        totalHoursField.setColumns(5);
        projectDataPanel.add(totalHoursField, c);

        c.gridx = 0;
        c.gridy++;

        JLabel taskNumberLabel = new JLabel("totaal aantal taken: ");
        GuiComponentStylingFactory.stylizeComponent(taskNumberLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(taskNumberLabel, c);

        c.gridx = 1;

        JLabel taskNumberValueLabel = new JLabel("" + project.getTasks().size());
        GuiComponentStylingFactory.stylizeComponent(taskNumberValueLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(taskNumberValueLabel, c);

        c.gridx = 0;
        c.gridy++;

        JLabel totalHoursAssigned = new JLabel("toegewezen uren: ");
        GuiComponentStylingFactory.stylizeComponent(totalHoursAssigned, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(totalHoursAssigned, c);

        c.gridx = 1;

        JLabel totalHoursAssignedValue = new JLabel("" + project.getAssignedHours());
        GuiComponentStylingFactory.stylizeComponent(
                totalHoursAssignedValue, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(totalHoursAssignedValue, c);

        c.gridx = 0;
        c.gridy++;

        JLabel totalEurosAssigned = new JLabel("toegewezen euros: ");
        GuiComponentStylingFactory.stylizeComponent(totalEurosAssigned, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(totalEurosAssigned, c);

        c.gridx = 1;

        JLabel totalEurosAssignedValue = new JLabel("" + (int) Math.round(project.getAssignedHoursCost()));
        GuiComponentStylingFactory.stylizeComponent(
                totalEurosAssignedValue, GuiComponentStylingFactory.DETAIL_COMPONENT);
        projectDataPanel.add(totalEurosAssignedValue, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;

        JButton ok = new JButton("bewaar wijzigingen");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                saveChanges();
            }
        });
        ok.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    saveChanges();
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        projectDataPanel.add(ok, c);

        c.gridy++;

        JLabel spacerLabel = new JLabel(" ");
        projectDataPanel.add(spacerLabel, c);

        c.gridy++;

        JLabel tasksLabel = new JLabel("projecttaken");
        GuiComponentStylingFactory.stylizeComponent(tasksLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        projectDataPanel.add(tasksLabel, c);

        c.gridwidth = 4;

        this.add(projectDataPanel, BorderLayout.PAGE_START);

        empSp = new JScrollPane(getEmployeeTable());
        GuiComponentStylingFactory.stylizeComponent(empSp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        empSp.setPreferredSize(new Dimension(850, 450));
        this.add(empSp, BorderLayout.CENTER);
    }

    /**
     * saves changes to the Project.
     */
    private void saveChanges() {
        try {
            int hours = Integer.parseInt(totalHoursField.getText());
            project.setTotalHours(hours);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "er is een foute uren-waarde ingevoerd: " + totalHoursField.getText()
                    + "\n; wijzigingen zijn niet", "Error", JOptionPane.WARNING_MESSAGE);

            return;
        }
        project.setDescription(descField.getText());
//        project.setHboHoursInclusive(hboHoursInclusiveCheck.isSelected());
    }

    /**
     * constructs the Employee tasks table.
     *
     * @return tasks table
     */
    private JTable getEmployeeTable() {
        JTable empTable = new JTable(new ProjectTaskTable(project));
        empTable.setAutoCreateRowSorter(true);
        TableColumn column;
        for (int i = 0; i < 5; i++) {
            column = empTable.getColumnModel().getColumn(i);
            if (i == 1 || i == 2 || i == 3) {
                column.setPreferredWidth(150);
            } else {
                column.setPreferredWidth(70);
            }
        }
        TableColumn teacherColumn = empTable.getColumnModel().getColumn(0);
        teacherColumn.setCellEditor(new DefaultCellEditor(this.teacherSelectionComboBox));

        TableColumn taskLabelsColumn = empTable.getColumnModel().getColumn(3);
        taskLabelsColumn.setCellEditor(new DefaultCellEditor(this.taskLabelSelectionComboBox));

        empTable.setColumnSelectionAllowed(true);
        empTable.setRowSelectionAllowed(true);
        return empTable;
    }

    /**
     * models the task list of the employee.
     */
    private class ProjectTaskTable extends AbstractTableModel {

        /**
         * the tsaks list.
         */
        private List<ProjectTask> tasks;

        /**
         * the column names for the table.
         */
        private final String[] columnNames = {
            "4-letter code",
            "Naam",
            "Omschrijving",
            "Taak label",
            "Uren",
            "Start (kwartaal)",
            "Duur (kwartalen)"};

        /**
         * contstructs with project.
         * @param project the project
         */
        public ProjectTaskTable(final Project project) {
            this.tasks = new ArrayList<ProjectTask>();
            tasks.addAll(project.getTasks());
            Collections.sort(tasks, new Comparator<ProjectTask>() {
                @Override
                public int compare(final ProjectTask t1, final ProjectTask t2) {
                    return t1.getEmployee().getEmployeeId().compareTo(t2.getEmployee().getEmployeeId());
                }
            });
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return tasks.size();
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            ProjectTask t = tasks.get(r);
            switch (c) {
                case 0:
                    return t.getEmployee().getEmployeeId();
                case 1:
                    return t.getEmployee().getFullName();
                case 2:
                    return t.getName();
                case 3:
                    return t.getTaskLabel();
                case 4:
                    return t.getTaskHours();
                case 5:
                    return t.getTimeframe().getQuarter();
                case 6:
                    return t.getTimeframe().getDuration();
                default:
                    return "";
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }

        /*Don't need to implement this method unless your table's editable.*/
        @Override
        public boolean isCellEditable(final int row, final int col) {
            return col != 1;
        }

        @Override
        public void setValueAt(final Object value, final int row, final int col) {
            //{"4-letter code", "Naam", "Omschrijving", "Uren", "Start (kwartaal)", "Duur (kwartalen)"};
            //System.out.println("changing value at r:c " + row + ":" + col);
            if (col == 0) {
                String emp = value.toString().substring(0, 4);
                if (!emp.equals("kies")) {
                    Employee newEmpl = controller.getDataCollection().getEmployee(emp);

                    /*first retrieve the employee that is being removed*/
                    ProjectTask changedTask = tasks.get(row);
                    Employee removedEmpl = changedTask.getEmployee();

                    /*lets the controller process all relevant changes*/
                    controller.swapProjectTaskEmployee(changedTask, removedEmpl, newEmpl);
                }
            } else if (col == 2) {
                /*name was changed*/
                tasks.get(row).setName(value.toString());
            } else if (col == 3) {
                /*task label was changed*/
                tasks.get(row).setTaskLabel((TaskLabel) value);
            } else if (col == 4) {
                /*total hours was changed*/
                try {
                    int newHours = Integer.parseInt(value.toString());
                    if (newHours == 0) {
                        ProjectTask pt = tasks.get(row);
                        project.removeTask(pt);
                        pt.getEmployee().removeProjectTask(pt);
                        tasks.remove(row);
                        controller.setViewedEmployee(pt.getEmployee());
                        controller.updateViews("projectdetail_panel");

                        return;
                    } else {
                        tasks.get(row).setTaskHours(newHours);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (col == 5) {
                /*total hours was changed*/
                try {
                    int newStart = Integer.parseInt(value.toString());
                    int newDuration = Integer.parseInt(getValueAt(row, 5).toString());
                    //System.out.println("start=" + newStart + " duration=" + newDuration);
                    tasks.get(row).setTimeframe(new Timeframe(newStart, newDuration));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (col == 6) {
                /*total hours was changed*/
                try {
                    int newStart = Integer.parseInt(getValueAt(row, 4).toString());
                    int newDuration = Integer.parseInt(value.toString());
                    //System.out.println("start=" + newStart + " duration=" + newDuration);
                    tasks.get(row).setTimeframe(new Timeframe(newStart, newDuration));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            fireTableCellUpdated(row, col);
            controller.setViewedEmployee(tasks.get(row).getEmployee());
            controller.updateViews("projectsoverview");
        }

    }
}
