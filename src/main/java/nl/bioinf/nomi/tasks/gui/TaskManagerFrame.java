/**
 *
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

import nl.bioinf.nomi.tasks.configuration.TaskManConfig;
import nl.bioinf.nomi.tasks.control.TaskManagerController;

/**
 * the main frame of the application.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class TaskManagerFrame extends JFrame {
    /**
     * The curricula tab pane.
     */
    public static final int CURRICULA_TAB = 0;
    /**
     * The employees tab pane.
     */
    public static final int EMPLOYEE_TAB = 1;
    /**
     * The projects tab pane.
     */
    public static final int PROJECTS_TAB = 2;
    /**
     * The classes tab pane.
     */
    public static final int CLASSES_TAB = 3;
    /**
     * The modules tab pane.
     */
    public static final int MODULES_TAB = 4;
    //public static final int PLANNING_TAB = 5;
    /**
     * The overview tab pane.
     */
    public static final int OVERVIEW_TAB = 6;
    /**
     * The curriculum variants tab pane.
     */
    public static final int CURRICULUM_VARIANT_TAB = 7;
    /**
     * The regular tasks tab pane.
     */
    public static final int REGULAR_TASKS_TAB = 8;
    /**
     * The financial overview tab pane.
     */
    public static final int FINANCES_TAB = 9;
    /**
     * the main controller.
     */
    private final TaskManagerController controller;
    /**
     * the tab containers.
     */
    private final TabPanel[] tabs = new TabPanel[10];
    /**
     * THE MENU BAR.
     */
    private JMenuBar menuBar;

    /**
     * default constructor.
     *
     * @param controller the controller
     */
    public TaskManagerFrame(final TaskManagerController controller) {
        this.controller = controller;

        /*get screen dimensions*/
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenHeight = screenSize.height;
        int screenWidth = screenSize.width;

        /*set screen dimensions to maximum*/
        //setBounds(30, 30, 1400, screenHeight - 300);
        setBounds(30, 30, screenWidth - 100, screenHeight - 80);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);

        /*set frame icon and title*/
        Image img = kit.getImage("tasks_icon_30_30.gif");
        setIconImage(img);
        setTitle(TaskManConfig.getString(TaskManConfig.MAIN_TITLE));
        GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

        /*add the menu bar*/
        addMenuBar();
        /*add the tabs*/
        addTabbedPane();
    }

    /**
     * adds the tabbed pane to the main frame.
     */
    private void addTabbedPane() {
        JTabbedPane tp = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
        GuiComponentStylingFactory.stylizeComponent(tp, GuiComponentStylingFactory.TAB_COMPONENT);
        add(tp);
        ImageIcon img = new ImageIcon("tasks_icon_30_30.gif");

        TabPanel cuPanel = new TabPanel();
        tabs[CURRICULA_TAB] = cuPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_ZERO_TITLE),
                img,
                cuPanel,
                TaskManConfig.getString(TaskManConfig.TABS_ZERO_POPUP));

        TabPanel plPanel = new TabPanel();
        tabs[CURRICULUM_VARIANT_TAB] = plPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_ONE_TITLE),
                img,
                plPanel,
                TaskManConfig.getString(TaskManConfig.TABS_ONE_POPUP));

        TabPanel tPanel = new TabPanel();
        tabs[EMPLOYEE_TAB] = tPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_TWO_TITLE),
                img,
                tPanel,
                TaskManConfig.getString(TaskManConfig.TABS_TWO_POPUP));

        TabPanel mPanel = new TabPanel();
        tabs[MODULES_TAB] = mPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_THREE_TITLE),
                img,
                mPanel,
                TaskManConfig.getString(TaskManConfig.TABS_THREE_POPUP));

        TabPanel rtPanel = new TabPanel();
        tabs[REGULAR_TASKS_TAB] = rtPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_FOUR_TITLE),
                img,
                rtPanel,
                TaskManConfig.getString(TaskManConfig.TABS_FOUR_POPUP));

        TabPanel pPanel = new TabPanel();
        tabs[PROJECTS_TAB] = pPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_FIVE_TITLE),
                img,
                pPanel,
                TaskManConfig.getString(TaskManConfig.TABS_FIVE_POPUP));

        TabPanel clPanel = new TabPanel();
        tabs[CLASSES_TAB] = clPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_SIX_TITLE),
                img,
                clPanel,
                TaskManConfig.getString(TaskManConfig.TABS_SIX_POPUP));

        TabPanel oPanel = new TabPanel();
        tabs[OVERVIEW_TAB] = oPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_SEVEN_TITLE),
                img,
                oPanel,
                TaskManConfig.getString(TaskManConfig.TABS_SEVEN_POPUP));

        TabPanel fPanel = new TabPanel();
        tabs[FINANCES_TAB] = fPanel;
        tp.addTab(TaskManConfig.getString(TaskManConfig.TABS_EIGHT_TITLE),
                img,
                fPanel,
                TaskManConfig.getString(TaskManConfig.TABS_EIGHT_POPUP));
    }

    /**
     * returns one of the tab panels.
     *
     * @param index the index of the tab panel
     * @return tabPanel
     */
    public TabPanel getTabPanel(final int index) {
        return tabs[index];
    }

    /**
     * creates and adds a menu bar to the frame.
     */
    private void addMenuBar() {
        this.menuBar = new JMenuBar();
        GuiComponentStylingFactory.stylizeComponent(menuBar, GuiComponentStylingFactory.MENU_COMPONENT);
        setJMenuBar(menuBar);

        addFileMenu();
        addEditMenu();
        addInsertMenu();
        addExportMenu();
        addHelpMenu();
    }

    /**
     * adds the help menu.
     */
    private void addHelpMenu() {
        /*help menu*/
        JMenu hm = new JMenu(TaskManConfig.getString(TaskManConfig.MENU_HELP));
        GuiComponentStylingFactory.stylizeComponent(hm, GuiComponentStylingFactory.MENU_COMPONENT);
        menuBar.add(hm);

        JMenuItem ha = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_HELP_ABOUT));
        GuiComponentStylingFactory.stylizeComponent(ha, GuiComponentStylingFactory.MENU_COMPONENT);
        ha.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.SHOW_HELP_ABOUT);
            }
        });
        hm.add(ha);

        JMenuItem hi = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_HELP_ITEMS));
        GuiComponentStylingFactory.stylizeComponent(hi, GuiComponentStylingFactory.MENU_COMPONENT);
        hi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.SHOW_HELP_ITEMS);
            }
        });
        hm.add(hi);
    }

    /**
     * adds the export menu.
     */
    private void addExportMenu() {
        /*export menu*/
        JMenu expm = new JMenu(TaskManConfig.getString(TaskManConfig.MENU_EXPORT));
        GuiComponentStylingFactory.stylizeComponent(expm, GuiComponentStylingFactory.MENU_COMPONENT);
        menuBar.add(expm);

        JMenuItem tle = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EXPORT_TASKLISTS));
        GuiComponentStylingFactory.stylizeComponent(tle, GuiComponentStylingFactory.MENU_COMPONENT);
        tle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXPORT_TASKOVERVIEWS);
            }
        });
        expm.add(tle);

        JMenuItem ate = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EXPORT_ALL_TASKS));
        GuiComponentStylingFactory.stylizeComponent(ate, GuiComponentStylingFactory.MENU_COMPONENT);
        ate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXPORT_ALL_TASKS);
            }
        });
        expm.add(ate);

        JMenuItem cle = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EXPORT_CURRICULUM_OVERVIEW));
        GuiComponentStylingFactory.stylizeComponent(cle, GuiComponentStylingFactory.MENU_COMPONENT);
        cle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXPORT_CURRICULA);
            }
        });
        expm.add(cle);

        JMenuItem ioe = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EXPORT_INTERNSHIP_OCCUPATION));
        GuiComponentStylingFactory.stylizeComponent(ioe, GuiComponentStylingFactory.MENU_COMPONENT);
        ioe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXPORT_INTERNSHIP_OVERVIEW);
            }
        });
        expm.add(ioe);

        JMenuItem pe = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EXPORT_PROJECT_OVERVIEW));
        GuiComponentStylingFactory.stylizeComponent(pe, GuiComponentStylingFactory.MENU_COMPONENT);
        pe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXPORT_PROJECTS_OVERVIEW);
            }
        });
        expm.add(pe);

        JMenuItem pre = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EXPORT_PLANNING_RULES));
        GuiComponentStylingFactory.stylizeComponent(pre, GuiComponentStylingFactory.MENU_COMPONENT);
        pre.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXPORT_PLANNING_RULES);
            }
        });
        expm.add(pre);
    }

    /**
     * adds the insert menu.
     */
    private void addInsertMenu() {

        /*insert menu*/
        JMenu im = new JMenu(TaskManConfig.getString(TaskManConfig.MENU_ADD));
        GuiComponentStylingFactory.stylizeComponent(im, GuiComponentStylingFactory.MENU_COMPONENT);
        menuBar.add(im);

        JMenuItem ti = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_EMPLOYEE));
        GuiComponentStylingFactory.stylizeComponent(ti, GuiComponentStylingFactory.MENU_COMPONENT);
        ti.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_EMPLOYEE);
            }
        });
        im.add(ti);

        JMenuItem rti = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_REGULAR_TASK));
        GuiComponentStylingFactory.stylizeComponent(rti, GuiComponentStylingFactory.MENU_COMPONENT);
        rti.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_REGULAR_TASK);
            }
        });
        im.add(rti);

        JMenuItem pi = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_PROJECT));
        GuiComponentStylingFactory.stylizeComponent(pi, GuiComponentStylingFactory.MENU_COMPONENT);
        pi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_PROJECT);
            }
        });
        im.add(pi);

        JMenuItem pti = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_PROJECT_TASK));
        GuiComponentStylingFactory.stylizeComponent(pti, GuiComponentStylingFactory.MENU_COMPONENT);
        pti.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_PROJECT_TASK);
            }
        });
        im.add(pti);

        JMenuItem wi = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_COURSE_METHOD));
        GuiComponentStylingFactory.stylizeComponent(wi, GuiComponentStylingFactory.MENU_COMPONENT);
        wi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EDIT_COURSE_METHODS);
            }
        });
        im.add(wi);

        JMenuItem mi = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_MODULE));
        GuiComponentStylingFactory.stylizeComponent(mi, GuiComponentStylingFactory.MENU_COMPONENT);
        mi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_MODULE);
            }
        });
        im.add(mi);

        JMenuItem opli = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_EDUCATION));
        GuiComponentStylingFactory.stylizeComponent(opli, GuiComponentStylingFactory.MENU_COMPONENT);
        opli.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_CURRICULUM);
            }
        });
        im.add(opli);

        JMenuItem ci = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_CURRICULUM));
        GuiComponentStylingFactory.stylizeComponent(ci, GuiComponentStylingFactory.MENU_COMPONENT);
        ci.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_CURRICULUMVARIANT);
            }
        });
        im.add(ci);

        JMenuItem cy = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_ADD_CLASS));
        GuiComponentStylingFactory.stylizeComponent(cy, GuiComponentStylingFactory.MENU_COMPONENT);
        cy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_CURRICULUMYEAR);
            }
        });
        im.add(cy);
    }

    /**
     * adds the edit menu.
     */
    private void addEditMenu() {
        /*edit menu*/
        JMenu em = new JMenu(TaskManConfig.getString(TaskManConfig.MENU_EDIT));
        GuiComponentStylingFactory.stylizeComponent(em, GuiComponentStylingFactory.MENU_COMPONENT);
        menuBar.add(em);

        JMenuItem mde = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EDIT_METADATA));
        GuiComponentStylingFactory.stylizeComponent(mde, GuiComponentStylingFactory.MENU_COMPONENT);
        mde.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EDIT_DATACOLLECTION);
            }
        });
        em.add(mde);

        JMenuItem we = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EDIT_COURSE_METHODS));
        GuiComponentStylingFactory.stylizeComponent(we, GuiComponentStylingFactory.MENU_COMPONENT);
        we.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EDIT_COURSE_METHODS);
            }
        });
        em.add(we);

        JMenuItem cte = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_EDIT_CURRICULUM_TYPES));
        GuiComponentStylingFactory.stylizeComponent(cte, GuiComponentStylingFactory.MENU_COMPONENT);
        cte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EDIT_CURRICULUM_TYPES);
            }
        });
        em.add(cte);
    }

    /**
     * adds the file menu.
     */
    private void addFileMenu() {
        /*file menu*/
        JMenu fm = new JMenu(TaskManConfig.getString(TaskManConfig.MENU_FILE));
        GuiComponentStylingFactory.stylizeComponent(fm, GuiComponentStylingFactory.MENU_COMPONENT);
        menuBar.add(fm);

        JMenuItem fn = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_FILE_NEW));
        GuiComponentStylingFactory.stylizeComponent(fn, GuiComponentStylingFactory.MENU_COMPONENT);
        fn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.CREATE_DATACOLLECTION);
            }
        });
        fm.add(fn);

        JMenuItem fshow = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_FILE_SHOW));
        GuiComponentStylingFactory.stylizeComponent(fshow, GuiComponentStylingFactory.MENU_COMPONENT);
        fshow.setEnabled(false);
        fm.add(fshow);

        JMenuItem fo = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_FILE_OPEN));
        GuiComponentStylingFactory.stylizeComponent(fo, GuiComponentStylingFactory.MENU_COMPONENT);
        fo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.FILE_OPEN);
            }
        });
        fm.add(fo);

        JMenuItem fs = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_FILE_SAVE));
        GuiComponentStylingFactory.stylizeComponent(fs, GuiComponentStylingFactory.MENU_COMPONENT);
        fs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.SAVE_FILE);
            }
        });
        fm.add(fs);

        JMenuItem fsa = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_FILE_SAVE_AS));
        GuiComponentStylingFactory.stylizeComponent(fsa, GuiComponentStylingFactory.MENU_COMPONENT);
        fsa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.SAVE_AS_FILE);
            }
        });
        fm.add(fsa);

        JMenuItem fe = new JMenuItem(TaskManConfig.getString(TaskManConfig.MENU_FILE_EXIT));
        GuiComponentStylingFactory.stylizeComponent(fe, GuiComponentStylingFactory.MENU_COMPONENT);
        fe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                controller.doAction(GuiAction.EXIT);
            }
        });
        fm.add(fe);
    }
}
