/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package nl.bioinf.nomi.tasks.gui;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.*;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;

/**
 * gives an overview of all finances associated with task labels and categories.
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class FinancialOverviewPanel extends JPanel {

    /**
     * default constructor.
     */
    public FinancialOverviewPanel() {
        initialize();
    }

    /**
     * init code creates the view.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);
        //get the controller instance
        TaskManagerController controller = TaskManagerController.getInstance();

        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "Financieel overzicht van jaar "
                + controller.getCurrentYear() + " - " + (controller.getCurrentYear() + 1);
        this.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.gridy++;

        this.add(getTaskAndFormationPlanPane(), c);
    }

    /**
     * returns a JTable of all the project tasks.
     *
     * @return jTable
     */
    private JScrollPane getTaskAndFormationPlanPane() {
        FinancesTableModel model = new FinancesTableModel();
        JTable financesTable = new JTable(model);

        TableColumn column;
        for (int i = 0; i < financesTable.getColumnCount(); i++) {
            column = financesTable.getColumnModel().getColumn(i);
            if (i == 0 || i == 1) {
                column.setPreferredWidth(300);
            } else if (i == 2) {
                column.setPreferredWidth(100);
            } else {
                column.setPreferredWidth(100);
                column.setCellRenderer(new NumberCellRenderer());
            }
        }
        financesTable.setColumnSelectionAllowed(true);
        financesTable.setRowSelectionAllowed(true);
        financesTable.setAutoCreateRowSorter(true);
        JScrollPane pane = new JScrollPane(financesTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(1000, 650));
        return pane;
    }

    /**
     * models the finances table.
     */
    private static class FinancesTableModel extends AbstractTableModel {

        private final String[] columnNames;
        private final Class[] columnClasses;
        private final ContentProvider[] contentProviders;
        private final int rows;

        /**
         * constructor.
         */
        public FinancesTableModel() {
            columnNames = new String[]{"categorie", "label", "gpl", "uren", "euros"};
            columnClasses = new Class[]{
                String.class,
                String.class,
                Double.class,
                Integer.class,
                Integer.class};

//            System.out.println("[FinancesTableModel] TaskCategory.values().length = " + TaskCategory.values().length);
//            System.out.println("[FinancesTableModel] TaskLabel.values().length = " + TaskLabel.values().length);

            //omitting "Professionalisering"
            this.rows = (TaskCategory.values().length) + (TaskLabel.values().length) + 1;
            contentProviders = new ContentProvider[rows];

            initContentProviders();
        }

//        @Override
//        public Class<?> getColumnClass(final int columnIndex) {
//            return this.columnClasses[columnIndex];
//        }
        @Override
        public String getColumnName(final int column) {
            return this.columnNames[column];
        }

        @Override
        public int getRowCount() {
            return this.rows;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
//            System.out.println("rowIndex = " + rowIndex + " colIndex = " + columnIndex);
//            System.out.println("contentProviders[rowIndex] = " + contentProviders[rowIndex]);
            if (contentProviders[rowIndex] == null) return "<null>";
            return contentProviders[rowIndex].provideContent(columnIndex);
        }

        /**
         * creates the row content providers.
         */
        private void initContentProviders() {
            TaskCategory[] catValues = TaskCategory.values();
            //TODO reverted omitting "Professionalisering" is correct?
            //catValues = Arrays.copyOfRange(catValues, 1, catValues.length);
            //TaskLabel[] labelValues = TaskLabel.values();
            int row = 0;
            ContentProvider cp;
            for (TaskCategory cat : catValues) {
                cp = new CategoryContentProvider(cat);
                this.contentProviders[row++] = cp;
                //create label rows
                List<TaskLabel> labels = cat.getTaskLabels();
                for (TaskLabel label : labels) {
                    cp = new LabelContentProvider(label);
                    this.contentProviders[row++] = cp;
                }
            }
            //totals provider
            this.contentProviders[row++] = new TotalsContentProvider();
        }

        /**
         * used by table model to serve content in a row-dependent manner.
         */
        private interface ContentProvider {

            /**
             * provides the content for each row.
             *
             * @param column the table column
             * @return content
             */
            Object provideContent(int column);
        }

        /**
         * provides category content.
         */
        private class CategoryContentProvider implements ContentProvider {

            private final TaskCategory category;

            /**
             * constructs with category.
             *
             * @param category the cat
             */
            public CategoryContentProvider(final TaskCategory category) {
                this.category = category;
            }

            @Override
            public Object provideContent(final int column) {
                //"Category", "Label", "gpl", "uren", "euros"
                switch (column) {
                    case 0:
                        return this.category.toString();
                    case 2:
                        return Employee.getGpl();
                    case 3:
                        return getCategoryHours(category);
                    case 4:
                        return Employee.getGpl() * getCategoryHours(category);
                    default:
                        return "";
                }
            }

        }

        /**
         * provides label content.
         */
        private class LabelContentProvider implements ContentProvider {

            private final TaskLabel label;

            /**
             * construct with label.
             *
             * @param label the label
             */
            public LabelContentProvider(final TaskLabel label) {
                this.label = label;
            }

            @Override
            public Object provideContent(final int column) {
                //"Category", "Label", "gpl", "uren", "euros"
                switch (column) {
                    case 1:
                        return this.label.toString();
                    case 2:
                        return Employee.getGpl();
                    case 3:
                        return getLabelHours(label);
                    case 4:
                        return Employee.getGpl() * getLabelHours(label);
                    default:
                        return "";
                }
            }
        }

        /**
         * provides label content.
         */
        private class TotalsContentProvider implements ContentProvider {

            @Override
            public Object provideContent(final int column) {
                //"Category", "Label", "gpl", "uren", "euros"
                switch (column) {
                    case 0:
                        return "TOTALEN";
                    case 3:
                        return getTotalHours();
                    case 4:
                        return getTotalEuros();
                    default:
                        return "";
                }
            }
        }

        /**
         * returns the total hours of the planning.
         *
         * @return totalHours
         */
        private int getTotalHours() {
            int totalHours = 0;
            for (int i = 0; i < getRowCount() - 1; i++) {
                if (contentProviders[i] instanceof CategoryContentProvider) {
                    int hours = (Integer) getValueAt(i, 3);
                    totalHours += hours;
                }
            }
            return totalHours;
        }

        /**
         * returns the total hours of the planning.
         *
         * @return totalHours
         */
        private double getTotalEuros() {
            double totalEuros = 0;
            for (int i = 0; i < getRowCount() - 1; i++) {
                if (contentProviders[i] instanceof CategoryContentProvider) {
                    double euros = (Double) getValueAt(i, 4);
                    totalEuros += euros;
                }
            }
            return totalEuros;
        }
        
        /**
         * fetches and returns the hours associated with a task category.
         *
         * @param category the task category
         * @return hours
         */
        private Integer getCategoryHours(final TaskCategory category) {
            int totalHours = 0;
            for (TaskLabel label : category.getTaskLabels()) {
                totalHours += getLabelHours(label);
            }
            return totalHours;
        }

        /**
         * fetches and returns the hours associated with a task label.
         *
         * @param label the label
         * @return hours
         */
        private Integer getLabelHours(final TaskLabel label) {
            

            YearDataCollection ydc = TaskManagerController.getInstance().getDataCollection();
            int totalHours = 0;
            /*iterate all task groups to sum the totals, starting with student groups*/
            for (CurriculumModule curriculumModule : ydc.getModules()) {
                for (StudentGroup sg : curriculumModule.getStudentGroups()) {
                    if (sg.getTaskLabel() == label) {
                        totalHours += sg.getTaskHours();
                    }
                }
            }
            /*regular tasks*/
            for (RegularTask rt : ydc.getRegularTasks()) {
                if (rt.getTaskLabel() == label) {
                    totalHours += rt.getTaskHours();
                }
            }
            /*project tasks*/
            for (Project p : ydc.getProjects()) {
                for (ProjectTask pt : p.getTasks()) {
                    if (pt.getTaskLabel() == label) {
                        totalHours += pt.getTaskHours();
                    }
                }
            }
            return totalHours;
        }
    }

    /**
     * number cell editor.
     */
    public class NumberCellRenderer extends DefaultTableCellRenderer {

        /**
         * constructs super as well.
         */
        NumberCellRenderer() {
            setHorizontalAlignment(SwingConstants.RIGHT);
        }

        @Override
        public void setValue(final Object aValue) {
            Object result = aValue;
            if ((aValue != null) && (aValue instanceof Number)) {
                Number numberValue = (Number) aValue;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(0);
                formatter.setGroupingUsed(true);
                result = formatter.format(numberValue.doubleValue());
            }
            super.setValue(result);
        }
    }
}
