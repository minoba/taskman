/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.HashMap;


/**
 * the GUI component styling factory
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class GuiComponentStylingFactory {
	public static final int GENERAL_COMPONENT = 0;
	public static final int MENU_COMPONENT = 1;
	public static final int TAB_COMPONENT = 2;
	public static final int PANEL_COMPONENT = 3;
	public static final int FORM_COMPONENT = 4;
	public static final int LIST_COMPONENT = 5;
	public static final int TREE_COMPONENT = 6;
	public static final int DETAIL_COMPONENT = 7;
	public static final int TITLE_COMPONENT = 8;
//	public static final int TABLE_COMPONENT = 8;
	
	/**
	 * the colors hashmap
	 */
	private static HashMap<Integer, ComponentStyle> styles;
	
	static{
		styles = new HashMap<Integer, ComponentStyle>();
		
		ComponentStyle generalStyle = new ComponentStyle();
		generalStyle.backGroundColor = new Color(250,250,255);
		generalStyle.font = new Font(Font.SANS_SERIF, Font.BOLD, 12);
		styles.put( GENERAL_COMPONENT, generalStyle );
		
		ComponentStyle menuStyle = new ComponentStyle();
		menuStyle.backGroundColor = new Color(250,250,200);
		menuStyle.font = new Font(Font.SANS_SERIF, Font.BOLD, 13);
		styles.put( MENU_COMPONENT, menuStyle );
		
		ComponentStyle tabStyle = new ComponentStyle();
		tabStyle.backGroundColor = new Color(255,255,235);
		tabStyle.font = new Font(Font.SANS_SERIF, Font.BOLD, 13);
		styles.put( TAB_COMPONENT, tabStyle );
		
		ComponentStyle panelStyle = new ComponentStyle();
		panelStyle.backGroundColor = new Color(250,250,255);
		panelStyle.font = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
		styles.put( PANEL_COMPONENT, panelStyle );
		
		ComponentStyle formStyle = new ComponentStyle();
		formStyle.backGroundColor = new Color(250,250,255);
		formStyle.font = new Font(Font.SANS_SERIF, Font.PLAIN, 11);
		styles.put( FORM_COMPONENT, formStyle );
		
		ComponentStyle listStyle = new ComponentStyle();
		listStyle.backGroundColor = generalStyle.backGroundColor;
		listStyle.font = new Font(Font.SANS_SERIF, Font.PLAIN, 13);
		styles.put( LIST_COMPONENT, listStyle );
		
		ComponentStyle detailsStyle = new ComponentStyle();
		detailsStyle.backGroundColor = new Color(240,240,200);
		detailsStyle.font = new Font(Font.SANS_SERIF, Font.BOLD, 13);
		styles.put( DETAIL_COMPONENT, detailsStyle );
		
		ComponentStyle titleStyle = new ComponentStyle();
		titleStyle.backGroundColor = new Color(240,240,200);
		titleStyle.font = new Font(Font.SANS_SERIF, Font.BOLD, 20);
		styles.put( TITLE_COMPONENT, titleStyle );

		ComponentStyle treeStyle = new ComponentStyle();
		treeStyle.backGroundColor = generalStyle.backGroundColor;
		treeStyle.font = new Font(Font.SANS_SERIF, Font.BOLD, 10);
		styles.put( TREE_COMPONENT, treeStyle );

	}

	/**
	 * adds a Font to the factory
	 * @param target the target type (MENU, TAB etc)
	 * @param color
	 */
	public static void addColor( int target, Color color ){
		styles.get(target).backGroundColor = color;
	}
	
	/**
	 * adds a font for a given tareget
	 * @param target
	 * @param font
	 */
	public static void addFont( int target, Font font ){
		styles.get(target).font = font;
	}
	
	/**
	 * styliza a Component according to target specs
	 * @param comp
	 * @param target
	 */
	public static void stylizeComponent( Component comp, int target  ){
		comp.setBackground( styles.get(target).backGroundColor );
		comp.setFont( styles.get(target).font );
	}
	
	
	/**
	 * inner class to encapsulate style information
	 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
	 * @version 0.1
	 */
	private static class ComponentStyle{
		private Font font;
		private Color backGroundColor;
		
	}

}
