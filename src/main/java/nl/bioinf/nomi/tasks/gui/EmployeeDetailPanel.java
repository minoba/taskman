package nl.bioinf.nomi.tasks.gui;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.*;
import nl.bioinf.nomi.tasks.io.FileExporter;
import nl.bioinf.nomi.tasks.io.TaskListEmailSender;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class creates the Employee detail panel that is found under the tab "Employee". In this panel, Employee
 * proprties can be changed and the task list of an employee can be exported.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 1.0
 */
public class EmployeeDetailPanel extends JPanel {

    private final Employee employee;
    private JTextField fteValueField;
    private final int year;
    private JTextField teamValueField;
    private JTextField remarksValueField;
    private final TaskManagerController controller;
    private JTextField emailValueField;

    /**
     *
     * @param controller the controller
     * @param employee the employee
     * @param year the school year
     */
    public EmployeeDetailPanel(final TaskManagerController controller, final Employee employee, final int year) {
        this.controller = controller;
        this.employee = employee;
        this.year = year;
        initialize();
    }

    /**
     * intializer code.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);
        this.setLayout(new BorderLayout());
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        JPanel emplDataPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
        emplDataPanel.setLayout(gbl);
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 3;
        c.ipadx = 4;
        c.ipady = 4;
        c.weightx = 0;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        JLabel titleLabel = new JLabel(employee.getFullName()
                + " (" + employee.getEmployeeId() + ")       " + this.year + "-" + (this.year + 1) + "     ");
        GuiComponentStylingFactory.stylizeComponent(titleLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        emplDataPanel.add(titleLabel, c);

        c.gridwidth = 1;
        c.gridx = 3;

        JButton export = new JButton("exporteer als pdf");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                //controller.writeEmployeeToPdf(employee, year);
                FileExporter.writeEmployeeToPdf(employee, year, controller.getMainFrame());
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    //controller.writeEmployeeToPdf(employee, year);
                    FileExporter.writeEmployeeToPdf(employee, year, controller.getMainFrame());
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        emplDataPanel.add(export, c);

        c.gridx = 4;

        JButton email = new JButton("email takenlijst");
        email.setBackground(new Color(10, 175, 50));
        email.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                sendEmail();
            }
        });
        email.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    sendEmail();
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        if (employee.getEmail() == null) {
            email.setEnabled(false);
        }
        emplDataPanel.add(email, c);

        if (!employee.getEmployeeId().equals(Employee.getVacantEmployee().getEmployeeId())) {
            c.gridx = 5;

            JButton delete = new JButton("verwijder medewerker");
            delete.setBackground(Color.ORANGE);
            delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    int n = JOptionPane.showConfirmDialog(
                            null,
                            "Weet u zeker dat u deze medewerker wilt verwijderen?",
                            "Bevestig verwijderen",
                            JOptionPane.OK_CANCEL_OPTION);
                    if (n == JOptionPane.OK_OPTION) {
                        controller.deleteEmployee(employee, year);
                    }
                }
            });
            delete.addKeyListener(new KeyListener() {
                @Override
                public void keyPressed(final KeyEvent e) {
                    if (e.getKeyCode() == 10) {
                        int n = JOptionPane.showConfirmDialog(
                                null,
                                "Weet u zeker dat u deze medewerker wilt verwijderen?",
                                "Bevestig verwijderen",
                                JOptionPane.OK_CANCEL_OPTION);
                        if (n == JOptionPane.OK_OPTION) {
                            controller.deleteEmployee(employee, year);
                        }
                    }
                }

                @Override
                public void keyReleased(final KeyEvent e) { }

                @Override
                public void keyTyped(final KeyEvent e) { }
            });
            emplDataPanel.add(delete, c);
        }

        c.gridx = 0;
        c.gridy++;

        JLabel fteLabel = new JLabel("aanstelling");
        GuiComponentStylingFactory.stylizeComponent(fteLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(fteLabel, c);

        c.gridx = 1;

        fteValueField = new JTextField(employee.getFte() + "");
        fteValueField.setColumns(5);
        emplDataPanel.add(fteValueField, c);

        c.gridx = 2;

        int totalHours = employee.getAssignableHours();
        JLabel fteValLabel = new JLabel(" (" + totalHours + " uren)");
        GuiComponentStylingFactory.stylizeComponent(fteValLabel, GuiComponentStylingFactory.LIST_COMPONENT);
        emplDataPanel.add(fteValLabel, c);

        c.gridx = 0;
        c.gridy++;

        JLabel teamLabel = new JLabel("team");
        GuiComponentStylingFactory.stylizeComponent(teamLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(teamLabel, c);

        c.gridx = 1;

        teamValueField = new JTextField(employee.getTeam());
        teamValueField.setColumns(5);
        emplDataPanel.add(teamValueField, c);

        c.gridx = 0;
        c.gridy++;

        JLabel emailLabel = new JLabel("email");
        GuiComponentStylingFactory.stylizeComponent(emailLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(emailLabel, c);

        c.gridx = 1;
        c.gridwidth = 2;
        emailValueField = new JTextField(employee.getEmail());
        emailValueField.setColumns(15);
        emplDataPanel.add(emailValueField, c);

        c.gridx = 0;
        c.gridwidth = 1;
        c.gridy++;

        JLabel remarksLabel = new JLabel("opmerkingen");
        GuiComponentStylingFactory.stylizeComponent(remarksLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(remarksLabel, c);

        c.gridx = 1;
        c.gridwidth = 5;
        remarksValueField = new JTextField(employee.getRemarks());
        remarksValueField.setColumns(50);
        emplDataPanel.add(remarksValueField, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;

        JButton ok = new JButton("bewaar wijzigingen");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                saveChanges();
            }
        });
        ok.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    saveChanges();
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        emplDataPanel.add(ok, c);

        c.gridy++;

        JLabel spacerLabel = new JLabel(" ");
        emplDataPanel.add(spacerLabel, c);

        c.gridy++;
        c.gridwidth = 2;

        JLabel totalHoursLabel = new JLabel("1: Aanstellingsgrootte ");
        GuiComponentStylingFactory.stylizeComponent(totalHoursLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(totalHoursLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        JLabel totalHoursValLabel = new JLabel("" + totalHours);
        GuiComponentStylingFactory.stylizeComponent(totalHoursValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(totalHoursValLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;

        JLabel hboIncHoursLabel = new JLabel("2: Totaal van niet curriculumgebonden taken");
        GuiComponentStylingFactory.stylizeComponent(hboIncHoursLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(hboIncHoursLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        JLabel hboIncHoursValLabel = new JLabel("" + employee.getNonCurriculumboundHours());
        GuiComponentStylingFactory.stylizeComponent(hboIncHoursValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(hboIncHoursValLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;

        JLabel totalMinHboHoursLabel = new JLabel("3: Totaal van curriculumgebonden taken (waarin 10% HBO uren)");
        GuiComponentStylingFactory.stylizeComponent(totalMinHboHoursLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(totalMinHboHoursLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        JLabel totalMinHboHoursValLabel = new JLabel("" + employee.getCurriculumboundHours());
        GuiComponentStylingFactory.stylizeComponent(totalMinHboHoursValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(totalMinHboHoursValLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;

        JLabel profHoursLabel = new JLabel("4: Professionaliseringsuren (4a + 4b)");
        GuiComponentStylingFactory.stylizeComponent(profHoursLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(profHoursLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        int professionalizationHours = employee.getProfessionalizationHours();
        JLabel profHoursValLabel = new JLabel("" + professionalizationHours);
        GuiComponentStylingFactory.stylizeComponent(profHoursValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(profHoursValLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;

        JLabel freeSpendableProfHoursLabel = new JLabel("    4a: Vrije professionaliseringsuren (40 uur bij >0,4 fte) ");
        GuiComponentStylingFactory.stylizeComponent(freeSpendableProfHoursLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(freeSpendableProfHoursLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        JLabel freeSpendableProfHoursValLabel = new JLabel("" + employee.getFreelySpendableProfessionalizationHours());
        GuiComponentStylingFactory.stylizeComponent(freeSpendableProfHoursValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(freeSpendableProfHoursValLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;

        JLabel assignableProfHoursLabel = new JLabel("    4b: Schoolgestuurde professionalisering ((CurrGebTaken / 1.1 * 0.15) - 80)  ");
        GuiComponentStylingFactory.stylizeComponent(assignableProfHoursLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(assignableProfHoursLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        JLabel assignableProfHoursValLabel = new JLabel("" + employee.getCummunalProfessionalizationHours());
        GuiComponentStylingFactory.stylizeComponent(assignableProfHoursValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(assignableProfHoursValLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;


        JLabel vacantLabel = new JLabel("5: Resturen (1 - 2 - 3 - 4b); negatief is overwerk");
        GuiComponentStylingFactory.stylizeComponent(vacantLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        emplDataPanel.add(vacantLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;

        JLabel vacantValLabel = new JLabel("" + employee.getVacantHours());
        GuiComponentStylingFactory.stylizeComponent(vacantValLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        if (employee.getVacantHours() < 0) {
            vacantValLabel.setForeground(Color.RED);
        } else {
            vacantValLabel.setForeground(Color.BLUE);
        }
        emplDataPanel.add(vacantValLabel, c);

        c.gridx = 0;
        c.gridy++;

        JLabel spacerLabel2 = new JLabel(" ");
        emplDataPanel.add(spacerLabel2, c);

        c.gridy++;

        c.gridwidth = 4;

        JLabel tasksLabel = new JLabel("Takentabel");
        GuiComponentStylingFactory.stylizeComponent(tasksLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        emplDataPanel.add(tasksLabel, c);

//        c.gridwidth = 4;

        this.add(emplDataPanel, BorderLayout.PAGE_START);

        JTable empTable = new JTable(new EmployeeTaskTable(employee));

        TableColumn column;
        for (int i = 0; i < 7; i++) {
            column = empTable.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(300);
            } else if (i == 2) {
                column.setPreferredWidth(350);
            } else if (i == 1) {
                column.setPreferredWidth(30);
            } else {
                column.setPreferredWidth(50);
            }
        }
        empTable.setAutoCreateRowSorter(true);
        empTable.setColumnSelectionAllowed(true);
        empTable.setRowSelectionAllowed(true);
        JScrollPane sp = new JScrollPane(empTable);
        GuiComponentStylingFactory.stylizeComponent(sp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        sp.setPreferredSize(new Dimension(1000, 450));
        this.add(sp, BorderLayout.CENTER);
    }

    /**
     * sends the tsak list as email attachment.
     */
    private void sendEmail() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateStr = format.format(new Date());
        String input = "\r\n" + employee.getFirstName() + ", hier is je concept takenlijst voor schooljaar "
                + year + "-" + (year + 1)
                + "\r\ngegenereerd op " + dateStr
                + "\r\nHij is verstuurd door " + System.getProperty("user.name", "planner SILS")
                + "vanuit het planningssysteem TaskMan."
                + "\r\nGebruik dit email adres niet om te reply-en!\r\n";
        JTextArea textArea = new JTextArea(8, 50);
        textArea.setText(input);
        textArea.setEditable(true);
        JScrollPane scrollPane = new JScrollPane(textArea);
        int n = JOptionPane.showConfirmDialog(
                null,
                scrollPane,
                "Geef email text",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE); //ConfirmDialog(null, scrollPane);
        if (n == JOptionPane.OK_OPTION) {
            String messageBody = textArea.getText();
            try {
                TaskListEmailSender.sendTaskListPDF(employee, year, messageBody);
            } catch (Exception ex) {
                Logger.getLogger(EmployeeDetailPanel.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(
                        null,
                        "Could not send email: " + ex.getMessage(),
                        "Email error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * saves changes to the Employee.
     */
    private void saveChanges() {
        try {
            double newFte = Double.parseDouble(fteValueField.getText());
            if (employee.getFte() != newFte) {
                /*only process when changed*/
                employee.setFte(newFte);
                controller.updateViews("employeeoverview");
            }
            String newTeam = teamValueField.getText();
            if (!employee.getTeam().equals(newTeam)) {
                employee.setTeam(newTeam);
            }

            String newEmail = emailValueField.getText();
            if (employee.getEmail() == null || !employee.getEmail().equals(newEmail)) {
                employee.setEmail(newEmail);
            }

            String newRemarks = remarksValueField.getText();
            if (!employee.getRemarks().equals(newRemarks)) {
                employee.setRemarks(newRemarks);
            }

        } catch (final Exception ex) {
            /*do nothing: ignore but log*/
            Logger.getLogger(EmployeeDetailPanel.class.getName()).log(Level.OFF, "error saving changes", ex);
        }
    }

    /**
     * models the task list of the employee.
     */
    private class EmployeeTaskTable extends AbstractTableModel {

        private final List<Task> tasks;
        private final String[] columnNames = {
            "Taak", "Type", "Taak label", "Kwartaal 1", "Kwartaal 2", "Kwartaal 3", "Kwartaal 4", "Totaal"};
        private final int[] quarterTotals = new int[5];
        private final String[] quarterFteSizes = new String[5];

        private NumberFormat nf = NumberFormat.getNumberInstance();
        /**
         *
         * @param employee the employee
         */
        public EmployeeTaskTable(final Employee employee) {
            nf.setMaximumFractionDigits(2);
            this.tasks = new ArrayList<Task>();
            tasks.addAll(employee.getProjectTasks());
            tasks.addAll(employee.getRegularTasksWithCommunalProfessionalizationTask());
            tasks.addAll(employee.getStudentGroups());
//            tasks.add(employee.getCommunalProfessionalizationHoursAsTask());
            calculateQuarterTotals();
        }

        /**
         * calculates the quarter totals.
         */
        private void calculateQuarterTotals() {
            /**
             * q1 / q2 / q3 / q4 / year
             */
            double[] qHours = new double[5];
            for (Task t : tasks) {
                for (int i = 0; i <= 3; i++) {
                    double taskHours = t.getQuarterHoursRaw(i + 1);
                    qHours[i] += taskHours;
                }
                qHours[4] += t.getTaskHours();
            }
            for (int q = 0; q < qHours.length; q++) {
                quarterTotals[q] = (int) Math.round(qHours[q]);
                quarterFteSizes[q] = nf.format(quarterTotals[q] / ((double) Employee.getFteSize() / 4));
            }
            quarterFteSizes[4] = nf.format(quarterTotals[4] / (double) Employee.getFteSize());
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return tasks.size() + 2;
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            if (r == tasks.size()) {
                if (c == 0) {
                    return "Totalen";
                } else if (c == 1 || c == 2) {
                    return " ";
                } else {
                    return quarterTotals[c - 3];
                }
            } else if (r == tasks.size() + 1) {
                //task pressure in fte
                if (c == 0) {
                    return "Taakbelasting (fte)";
                } else if (c == 1 || c == 2) {
                    return " ";
                } else {
                    return quarterFteSizes[c - 3];
                }
            } else {
                Task t = tasks.get(r);
                if (c == 0) {
                    String content;
                    if (! t.isCurriculumBound()) {
                        content = "*" + t.getName();
                    } else {
                        content = t.getName();
                    }
                    return content;
                } else if (c == 1) {
                    String type = "";
                    if (t instanceof ProjectTask) {
                        type = "P";
                    } else if (t instanceof RegularTask) {
                        type = "R";
                    } else if (t instanceof StudentGroup) {
                        type = "O";
                    } else {
                        type = "";
                    }
                    return type;
                } else if (c == 2) {
                    return t.getTaskLabel().toString();
                } else if (c == 7) {
                    return t.getTaskHours();
                } else {
                    int qh = t.getQuarterHours(c - 2);
                    if (qh == 0) {
                        return "";
                    }
                    return qh;
                }
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }
    }
}
