package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.Lesson;
import nl.bioinf.nomi.tasks.datamodel.Lesson.LessonType;
import nl.bioinf.nomi.tasks.datamodel.PlanningRules;
import nl.bioinf.nomi.tasks.datamodel.PlanningRules.WeekRules;

public class PlanningRulesEditingFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private static final Double[] lessonDurationTimes = 
		{Double.valueOf(0.15), Double.valueOf(0.30), Double.valueOf(0.45), Double.valueOf(1.0), Double.valueOf(1.5), Double.valueOf(2.0), Double.valueOf(3.0), Double.valueOf(4.5), Double.valueOf(6.0), Double.valueOf(8)};

	private CurriculumModule curriculumModule;
	/*this form keeps a cloned copy of the modules' planning rules. Only on commit, the original planning rules are replaced*/
	private PlanningRules planningRules;
	private JPanel contentPane;
	private JPanel formPanel;
	private String titleText;
	private JTextField requirementsTextField;
	private JTextField commentsTextField;
	
	//private HashMap<Integer,WeekRuleFormElement> formElements = new HashMap<Integer, WeekRuleFormElement>();
	private TaskManagerController controller;
	
	public PlanningRulesEditingFrame(CurriculumModule curriculumModule, TaskManagerController controller ){
		this.curriculumModule = curriculumModule;
		this.controller = controller;
		if( curriculumModule.getPlanningRules() != null ){
			this.planningRules = curriculumModule.getPlanningRules().clone();
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 750, 900);
		
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		createForm();
		
		setTitle("planningsrgels maken en bewerken voor curriculumModule " + this.titleText);
	}
	
	private void createForm(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		this.contentPane = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( contentPane );
		/*remove all for refilling of form*/
		this.getContentPane().removeAll();
		this.getContentPane().add( sp );
		contentPane.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.insets = GuiComponentFactory.getLayoutInsets();
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		this.titleText = curriculumModule.getName() + " (" + curriculumModule.getModuleId() + ")       kwartaal " + curriculumModule.getTimeframe().getQuarter();
		contentPane.add(GuiComponentFactory.getJLabel( titleText, GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		if( curriculumModule.getPlanningRules() != null && curriculumModule.getPlanningRules().isDefaultPlanningRules() ){
			c.gridy++;
			contentPane.add(GuiComponentFactory.getJLabel( "Dit is een standaard planningsregel set!", GuiComponentStylingFactory.DETAIL_COMPONENT), c);
			c.gridy++;
			contentPane.add(GuiComponentFactory.getJLabel( "Bij wijzigingen hieraan zal een niet-standaard regelset aangemaakt worden voor deze curriculumModule ", GuiComponentStylingFactory.DETAIL_COMPONENT), c);
		}
		
		c.gridy++;
		c.gridwidth = 2;
		
		contentPane.add( GuiComponentFactory.getJLabel("Kies eventueel een van de standaard planningssets", GuiComponentStylingFactory.FORM_COMPONENT), c );
		
		c.gridx = 2;
		c.gridwidth = 1;
		
		/*create defaultchooser*/
		String[] defaultNames = PlanningRules.getDefaultNames();
		String[] defaultNamesAndChoose = new String[defaultNames.length + 1];
		defaultNamesAndChoose[0] = "kies";
		for( int i=0; i<defaultNames.length; i++){
			defaultNamesAndChoose[i+1] = defaultNames[i];
		}
		JComboBox<String> defaultChooser = new JComboBox<String>(defaultNamesAndChoose);
		defaultChooser.addActionListener(
			new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					@SuppressWarnings("unchecked")
					String defaultName = (String)((JComboBox<String>)ae.getSource()).getSelectedItem();
					PlanningRules pr = PlanningRules.getDefault(defaultName);
					PlanningRulesEditingFrame.this.planningRules = pr;
					//System.out.println(pr);
					//formPanel.removeAll();
					//JPanel planningRulesPanel = createPlanningRulesPanel(pr);
					//formPanel.add(planningRulesPanel);
					createForm();
					
					/*repaint the frame*/
					paintComponents(getGraphics());
				}
			}
		);
		contentPane.add(defaultChooser, c);
		
		c.gridy++;
		c.gridwidth = 1;
		c.gridx = 0;

		contentPane.add(getOKbutton(), c);

		c.gridx = 1;
		
		contentPane.add(getCancelButton(), c);

		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 4;
		
		this.formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		contentPane.add(formPanel, c);
		
		if( this.planningRules != null ){
			JPanel planningRulesPanel = createPlanningRulesPanel(this.planningRules);
			formPanel.add(planningRulesPanel);
		}
		
		c.gridy++;
		c.gridwidth = 1;
		c.gridx = 0;

		contentPane.add(getOKbutton(), c);

		c.gridx = 1;
		
		contentPane.add(getCancelButton(), c);
		
	}
	
	private JButton getOKbutton(){
		JButton ok = new JButton( "OK" );
		ok.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				processFormData();
			}
		} );

		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					processFormData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		return ok;
	}
	
	private JButton getCancelButton(){
		JButton cancel = new JButton( "Cancel" );
		cancel.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PlanningRulesEditingFrame.this.dispose();
			}
		} );

		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					PlanningRulesEditingFrame.this.dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});

		return cancel;
	}

	private JPanel createPlanningRulesPanel(PlanningRules planningRules) {
		JPanel planningRulesPanel = new JPanel();
		GuiComponentStylingFactory.stylizeComponent(planningRulesPanel, GuiComponentStylingFactory.PANEL_COMPONENT);
		
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		planningRulesPanel.setLayout(gbl);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 5;
		c.insets = GuiComponentFactory.getLayoutInsets();
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		c.gridwidth = 1;
		
		planningRulesPanel.add( GuiComponentFactory.getJLabel("Vereisten", GuiComponentStylingFactory.FORM_COMPONENT), c );

		c.gridx++;
		c.gridwidth = 4;
		
		this.requirementsTextField = new JTextField();
		requirementsTextField.setColumns(44);
		requirementsTextField.setText(planningRules.getRequirements());
		requirementsTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				PlanningRulesEditingFrame.this.planningRules.setRequirements(requirementsTextField.getText());
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {/*ignored*/}
		});
		planningRulesPanel.add(requirementsTextField, c);
		
		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 1;

		planningRulesPanel.add( GuiComponentFactory.getJLabel("Commentaar", GuiComponentStylingFactory.FORM_COMPONENT), c );

		c.gridx++;
		c.gridwidth = 4;
		
		this.commentsTextField = new JTextField();
		commentsTextField.setColumns(44);
		commentsTextField.setText(planningRules.getComments());
		commentsTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				PlanningRulesEditingFrame.this.planningRules.setComments(commentsTextField.getText());
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {/*ignored*/}
		});
		planningRulesPanel.add(commentsTextField, c);
		
		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 1;
		
		for( WeekRules wr : planningRules.getRules() ){
			
			c.gridx = 0;
			
			planningRulesPanel.add( GuiComponentFactory.getJLabel("Week "+wr.getWeekNumber(), GuiComponentStylingFactory.FORM_COMPONENT), c);
			
			
			//WeekRuleFormElement wrfe = new WeekRuleFormElement();
			
			//this.formElements.put(wr.getWeekNumber(), wrfe);
			int count = 0;
			for( count=0; count<wr.getLessons().size(); count++){ //Lesson l : wr.getLessons() ){
				Lesson l = wr.getLessons().get(count);

				c.gridx = 1;
				planningRulesPanel.add( GuiComponentFactory.getJLabel("Les "+(count+1), GuiComponentStylingFactory.FORM_COMPONENT), c);
				
				c.gridx = 2;
				
				LessonType lt = l.getType();
				int selectedIndex = 0;
				for( int i=0; i<LessonType.values().length; i++ ){
					if( LessonType.values()[i] == lt ) selectedIndex=i;
				}

				JComboBox<LessonType> lessonTypeCombo = new JComboBox<LessonType>(LessonType.values());
				lessonTypeCombo.setSelectedIndex(selectedIndex);
				lessonTypeCombo.setActionCommand(wr.getWeekNumber()+";"+count);
				lessonTypeCombo.addActionListener( new ActionListener() {
					@SuppressWarnings("unchecked")
					@Override
					public void actionPerformed(ActionEvent e) {
						String[] elmnts = e.getActionCommand().split(";");
						int week = Integer.parseInt(elmnts[0]);
						int lesson = Integer.parseInt(elmnts[1]);
						Lesson l = PlanningRulesEditingFrame.this.planningRules.getRules().get(week-1).getLessons().get(lesson);
						//System.out.println("lesson l=" + l);
						l.setType( (LessonType)(((JComboBox<String>)e.getSource()).getSelectedItem()) );
						//System.out.println("lesson l=" + l);
					}
				});
				planningRulesPanel.add( lessonTypeCombo, c);
				
				c.gridx = 3;
	
				//System.out.println("duration="+l.getDuration());
				
				SpinnerModel spinnerModel = new SpinnerListModel(lessonDurationTimes);
				spinnerModel.setValue( Double.valueOf(l.getDuration()) );
				JSpinner durationSpinner = new JSpinner(spinnerModel);
				durationSpinner.setName(wr.getWeekNumber()+";"+count);
				
				durationSpinner.addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
						JSpinner src = ((JSpinner)e.getSource());
						
						String[] elmnts = src.getName().split(";");
						int week = Integer.parseInt(elmnts[0]);
						int lesson = Integer.parseInt(elmnts[1]);
						Lesson l = PlanningRulesEditingFrame.this.planningRules.getRules().get(week-1).getLessons().get(lesson);
						//System.out.println("lesson l=" + l);
						l.setDuration( (Double)src.getValue() );
						//System.out.println("lesson l=" + l);
					}
				});
				planningRulesPanel.add( durationSpinner, c);
				
				c.gridx = 4;
				
				JTextField commentsTextField = new JTextField(l.getComments());
				commentsTextField.setColumns(30);
				commentsTextField.setName(wr.getWeekNumber()+";"+count);
				commentsTextField.addFocusListener(new FocusListener() {
					@Override
					public void focusLost(FocusEvent e) {
						JTextField src = ((JTextField)e.getSource());
						
						String[] elmnts = src.getName().split(";");
						int week = Integer.parseInt(elmnts[0]);
						int lesson = Integer.parseInt(elmnts[1]);
						Lesson l = PlanningRulesEditingFrame.this.planningRules.getRules().get(week-1).getLessons().get(lesson);
						//System.out.println("lesson l=" + l);
						l.setComments( src.getText() );
						//System.out.println("lesson l=" + l);
					}
					@Override
					public void focusGained(FocusEvent arg0) {/*ignore*/}
				});
				planningRulesPanel.add( commentsTextField, c);
				
				//wrfe.addLessonElements(new LessonElements(lessonTypeCombo, durationSpinner, commentsTextField));
				
				
				c.gridx = 5;
				
				
				JButton removeButton = new JButton("Verwijder");
				removeButton.setActionCommand(wr.getWeekNumber()+";"+count);
				removeButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent ae) {
						String command = ae.getActionCommand();
						String[] elmnts = command.split(";");
						int week = Integer.parseInt(elmnts[0]);
						int lesson = Integer.parseInt(elmnts[1]);
													
						PlanningRules pr = PlanningRulesEditingFrame.this.planningRules;
						List<Lesson> thisWeeksLessons = pr.getRules().get(week-1).getLessons();
						thisWeeksLessons.remove(lesson);
						//System.out.println("removing lesson " + lesson + " from week " + week );
						
						createForm();
						paintComponents(getGraphics());
					}
				});

				
				planningRulesPanel.add(removeButton, c);
				
				c.gridy++;
			}
			
			JButton addButton = new JButton("Voeg les toe");
			addButton.setActionCommand(wr.getWeekNumber()+"");
			addButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					int week = Integer.parseInt(ae.getActionCommand());
												
					PlanningRules pr = PlanningRulesEditingFrame.this.planningRules;
					List<Lesson> thisWeeksLessons = pr.getRules().get(week-1).getLessons();
					
					//System.out.println("adding lesson to week " + week + " which already has " + thisWeeksLessons.size() );
					
					if( thisWeeksLessons.size() == 0 ){
						pr.addLesson(week, new Lesson(LessonType.THEORY, Lesson.STANDARD_DURATION));
					}
					else{
						Lesson previousLesson = thisWeeksLessons.get(thisWeeksLessons.size()-1);
						pr.addLesson(week, new Lesson(previousLesson.getType(), previousLesson.getDuration()));
					}
					createForm();
					paintComponents(getGraphics());
				}
			});
			c.gridx = 2;
			c.gridwidth = 2;
			c.gridy++;
			planningRulesPanel.add( addButton, c);
			c.gridwidth = 1;
			c.gridy++;
		}
		return planningRulesPanel;
	}

	private void processFormData(){
		curriculumModule.setPlanningRules(planningRules);
		controller.updateViews("");
		this.dispose();
	}
	
	
//	private class WeekRuleFormElement{
//		private ArrayList<LessonElements> lessonElements = new ArrayList<LessonElements>();
//		public WeekRuleFormElement(){}
//		public void addLessonElements( LessonElements lessonElements ){
//			this.lessonElements.add(lessonElements);
//		}
//	}
	
//	private class LessonElements{
//		JSpinner duration;
//		JComboBox type;
//		JTextField comments;
//
//		public LessonElements(JComboBox typeCombo, JSpinner durationSpinner, JTextField commentsTextField){
//			this.type = typeCombo;
//			this.duration = durationSpinner;
//			this.comments = commentsTextField;
//		}
//	}
	
//	public static void main(String[] args){
//		File file = new File("C:\\Users\\Michiel\\projects\\taken_admin\\data\\planning2011-2012-kw2_roosterregels.xml");
//		XmlDataReader xdr = new XmlDataReader(file);
//		try {
//			/*read and add the collection*/
//			YearDataCollection ydc = xdr.readDataCollection();
//			PlanningRulesEditingFrame f = new PlanningRulesEditingFrame(ydc.getCurriculumModule("@_ALGDATA1BIN"));
//			f.toFront();
//			f.requestFocus();
//			f.setVisible(true);
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
