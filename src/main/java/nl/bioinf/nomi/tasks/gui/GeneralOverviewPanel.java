/**
 *
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CourseMethod;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;
import nl.bioinf.nomi.tasks.datamodel.Employee;

/**
 * Shows financial overview for curricula.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class GeneralOverviewPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private TaskManagerController controller;
    private HashMap<String, List<CurriculumYear>> curriculumYears;
    private HashMap<String, CurriculumEmploymentTable> curriculumTables;
    private JLabel curriculumCostsLabel;

    /**
     * construct with the controller
     *
     * @param controller
     */
    public GeneralOverviewPanel(TaskManagerController controller) {
        this.controller = controller;
        curriculumYears = new HashMap<String, List<CurriculumYear>>();
        curriculumTables = new HashMap<String, CurriculumEmploymentTable>();
        initialize();
    }

    /**
     * init code creates the view.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

        /*fetch the collection of classes*/
        List<Curriculum> curricula = controller.getDataCollection().getCurricula();
        for (Curriculum curriculum : curricula) {
            curriculumYears.put(curriculum.getCurriculumID(), new ArrayList<CurriculumYear>());
            for (int y = 1; y <= 4; y++) {
                List<CurriculumYear> varYears = curriculum.getCurriculumYearVariants(y);
                curriculumYears.get(curriculum.getCurriculumID()).addAll(varYears);
            }
        }
        List<String> currIds = new ArrayList<String>();
        currIds.addAll(curriculumYears.keySet());

        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();

        //JPanel emplDataPanel = new JPanel( gbl );
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "Curriculum kostenoverzicht van jaar " + controller.getCurrentYear() + " - " + (controller.getCurrentYear() + 1);
        this.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.gridx++;

        JButton export = new JButton("exporteer naar excel");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.doAction(GuiAction.EXPORT_GENERAL_OVERVIEW);
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    controller.doAction(GuiAction.EXPORT_GENERAL_OVERVIEW);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        this.add(export, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;

        curriculumCostsLabel = GuiComponentFactory.getJLabel("Totale kosten van het onderwijs: ", GuiComponentStylingFactory.GENERAL_COMPONENT);
        this.add(curriculumCostsLabel, c);

        c.gridy++;

        for (String currId : currIds) {
            this.add(GuiComponentFactory.getJLabel(("opleiding " + currId), GuiComponentStylingFactory.DETAIL_COMPONENT), c);
            c.gridy++;

            this.add(getCurriculumPane(currId), c);
            c.gridy++;
        }

        int totalCosts = 0;
        int totalHours = 0;
        for (CurriculumEmploymentTable cet : this.curriculumTables.values()) {
            int cost = Integer.parseInt(cet.getValueAt(cet.getRowCount() - 1, 8).toString());
            int hours = Integer.parseInt(cet.getValueAt(cet.getRowCount() - 1, 7).toString());
            totalCosts += cost;
            totalHours += hours;
        }
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        curriculumCostsLabel.setText("Totaal onderwijs; kosten=" + nf.format(totalCosts) + " euros; uren=" + nf.format(totalHours)
                + " (" + nf.format(((double) totalHours / Employee.getFteSize())) + " fte's)");
    }

    /**
     * returns a JTable of all the project tasks
     *
     * @return jTable
     */
    private JScrollPane getCurriculumPane(String curriculumId) {
        CurriculumEmploymentTable cet = new CurriculumEmploymentTable(curriculumId);
        curriculumTables.put(curriculumId, cet);
        JTable currEmplTable = new JTable(cet);

        TableColumn column = null;
        for (int i = 0; i < currEmplTable.getColumnCount(); i++) {
            column = currEmplTable.getColumnModel().getColumn(i);
            if (i == 0) {//
                column.setPreferredWidth(200);
            } else if (i == 1) {//jaar
                column.setPreferredWidth(50);
            } else {
                column.setPreferredWidth(100);
            }
        }
        currEmplTable.setColumnSelectionAllowed(true);
        currEmplTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(currEmplTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(1000, 200));
        return pane;
    }

    /**
     * models the task list of the employee
     */
    private class CurriculumEmploymentTable extends AbstractTableModel {

        private static final long serialVersionUID = 1L;
        private String[] columnNames = {"Curriculumvariant", "Jaar", "Lab / praktijkdocent (uren)", "Lab / theoriedocent (uren)",
            "Theorie (uren)", "Speciale onderwijstaken (uren)", "Stage/Afstuderen (uren)", "Totaal (uren)", "Totaal (euro)", "Studenten", "Euro/student"};
        private String curriculumId;
        private HashSet<Integer> practicumScales;
        private HashSet<Integer> theoryScales;
        private HashSet<Integer> allScales;
        private HashSet<CourseMethod> allMethods;
        private NumberFormat numberFormat;

        public CurriculumEmploymentTable(String curriculumId) {
            this.curriculumId = curriculumId;
            this.practicumScales = new HashSet<Integer>();
            practicumScales.add(6);
            practicumScales.add(7);
            practicumScales.add(8);
            practicumScales.add(9);
            practicumScales.add(10);
            this.theoryScales = new HashSet<Integer>();
            theoryScales.add(11);
            theoryScales.add(12);
            this.allScales = new HashSet<Integer>();
            allScales.addAll(Employee.getSalaryScales());
            this.allMethods = new HashSet<CourseMethod>();
            allMethods.addAll(Curriculum.getCourseMethods());

            numberFormat = NumberFormat.getInstance();
            numberFormat.setMaximumFractionDigits(0);
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return curriculumYears.get(curriculumId).size() + 1;
        }

        @Override
        public Object getValueAt(int r, int c) {
            if (r == getRowCount() - 1) {
                switch (c) {
                    case 0:
                        return "Totalen";
                    case 1:
                        return "Alle jaren";
                    case 10:
                        return numberFormat.format(Double.parseDouble(getValueAt(r, 8).toString()) / Double.parseDouble(getValueAt(r, 9).toString()));
                    default: {
                        int total = 0;
                        for (int row = 0; row < getRowCount() - 1; row++) {
                            total += Integer.parseInt(getValueAt(row, c).toString());
                        }
                        return total;
                    }
                }

            } else {
                switch (c) {
                    case 0:
                        return curriculumYears.get(curriculumId).get(r).getCurriculumVariant().getName();
                    case 1:
                        return curriculumYears.get(curriculumId).get(r).getYear();
                    case 2:
                        return curriculumYears.get(curriculumId).get(r).getInvestedHours(practicumScales, Curriculum.getLabMethods());
                    case 3:
                        return curriculumYears.get(curriculumId).get(r).getInvestedHours(theoryScales, Curriculum.getLabMethods());
                    case 4:
                        return curriculumYears.get(curriculumId).get(r).getInvestedHours(theoryScales, Curriculum.getTheoryMethods());
                    case 5:
                        return curriculumYears.get(curriculumId).get(r).getInvestedHours(allScales, Curriculum.getSpecialMethods());
                    case 6:
                        return curriculumYears.get(curriculumId).get(r).getInvestedHours(allScales, Curriculum.getGraduationMethods());
                    case 7:
                        return curriculumYears.get(curriculumId).get(r).getInvestedHours(allScales, allMethods);
                    case 8:
                        return curriculumYears.get(curriculumId).get(r).getInvestedEuros(allScales, allMethods);
                    case 9:
                        return curriculumYears.get(curriculumId).get(r).getStudentNumber(1);
                    case 10: {
                        Integer studentNumber = Integer.parseInt(getValueAt(r, 9).toString());
                        if (studentNumber == 0) {
                            return "NA";
                        } else {
                            return numberFormat.format(Double.parseDouble(getValueAt(r, 8).toString()) / studentNumber);
                        }
                    }
                    default:
                        return "";
                }
            }
        }

        public String getColumnName(int c) {
            return columnNames[c];
        }

        /*Don't need to implement this method unless your table's editable.*/
        public boolean isCellEditable(int row, int col) {
            //Omschrijving en totaal uren zijn editable
            if (col == 2) {
                return true;
            } else {
                return false;
            }
        }
    }

}
