package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

/**
 * models the panel shown in all tabs: a left-side list pane and a right-side details pane.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class TabPanel extends JPanel {
    /**
     * the list panel.
     */
    private JPanel listPanel;
    /**
     * the details panel.
     */
    private JPanel detailsPanel;
    /**
     * width.
     */
    private final int listPanelWidth = 200;
    /**
     * height.
     */
    private final int listPanelHeight = 600;

    /**
     * default constructor.
     */
    public TabPanel() {
        initialize();
    }

    /**
     * returns the panel that contains the list.
     *
     * @return listPanel
     */
    public JPanel getListPanel() {
        return this.listPanel;
    }

    /**
     * sets the list component to be displayed.
     *
     * @param listComponent the list component
     */
    public void setListComponent(final JComponent listComponent) {
        this.listPanel.removeAll();
        this.listPanel.add(listComponent);
    }

    /**
     * returns the panel that contains the detailed view.
     *
     * @return details the details panel
     */
    public JPanel getDetailsPanel() {
        return detailsPanel;
    }

    /**
     * sets the details component to be displayed.
     *
     * @param detailComponent
     */
    public void setDetailComponent(final JComponent detailComponent) {
        this.detailsPanel.removeAll();
        if (detailComponent != null) {
            this.detailsPanel.add(detailComponent);
        }
    }

    /**
     * initialization procedures.
     */
    private void initialize() {
        /*create the list panel*/
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

        this.listPanel = new JPanel();
        GuiComponentStylingFactory.stylizeComponent(listPanel, GuiComponentStylingFactory.PANEL_COMPONENT);
        JScrollPane lsp = new JScrollPane(listPanel);
        lsp.getVerticalScrollBar().setUnitIncrement(20);
        lsp.setPreferredSize(new Dimension(this.listPanelWidth, this.listPanelHeight));

        /*create the details panel*/
        this.detailsPanel = new JPanel();
        GuiComponentStylingFactory.stylizeComponent(detailsPanel, GuiComponentStylingFactory.PANEL_COMPONENT);
        JScrollPane dsp = new JScrollPane(detailsPanel);
        dsp.getVerticalScrollBar().setUnitIncrement(20);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, lsp, dsp);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(175);
        splitPane.setAutoscrolls(false);

        /*organise the layout manager and add both panels*/
        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        c.ipadx = 2;
        c.ipady = 2;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        this.add(splitPane, c);
    }
}
