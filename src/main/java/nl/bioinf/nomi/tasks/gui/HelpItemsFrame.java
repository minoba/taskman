/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 * @author Cellingo
 *
 */
public class HelpItemsFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	public HelpItemsFrame( ) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 600, 500);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);
		setTitle("Help");
		/*creates the form/content*/
		createContent();
		
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		java.net.URL helpURL = getClass().getResource( "/text/TaskManagerHelpItems.html" );
		if (helpURL != null) {
		    try {
				JEditorPane helpPane = new JEditorPane();
				helpPane.setEditable(false);
		        helpPane.setPage(helpURL);
				//Put the editor pane in a scroll pane.
				JScrollPane editorScrollPane = new JScrollPane(helpPane);
				editorScrollPane.setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_ALWAYS );
				editorScrollPane.setPreferredSize(new Dimension(250, 145));
				editorScrollPane.setMinimumSize(new Dimension(10, 10));
				getContentPane().add(editorScrollPane);
				toFront();
				requestFocus();
				setAlwaysOnTop(true);
				setVisible(true);
		    } catch (IOException e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL);
		    }
		} else {
			JOptionPane.showMessageDialog(null, "kan help bestand niet vinden", "fout", JOptionPane.WARNING_MESSAGE);
			this.dispose();
		}
	}
	
}
