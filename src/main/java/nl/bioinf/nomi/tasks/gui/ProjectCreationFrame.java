/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Project;

/**
 * @author Cellingo
 *
 */
public class ProjectCreationFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private JTextField idTextField;
	private JTextField nameTextField;
	private JTextField descriptionTextField;
	private JTextField hoursTextField;
	private JCheckBox hboInclusiveCheck;

	public ProjectCreationFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 750, 400);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Nieuw project");
		/*creates the form/content*/
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
		//setVisible( true );
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "maak een nieuw project aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);

		c.ipadx = 10;
		c.ipady = 4;
		//c.gridwidth = 1;
		c.gridwidth = 2;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "ID", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;
		
		idTextField = new JTextField();
		idTextField.setColumns(12);
		formPanel.add( idTextField, c );
		
		c.gridy++;
		c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "naam", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;
		//c.gridwidth = 2;

		nameTextField = new JTextField();
		nameTextField.setColumns(12);
		formPanel.add( nameTextField, c );

		c.gridx = 0;
		c.gridwidth = 1;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "omschrijving", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;
		c.gridwidth = 3;

		descriptionTextField = new JTextField();
		descriptionTextField.setColumns(50);
		formPanel.add( descriptionTextField, c );

		c.gridx = 0;
		c.gridwidth = 1;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "urentotaal (kan 0 blijven)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;

		hoursTextField = new JTextField();
		hoursTextField.setText("0");
		hoursTextField.setColumns(4);
		formPanel.add( hoursTextField, c );
		
		c.gridx = 0;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "is inclusief HBO uren ", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;

		hboInclusiveCheck = new JCheckBox();
		formPanel.add( hboInclusiveCheck, c );
		
		c.gridy++;
		c.gridx = 0;

		JButton ok = new JButton("Bewaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("projectcreation");
				if( dataOk ) dispose();
			}
		});
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("projectcreation");
					if( dataOk ) dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});

		formPanel.add( ok, c );
		
		c.gridx = 1;
		c.gridwidth = 2;

		JButton okNew = new JButton("Bewaar en nog een project maken");
		okNew.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("projectcreation");
				if( dataOk ) clearData();
			}
		});
		okNew.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("projectcreation");
					if( dataOk ) clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});

		formPanel.add( okNew, c );
		
		c.gridx = 3;
		c.gridwidth = 1;
		//c.gridy++;
		
		JButton cancel = new JButton("Annuleren");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});

		formPanel.add( cancel, c );
	}
	
	/**
	 * verifies the data and processes them if they are OK
	 * @return dataOk
	 */
	private boolean verifyAndProcess(){
		String id = idTextField.getText();
		if( controller.getDataCollection().getProjectById(id) != null ){
			JOptionPane.showMessageDialog(getContentPane(), "er bestaat al een project met de ID " + id, "Fout", JOptionPane.WARNING_MESSAGE);
			idTextField.requestFocus();
			return false;
		}
		else if( id == null || id.length()==0 ){
			JOptionPane.showMessageDialog(getContentPane(), "de id mag niet leeg zijn " + id, "Fout", JOptionPane.WARNING_MESSAGE);
			idTextField.requestFocus();
			return false;
		}
		
		String re = "[^a-zA-Z_0-9]+";
		Pattern p = Pattern.compile(re);
		Matcher m = p.matcher(id);
		if(m.find()){
			JOptionPane.showMessageDialog(getContentPane(), "het ID veld mag alleen letters, cijfers en underscores bevatten " + id, "Fout", JOptionPane.WARNING_MESSAGE);
			idTextField.requestFocus();
			return false;
		}
		
		String name = nameTextField.getText();
		if( name == null || name.length()==0 ){
			JOptionPane.showMessageDialog(getContentPane(), "de naam mag niet leeg zijn " + name, "Fout", JOptionPane.WARNING_MESSAGE);
			nameTextField.requestFocus();
			return false;
		}
		
		String description = descriptionTextField.getText();
		
		int hours = 0;
		String hoursStr = hoursTextField.getText();
		try{
			hours = Integer.parseInt(hoursStr);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "de uren zijn niet goed ingevoerd", "Fout", JOptionPane.WARNING_MESSAGE);
			hoursTextField.requestFocus();
			return false;
		}
		
		boolean hboInclusive = hboInclusiveCheck.isSelected();
		
		Project project = new Project(id, name);
		project.setDescription(description);
		project.setTotalHours(hours);

		/*add the project to the collection*/
		controller.getDataCollection().addProject(project);
		
		return true;
	}
	
	/**
	 * clears the form for reuse
	 */
	private void clearData(){
		nameTextField.setText("");
		descriptionTextField.setText("");
		hoursTextField.setText("");
		hboInclusiveCheck.setSelected(false);
	}
}
