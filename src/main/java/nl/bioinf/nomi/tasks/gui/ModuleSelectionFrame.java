/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;
import nl.bioinf.nomi.tasks.gui.CurriculumYearDetailPanel.TableChangeActionListener;

/**
 * @author Cellingo
 *
 */
public class ModuleSelectionFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private Timeframe timeframe;
	private JComboBox<Object> subselectionCombo;
	private JPanel selectionPanel;
	private TableChangeActionListener tableChangeActionListener;
	private List<CurriculumModule> selectedCurriculumModules;
	private ArrayList<JCheckBox> checks;

	public ModuleSelectionFrame( TaskManagerController controller, Timeframe timeframe, TableChangeActionListener tableChangeActionListener ) {
		this.controller = controller;
		this.timeframe = timeframe;
		this.tableChangeActionListener = tableChangeActionListener;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 600, 700);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("CurriculumModule selection");
		/*creates the form/content*/
		this.checks = new ArrayList<JCheckBox>();
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JScrollPane sp = new JScrollPane( formPanel );
		GuiComponentStylingFactory.stylizeComponent(sp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);

		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "Selecteer modules", GuiComponentStylingFactory.TITLE_COMPONENT), c);

		c.ipadx = 10;
		c.ipady = 4;
		c.gridwidth = 1;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "kies subselectie", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;
		c.gridwidth = 2;

		ArrayList<String> currIds = new ArrayList<String>();
		currIds.add("kies selectie");
		currIds.add("alle modules");
		currIds.add("niet-toegewezen modules");
		List<Curriculum> curricula = controller.getDataCollection().getCurricula();
		for( Curriculum curriculum : curricula ){
			currIds.add( curriculum.getCurriculumID() );
		}
		subselectionCombo = new JComboBox<Object>( currIds.toArray() );
		subselectionCombo.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				createModuleSelectionPanel( subselectionCombo.getSelectedItem().toString() );
			} } );
		formPanel.add( subselectionCombo, c );

		c.gridx = 0;
		c.gridwidth = 1;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "selectielijst", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridy++;
		//c.gridx = 1;
		c.gridwidth = 4;

		//this.selectionPanel = getModuleSelectionPanel( "alle modules" );
		JPanel dummy = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		dummy.add( new JLabel("selectie") );
		this.selectionPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		selectionPanel.add(dummy);
		JScrollPane ssp = new JScrollPane( selectionPanel );
		GuiComponentStylingFactory.stylizeComponent(ssp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
		ssp.setPreferredSize( new Dimension( 500,400) );
		formPanel.add(ssp, c);
			
		c.gridwidth = 1;
		c.gridy++;
		c.gridx = 0;

		JButton ok = new JButton("Bewaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				createSelectionList();
				tableChangeActionListener.modulesSelected(selectedCurriculumModules);
				dispose();
			}
		});
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					createSelectionList();
					tableChangeActionListener.modulesSelected(selectedCurriculumModules);
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( ok, c );
		
		c.gridx = 2;
		c.gridwidth = 1;
		
		JButton cancel = new JButton("Annuleer");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( cancel, c );
	}
	
	private void createModuleSelectionPanel( String curriculumSelection ) {
		selectionPanel.removeAll();
		JPanel content = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		content.setLayout( new BoxLayout(content, BoxLayout.Y_AXIS) );
		if( curriculumSelection.equals("alle modules") ){
			List<CurriculumModule> curriculumModules = controller.getDataCollection().getModules(timeframe);
			if( curriculumModules.size() == 0 ) content.add( new JLabel("lege selectie") );
			for( CurriculumModule curriculumModule : curriculumModules){
				String title = curriculumModule.getName() + " (" + curriculumModule.getModuleId() + ")";
				JCheckBox check = new JCheckBox(title);
				check.setName(curriculumModule.getModuleId());
				checks.add(check);
				content.add(check);
			}
		}
		else if( curriculumSelection.equals("niet-toegewezen modules") ){
			List<CurriculumModule> curriculumModules = controller.getDataCollection().getModules(timeframe);
			if( curriculumModules.size() == 0 ) content.add( new JLabel("lege selectie") );
			for( CurriculumModule curriculumModule : curriculumModules){
				if(curriculumModule.getAttendingClasses().size() == 0 ){
					String title = curriculumModule.getName() + " (" + curriculumModule.getModuleId() + ")";
					JCheckBox check = new JCheckBox(title);
					check.setName(curriculumModule.getModuleId());
					checks.add(check);
					content.add(check);
				}
			}
		}
		else{
			Curriculum curriculum = controller.getDataCollection().getCurriculum(curriculumSelection);
			if( curriculum != null ){
				/*create a year-independent timeframe*/
				Timeframe tf = new Timeframe(timeframe.getQuarter(), timeframe.getDuration() );
				List<CurriculumModule> curriculumModules = curriculum.getModules(tf);
				if( curriculumModules.size() == 0 ) content.add( new JLabel("lege selectie") );
				for( CurriculumModule curriculumModule : curriculumModules){
					String title = curriculumModule.getName() + " (" + curriculumModule.getModuleId() + ")";
					JCheckBox check = new JCheckBox(title);
					check.setName(curriculumModule.getModuleId());
					checks.add(check);
					content.add(check);
				}
			}
		}
		selectionPanel.add(content);
		//this.repaint();
		paintComponents(getGraphics());
	}

	
	private void createSelectionList(){
		//System.out.println( "creating selection list " );
		selectedCurriculumModules = new ArrayList<CurriculumModule>();
		for(JCheckBox check : checks ){
			if( check.isSelected() ){
				String id = check.getName();
				CurriculumModule curriculumModule = controller.getDataCollection().getModule(id);
				if( curriculumModule != null ) selectedCurriculumModules.add(curriculumModule);
			}
		}
	}
	
}
