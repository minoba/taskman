package nl.bioinf.nomi.tasks.gui;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * JFrame form to create a regular task.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.2
 */
public class RegularTaskCreationFrame extends JFrame {
    private final TaskManagerController controller;
    private JTextField nameTextField;
    private JTextField hoursTextField;
    private JComboBox<String> catsCombo;
    private JComboBox<String> employeesCombo;
    private JComboBox<String> startQuarterCombo;
    private JComboBox<String> durationCombo;
//    private JCheckBox hboInclusiveCheck;
    private final JTextField[] qPartitionTextFields = new JTextField[4];

    /**
     * constructs with controller.
     * @param controller the controller
     */
    public RegularTaskCreationFrame(final TaskManagerController controller) {
        this.controller = controller;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(200, 200, 900, 400);
        /*set frame icon and title*/
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.getImage("graphics/tasks_icon.gif");
        setIconImage(img);

        setTitle("Nieuwe reguliere taak");
        /*creates the form/content*/
        createContent();
        toFront();
        requestFocus();
        setAlwaysOnTop(true);
    }

    private void createContent() {
        GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
        JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        JScrollPane sp = new JScrollPane(formPanel);
        this.getContentPane().add(sp);
        formPanel.setLayout(gbl);

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 5;
        c.ipadx = 50;
        c.ipady = 50;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        formPanel.add(GuiComponentFactory.getJLabel(
                "maak een nieuwe reguliere taak aan",
                GuiComponentStylingFactory.TITLE_COMPONENT),
                c);

        c.ipadx = 10;
        c.ipady = 4;
        c.gridwidth = 1;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel("taak label", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;
        c.gridwidth = 3;

        TaskLabel[] cats = TaskLabel.values();
        String[] cNames = new String[cats.length + 1];
        cNames[0] = "kies";
        for (int i = 0; i < cats.length; i++) {
            cNames[i + 1] = cats[i].toString();
        }
        catsCombo = new JComboBox<>(cNames);
        formPanel.add(catsCombo, c);

        c.gridy++;
        c.gridx = 0;
        c.gridwidth = 1;

        formPanel.add(GuiComponentFactory.getJLabel(
                "medewerker (kies _VAC voor vacature)",
                GuiComponentStylingFactory.FORM_COMPONENT),
                c);

        c.gridx = 1;

        List<Employee> employees = controller.getDataCollection().getEmployees();
        String[] eNames = new String[employees.size() + 1];
        eNames[0] = "kies";
        for (int i = 0; i < employees.size(); i++) {
            eNames[i + 1] = employees.get(i).getEmployeeId();
        }
        employeesCombo = new JComboBox<>(eNames);
        formPanel.add(employeesCombo, c);

        c.gridx = 0;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel("taakuren", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        hoursTextField = new JTextField();
        //hoursTextField.setText("0");
        hoursTextField.setColumns(5);
        formPanel.add(hoursTextField, c);

        c.gridx = 0;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel("start (kwartaal)", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        String[] starts = {"kies", "1", "2", "3", "4"};
        startQuarterCombo = new JComboBox<>(starts);
        startQuarterCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                createQuarterPartitions();
            }
        });
        formPanel.add(startQuarterCombo, c);

        c.gridx = 2;

        formPanel.add(GuiComponentFactory.getJLabel("duur (kwartalen)", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 3;

        durationCombo = new JComboBox<>(starts);
        durationCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                createQuarterPartitions();
            }
        });
        formPanel.add(durationCombo, c);

        c.gridx = 0;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel(
                "procentuele verdeling ",
                GuiComponentStylingFactory.FORM_COMPONENT),
                c);

        c.gridx = 1;
        c.gridwidth = 4;

        JPanel tfPanel = new JPanel();
        tfPanel.setLayout(new GridLayout(1, 5));

        JTextField qOnePartitionTextField = new JTextField();
        qOnePartitionTextField.setColumns(3);
        qPartitionTextFields[0] = qOnePartitionTextField;
        tfPanel.add(qOnePartitionTextField);

        JTextField qTwoPartitionTextField = new JTextField();
        qTwoPartitionTextField.setColumns(3);
        qPartitionTextFields[1] = qTwoPartitionTextField;
        tfPanel.add(qTwoPartitionTextField);

        JTextField qThreePartitionTextField = new JTextField();
        qThreePartitionTextField.setColumns(3);
        qPartitionTextFields[2] = qThreePartitionTextField;
        tfPanel.add(qThreePartitionTextField);

        JTextField qFourPartitionTextField = new JTextField();
        qFourPartitionTextField.setColumns(3);
        qPartitionTextFields[3] = qFourPartitionTextField;
        tfPanel.add(qFourPartitionTextField);
        formPanel.add(tfPanel, c);

//        c.gridx = 0;
//        c.gridy++;
//
//        formPanel.add(GuiComponentFactory.getJLabel(
//                "is inclusief HBO uren ",
//                GuiComponentStylingFactory.FORM_COMPONENT),
//                c);

//        c.gridx = 1;

//        hboInclusiveCheck = new JCheckBox();
//        formPanel.add(hboInclusiveCheck, c);
//        hboInclusiveCheck.setSelected(false);

        c.gridx = 0;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel("taak naam", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;
        c.gridwidth = 2;

        nameTextField = new JTextField();
        nameTextField.setColumns(12);
        formPanel.add(nameTextField, c);

        c.gridx = 0;
        c.gridy++;

        JButton ok = new JButton("Bewaar");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                boolean dataOk = verifyAndProcess();
                controller.updateViews("projecttaskcreation");
                if (dataOk) {
                    dispose();
                }
            }
        });
        ok.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    boolean dataOk = verifyAndProcess();
                    controller.updateViews("projecttaskcreation");
                    if (dataOk) {
                        dispose();
                    }
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        formPanel.add(ok, c);

        c.gridx = 1;
        c.gridwidth = 2;

        JButton okNew = new JButton("Bewaar en maak nog een taak");
        okNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                boolean dataOk = verifyAndProcess();
                controller.updateViews("regulartaskcreation");
                if (dataOk) {
                    clearData();
                }
            }
        });
        okNew.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    boolean dataOk = verifyAndProcess();
                    controller.updateViews("regulartaskcreation");
                    if (dataOk) {
                        clearData();
                    }
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        formPanel.add(okNew, c);

        c.gridx = 3;
        c.gridwidth = 1;

        JButton cancel = new JButton("Annuleren");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                dispose();
            }
        });
        cancel.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    dispose();
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        formPanel.add(cancel, c);
    }

    /**
     * creates percentual partition over quarters.
     */
    private void createQuarterPartitions() {
        if (this.startQuarterCombo.getSelectedItem().toString().equals("kies")
                || this.durationCombo.getSelectedItem().toString().equals("kies")) {
            return;
        }
        try {
            int startQuarter = Integer.parseInt(this.startQuarterCombo.getSelectedItem().toString());
            int duration = Integer.parseInt(this.durationCombo.getSelectedItem().toString());

            if (startQuarter < 1 || startQuarter > 4 || duration < 1 || duration > 4 || startQuarter + duration > 5) {
                JOptionPane.showMessageDialog(
                        this,
                        "foute invoer: start kwartaal + duur overschrijden 4",
                        "foute invoer",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            for (JTextField tf : this.qPartitionTextFields) {
                tf.setText("");
            }
            switch (duration) {
                case 1: {
                    this.qPartitionTextFields[startQuarter - 1].setText("100");
                    break;
                }
                case 2: {
                    this.qPartitionTextFields[startQuarter - 1].setText("50");
                    this.qPartitionTextFields[startQuarter].setText("50");
                    break;
                }
                case 3: {
                    this.qPartitionTextFields[startQuarter - 1].setText("33");
                    this.qPartitionTextFields[startQuarter].setText("33");
                    this.qPartitionTextFields[startQuarter + 1].setText("34");
                    break;
                }
                case 4: {
                    this.qPartitionTextFields[startQuarter - 1].setText("25");
                    this.qPartitionTextFields[startQuarter].setText("25");
                    this.qPartitionTextFields[startQuarter + 1].setText("25");
                    this.qPartitionTextFields[startQuarter + 2].setText("25");
                    break;
                }
            }
        } catch (NumberFormatException | HeadlessException e) {
            JOptionPane.showMessageDialog(this, "foute invoer voor uren ", "foute invoer", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * verifies the data and processes them if they are OK.
     *
     * @return dataOk the form is OK
     */
    private boolean verifyAndProcess() {
        int taskLabelIndex = catsCombo.getSelectedIndex();
        if (taskLabelIndex == 0) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "taak moet een categorie hebben",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            catsCombo.requestFocus();
            return false;
        }
        TaskLabel tl = (TaskLabel.values())[taskLabelIndex - 1];
//        System.out.println("tasklabel = " + tl);

        String empName = employeesCombo.getSelectedItem().toString();
        Employee employee = controller.getDataCollection().getEmployee(empName);
        if (employee == null) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "taak moet een medewerker hebben (kies evt _VAC)",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            employeesCombo.requestFocus();
            return false;
        }

        String name = nameTextField.getText();
        if (name == null || name.equals("")) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "taak moet een naam hebben",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            nameTextField.requestFocus();
            return false;
        }

//        String description = descriptionTextField.getText();

        int hours;
        String hoursStr = hoursTextField.getText();
        try {
            hours = Integer.parseInt(hoursStr);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "de uren zijn niet goed ingevoerd",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            hoursTextField.requestFocus();
            return false;
        }

        String startQ = startQuarterCombo.getSelectedItem().toString();
        int start;
        try {
            start = Integer.parseInt(startQ);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "projecttaak moet een startkwartaal hebben",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            startQuarterCombo.requestFocus();
            return false;
        }

        String durationStr = durationCombo.getSelectedItem().toString();
        int duration;
        try {
            duration = Integer.parseInt(durationStr);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "projecttaak moet een duur hebben",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            durationCombo.requestFocus();
            return false;
        }
        if (start + duration > 5) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "start en duur van projecttaak zijn niet verenigbaar (gaan over het jaar heen)",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            durationCombo.requestFocus();
            return false;
        }

        /*check whether partitioning required*/
        boolean partReq = false;
        //System.out.println("testing for custom partitioning; duration =" + duration);
        switch (duration) {
            case 1: {
                if (Integer.parseInt(this.qPartitionTextFields[start - 1].getText()) != 100) {
                    partReq = true;
                }
                break;
            }
            case 2: {
                if (!(Integer.parseInt(this.qPartitionTextFields[start - 1].getText()) == 50
                        && Integer.parseInt(this.qPartitionTextFields[start].getText()) == 50)) {
                    partReq = true;
                }
                break;
            }
            case 3: {
                if (!(Integer.parseInt(this.qPartitionTextFields[start - 1].getText()) == 33
                        && Integer.parseInt(this.qPartitionTextFields[start].getText()) == 33
                        && Integer.parseInt(this.qPartitionTextFields[start + 1].getText()) == 34)) {
                    partReq = true;
                }
                break;
            }
            case 4: {
                if (!(Integer.parseInt(this.qPartitionTextFields[start - 1].getText()) == 25
                        && Integer.parseInt(this.qPartitionTextFields[start].getText()) == 25
                        && Integer.parseInt(this.qPartitionTextFields[start + 1].getText()) == 25
                        && Integer.parseInt(this.qPartitionTextFields[start + 2].getText()) == 25)) {
                    partReq = true;
                }
                break;
            }
        }

        int[] qPartition = null;
        if (partReq) {
            qPartition = new int[duration];

            /*check first*/
            try {
                int total = 0;
                for (int i = 0; i < 4; i++) {
                    String qp = this.qPartitionTextFields[i].getText();
                    //System.out.println("q=" + i + "; %=" + qp);
                    if (qp.length() > 0) {
                        int sub = Integer.parseInt(this.qPartitionTextFields[i].getText());
                        //System.out.println("q=" + i + "; sub=" + sub + "; total=" + total);
                        total += sub;
                    }
                }
                //System.out.println("total=" + total);
                if (total != 100) {
                    throw new Exception();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(
                        getContentPane(),
                        "foutieve verdeling over kwartalen:"
                                + " moet tot 100 optellen en overeenkomen met opgegeven start en duur",
                        "Fout",
                        JOptionPane.WARNING_MESSAGE);
                return false;
            }

            /*process partition*/
            for (int i = 0; i < duration; i++) {
                int val = Integer.parseInt(this.qPartitionTextFields[i + start - 1].getText());
                //System.out.println("i=" + i + "; %=" + val);
                qPartition[i] = val;
            }
        }

        RegularTask task = new RegularTask(controller.getDataCollection().getNewRegularTaskId(), hours);
        task.setTaskLabel(tl);
        task.setName(name);
        task.setEmployee(employee);
        if (partReq) {
            task.setTimeframe(new Timeframe(start, duration, qPartition));
        } else {
            task.setTimeframe(new Timeframe(start, duration));
        }

        /*add the task to the project AND the employee*/
        employee.addRegularTask(task);
        controller.getDataCollection().addRegularTask(task);
        controller.setViewedEmployee(employee);
        controller.setViewedTaskCategory(tl.getTaskCategory());
        controller.updateViews("");
        return true;
    }

    /**
     * clears the form for reuse.
     */
    private void clearData() {
        nameTextField.setText("");
        hoursTextField.setText("");
        catsCombo.setSelectedIndex(0);
        employeesCombo.setSelectedIndex(0);
        startQuarterCombo.setSelectedIndex(0);
        durationCombo.setSelectedIndex(0);
    }
}
