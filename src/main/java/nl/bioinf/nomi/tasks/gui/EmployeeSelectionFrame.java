/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Comparator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;

/**
 * @author Michiel
 *
 */
public class EmployeeSelectionFrame extends JFrame implements ActionListener, ItemListener{
	private static final long serialVersionUID = 1L;
	private final String SELECT_ALL = "alle teams";
	private JComboBox<String> teamSelectionCombo;
	private TaskManagerController controller;
	private ArrayList<JCheckBox> allChecks;
	private JPanel checkCardsPanel;
	private List<Employee> employees;
	private HashMap<String, JPanel> teamCheckCards = new HashMap<String, JPanel>();
	private ArrayList<JCheckBox> teamChecks;
	private String selectedGroup = SELECT_ALL;
	
	public EmployeeSelectionFrame( TaskManagerController controller ){
		
		this.controller = controller;
		this.employees = controller.getDataCollection().getEmployees(new Comparator<Employee>(){
			@Override
			public int compare(Employee arg0, Employee arg1) {
				// TODO Auto-generated method stub
				return 0;
			}
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 600, 700);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("kies medewerkers voor PDF export");
		createSelectionForm();
		setAlwaysOnTop(true);
		toFront();
		requestFocus();
	}
	
	private void createSelectionForm(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 3;
		c.ipadx = 4;
		c.ipady = 4;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		String title = "Kies welke medewerkers of medewerker-teams u wilt exporteren";
		formPanel.add(GuiComponentFactory.getJLabel( title, GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		c.gridy++;
		c.gridwidth = 1;
		
		List<String> teamIds = controller.getDataCollection().getTeams();
		String[] teamIdsArr = new String[teamIds.size()+1];
		teamIdsArr[0] = SELECT_ALL;
		for( int i=0; i<teamIds.size(); i++ ){
			teamIdsArr[i+1] = teamIds.get(i);
		}
		
		teamSelectionCombo = new JComboBox<String>(teamIdsArr);
		teamSelectionCombo.addItemListener(this);
		formPanel.add( teamSelectionCombo, c);
		
		c.gridy++;
		c.gridwidth = 3;
		
		JCheckBox groupSelectCheck = new JCheckBox( "(de-)selecteer alle" );
		groupSelectCheck.setSelected(true);
		groupSelectCheck.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = ((JCheckBox)e.getSource()).isSelected();
				
				//System.out.println("all selected: " + selected);
				
				if( selectedGroup.equals(SELECT_ALL)){
					for( JCheckBox check : allChecks ){
						check.setSelected(selected);
					}
					for( JCheckBox check : teamChecks ){
						check.setSelected(selected);
					}
				}
				else{
					for( JCheckBox check : allChecks ){
						if( check.getName().equals( selectedGroup ) ) check.setSelected(selected);
					}
					for( JCheckBox check : teamChecks ){
						if( check.getName().equals(selectedGroup ) ) check.setSelected(selected);
					}
				}
			}
		});
		
		formPanel.add( groupSelectCheck, c);
		
		c.gridy++;
		c.gridwidth = 1;

		formPanel.add(getOKbutton(), c);
		
		c.gridx++;
		
		formPanel.add(getCancelButton(), c);

		c.gridx = 0;
		c.gridy++;
		
		checkCardsPanel = GuiComponentFactory.getJPanel( GuiComponentStylingFactory.DETAIL_COMPONENT );
		checkCardsPanel.setLayout(new CardLayout() );
		
		/*create panel for each team to hold checks*/
		for( String teamID : teamIdsArr ){
			JPanel teamChecksPanel = GuiComponentFactory.getJPanel( GuiComponentStylingFactory.DETAIL_COMPONENT );
			this.teamCheckCards.put(teamID, teamChecksPanel);
			teamChecksPanel.setLayout(new BoxLayout(teamChecksPanel, BoxLayout.Y_AXIS));
			checkCardsPanel.add( teamChecksPanel, teamID );
		}
		
		this.allChecks = new ArrayList<JCheckBox>();
		this.teamChecks = new ArrayList<JCheckBox>();
		
		/*create the checks*/
		for( Employee emp : employees ){
			JCheckBox check1 = new JCheckBox( emp.getEmployeeId() , true);
			check1.setName(emp.getTeam());
			teamCheckCards.get(emp.getTeam()).add(check1);
			teamChecks.add(check1);
			
			JCheckBox check2 = new JCheckBox( emp.getEmployeeId() , true);
			check2.setName(emp.getTeam());
			teamCheckCards.get(SELECT_ALL).add(check2);
			allChecks.add(check2);
		}
		
		c.gridwidth = 3;
		formPanel.add( checkCardsPanel, c);
		
		c.gridy++;
		c.gridwidth = 1;
		
		formPanel.add(getOKbutton(), c);
		
		c.gridx++;
		
		formPanel.add(getCancelButton(), c);
	}


    public void itemStateChanged(ItemEvent evt) {
    	//System.out.println("changing content to " + (String)evt.getItem() );
    	this.selectedGroup  = (String)evt.getItem();
    		
        CardLayout cl = (CardLayout)(checkCardsPanel.getLayout());
        cl.show(checkCardsPanel, selectedGroup);
        
    }
    
	@Override
	public void actionPerformed(ActionEvent arg0) {
		processSelection();
	}

	private void processSelection(){
		List<Employee> selectedEmployees = new ArrayList<Employee>();
		if(selectedGroup.equals(SELECT_ALL)){
			for(JCheckBox check : allChecks ){
				if( check.isSelected() ) selectedEmployees.add( controller.getDataCollection().getEmployee(check.getText() ) );
			}
		}
		else{
			for(JCheckBox check : teamChecks ){
				if( check.getName().equals(selectedGroup) && check.isSelected() ){
					selectedEmployees.add( controller.getDataCollection().getEmployee(check.getText() ) );
				}
			}
		}
		this.dispose();
		controller.exportEmployeeTaskoverviews(selectedEmployees);
	}
	
	public JButton getCancelButton(){
		JButton cancel = new JButton( "cancel" );
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				EmployeeSelectionFrame.this.dispose();
			}
			
		} );
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					EmployeeSelectionFrame.this.dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		return cancel;
	}
	
	public JButton getOKbutton(){
		JButton ok = new JButton( "OK" );
		ok.addActionListener( this );
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					processSelection();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		return ok;
	}

}
