/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumVariant;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;

/**
 * @author Cellingo
 *
 */
public class CurriculumyearCreationFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	
	private JComboBox<String> curriculumCombo;
	private JComboBox<String> curriculumVariantCombo;
	private String[] currIds;
	private String[] currVariantIds;

	private JTextField studentNumberTextField;
	private JComboBox<String> yearCombo;

	public CurriculumyearCreationFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 750, 400);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Nieuwe klas");
		/*creates the form/content*/
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		//this.setLayout( gbl );
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "maak een nieuwe klas aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);
		
		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;
		
		formPanel.add(GuiComponentFactory.getJLabel( "een klas hoort bij een enkel jaar van een curriculum en heeft 1 of meerdere kwartalen", GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		c.gridwidth = 1;
		c.gridy++;
		//c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "opleiding", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;

		List<Curriculum> currs = controller.getDataCollection().getCurricula();
		currIds = new String[ currs.size()+1 ];
		String[] currNames = new String[ currs.size()+1 ];
		currIds[0] = "kies";
		currNames[0] = "kies";
		for( int i=1; i<=currs.size(); i++ ){
			currIds[i] = currs.get(i-1).getCurriculumID();
			currNames[i] = currs.get(i-1).getName();
		}
		curriculumCombo = new JComboBox<String>( currIds );
		curriculumCombo.addActionListener( new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				/*creates a list of selectable items for the selected curriculum*/
				String currId = currIds[ curriculumCombo.getSelectedIndex() ]; 
				Curriculum curr = controller.getDataCollection().getCurriculum(currId);
				List<CurriculumVariant> variants = curr.getcurriculumVariants();
				
				curriculumVariantCombo.removeAllItems();
				currVariantIds = new String[ variants.size() ];
				//currVariantIds[0] = "kies";
				for( int i=0; i<variants.size(); i++ ){
					currVariantIds[i] = variants.get(i).getType();
					curriculumVariantCombo.addItem( variants.get(i).getName() );
				}
			}
		} );
		formPanel.add(curriculumCombo, c);

		c.gridy++;
		c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "curriculum", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;
		
		String[] options = {"kies"};
		curriculumVariantCombo = new JComboBox<String>( options );
		formPanel.add(curriculumVariantCombo, c);

		c.gridy++;
		c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "jaar", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;

		String[] years = { "kies", "1", "2", "3", "4" };
		yearCombo = new JComboBox<String>( years );
		formPanel.add( yearCombo, c );

		c.gridy++;
		c.gridx = 0;

		formPanel.add( GuiComponentFactory.getJLabel( "studentaantal (naar 4 kwartalen gekopieerd)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;

		studentNumberTextField = new JTextField();
		studentNumberTextField.setColumns(12);
		formPanel.add(studentNumberTextField, c);
		
		c.gridy++;
		c.gridx = 0;


		JButton ok = new JButton("Bewaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("curriculumyearcreation");
				if( dataOk ) dispose();
			}
		});
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("curriculumyearcreation");
					if( dataOk ) dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( ok, c );
		
		c.gridx = 1;
		//c.gridwidth = 1;

		JButton okNew = new JButton("Bewaar en maak nog een klas");
		okNew.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("curriculumyearcreation");
				if( dataOk ) clearData();
			}
		});
		okNew.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("curriculumyearcreation");
					if( dataOk ) clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( okNew, c );
		
		c.gridx = 2;
		//c.gridwidth = 1;
		
		JButton cancel = new JButton("Annuleren");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( cancel, c );
	}
	
	/**
	 * verifies the data and processes them if they are OK
	 * @return dataOk
	 */
	private boolean verifyAndProcess(){
		
		String currIdStr = curriculumCombo.getSelectedItem().toString();
		Curriculum curriculum = controller.getDataCollection().getCurriculum(currIdStr);
		if( curriculum == null ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een opleiding geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumCombo.requestFocus();
			return false;
		}

		String curVarType = currVariantIds[ curriculumVariantCombo.getSelectedIndex() ];//curriculumvariantTypeTextField.getText();
		CurriculumVariant cv = curriculum.getCurriculumVariant( curVarType );
		if( cv == null ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een curriculum geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumVariantCombo.requestFocus();
			return false;
		}
		
		int year = yearCombo.getSelectedIndex();
		if( year == 0 ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een jaar geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
			yearCombo.requestFocus();
			return false;
		}
		if( cv.containsCurriculumYear(year) ){
			JOptionPane.showMessageDialog(getContentPane(), "jaar " + year + " bestaat al", "Fout", JOptionPane.WARNING_MESSAGE);
			yearCombo.requestFocus();
			return false;
		}
		
		String studentNumberStr = studentNumberTextField.getText();
		int studentNumber = 0;
		try{
			studentNumber = Integer.parseInt( studentNumberStr );
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "klas moet een student aantal hebben (0 mag)", "Fout", JOptionPane.WARNING_MESSAGE);
			studentNumberTextField.requestFocus();
			return false;
		}
		if( studentNumber < 0 ){
			JOptionPane.showMessageDialog(getContentPane(), "klas moet een positief student aantal hebben (0 mag)", "Fout", JOptionPane.WARNING_MESSAGE);
			studentNumberTextField.requestFocus();
			return false;
		}
		
		/*create and register both ways*/
		CurriculumYear cy = new CurriculumYear(cv, year, new int[] {studentNumber,studentNumber,studentNumber,studentNumber});
		cv.addCurriculumYear(cy);
		return true;
	}
	
	/**
	 * clears the form for reuse
	 */
	private void clearData(){
		curriculumCombo.setSelectedIndex(0);
		curriculumVariantCombo.setSelectedIndex(0);
		studentNumberTextField.setText("");
		yearCombo.setSelectedIndex(0);
	}
}
