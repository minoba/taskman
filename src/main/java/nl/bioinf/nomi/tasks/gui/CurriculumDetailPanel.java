/**
 *
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.StudentGroup;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * This class shows the employee occupation for a given curriculum. Groups can be reassigned to teachers.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 1.0
 */
public class CurriculumDetailPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private Curriculum curriculum;
    private int schoolYear;
    private TaskManagerController controller;
    private JComboBox<String> teacherSelectionComboBox;

    /**
     *
     * @param taskManagerController
     * @param curriculum
     * @param schoolYear
     */
    public CurriculumDetailPanel(TaskManagerController taskManagerController, Curriculum curriculum, int schoolYear) {
        this.controller = taskManagerController;
        this.curriculum = curriculum;
        this.schoolYear = schoolYear;
        initialize();
    }

    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

        /*create teacher selection combo box*/
        List<Employee> eList = controller.getDataCollection().getEmployees(new Comparator<Employee>() {
            @Override
            public int compare(Employee emp1, Employee emp2) {
                return emp1.getEmployeeId().compareTo(emp2.getEmployeeId());
            }
        });
        String[] teachers = new String[eList.size() + 1];
        teachers[0] = "kies";
        for (int i = 0; i < eList.size(); i++) {
            teachers[i + 1] = eList.get(i).getEmployeeId() + "@" + eList.get(i).getVacantHours();
        }

        this.teacherSelectionComboBox = new JComboBox<String>(teachers);
        teacherSelectionComboBox.setFont(new Font(Font.MONOSPACED, Font.BOLD, 11));

        /*laying out the panel*/
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        this.setLayout(gbl);

        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 2;
        //c.weightx = 1;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

		//System.out.println("curriculum " + curriculum );
        JLabel titleLabel = new JLabel(curriculum.getName() + "          " + this.schoolYear + "-" + (this.schoolYear + 1));
        GuiComponentStylingFactory.stylizeComponent(titleLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        this.add(titleLabel, c);

        c.gridwidth = 1;
        c.gridx = 2;

        JButton export = new JButton("exporteer als pdf");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.writeCurriculumToPdf(curriculum, schoolYear);
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    controller.writeCurriculumToPdf(curriculum, schoolYear);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        this.add(export, c);

        c.gridx = 3;

        JButton delete = new JButton("verwijder opleiding");
        delete.setBackground(Color.ORANGE);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int n = JOptionPane.showConfirmDialog(null, "Weet u zeker dat u deze opleiding wilt verwijderen?", "Bevestig verwijderen", JOptionPane.OK_CANCEL_OPTION);
                if (n == JOptionPane.OK_OPTION) {
                    controller.deleteCurriculum(curriculum, schoolYear);
                }
            }
        });
        delete.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    int n = JOptionPane.showConfirmDialog(null, "Weet u zeker dat u deze opleiding wilt verwijderen?", "Bevestig verwijderen", JOptionPane.OK_CANCEL_OPTION);
                    if (n == JOptionPane.OK_OPTION) {
                        controller.deleteCurriculum(curriculum, schoolYear);
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        this.add(delete, c);

        c.gridx = 0;
        c.gridy++;

        /*fetching the quarter and half-year timeframes*/
        int prevYear = -1;
        List<Timeframe> timeFrames = curriculum.getTimeframes();
        for (Timeframe tf : timeFrames) {
            //System.out.println( tf );
            if (tf.getYear() != prevYear) {
                c.gridx = 0;
                c.gridwidth = 4;
                c.gridy++;
                this.add(getClassesLabel(tf.getYear()), c);
                c.gridy += 2;
                prevYear = tf.getYear();
            }

            if (tf.getDuration() == 1) {
                c.gridwidth = 1;
                c.gridy--;
                this.add(getQuarterLabel(tf), c);
                c.gridy++;
                this.add(getQuarterTablePane(curriculum, tf), c);
                c.gridx++;
            } else if (tf.getDuration() == 2) {
                c.gridwidth = 2;
                c.gridy--;
                this.add(getQuarterLabel(tf), c);
                c.gridy++;
                this.add(getQuarterTablePane(curriculum, tf), c);
                c.gridx += 2;
            }
        }

    }

    /**
     * returns a JLable representing the classes participating in this year
     *
     * @param year
     * @return
     */
    private JLabel getClassesLabel(int year) {
        StringBuilder classes = new StringBuilder();
        for (CurriculumYear cy : curriculum.getCurriculumYearVariants(year)) {
            classes.append("| ");
            classes.append(cy.toString());
            classes.append(" studenten: (kw 1: ");
            classes.append(cy.getStudentNumber(1));
            classes.append(", kw 2: ");
            classes.append(cy.getStudentNumber(2));
            classes.append(", kw 3: ");
            classes.append(cy.getStudentNumber(3));
            classes.append(", kw 4: ");
            classes.append(cy.getStudentNumber(4));
            classes.append(") | ");
        }

        JLabel classLabel = new JLabel(classes.toString());
        GuiComponentStylingFactory.stylizeComponent(classLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        return classLabel;
    }

    /**
     * creates and styles a JLable for a timeframe table
     *
     * @param quarter
     * @return
     */
    private JLabel getQuarterLabel(Timeframe tf) {
        String t = "kwartaal ";
        if (tf.getDuration() == 1) {
            t += tf.getOverallQuarterNumber();
        } else {
            t += (tf.getOverallQuarterNumber() + "-" + (tf.getOverallQuarterNumber() + tf.getDuration() - 1));
        }
        JLabel label = new JLabel(t);
        GuiComponentStylingFactory.stylizeComponent(label, GuiComponentStylingFactory.DETAIL_COMPONENT);
        return label;
    }

    /**
     * returns a JTable of the requested quarter for the given curriculum
     *
     * @param c the curriculum
     * @param year
     * @param quarter
     * @return jTable
     */
    private JScrollPane getQuarterTablePane(Curriculum c, Timeframe timeframe) {
        JTable table = new JTable(new CurriculumQuarterTable(curriculum, timeframe));
        table.setAutoCreateRowSorter(true);
        TableColumn column = null;
        for (int i = 0; i < table.getColumnCount(); i++) {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {//module
                column.setPreferredWidth(150);
            } else if (i == 1) {//klassen
                column.setPreferredWidth(75);
            } else if (i == 2) {//groep
                column.setPreferredWidth(20);
            } else {//docent
                column.setPreferredWidth(65);
            }
        }
        table.setColumnSelectionAllowed(true);
        table.setRowSelectionAllowed(true);

        TableColumn teacherColumn = table.getColumnModel().getColumn(3);
        teacherColumn.setCellEditor(new DefaultCellEditor(teacherSelectionComboBox));

        JScrollPane pane = new JScrollPane(table);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        if (timeframe.getDuration() == 1) {
            pane.setPreferredSize(new Dimension(310, 150));
        } else if (timeframe.getDuration() == 2) {
            pane.setPreferredSize(new Dimension(620, 175));
        }
        return pane;
    }

    /**
     * models the task list of the employee
     */
    private class CurriculumQuarterTable extends AbstractTableModel {

        private static final long serialVersionUID = 1L;
        private String[] columnNames = {"CurriculumModule", "Klassen", "Groep", "Docent"};
        private List<StudentGroup> sgList;

        public CurriculumQuarterTable(Curriculum curriculum, Timeframe timeframe) {
            sgList = curriculum.getStudentGroups(timeframe);
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return sgList.size();
        }

        @Override
        public Object getValueAt(int r, int c) {
            switch (c) {
                case 0:
                    return sgList.get(r).getCurriculumModule().getName();
                case 1: {
                    String cl = "";
                    List<CurriculumYear> classes = sgList.get(r).getCurriculumModule().getAttendingClasses();
                    for (CurriculumYear cy : classes) {
                        cl += (curriculum.getCurriculumID() + cy.getCurriculumVariant().getType() + cy.getYear() + ",");
                        //cl += (cy.toString() + ",");
                    }
                    if (cl.length() > 1) {
                        cl = cl.substring(0, cl.length() - 1);
                    }
                    return cl;
                }
                case 2:
                    return sgList.get(r).getNumber();
                case 3:
                    return sgList.get(r).getEmployee().getEmployeeId();
                default:
                    return "";
            }
        }

        public String getColumnName(int c) {
            return columnNames[c];
        }

        /*Don't need to implement this method unless your table's editable.*/
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col == 3) {
                return true;
            } else {
                return false;
            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            String emp = value.toString().substring(0, 4);

            if (!emp.equals("kies")) {
                Employee newEmpl = controller.getDataCollection().getEmployee(emp);

                /*first retrieve the employee that is being removed*/
                Employee removedEmpl = sgList.get(row).getEmployee();

                /*lets the controller process all relevant changes*/
                controller.swapStudentGroupEmployee(sgList.get(row), removedEmpl, newEmpl);

                /*generate the new view of the table*/
                fireTableCellUpdated(row, col);
            }

        }

    }

}
