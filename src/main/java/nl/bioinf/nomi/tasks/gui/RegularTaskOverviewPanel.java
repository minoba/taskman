package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.TaskCategory;

/**
 * Shows the overview of regular tasks.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class RegularTaskOverviewPanel extends JPanel {

    /**
     * the controller.
     */
    private final TaskManagerController controller;

    /**
     * construct with the controller.
     *
     * @param controller the controller
     */
    public RegularTaskOverviewPanel(final TaskManagerController controller) {
        this.controller = controller;
        initialize();
    }

    /**
     * initialization code.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);

        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "overzicht reguliere taken van jaar "
                + controller.getCurrentYear() + " - " + (controller.getCurrentYear() + 1);
        this.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.gridx = 0;
        c.gridy++;

        this.add(getTasksSummaryPane(), c);

        c.gridy++;

        JButton export = new JButton("exporteer reguliere taken overzicht naar excel");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                controller.doAction(GuiAction.EXPORT_REGULAR_TASKS_OVERVIEW);
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    controller.doAction(GuiAction.EXPORT_REGULAR_TASKS_OVERVIEW);
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        this.add(export, c);
        c.gridy++;

        this.add(GuiComponentFactory.getJLabel(
                "reguliere taken per docent (uren zijn hier niet te wijzigen)",
                GuiComponentStylingFactory.DETAIL_COMPONENT),
                c);

        c.gridy++;
        this.add(getTasksListingPane(), c);
    }

    /**
     * returns a JTable of all the project tasks.
     *
     * @return jTable the table
     */
    private JScrollPane getTasksListingPane() {
        JTable regTasksTable = new JTable(new RegularTasksTable());
        regTasksTable.setAutoCreateRowSorter(true);
        TableColumn column;
        for (int i = 0; i < regTasksTable.getColumnCount(); i++) {
            column = regTasksTable.getColumnModel().getColumn(i);
            if (i == 0) {
                //4-letter code
                column.setMinWidth(55);
            } else if (i == 1) {
                //vrije uren
                column.setMinWidth(40);
            } else {
                column.setPreferredWidth(60);
            }
        }
        regTasksTable.setColumnSelectionAllowed(true);
        regTasksTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(regTasksTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        pane.setPreferredSize(new Dimension(800, 900));
        return pane;
    }

    /**
     * models the task list of the employee.
     */
    private class RegularTasksTable extends AbstractTableModel {
        /**
         * the column names.
         */
        private final ArrayList<String> columnNames;
        /**
         * the list of employees.
         */
        private final List<Employee> employees;

        /**
         * constructor.
         */
        public RegularTasksTable() {
            columnNames = new ArrayList<String>();
            columnNames.add("Afkorting");
            columnNames.add("Uren over");

            for (TaskCategory tc : TaskCategory.values()) {
                columnNames.add(tc.toString());
            }
            employees = controller.getDataCollection().getEmployees();
        }

        @Override
        public int getColumnCount() {
            return columnNames.size();
        }

        @Override
        public int getRowCount() {
            return employees.size();
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            if (c == 0) {
                return employees.get(r).getEmployeeId();
            } else if (c == 1) {
                return employees.get(r).getVacantHours();
            } else {
                /*return the task hours for the employee for a particular project*/
                int hours = employees.get(r).getRegularTaskHours((TaskCategory.values())[c - 2]);
                String hoursStr = (hours == 0 ? "" : hours + "");
                return hoursStr;
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames.get(c);
        }
    }

    /**
     * returns a JTable of the projects overview.
     *
     * @return jTable summary table
     */
    private JScrollPane getTasksSummaryPane() {
        JTable tasksSummaryTable = new JTable(new TasksSummaryTable());

        TableColumn column;
        for (int i = 0; i < tasksSummaryTable.getColumnCount(); i++) {
            column = tasksSummaryTable.getColumnModel().getColumn(i);
            if (i == 0) {
                //cat naam
                column.setPreferredWidth(250);
            } else {
                column.setPreferredWidth(80);
            }
        }
        tasksSummaryTable.setColumnSelectionAllowed(true);
        tasksSummaryTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(tasksSummaryTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(800, 200));
        return pane;
    }

    /**
     * models the task cats overview.
     */
    private class TasksSummaryTable extends AbstractTableModel {
        /**
         * column names.
         */
        private final String[] columnNames = {"Taak categorie", "Aantal taken", "Totaal uren", "Kosten (geschat)"};

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return TaskCategory.values().length + 1;
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            if (r == TaskCategory.values().length) {
                switch (c) {
                    case 0:
                        return "TOTAAL";
                    case 1:
                        return controller.getDataCollection().getRegularTasks().size();
                    case 2:
                        return controller.getDataCollection().getRegularTaskHours();
                    case 3:
                        return (int) controller.getDataCollection().getRegularTaskCosts();
                    default:
                        return "";
                }
            } else {
                TaskCategory tc = (TaskCategory.values())[r];
                switch (c) {
                    case 0:
                        return tc.toString();
                    case 1:
                        return controller.getDataCollection().getRegularTasks(tc).size();
                    case 2:
                        return controller.getDataCollection().getRegularTaskHours(tc);
                    case 3:
                        return (int) controller.getDataCollection().getRegularTaskCosts(tc);
                    default:
                        return "";
                }

            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }
    }
}
