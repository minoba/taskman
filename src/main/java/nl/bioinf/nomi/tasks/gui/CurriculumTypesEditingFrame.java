/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;

/**
 * @author Cellingo
 *
 */
public class CurriculumTypesEditingFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private JTextField newIdTextField;
	private JTextField newNameTextField;
	private ArrayList<JTextField> nameFields;
	private JPanel curriculumTypesPanel;
	private List<String> typeIds;

	public CurriculumTypesEditingFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 600, 750);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Curriculum types");
		/*creates the form/content*/
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		//this.setLayout( gbl );
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 6;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "bewerk en creeer curriculum types", GuiComponentStylingFactory.TITLE_COMPONENT), c);

		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "bestaande types", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridy++;

		curriculumTypesPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		createCurriculumTypesPanel();
		JScrollPane ssp = new JScrollPane( curriculumTypesPanel );
		ssp.setPreferredSize( new Dimension( 450,500) );
		formPanel.add(ssp, c);

		
		c.gridy++;
		c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "voeg nieuw type toe", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridy++;
		c.gridwidth = 1;

		formPanel.add( GuiComponentFactory.getJLabel( "ID (3 letters, bijv REG)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 1;

		newIdTextField = new JTextField();
		newIdTextField.setColumns(4);
		formPanel.add( newIdTextField, c );

		c.gridx = 2;
		
		formPanel.add( GuiComponentFactory.getJLabel( "naam", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 3;
		c.gridwidth = 2;

		newNameTextField = new JTextField();
		newNameTextField.setColumns(20);
		formPanel.add( newNameTextField, c );

		c.gridx = 5;
		
		JButton add = new JButton("Voeg toe");
		add.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				verifyAndProcessNewType();
				createCurriculumTypesPanel();
				clearData();
			}
		});
		add.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					verifyAndProcessNewType();
					createCurriculumTypesPanel();
					clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});

		formPanel.add( add, c );


		c.gridy++;
		c.gridx = 0;

		JButton ok = new JButton("Klaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( ok, c );

		c.gridx = 1;

		JButton cancel = new JButton("Annuleer");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( cancel, c );
	}
	
	/**
	 * creates a listing of all known curriculum types
	 * @return curriculumTypesPanel
	 */
	private void createCurriculumTypesPanel(){
		this.curriculumTypesPanel.removeAll();
		nameFields = new ArrayList<JTextField>();
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel content = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		content.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.ipadx = 3;
		c.ipady = 2;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		typeIds = Curriculum.getCurriculumTypes();
		
		for( String type : typeIds ){
			
			JLabel idLabel = new JLabel();
			idLabel.setText( type );
			
			c.gridy++;
			c.gridx = 0;
			content.add( idLabel, c );

			JTextField nameTextField = new JTextField();
			nameTextField.setColumns(12);
			nameTextField.setName( type );
			nameTextField.setText( Curriculum.getCurriculumTypeDescription(type) );
			nameFields.add(nameTextField);
			
			c.gridx = 1;
			content.add( nameTextField, c );
			
			JButton change = new JButton("bewaar nieuwe naam");
			change.setName( type );
			change.addActionListener( new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					//System.out.println( "clicked " + ((JButton)e.getSource()).getName() );
					String changeType = ((JButton)e.getSource()).getName();
					for(JTextField field : nameFields ){
						//System.out.println( "checking field=" + field.getName() + " value=" + field.getText() );
						if( field.getName().equals(changeType) ){
							//System.out.println( "changed " + changeType + " from " + Curriculum.getCurriculumTypeDescription(changeType) + " to " + field.getText() );
							Curriculum.addCurriculumType(changeType, field.getText());
							break;
						}
					}
					createCurriculumTypesPanel();
					controller.updateViews("curriculumtypeseditingframe");
				}
			});
			
			change.addKeyListener(new KeyListener(){
				@Override
				public void keyPressed(KeyEvent e) {
					if( e.getKeyCode() == 10 ){
						//enter was pressed
						//System.out.println("key pressed char=" + e.getKeyChar() + " code=" + e.getKeyCode() + " isActionKey=" + e.isActionKey() );
						String changeType = ((JButton)e.getSource()).getName();
						for(JTextField field : nameFields ){
							if( field.getName().equals(changeType) ) Curriculum.addCurriculumType(changeType, field.getText());
							break;
						}
						createCurriculumTypesPanel();
						controller.updateViews("curriculumtypeseditingframe");
					}
				}
				@Override
				public void keyReleased(KeyEvent e) { }
				@Override
				public void keyTyped(KeyEvent e) { }
				
			});

			c.gridx = 2;
			content.add( change, c );

			JButton remove = new JButton("verwijder type");
			remove.setName( type );
			remove.addActionListener( new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					//System.out.println( "clicked " + ((JButton)e.getSource()).getName() );
					String removeType = ((JButton)e.getSource()).getName();
					boolean legal = true;
					List<Curriculum> curricula = controller.getDataCollection().getCurricula();
					for( Curriculum curriculum : curricula ){
						if( curriculum.getCurriculumVariant(removeType) != null ){
							legal = false;
							break;
						}
					}
					if( legal ){
						Curriculum.removeCurriculumVariantType(removeType);
						createCurriculumTypesPanel();
						controller.updateViews("curriculumtypeseditingframe");
					}
					else{
						JOptionPane.showMessageDialog(getContentPane(), "dit type wordt gebruikt door 1 of meerdere opleidingen en kan niet verwijderd worden: " + removeType, "Fout", JOptionPane.WARNING_MESSAGE);	
					}
				}
			});
			
			remove.addKeyListener(new KeyListener(){
				@Override
				public void keyPressed(KeyEvent e) {
					if( e.getKeyCode() == 10 ){
						//enter was pressed
						//System.out.println("key pressed char=" + e.getKeyChar() + " code=" + e.getKeyCode() + " isActionKey=" + e.isActionKey() );
						String removeType = ((JButton)e.getSource()).getName();
						boolean legal = true;
						List<Curriculum> curricula = controller.getDataCollection().getCurricula();
						for( Curriculum curriculum : curricula ){
							if( curriculum.getCurriculumVariant(removeType) != null ){
								legal = false;
								break;
							}
						}
						if( legal ){
							Curriculum.removeCurriculumVariantType(removeType);
							createCurriculumTypesPanel();
							controller.updateViews("curriculumtypeseditingframe");
						}
						else{
							JOptionPane.showMessageDialog(getContentPane(), "dit type wordt gebruikt door 1 of meerdere opleidingen en kan niet verwijderd worden: " + removeType, "Fout", JOptionPane.WARNING_MESSAGE);	
						}
					}
				}
				@Override
				public void keyReleased(KeyEvent e) { }
				@Override
				public void keyTyped(KeyEvent e) { }
			});


			c.gridx = 3;
			content.add( remove, c );
			
		}
		curriculumTypesPanel.add(content);
		paintComponents(getGraphics());
	}
	
	/**
	 * processes a new curriculum type
	 * @return allOK
	 */
	private boolean verifyAndProcessNewType(){
		String id = newIdTextField.getText();
		if( id == null || id.equals("") || id.length() != 3){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een ID van precies drie letters gegeven worden: " + id, "Fout", JOptionPane.WARNING_MESSAGE);
			newIdTextField.requestFocus();
			return false;
		}
		if( Curriculum.isKnownCurriculumType(id) ){
			JOptionPane.showMessageDialog(getContentPane(), "er bestaat al een type met dit ID " + id, "Fout", JOptionPane.WARNING_MESSAGE);
			newIdTextField.requestFocus();
			return false;
		}

		String name = newNameTextField.getText();
		if( name == null || name.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een naam gegeven worden", "Fout", JOptionPane.WARNING_MESSAGE);
			newNameTextField.requestFocus();
			return false;
		}
		Curriculum.addCurriculumType(id, name);
		controller.updateViews("curriculumtypeseditingframe");
		return true;
	}
	

	/**
	 * clears the form for reuse
	 */
	private void clearData(){
		newIdTextField.setText("");
		newNameTextField.setText("");
	}
}
