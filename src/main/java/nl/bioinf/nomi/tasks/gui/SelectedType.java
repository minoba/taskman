/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

/**
 * the type of information that was selected
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public enum SelectedType {
	CLASS("student class"),
	CURRICULUM("curriculum"),
	CURRICULUMVARIANT("curriculum variant"),
	MODULE("module"),
	PROJECT("project"),
	REGULAR_TASK("reguliere taak"),
	TEACHER("teacher");
	
	private String type;
	
	private SelectedType(String type){
		this.type = type;
	}

	public String toString(){
		return type;
	}
}
