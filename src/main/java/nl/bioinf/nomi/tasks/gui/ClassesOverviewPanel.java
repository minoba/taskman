package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;

/**
 * @author Cellingo
 *
 */


public class ClassesOverviewPanel extends JPanel {
    /**
     * the controller.
     */
    private final TaskManagerController controller;
    /**
     * the curriculum years.
     */
    private final HashMap<String, List<CurriculumYear>> curriculumYears;

    /**
     * construct with the controller.
     *
     * @param controller the controller
     */
    public ClassesOverviewPanel(final TaskManagerController controller) {
        this.controller = controller;
        curriculumYears = new HashMap<>();
        initialize();
    }

    /**
     * init code.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

        /*fetch the collection of classes*/
        List<Curriculum> curricula = controller.getDataCollection().getCurricula();
        for (Curriculum curriculum : curricula) {
            curriculumYears.put(curriculum.getCurriculumID(), new ArrayList<CurriculumYear>());
            for (int y = 1; y <= 4; y++) {
                List<CurriculumYear> varYears = curriculum.getCurriculumYearVariants(y);
                curriculumYears.get(curriculum.getCurriculumID()).addAll(varYears);
            }
        }
        List<String> currIds = new ArrayList<>();
        currIds.addAll(curriculumYears.keySet());

        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "klassenoverzicht van jaar " + controller.getCurrentYear()
                + " - " + (controller.getCurrentYear() + 1);
        this.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.gridx = 0;
        c.gridy++;

        this.add(GuiComponentFactory.getJLabel(
                "Overzicht van klassen. Studentaantallen zijn hier te wijzigen",
                GuiComponentStylingFactory.DETAIL_COMPONENT),
                c);

        c.gridy++;

        for (String currId : currIds) {
            this.add(GuiComponentFactory.getJLabel((
                    "opleiding " + currId),
                    GuiComponentStylingFactory.DETAIL_COMPONENT),
                    c);
            c.gridy++;

            this.add(getClassesPane(currId), c);
            c.gridy++;
        }

    }

    /**
     * returns a JTable of all the project tasks.
     * @param curriculumId the curriculum ID
     * @return jTable the JTable the class in a curriculum
     */
    private JScrollPane getClassesPane(final String curriculumId) {
        JTable classesTable = new JTable(new ClassesTable(curriculumId));

        TableColumn column;
        for (int i = 0; i < classesTable.getColumnCount(); i++) {
            column = classesTable.getColumnModel().getColumn(i);
            if (i == 0) { //4-letter code
                column.setPreferredWidth(200);
            } else if (i == 2) { //naam
                column.setPreferredWidth(120);
            } else {
                column.setPreferredWidth(60);
            }
        }
        classesTable.setColumnSelectionAllowed(true);
        classesTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(classesTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(370, 200));
        return pane;
    }

    /**
     * models the task list of the employee.
     */
    private class ClassesTable extends AbstractTableModel {
        /**
         * the column names.
         */
        private final String[] columnNames = {"Curriculumvariant", "Jaar", "Studenten Kw 1", "Kw 2", "Kw 3", "Kw 4"};
        /**
         * the curriculum ID.
         */
        private final String curriculumId;

        /**
         * constructs with a curriculum ID.
         * @param curriculumId the curriculum ID
         */
        public ClassesTable(final String curriculumId) {
            this.curriculumId = curriculumId;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return curriculumYears.get(curriculumId).size();
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            switch (c) {
                case 0:
                    return curriculumYears.get(curriculumId).get(r).getCurriculumVariant().getName();
                case 1:
                    return curriculumYears.get(curriculumId).get(r).getYear();
                case 2:
                    return curriculumYears.get(curriculumId).get(r).getStudentNumber(1);
                case 3:
                    return curriculumYears.get(curriculumId).get(r).getStudentNumber(2);
                case 4:
                    return curriculumYears.get(curriculumId).get(r).getStudentNumber(3);
                case 5:
                    return curriculumYears.get(curriculumId).get(r).getStudentNumber(4);
                default:
                    return "";
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }

        @Override
        public boolean isCellEditable(final int row, final int col) {
            //Omschrijving en totaal uren zijn editable
            return col > 1;
        }

        @Override
        public void setValueAt(final Object value, final int row, final int col) {
            if (col > 1) {
                try {
                    //the new student number
                    int stNumber = Integer.parseInt(value.toString());
                    //quarter is column number minus 2
                    int quarter = col - 1;
                    //curryear
                    CurriculumYear cy = curriculumYears.get(curriculumId).get(row);
                    cy.setStudentNumber(quarter, stNumber);
                    controller.classSizeChanged(cy, quarter);
                    fireTableCellUpdated(row, col);
                } catch (final Exception e) {
                    /*do nothing: ignore edit */
                }

            }
        }
    }

}
