/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Insets;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import nl.bioinf.nomi.tasks.control.TaskManagerController.ExtListSelectionListener;
import nl.bioinf.nomi.tasks.control.TaskManagerController.TreeSelectionListenerImpl;
import nl.bioinf.nomi.tasks.datamodel.*;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;

/**
 * generates all gui components
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class GuiComponentFactory {
	/**
	 * the unique singleton instance
	 */
	private static GuiComponentFactory uniqueInstance;
	
	/**
	 * private constructor for singleton pattern
	 */
	private GuiComponentFactory(){ }
	
	/**
	 * returns the unique instance of this object
	 * @return uniqueInstance
	 */
	public static synchronized GuiComponentFactory getInstance(){
		if( uniqueInstance == null ){
			uniqueInstance = new GuiComponentFactory();
		}
		return uniqueInstance;
	}
	
	
	/**
	 * creates a listing of teachers
	 * @return teacherslist
	 */
	public JComponent getListPanel( String listName, String[] listElements, ExtListSelectionListener listener, int selectedIndex ){
		
		JList<String> list = new JList<String>( listElements );
		list.setName( listName );
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		list.addListSelectionListener(listener);
		if( selectedIndex >= 0 ) list.setSelectedIndex( selectedIndex );
		listener.setJList( list );
		GuiComponentStylingFactory.stylizeComponent(list, GuiComponentStylingFactory.LIST_COMPONENT);
		return list;
		
	}
	
	/**
	 * returns a JTree that holds the collection of modules
	 * @param treeName
	 * @param ydc
	 * @param listener
	 * @return
	 */
	public JComponent getModuleTreePanel( String treeName, YearDataCollection ydc, TreeSelectionListenerImpl listener ){
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Modules");
		
		JTree tree = new JTree(top);

		tree.setName(treeName);
		DefaultMutableTreeNode cNode = null;
		DefaultMutableTreeNode cvNode = null;
		DefaultMutableTreeNode cyNode = null;
		DefaultMutableTreeNode qNode = null;
		DefaultMutableTreeNode mNode = null;
		
	    /*get unassigned modules*/
		DefaultMutableTreeNode lNode = new DefaultMutableTreeNode("All");
		top.add( lNode );
		
		DefaultMutableTreeNode aNode = null;
		DefaultMutableTreeNode uNode = null;
	    for( CurriculumModule curriculumModule : ydc.getModules() ){
    		mNode = new DefaultMutableTreeNode(curriculumModule);
	    	if( curriculumModule.getAttendingClasses().size() == 0 ){
	    		if(uNode == null ){
	    			uNode = new DefaultMutableTreeNode("Unassigned");
	    			lNode.add( uNode );
	    		}
	    		uNode.add(mNode);
	    	}
	    	else{
	    		if(aNode == null ){
	    			aNode = new DefaultMutableTreeNode("Assigned");
	    			lNode.add( aNode );
	    		}
	    		aNode.add(mNode);
	    	}
	    }

		
		for( Curriculum c : ydc.getCurricula() ){
			cNode = new DefaultMutableTreeNode( c );
		    top.add(cNode);
//		    tree.expandPath(new TreePath(cNode));
		    
		    /*traverse variants*/
		    for( CurriculumVariant cv : c.getcurriculumVariants() ){
		    	cvNode = new DefaultMutableTreeNode( cv );
		    	cNode.add(cvNode);
		    	/*traverse years*/
			    for( int i=1; i<=4; i++){
			    	if(cv.containsCurriculumYear(i)){
				    	cyNode = new DefaultMutableTreeNode( "jaar " + i );
				    	cvNode.add(cyNode);
				    	CurriculumYear cy = cv.getCurriculumYear(i);
				    	
				    	/*traverse quarters*/
				    	for(int q=1; q<=4; q++){
				    		
				    		List<CurriculumModule> modList = cy.getModules(q);
				    		if( modList.size() > 0 ){
					    		qNode = new DefaultMutableTreeNode( "kwartaal " + q );
					    		cyNode.add(qNode);
						    	for(CurriculumModule m : modList ){
						    		mNode = new DefaultMutableTreeNode( m );
						    		qNode.add(mNode);
						    	}
				    		}
				    	}
			    	}
			    }
		    }
		}

		//Where the tree is initialized:
	    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	    tree.setShowsRootHandles(true);
	    /*register listener and tree*/
	    listener.setTree(tree);
	    tree.addTreeSelectionListener(listener);
	    
	    TreePath tp = new TreePath(top);
	    tree.expandPath( tp );

        TreeNode node = (TreeNode)tp.getLastPathComponent();
        if (node.getChildCount() >= 0) {
            for (@SuppressWarnings("rawtypes")
			Enumeration e=node.children(); e.hasMoreElements(); ) {
                TreeNode n = (TreeNode)e.nextElement();
                TreePath path = tp.pathByAddingChild(n);
                
//                if (n.getChildCount() >= 0) {
//                    for (Enumeration en=n.children(); en.hasMoreElements(); ) {
//                        TreeNode nn = (TreeNode)en.nextElement();
//                    }
//                }
                
                tree.expandPath(path);
            }
        }
		GuiComponentStylingFactory.stylizeComponent(tree, GuiComponentStylingFactory.TREE_COMPONENT);
		return tree;
	}
	

	/**
	 * returns a JTree that holds the collection of modules
	 * @param treeName
	 * @param ydc
	 * @param listener
	 * @return
	 */
	public JComponent getCurriculumVariantTreePanel( String treeName, YearDataCollection ydc, TreeSelectionListenerImpl listener ){
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Curricula");
		
		JTree tree = new JTree(top);
		tree.setName(treeName);
		
		DefaultMutableTreeNode cNode = null;
		DefaultMutableTreeNode cvNode = null;
		DefaultMutableTreeNode cyNode = null;
		

		for( Curriculum c : ydc.getCurricula() ){
			cNode = new DefaultMutableTreeNode( c );
		    top.add(cNode);
//		    tree.expandPath(new TreePath(cNode));
		    
		    /*traverse variants*/
		    for( CurriculumVariant cv : c.getcurriculumVariants() ){
		    	cvNode = new DefaultMutableTreeNode( cv );
		    	cNode.add(cvNode);
		    	/*traverse years*/
			    for( int i=1; i<=4; i++){
			    	if(cv.containsCurriculumYear(i)){
			    		CurriculumYear cy = cv.getCurriculumYear(i);
				    	cyNode = new DefaultMutableTreeNode( cy );
				    	cvNode.add(cyNode);
				    	//CurriculumYear cy = cv.getCurriculumYear(i);
				    	
			    	}
			    }
		    }
		}

		//Where the tree is initialized:
	    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	    tree.setShowsRootHandles(true);
	    /*register listener and tree*/
	    listener.setTree(tree);
	    tree.addTreeSelectionListener(listener);
	    
	    TreePath tp = new TreePath(top);
	    tree.expandPath( tp );

        TreeNode node = (TreeNode)tp.getLastPathComponent();
        if (node.getChildCount() >= 0) {
            for (@SuppressWarnings("rawtypes")
			Enumeration e=node.children(); e.hasMoreElements(); ) {
                TreeNode n = (TreeNode)e.nextElement();
                TreePath path = tp.pathByAddingChild(n);
                tree.expandPath(path);
            }
        }
		GuiComponentStylingFactory.stylizeComponent(tree, GuiComponentStylingFactory.TREE_COMPONENT);
		return tree;
	}

	/**
	 *  returns a label to be used for panels, styled according to target
	 * @param text
	 * @param target
	 * @return
	 */
	public static JLabel getJLabel( String text, int target ){
		JLabel label = new JLabel( text );
		GuiComponentStylingFactory.stylizeComponent(label, target);
		return label;
	}
	
	/**
	 *  returns a panel to be used for panels, styled according to target
	 * @param text
	 * @param target
	 * @return
	 */
	public static JPanel getJPanel( int target ){
		JPanel panel = new JPanel( );
		GuiComponentStylingFactory.stylizeComponent(panel, target);
		return panel;
	}
	
	/**
	 * creates the default insets for layout managements
	 * @return insets
	 */
	public static Insets getLayoutInsets(){
		return new Insets(0,0,3,3);
	}

	

}
