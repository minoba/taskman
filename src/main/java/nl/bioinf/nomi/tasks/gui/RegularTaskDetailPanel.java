package nl.bioinf.nomi.tasks.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.RegularTask;
import nl.bioinf.nomi.tasks.datamodel.TaskCategory;
import nl.bioinf.nomi.tasks.datamodel.TaskLabel;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * Shows details of regular tasks.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class RegularTaskDetailPanel extends JPanel {

    /**
     * controller.
     */
    private final TaskManagerController controller;
    /**
     * task category.
     */
    private final TaskCategory taskCategory;
    /**
     * teachers.
     */
    private JComboBox<String> teacherSelectionComboBox;
    /**
     * the task labels selection combo.
     */
    private JComboBox<TaskLabel> taskLabelSelectionComboBox;

    /**
     * construct with the controller.
     *
     * @param controller the controller
     * @param taskCategory the task category
     */
    public RegularTaskDetailPanel(final TaskManagerController controller, final TaskCategory taskCategory) {
        this.controller = controller;
        this.taskCategory = taskCategory;
        initialize();
    }

    /**
     * initializer.
     */
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

        /*create teacher selection combo box*/
        List<Employee> eList = controller.getDataCollection().getEmployees(new Comparator<Employee>() {
            @Override
            public int compare(final Employee emp1, final Employee emp2) {
                return emp1.getEmployeeId().compareTo(emp2.getEmployeeId());
            }
        });
        String[] teachers = new String[eList.size() + 1];
        teachers[0] = "kies";
        for (int i = 0; i < eList.size(); i++) {
            teachers[i + 1] = eList.get(i).getEmployeeId() + "@" + eList.get(i).getVacantHours();
        }
        this.teacherSelectionComboBox = new JComboBox<>(teachers);
        teacherSelectionComboBox.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));

        /*create task label selection combo*/
        TaskLabel[] tlArr = TaskLabel.values();
        this.taskLabelSelectionComboBox = new JComboBox<>(tlArr);

        this.setLayout(new BorderLayout());
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        JPanel tasksDataPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
        tasksDataPanel.setLayout(gbl);
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 3;
        //c.fill = GridBagConstraints.NONE;
        c.ipadx = 4;
        c.ipady = 4;
        c.weightx = 0;
        //c.weighty = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        String title = "Reguliere taken van categorie " + taskCategory.toString()
                + " van jaar " + controller.getCurrentYear() + " - " + (controller.getCurrentYear() + 1);
        //this.add(GuiComponentFactory.getJLabel( title, GuiComponentStylingFactory.TITLE_COMPONENT), c);
        JLabel titleLabel = GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT);
        //GuiComponentStylingFactory.stylizeComponent(titleLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        tasksDataPanel.add(titleLabel, c);

        c.gridwidth = 1;

        c.gridx = 0;
        c.gridy++;
        int hours = controller.getDataCollection().getRegularTaskHours(taskCategory);
        JLabel totalHoursLabel = new JLabel("totaal aantal uren voor deze categorie: " + hours);
        GuiComponentStylingFactory.stylizeComponent(totalHoursLabel, GuiComponentStylingFactory.LIST_COMPONENT);
        tasksDataPanel.add(totalHoursLabel, c);

        c.gridx = 0;
        c.gridy++;

        JLabel taskNumberLabel = new JLabel("totaal aantal taken: "
                        + controller.getDataCollection().getRegularTasks(taskCategory).size());
        GuiComponentStylingFactory.stylizeComponent(taskNumberLabel, GuiComponentStylingFactory.DETAIL_COMPONENT);
        tasksDataPanel.add(taskNumberLabel, c);

        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 1;

        JLabel spacerLabel = new JLabel(" ");
        tasksDataPanel.add(spacerLabel, c);

        JLabel tasksLabel = new JLabel("Taken van deze categorie");
        GuiComponentStylingFactory.stylizeComponent(tasksLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
        tasksDataPanel.add(tasksLabel, c);

        c.gridwidth = 4;

        this.add(tasksDataPanel, BorderLayout.PAGE_START);

        JScrollPane taskSp = new JScrollPane(getTasksTable());
        GuiComponentStylingFactory.stylizeComponent(taskSp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        taskSp.setPreferredSize(new Dimension(850, 450));
        this.add(taskSp, BorderLayout.CENTER);

        c.gridx = 3;

        JButton export = new JButton("exporteer als pdf");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                controller.writeRegularTaskCatToPdf(taskCategory, controller.getCurrentYear());
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    controller.writeRegularTaskCatToPdf(taskCategory, controller.getCurrentYear());
                }
            }

            @Override
            public void keyReleased(final KeyEvent e) { }

            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        //tasksDataPanel.add(export, c);
        //export.setBackground(Color.ORANGE);
        this.add(export, BorderLayout.SOUTH);
    }

    /**
     * creates the Tasks Table.
     * @return tasksTable the tasks table
     */
    private JTable getTasksTable() {
        JTable empTable = new JTable(new RegularTaskTable());
        empTable.setAutoCreateRowSorter(true);
        TableColumn column;
        for (int i = 0; i < 5; i++) {
            column = empTable.getColumnModel().getColumn(i);
            if (i == 1 || i == 2) {
                column.setPreferredWidth(150); //third column is bigger
            } else if (i == 3) {
                column.setPreferredWidth(200);
            } else {
                column.setPreferredWidth(50);
            }
        }
        TableColumn teacherColumn = empTable.getColumnModel().getColumn(0);
        teacherColumn.setCellEditor(new DefaultCellEditor(this.teacherSelectionComboBox));
        TableColumn taskLabelsColumn = empTable.getColumnModel().getColumn(3);
        taskLabelsColumn.setCellEditor(new DefaultCellEditor(this.taskLabelSelectionComboBox));

        empTable.setColumnSelectionAllowed(true);
        empTable.setRowSelectionAllowed(true);
        return empTable;
    }

    /**
     * models the task list of the employee.
     */
    private class RegularTaskTable extends AbstractTableModel {
        /**
         * the tasks.
         */
        private List<RegularTask> tasks;
        /**
         * row count.
         */
        private final int rowCount;
        //naam | omschrijving | euri | uren op basis van GPL | GPL uren  HBO | /
        //Uren uitgezet | Uren over (GPL) | Euri uitgezet

        /**
         * the column names.
         */
        private String[] columnNames = {
            "4-letter code",
            "Naam",
            "Taak naam",
            "Taak label",
            "Uren",
            "Start (kwartaal)",
            "Duur (kwartalen)"};

        /**
         * the tasks table constructor.
         */
        public RegularTaskTable() {
            this.tasks = new ArrayList<>();
            tasks.addAll(controller.getDataCollection().getRegularTasks(taskCategory));
            Collections.sort(tasks, new Comparator<RegularTask>() {

                @Override
                public int compare(final RegularTask t1, final RegularTask t2) {
                    return t1.getName().compareTo(t2.getName());
                }

            });
            this.rowCount = tasks.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return rowCount;
        }

        @Override
        public Object getValueAt(final int r, final int c) {
            RegularTask t = tasks.get(r);
            switch (c) {
                case 0:
                    return t.getEmployee().getEmployeeId();
                case 1:
                    return t.getEmployee().getFullName();
                case 2:
                    return t.getName();
                case 3:
                    return t.getTaskLabel();
                case 4:
                    return t.getTaskHours();
                case 5:
                    return t.getTimeframe().getQuarter();
                case 6:
                    return t.getTimeframe().getDuration();
                default:
                    return "";
            }
        }

        @Override
        public String getColumnName(final int c) {
            return columnNames[c];
        }

        /*Except for Employee name (second column), all fields are editable*/
        @Override
        public boolean isCellEditable(final int row, final int col) {
            return col != 1;
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        @Override
        public void setValueAt(final Object value, final int row, final int col) {
            //{"4-letter code", "Naam", "Omschrijving", "Uren", "Start (kwartaal)", "Duur (kwartalen)"};
            //System.out.println("changing value at r:c " + row + ":" + col);

            if (col == 0) {
                String emp = value.toString().substring(0, 4);

                if (!emp.equals("kies")) {
                    Employee newEmpl = controller.getDataCollection().getEmployee(emp);

                    /*first retrieve the employee that is being removed*/
                    RegularTask changedTask = tasks.get(row);
                    Employee removedEmpl = changedTask.getEmployee();

                    /*lets the controller process all relevant changes*/
                    controller.swapRegularTaskEmployee(changedTask, removedEmpl, newEmpl);

                    /*generate the new view of the table*/
                    //fireTableCellUpdated(row, col);
                }

            } else if (col == 2) {
                /*name was changed*/
                tasks.get(row).setName(value.toString());
            } else if (col == 3) {
                /*task label was changed*/
                tasks.get(row).setTaskLabel((TaskLabel) value);
            } else if (col == 4) {
                /*total hours was changed*/
                try {
                    int newHours = Integer.parseInt(value.toString());
                    if (newHours == 0) {
                        RegularTask rt = tasks.get(row);
                        rt.getEmployee().removeRegularTask(rt);
                        controller.getDataCollection().removeRegularTask(rt.getTaskId());
                        //tasks.remove(row);
                        controller.setViewedEmployee(rt.getEmployee());
                        controller.updateViews("regularTaskdetail_panel");
                        return;
                    } else {
                        tasks.get(row).setTaskHours(newHours);
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            } else if (col == 5) {
                /*total hours was changed*/
                try {
                    int newStart = Integer.parseInt(value.toString());
                    int newDuration = Integer.parseInt(getValueAt(row, 6).toString());
                    //System.out.println("start=" + newStart + " duration=" + newDuration);
                    if (newStart + newDuration > 5) {
                        newDuration = 5 - newStart;
                    }
                    if (newTimeFrameOk(newStart, newDuration)) {
                        tasks.get(row).setTimeframe(new Timeframe(newStart, newDuration));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //TODO handle exception
                }
            } else if (col == 6) {
                /*total hours was changed*/
                try {
                    int newStart = Integer.parseInt(getValueAt(row, 5).toString());
                    int newDuration = Integer.parseInt(value.toString());
                    //System.out.println("start=" + newStart + " duration=" + newDuration);
                    if (newTimeFrameOk(newStart, newDuration)) {
                        tasks.get(row).setTimeframe(new Timeframe(newStart, newDuration));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //TODO handle exception
                }
            }
            fireTableDataChanged();
            controller.setViewedEmployee(tasks.get(row).getEmployee());
            controller.updateViews("projectsoverview");
        }

        /**
         * checks the new timeframe settings.
         * @param newStart the new start
         * @param newDuration the new duration
         * @return allOK settnigs are OK
         */
        private boolean newTimeFrameOk(final int newStart, final int newDuration) {
            if (newStart < 1 || newStart > 4) {
                return false;
            }
            if (newDuration < 1 || newDuration > 4) {
                return false;
            }
            return newStart + newDuration <= 5;
        }
    }
}
