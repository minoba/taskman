package nl.bioinf.nomi.tasks.gui;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.StudentGroup;

public class GroupSelectionFrame extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private static int offset = 0;
	private TaskManagerController controller;
	private CurriculumModule curriculumModule;
	private ArrayList<JCheckBox> checks;
	private	int groupNumberToRemove;
	private JLabel errorLabel;
	
	public GroupSelectionFrame( TaskManagerController controller, CurriculumModule curriculumModule){
		
		this.controller = controller;
		this.curriculumModule = curriculumModule;
		this.checks = new ArrayList<JCheckBox>();
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(200+offset, 200+offset, 500, 400);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("kies studentgroepen om te verwijderen");
		createSelectionForm();
		toFront();
		requestFocus();
		
		offset += 25;
		if( offset > 500 ) offset = 0;
	}
	
	private void createSelectionForm(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		this.groupNumberToRemove = curriculumModule.getGroupsNumberToRemove();

		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.ipadx = 4;
		c.ipady = 4;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		String title = curriculumModule.getName() + " (" + curriculumModule.getModuleId() + ")       kwartaal " + curriculumModule.getTimeframe().getQuarter();
		formPanel.add(GuiComponentFactory.getJLabel( title, GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		c.gridy++;
		
		String message = groupNumberToRemove + " groepen moeten verwijderd worden uit deze curriculumModule";
		formPanel.add(GuiComponentFactory.getJLabel( message, GuiComponentStylingFactory.TITLE_COMPONENT), c);
		
		c.gridy++;
		//c.gridwidth = 1;
		
		for( StudentGroup sg : curriculumModule.getStudentGroups() ){
			JCheckBox check = new JCheckBox( "groep " + sg.getNumber() + ": " + sg.getEmployee().getEmployeeId() + " (assignable hours =" + sg.getEmployee().getAssignableHours() + ")");
			check.setName( sg.getGroupId() + "" );
			formPanel.add(check, c);
			checks.add(check);
			c.gridy++;
		}
		
		errorLabel = new JLabel("");
		errorLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14 ));
		formPanel.add(errorLabel, c);
		
		c.gridy++;
		
		JButton ok = new JButton( "OK" );
		ok.addActionListener( this );
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					processSelection();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});

		formPanel.add(ok, c);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		processSelection();
	}

	private void processSelection(){
		int checkedNumber = 0;
		for( JCheckBox check : checks ){
			if( check.isSelected() ) checkedNumber++;
		}
		if( checkedNumber == curriculumModule.getGroupNumber() ){
			errorLabel.setText("    !! er moet ten minste 1 groep overblijven !!    ");
			return; 
		}
		if( checkedNumber == 0 ) curriculumModule.redistributeAttendingStudents();
		else{//( checkedNumber < curriculumModule.getGroupNumber() ){
			for( JCheckBox check : checks ){
				if( check.isSelected() ){
					//System.out.println("removing student group " + check.getName());
					curriculumModule.removeStudentGroup( Integer.parseInt( check.getName() ) );
				}
			}
		}
		controller.updateViews( "groupselectionframe" );
		this.dispose();
		
//		if(checkedNumber == this.groupNumberToRemove ){
//			for( JCheckBox check : checks ){
//				if( check.isSelected() ) curriculumModule.removeStudentGroup( Integer.parseInt( check.getName() ) );
//			}
//			controller.updateViews( "groupselectionframe" );
//			this.dispose();
//		}
//		else{
//			errorLabel.setText("er moeten precies " + groupNumberToRemove + " groepen geselecteerd worden");
//		}
	}
}
