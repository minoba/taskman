package nl.bioinf.nomi.tasks.gui;

/**
 * @author michiel
 *
 */
public enum GuiAction {
	FILE_OPEN("open file"),
	FILE_SHOW("toon geladen file"),
	SAVE_FILE("save file"),
	SAVE_AS_FILE("save file as"),
	EXIT("exit program"),
	CREATE_DATACOLLECTION("create a new data collection"),
	CREATE_EMPLOYEE("create a new teacher"),
	CREATE_REGULAR_TASK("create a new regular task"),
	CREATE_PROJECT("create a new project"),
	CREATE_PROJECT_TASK("create a new project task"),
	CREATE_MODULE("create a new module"),
	CREATE_CURRICULUM("create a curriculum"),
	CREATE_CURRICULUMVARIANT("create a curriculum variant"),
	CREATE_CURRICULUMYEAR("create a curriculum year"),
	EXPORT_TASKOVERVIEWS("exporteer alle takenlijsten"),
	EXPORT_ALL_TASKS("exporteer alle taken"),
	EXPORT_CURRICULA("exporteer alle curriculumoverzichten"),
	EXPORT_INTERNSHIP_OVERVIEW("exporteer stage- en afstudeer bezettingsoverzicht"),
	EXPORT_PROJECTS_OVERVIEW("exporteer projectenoverzicht naar excel"),
	EXPORT_GENERAL_OVERVIEW("exporteer algemeen/financieel overzicht naar excel"),
	EXPORT_REGULAR_TASKS_OVERVIEW("exporteer overzich van reguliere taken naar excel"),
	EXPORT_PLANNING_RULES("exporteer roosterregels voor alle opleidingen"),
	EDIT_DATACOLLECTION("edit the collection metadata"),
	EDIT_COURSE_METHODS("create and edit course methods"),
	EDIT_CURRICULUM_TYPES("edit and add curriculum variant types"),
	SHOW_HELP_ABOUT("shows general info"),
	SHOW_HELP_ITEMS("shows help items");
	
	private String type;
	
	private GuiAction( String type ){
		this.type = type;
	}
	
	public String toString(){
		return type;
	}
}
