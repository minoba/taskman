package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;

/**
 * @author Cellingo
 *
 */
public class EmployeeCreationFrame extends JFrame {
    /**
     * the controller.
     */
    private final TaskManagerController controller;
    /**
     * the ID TextField.
     */
    private JTextField idTextField;
    /**
     * the first name TextField.
     */
    private JTextField firstNameTextField;
    /**
     * the infox TextField.
     */
    private JTextField infixTextField;
    /**
     * the last name TextField.
     */
    private JTextField lastNameTextField;
    /**
     * the the fte TextField.
     */
    private JTextField fteTextField;
    /**
     * the salary scale combo.
     */
    private JComboBox<Object> salaryScaleCombo;
    /**
     * the salary scales.
     */
    private List<Integer> salaryScales;
    /**
     * the team TextField.
     */
    private JTextField teamTextField;
    /**
     * the remarks TextField.
     */
    private JTextField remarksTextField;
    /**
     * the email TextField.
     */
    private JTextField emailTextField;

    /**
     *
     * @param controller the controller
     */
    public EmployeeCreationFrame(final TaskManagerController controller) {
        this.controller = controller;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(200, 200, 750, 400);
        /*set frame icon and title*/
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.getImage("graphics/tasks_icon.gif");
        setIconImage(img);

        setTitle("Nieuwe medewerker");
        /*creates the form/content*/
        createContent();

        toFront();
        requestFocus();
        setAlwaysOnTop(true);
    }

    /**
     * generates form content.
     */
    private void createContent() {
        GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
        JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        JScrollPane sp = new JScrollPane(formPanel);
        this.getContentPane().add(sp);
        formPanel.setLayout(gbl);

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 6;
        c.ipadx = 50;
        c.ipady = 50;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        formPanel.add(GuiComponentFactory.getJLabel(
                "maak een nieuwe medewerker aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);
        c.ipadx = 10;
        c.ipady = 4;
        c.gridwidth = 1;

        c.gridy++;
        formPanel.add(GuiComponentFactory.getJLabel(
                "afkorting (4 letters)", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 1;
        c.gridwidth = 1;
        idTextField = new JTextField();
        idTextField.setColumns(5);
        formPanel.add(idTextField, c);
        c.gridx = 2;
        formPanel.add(GuiComponentFactory.getJLabel(" team", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 3;
        teamTextField = new JTextField();
        teamTextField.setColumns(3);
        formPanel.add(teamTextField, c);

        c.gridy++;
        c.gridx = 0;
        formPanel.add(GuiComponentFactory.getJLabel("voornaam", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 1;
        firstNameTextField = new JTextField();
        firstNameTextField.setColumns(5);
        formPanel.add(firstNameTextField, c);
        c.gridx = 2;
        formPanel.add(GuiComponentFactory.getJLabel(" tussenvoegsel", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 3;
        infixTextField = new JTextField();
        infixTextField.setColumns(3);
        formPanel.add(infixTextField, c);
        c.gridx = 4;
        formPanel.add(GuiComponentFactory.getJLabel("achternaam", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 5;
        lastNameTextField = new JTextField();
        lastNameTextField.setColumns(10);
        formPanel.add(lastNameTextField, c);

        c.gridy++;
        c.gridx = 0;
        formPanel.add(GuiComponentFactory.getJLabel(
                "aanstelling (0 - 1.0)", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 1;
        fteTextField = new JTextField();
        fteTextField.setColumns(5);
        formPanel.add(fteTextField, c);
        c.gridx = 2;
        formPanel.add(GuiComponentFactory.getJLabel(" salarisschaal", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 3;
        salaryScales = Employee.getSalaryScales();
        salaryScales.add(0, 0);
        salaryScaleCombo = new JComboBox<Object>(salaryScales.toArray());
        formPanel.add(salaryScaleCombo, c);
        c.gridx = 4;
        formPanel.add(GuiComponentFactory.getJLabel(" email", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridx = 5;
        emailTextField = new JTextField();
        emailTextField.setColumns(10);
        formPanel.add(emailTextField, c);

        c.gridy++;
        c.gridx = 0;
        formPanel.add(GuiComponentFactory.getJLabel("opmerkingen", GuiComponentStylingFactory.FORM_COMPONENT), c);
        c.gridy++;
        c.gridwidth = 6;
        remarksTextField = new JTextField();
        remarksTextField.setColumns(40);
        formPanel.add(remarksTextField, c);
        c.gridy++;
        c.gridx = 0;

        JButton ok = new JButton("Bewaar");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                boolean dataOk = verifyAndProcess();
                controller.updateViews("employeecreation");
                if (dataOk) {
                    dispose();
                }
            }
        });
        ok.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    boolean dataOk = verifyAndProcess();
                    controller.updateViews("employeecreation");
                    if (dataOk) {
                        dispose();
                    }
                }
            }
            @Override
            public void keyReleased(final KeyEvent e) { }
            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        formPanel.add(ok, c);

        c.gridx = 1;
        c.gridwidth = 3;

        JButton okNew = new JButton("Bewaar en nog een medewerker maken");
        okNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                boolean dataOk = verifyAndProcess();
                controller.updateViews("employeecreation");
                if (dataOk) {
                    clearData();
                }
            }
        });
        okNew.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    boolean dataOk = verifyAndProcess();
                    controller.updateViews("employeecreation");
                    if (dataOk) {
                        clearData();
                    }
                }
            }
            @Override
            public void keyReleased(final KeyEvent e) { }
            @Override
            public void keyTyped(final KeyEvent e) { }
        });

        formPanel.add(okNew, c);

        c.gridx = 5;
        c.gridwidth = 1;

        JButton cancel = new JButton("Annuleren");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                dispose();
            }
        });
        cancel.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    dispose();
                }
            }
            @Override
            public void keyReleased(final KeyEvent e) { }
            @Override
            public void keyTyped(final KeyEvent e) { }
        });
        formPanel.add(cancel, c);
    }

    /**
     * verifies the data and processes them if they are OK.
     *
     * @return dataOk
     */
    private boolean verifyAndProcess() {
        String id = idTextField.getText();
        id = id.toUpperCase();
        if (controller.getDataCollection().getEmployee(id) != null) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "de opgegeven afkorting bestaat al", "Fout", JOptionPane.WARNING_MESSAGE);
            idTextField.requestFocus();
            return false;
        }
        if (id.length() != 4) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is geen 4-letter code gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            idTextField.requestFocus();
            return false;
        }
        String firstName = firstNameTextField.getText();
        if (firstName == null || firstName.equals("")) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is geen voornaam gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            firstNameTextField.requestFocus();
            return false;
        }

        String infix = infixTextField.getText();

        String lastName = lastNameTextField.getText();
        if (lastName == null || lastName.equals("")) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is geen achternaam gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            lastNameTextField.requestFocus();
            return false;
        }

        String fteStr = fteTextField.getText();
        double fte;
        if (fteStr == null || fteStr.equals("")) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is geen fte gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            fteTextField.requestFocus();
            return false;
        }
        try {
            fte = Double.parseDouble(fteStr);
            if (fte < 0 || fte > 1) {
                JOptionPane.showMessageDialog(
                        getContentPane(),
                        "er is een foute waarde voor fte gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
                fteTextField.requestFocus();
                return false;
            }
        } catch (NumberFormatException | HeadlessException e) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is een foute waarde voor fte gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            fteTextField.requestFocus();
            return false;
        }

        int salaryScale;
        try {
            salaryScale = Integer.parseInt(salaryScaleCombo.getSelectedItem().toString());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "er is een foute waarde voor de salarisschaal gegeven",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            salaryScaleCombo.requestFocus();
            return false;
        }
        if (salaryScale == 0) {
            JOptionPane.showMessageDialog(
                    getContentPane(),
                    "er is een foute waarde voor de salarisschaal gegeven",
                    "Fout",
                    JOptionPane.WARNING_MESSAGE);
            salaryScaleCombo.requestFocus();
            return false;
        }

        String team = teamTextField.getText();
        team = team.toUpperCase();
        if (team == null || team.equals("")) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is geen team gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            teamTextField.requestFocus();
            return false;
        }

        String email = emailTextField.getText();
        if (email == null || email.equals("")) {
            JOptionPane.showMessageDialog(
                    getContentPane(), "er is geen email gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
            emailTextField.requestFocus();
            return false;
        }

        String remarks = remarksTextField.getText();

        Employee employee = new Employee(id, fte);
        employee.setFirstName(firstName);
        if (infix != null && !infix.equals("")) {
            employee.setNameInfix(infix);
        }
        employee.setFamilyName(lastName);
        employee.setFunctionScale(salaryScale);
        employee.setTeam(team);
        employee.setEmail(email);
        if (remarks != null && !remarks.equals("")) {
            employee.setRemarks(remarks);
        }
        controller.getDataCollection().addEmployee(employee);
        return true;
    }

    /**
     * clears the form for reuse.
     */
    private void clearData() {
        idTextField.setText("");
        firstNameTextField.setText("");
        infixTextField.setText("");
        lastNameTextField.setText("");
        fteTextField.setText("");
        salaryScaleCombo.setSelectedIndex(0);
        teamTextField.setText("");
        emailTextField.setText("");
        remarksTextField.setText("");
    }
}
