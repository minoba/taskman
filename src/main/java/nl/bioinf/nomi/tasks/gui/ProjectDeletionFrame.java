/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Project;

/**
 * @author Cellingo
 *
 */
public class ProjectDeletionFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private List<Project> selectedProjects;
	private ArrayList<JCheckBox> checks;

	public ProjectDeletionFrame( TaskManagerController controller ) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 600, 500);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Projecten selectie");
		/*creates the form/content*/
		this.checks = new ArrayList<JCheckBox>();
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "Selecteer projecten voor verwijderen", GuiComponentStylingFactory.TITLE_COMPONENT), c);

		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;
		
		JScrollPane ssp = new JScrollPane( getSelectionPanel() );
		ssp.setPreferredSize( new Dimension( 400, 300) );
		formPanel.add(ssp, c);
			
		c.gridwidth = 1;
		c.gridy++;
		c.gridx = 0;

		JButton delete = new JButton("Verwijder");
		delete.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				createSelectionList();
				controller.deleteProjects( selectedProjects );
				dispose();
			}
		});
		delete.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					createSelectionList();
					controller.deleteProjects( selectedProjects );
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( delete, c );
		
		c.gridx = 2;
		c.gridwidth = 1;
		
		JButton cancel = new JButton("Annuleer");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( cancel, c );
	}
	
	private JPanel getSelectionPanel( ) {
		//selectionPanel.removeAll();
		JPanel content =  GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		content.setLayout( new BoxLayout(content, BoxLayout.Y_AXIS) );
		
		for( Project project : controller.getDataCollection().getProjects() ){
			
			JCheckBox check = new JCheckBox( project.getName() );
			check.setName( project.getName() );
			checks.add(check);
			content.add(check);
		}
		return content;
	}

	
	private void createSelectionList(){
		//System.out.println( "creating selection list " );
		selectedProjects = new ArrayList<Project>();
		for(JCheckBox check : checks ){
			if( check.isSelected() ){
				String id = check.getName();
				Project project = controller.getDataCollection().getProjectById(id);
				if( project != null ) selectedProjects.add(project);
			}
		}
	}
	
}
