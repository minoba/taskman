package nl.bioinf.nomi.tasks.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CourseMethod;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.TimeAssignmentFactors;
import nl.bioinf.nomi.tasks.datamodel.TimeFactor;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;
import nl.bioinf.nomi.tasks.gui.GuiComponentFactory;
import nl.bioinf.nomi.tasks.gui.GuiComponentStylingFactory;

public class CourseMethodsEditingFrame extends JFrame {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				CourseMethodsEditingFrame cmef = new CourseMethodsEditingFrame(null);
				cmef.setVisible(true);
			}
		});
	}

	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private JTextField idTextField;
	private JTextField nameTextField;
	private JTextField groupSizeTextField;
	private JComboBox<String> methodsCombo;
	private ArrayList<String> methodIds;
	private CourseMethod selectedMethod;
	//private CourseMethod newMethod;
	private CourseMethodFactorTable[] courseMethodFactorTables = new CourseMethodFactorTable[3];
	private String[] tableNames = {"uren per docent", "uren per groep", "uren per student"};
	private JButton deleteButton;

	public CourseMethodsEditingFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 850, 550);
		/* set frame icon and title */
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Bewerk en maak nieuwe werkvormen");
		/* creates the form/content */
		createContent();

		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	/**
	 * fetches the course methods and creates the combo
	 */
	private void createCourseMethodsCombo() {
		List<CourseMethod> methods = Curriculum.getCourseMethods();
		
		methodIds = new ArrayList<String>();
		methodIds.add("");
		String[] methodNames = new String[methods.size()+1];//{ "kies", "hoorcollege", "lab practicum", "computer practicum" };
		methodNames[0] = "kies";
		for( int i=0; i<methods.size(); i++ ){
			methodIds.add( methods.get(i).getId() );
			methodNames[i+1] = methods.get(i).getName();
		}
		
		methodsCombo = new JComboBox<String>(methodNames);
		
		//System.out.println("total items: " + methodsCombo.getItemCount() );
		
		methodsCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("item selected (name): "	+ methodsCombo.getSelectedItem());
				//System.out.println("item selected (id): " + methodIds.get(methodsCombo.getSelectedIndex()));
				if(methodsCombo.getSelectedIndex() == 0){
					selectedMethod = null;
					clearData();
				}
				else{//(methodsCombo.getSelectedIndex() > 0) 
					showMethodData(methodIds.get(methodsCombo.getSelectedIndex()));
				}
			}
		});
	}

	/**
	 * shows the data of the selected CourseMethod
	 * 
	 * @param methodId
	 */
	private void showMethodData(String methodId) {
		this.selectedMethod = Curriculum.getCourseMethod(methodId);
		if (selectedMethod != null) {
			idTextField.setText(selectedMethod.getId());
			idTextField.setEnabled(false);
			nameTextField.setText(selectedMethod.getName());
			groupSizeTextField.setText("" + selectedMethod.getGroupSize());
			YearDataCollection ydc = controller.getDataCollection();
			/*is in use and cannot be removed?*/
			if( ydc.isCourseMethodUsed( selectedMethod ) )	deleteButton.setEnabled(false);
			else deleteButton.setEnabled(true);
			
			for(int i=0; i<3; i++){
				courseMethodFactorTables[i].fireTableDataChanged();
			}
			
		}
		else deleteButton.setEnabled(false);

	}

	/**
	 * creates frame content
	 */
	private void createContent() {
		//this.removeAll();
		createCourseMethodsCombo();

		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);

		GridBagLayout gbl = new GridBagLayout();
		// this.setLayout( gbl );
		GridBagConstraints c = new GridBagConstraints();

		JScrollPane sp = new JScrollPane(formPanel);
		this.getContentPane().add(sp);
		formPanel.setLayout(gbl);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 6;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor = GridBagConstraints.FIRST_LINE_START;

		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;

		formPanel.add( GuiComponentFactory.getJLabel( "alle module hebben een werkvorm; deze wordt gebruikt om groepen te vormen en uren te berekenen",
				GuiComponentStylingFactory.FORM_COMPONENT), c);

		c.gridy++;

		formPanel.add(GuiComponentFactory.getJLabel( "de uren-formule is  uren=(A*EC)+(B*EC)+C+(D*studentaantal)",
				GuiComponentStylingFactory.FORM_COMPONENT), c);

		c.gridy++;

		formPanel.add( GuiComponentFactory.getJLabel("selecteer een werkvorm om te bewerken of maak een nieuwe werkvorm aan",
				GuiComponentStylingFactory.FORM_COMPONENT), c);

		c.gridy++;

		formPanel.add(methodsCombo, c);

		
		c.gridwidth = 1;
		c.gridy++;

		formPanel.add(GuiComponentFactory.getJLabel("ID (4 letters)", GuiComponentStylingFactory.FORM_COMPONENT), c);

		c.gridx = 1;
		c.gridwidth = 1;

		idTextField = new JTextField();
		idTextField.setColumns(4);
		formPanel.add(idTextField, c);

		c.gridx = 2;
		
		deleteButton = new JButton("Verwijder werkvorm");
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				deleteSelectedMethod();
			}
		});
		deleteButton.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					deleteSelectedMethod();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		formPanel.add(deleteButton, c);
		deleteButton.setEnabled(false);

		c.gridx = 0;
		c.gridwidth = 1;
		c.gridy++;

		formPanel.add(GuiComponentFactory.getJLabel("naam",	GuiComponentStylingFactory.FORM_COMPONENT), c);

		c.gridx = 1;
		c.gridwidth = 3;

		nameTextField = new JTextField();
		nameTextField.setColumns(20);
		formPanel.add(nameTextField, c);

		c.gridx = 0;
		c.gridwidth = 1;
		c.gridy++;

		formPanel.add(GuiComponentFactory.getJLabel("max groepsgrootte", GuiComponentStylingFactory.FORM_COMPONENT), c);

		c.gridx = 1;
		c.gridwidth = 1;

		groupSizeTextField = new JTextField();
		groupSizeTextField.setColumns(4);
		formPanel.add(groupSizeTextField, c);

		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 2;

		// formPanel.add( GuiComponentFactory.getJLabel(
		// "uren per docent per EC (A, decimaal)",
		// GuiComponentStylingFactory.FORM_COMPONENT ), c );
		formPanel.add(getTimeFactorPanel(TimeFactor.HOURS_PER_TEACHER), c);

		c.gridx = 2;

		formPanel.add(getTimeFactorPanel(TimeFactor.HOURS_PER_GROUP), c);

		c.gridx = 4;

		formPanel.add(getTimeFactorPanel(TimeFactor.HOURS_PER_STUDENT), c);

		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 1;

		JButton ok = new JButton("Bewaar");
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				//controller.updateViews("coursemethodcreation");
				if (dataOk) clearData();
			}
		});
		ok.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					boolean dataOk = verifyAndProcess();
					//controller.updateViews("coursemethodcreation");
					if (dataOk)	clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		formPanel.add(ok, c);

		c.gridx = 2;
		c.gridwidth = 1;

		JButton okNew = new JButton("Wis formulier");
		okNew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clearData();
			}
		});
		okNew.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		formPanel.add(okNew, c);

		c.gridx = 4;
		c.gridwidth = 1;
		// c.gridy++;

		JButton cancel = new JButton("Sluit");
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {}
			@Override
			public void keyTyped(KeyEvent e) {}
		});
		formPanel.add(cancel, c);
	}

	/**
	 * verifies the data and processes them if they are OK
	 * 
	 * @return dataOk
	 */
	private boolean verifyAndProcess() {
		
		String id = idTextField.getText();
		if (selectedMethod == null && Curriculum.getCourseMethod(id) != null) {
			JOptionPane.showMessageDialog(getContentPane(), "er bestaat al een werkvorm met deze id: " + id, "Fout", JOptionPane.WARNING_MESSAGE);
			idTextField.requestFocus();
			return false;
		}

		String name = nameTextField.getText();
		if (name == null || name.equals("")) {
			JOptionPane.showMessageDialog(getContentPane(),	"een werkvorm moet een naam hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			nameTextField.requestFocus();
			return false;
		}

		int maxStudents = 0;
		String maxStudentsStr = groupSizeTextField.getText();
		try {
			maxStudents = Integer.parseInt(maxStudentsStr);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(),	"de maximale groepsgrootte is niet of niet goed ingevuld", "Fout", JOptionPane.WARNING_MESSAGE);
			groupSizeTextField.requestFocus();
			return false;
		}
		if (maxStudents < 1) {
			JOptionPane.showMessageDialog(getContentPane(),	"de maximale groepsgrootte moet minimaal 1 zijn", "Fout", JOptionPane.WARNING_MESSAGE);
			groupSizeTextField.requestFocus();
			return false;
		}

		boolean methodChanged = false;
		TimeAssignmentFactors taf = null;
		if( selectedMethod == null ){
			/*a new method was created*/
			taf = new TimeAssignmentFactors();
			selectedMethod = new CourseMethod(id, name, maxStudents, taf );
			Curriculum.addCourseMethod(selectedMethod);
		}
		else{
			/*an existing method was edited*/
			selectedMethod.setGroupSize(maxStudents);
			selectedMethod.setName(name);
			taf = selectedMethod.getTimeAssignmentFactors();
			taf.clear();
			methodChanged = true;
		}
		
		for( CourseMethodFactorTable cmft : courseMethodFactorTables ){
			List<TimeFactor> timeFactors = cmft.getTimeFactors();
			for( TimeFactor tf : timeFactors ){
				taf.addTimeFactor( tf );
			}
		}
		
		if( methodChanged )	controller.courseMethodChanged(selectedMethod);
		else{
			methodsCombo.addItem(selectedMethod.getName());
			methodIds.add(selectedMethod.getId() );
			this.repaint();
		}
		
		controller.updateViews("coursemethodcreation");
		
		return true;
	}

	/**
	 * clears the form for reuse
	 */
	private void clearData() {
		idTextField.setText("");
		idTextField.setEnabled(true);
		nameTextField.setText("");
		groupSizeTextField.setText("");
		selectedMethod = null;
		methodsCombo.setSelectedIndex(0);
		methodsCombo.requestFocus();
		deleteButton.setEnabled(false);
		for( CourseMethodFactorTable table : courseMethodFactorTables ){
			table.fireTableDataChanged();
		}
	}
	
	/**
	 * deletes the selected method
	 */
	private void deleteSelectedMethod(){
		if(selectedMethod != null){
			YearDataCollection ydc = controller.getDataCollection();
			if( ydc.isCourseMethodUsed( selectedMethod ) ){
				/*should never be reached but security*/
				/*is in use and cannot be removed*/
				JOptionPane.showMessageDialog(this, "werkvorm is in gebruik en kan niet verwijderd worden", "Fout", JOptionPane.ERROR_MESSAGE);
			}
			else{
				/*first process for this view*/
				Curriculum.removeCourseMethod( selectedMethod.getId() );
				
				//System.out.println("removing method " + selectedMethod.getId() + " index=" + methodsCombo.getSelectedIndex() + " items=" + methodsCombo.getItemCount() );
				
				int i = methodsCombo.getSelectedIndex();
				methodIds.remove(i);
				
				//System.out.println(methodIds.toString());
				
				methodsCombo.removeItemAt(i);
				clearData();

				repaint();
//				paintComponents(getGraphics());
			}
			
		}
		
	}

	/**
	 * creates and returns a panel containing the timefactors of a given category
	 * @param category
	 * @return panel
	 */
	private JPanel getTimeFactorPanel(int category) {
		JPanel tfp = new JPanel();
		tfp.setLayout(new BorderLayout());
		GuiComponentStylingFactory.stylizeComponent(tfp, GuiComponentStylingFactory.PANEL_COMPONENT);
		
		tfp.add(GuiComponentFactory.getJLabel(tableNames[category], GuiComponentStylingFactory.FORM_COMPONENT), BorderLayout.NORTH);

		CourseMethodFactorTable cmft = new CourseMethodFactorTable(category);
		courseMethodFactorTables[category] = cmft;
		JTable table = new JTable(cmft);
		table.getColumnModel().getColumn(0).setPreferredWidth(130);
		table.getColumnModel().getColumn(1).setPreferredWidth(40);
		table.getColumnModel().getColumn(2).setPreferredWidth(30);

		JScrollPane sp = new JScrollPane(table);
		GuiComponentStylingFactory.stylizeComponent(sp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
		sp.setPreferredSize( new Dimension(200, 120) );

		tfp.add(sp, BorderLayout.CENTER);
		return tfp;
	}

	/**
	 * models the list of modules for a single quarter of a curriculum variant
	 */
	public class CourseMethodFactorTable extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		private String[] columnNames = { "omschrijving", "uren", "per EC" };
		private int category;
		private int numrows;
		private ArrayList<TimeFactor> timeFactors = new ArrayList<TimeFactor>();

		/**
		 * constructs with a category of time factors 
		 * @param category
		 */
		public CourseMethodFactorTable(int category) {
			this.category = category;
			updateTableData();
		}

		/**
		 * returns the timefactor category of this table
		 * @return category
		 */
		public int getTimeFactorCategory(){
			return category;
		}
		
		/**
		 * returns a list of timefactors contained in this table
		 * @return timeFactors
		 */
		public List<TimeFactor> getTimeFactors(){
			return timeFactors;
		}
		
		/**
		 * updates the table data
		 */
		private void updateTableData(){
			timeFactors.clear();
			if( selectedMethod == null ){
				numrows = 5;
			}
			else{
				//System.out.println( "updating table " + this.category );
				TimeAssignmentFactors taf = selectedMethod.getTimeAssignmentFactors();
				for( TimeFactor tf : taf.getTimeFactors(category) ){
					//System.out.println( "adding time factor to table " + this.category + ": " + tf.toString() );
					timeFactors.add( tf.clone() );
				}
				numrows = ( timeFactors.size() < 5 ? 5 : timeFactors.size() );
				//numrows = timeFactors.size();
			}
		}

		public void fireTableDataChanged() {
			updateTableData();
			super.fireTableDataChanged();
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return numrows;
		}

		@Override
		public Object getValueAt(int r, int c) {
			if (r >= timeFactors.size() ){
				if( c < 2 ) return "";
				else return false;
			}
			else{
				if( c == 0 ) return timeFactors.get(r).getName();
				else if( c == 1 ) return timeFactors.get(r).getHours();
				else if( c == 2 ) return timeFactors.get(r).isEcDependent();
				else return "";
			}
		}

		public String getColumnName(int c) {
			return columnNames[c];
		}

		/* Don't need to implement this method unless your table's editable. */
		public boolean isCellEditable(int row, int col) {
			return true;
		}

		/*
		 * Don't need to implement this method unless your table's data can
		 * change.
		 */
		public void setValueAt(Object value, int row, int col) {
			TimeFactor tf = null;
			if( row < this.timeFactors.size() ){
				/*an existing factor is being edited*/
				tf = timeFactors.get(row);
			}
			else{
				/*a new factor is being created*/
				tf = new TimeFactor();
				tf.setCategory(category);
				timeFactors.add(tf);
			}
			
			if( col == 0 ){
				if(value.toString().length() == 0 ) timeFactors.remove(row);
				else tf.setName( value.toString() ); /*name is entered*/
			}
			else if( col == 1 ){
				try{
					tf.setHours( Double.parseDouble( value.toString() ) );
				}catch (Exception e) {/*ignore*/}
			}
			else if( col == 2 ){
				try{
					tf.setEcDependent( Boolean.parseBoolean( value.toString() ) );
				}catch (Exception e) {/*ignore*/}
			}
			
			/* generate the new view of the table */
			fireTableCellUpdated(row, col);
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int column) {
			if( column < 2 ) return String.class;
			else return Boolean.class;
		}
	}

}
