/**
 *
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CourseMethod;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * @author Cellingo
 *
 */
public class ModuleCreationFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private TaskManagerController controller;
    private JComboBox<String> startQuarterCombo;
    private JComboBox<String> durationCombo;
    private JTextField moduleIdTextField;
    private JTextField moduleNaamTextField;
    private JTextField moduleEcTextField;
    private JComboBox<String> methodsCombo;
    private String[] mIds;

    public ModuleCreationFrame(TaskManagerController controller) {
        this.controller = controller;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(200, 200, 750, 400);
        /*set frame icon and title*/
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.getImage("graphics/tasks_icon.gif");
        setIconImage(img);

        setTitle("Nieuwe module");
        /*creates the form/content*/
        createContent();

        toFront();
        requestFocus();
        setAlwaysOnTop(true);
        //setVisible( true );
    }

    private void createContent() {
        GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
        JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        JScrollPane sp = new JScrollPane(formPanel);
        this.getContentPane().add(sp);
        formPanel.setLayout(gbl);

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 4;
        c.ipadx = 50;
        c.ipady = 50;
        c.anchor = GridBagConstraints.FIRST_LINE_START;

        formPanel.add(GuiComponentFactory.getJLabel("maak een nieuwe module aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);

        c.ipadx = 10;
        c.ipady = 4;
        c.gridwidth = 1;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel(
                "module ID (bv BFVTHM1LAB)", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        moduleIdTextField = new JTextField();
        moduleIdTextField.setColumns(6);
        formPanel.add(moduleIdTextField, c);

        c.gridy++;
        c.gridx = 0;

        formPanel.add(GuiComponentFactory.getJLabel("module naam", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        moduleNaamTextField = new JTextField();
        moduleNaamTextField.setColumns(20);
        formPanel.add(moduleNaamTextField, c);

        c.gridy++;
        c.gridx = 0;

        formPanel.add(GuiComponentFactory.getJLabel(
                "credits (EC) voor de module", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        moduleEcTextField = new JTextField();
        moduleEcTextField.setColumns(6);
        formPanel.add(moduleEcTextField, c);

        c.gridx = 2;

        formPanel.add(GuiComponentFactory.getJLabel(
                "werkvorm voor de module", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 3;

        List<CourseMethod> methods = Curriculum.getCourseMethods();
        String[] mNames = new String[methods.size() + 1];
        mIds = new String[methods.size() + 1];
        mNames[0] = "kies";
        mIds[0] = " ";
        for (int i = 0; i < methods.size(); i++) {
            mNames[i + 1] = methods.get(i).getName();
            mIds[i + 1] = methods.get(i).getId();
        }

        methodsCombo = new JComboBox<String>(mNames);
        formPanel.add(methodsCombo, c);

        c.gridx = 0;
        c.gridy++;

        formPanel.add(GuiComponentFactory.getJLabel("start (kwartaal)", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 1;

        String[] starts = {"kies", "1", "2", "3", "4"};
        startQuarterCombo = new JComboBox<String>(starts);
        formPanel.add(startQuarterCombo, c);

        c.gridx = 2;

        formPanel.add(GuiComponentFactory.getJLabel("duur (kwartalen)", GuiComponentStylingFactory.FORM_COMPONENT), c);

        c.gridx = 3;

        durationCombo = new JComboBox<String>(starts);
        formPanel.add(durationCombo, c);

        c.gridx = 0;
        c.gridy++;

        JButton ok = new JButton("Bewaar");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                boolean dataOk = verifyAndProcess();
                controller.updateViews("modulecreation");
                if (dataOk) {
                    dispose();
                }
            }
        });
        ok.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    boolean dataOk = verifyAndProcess();
                    controller.updateViews("modulecreation");
                    if (dataOk) {
                        dispose();
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        formPanel.add(ok, c);

        c.gridx = 1;
        c.gridwidth = 2;

        JButton okNew = new JButton("Bewaar en maak nog een module");
        okNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                boolean dataOk = verifyAndProcess();
                controller.updateViews("modulecreation");
                if (dataOk) {
                    clearData();
                }
            }
        });
        okNew.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    boolean dataOk = verifyAndProcess();
                    controller.updateViews("modulecreation");
                    if (dataOk) {
                        clearData();
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        formPanel.add(okNew, c);

        c.gridx = 3;
        c.gridwidth = 1;
		//c.gridy++;

        JButton cancel = new JButton("Annuleren");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                dispose();
            }
        });
        cancel.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    dispose();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        formPanel.add(cancel, c);
    }

    /**
     * verifies the data and processes them if they are OK
     *
     * @return dataOk
     */
    private boolean verifyAndProcess() {

        String moduleID = moduleIdTextField.getText();
        CurriculumModule curriculumModule = controller.getDataCollection().getModule(moduleID);
        if (curriculumModule != null) {
            JOptionPane.showMessageDialog(getContentPane(), "een curriculumModule met deze ID bestaat al", "Fout", JOptionPane.WARNING_MESSAGE);
            moduleIdTextField.requestFocus();
            return false;
        }

        String name = moduleNaamTextField.getText();
        if (name == null || name.equals("")) {
            JOptionPane.showMessageDialog(getContentPane(), "curriculumModule moet een naam hebben", "Fout", JOptionPane.WARNING_MESSAGE);
            moduleNaamTextField.requestFocus();
            return false;
        }

        String ecStr = moduleEcTextField.getText();
        double credits = 0;
        try {
            credits = Double.parseDouble(ecStr);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(getContentPane(), "curriculumModule moet een aantal credits hebben", "Fout", JOptionPane.WARNING_MESSAGE);
            moduleEcTextField.requestFocus();
            return false;
        }
        if (credits < 0) {
            JOptionPane.showMessageDialog(getContentPane(), "curriculumModule moet een positief getal als credits toekenning hebben", "Fout", JOptionPane.WARNING_MESSAGE);
            moduleEcTextField.requestFocus();
            return false;
        }

        int mIndex = methodsCombo.getSelectedIndex();
        CourseMethod cm = Curriculum.getCourseMethod(mIds[mIndex]);
        if (cm == null) {
            JOptionPane.showMessageDialog(getContentPane(), "er moet een werkvorm geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
            methodsCombo.requestFocus();
            return false;
        }

        String startQ = startQuarterCombo.getSelectedItem().toString();
        int start = 0;
        try {
            start = Integer.parseInt(startQ);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(getContentPane(), "curriculumModule moet een startkwartaal hebben", "Fout", JOptionPane.WARNING_MESSAGE);
            startQuarterCombo.requestFocus();
            return false;
        }

        String durationStr = durationCombo.getSelectedItem().toString();
        int duration = 0;
        try {
            duration = Integer.parseInt(durationStr);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(getContentPane(), "curriculumModule moet een duur hebben", "Fout", JOptionPane.WARNING_MESSAGE);
            durationCombo.requestFocus();
            return false;
        }
        if (start + duration > 5) {
            JOptionPane.showMessageDialog(getContentPane(), "start en duur van curriculumModule zijn niet verenigbaar (gaan over het jaar heen)", "Fout", JOptionPane.WARNING_MESSAGE);
            durationCombo.requestFocus();
            return false;
        }

        CurriculumModule newCurriculumModule = new CurriculumModule(moduleID, credits, new Timeframe(start, duration), name, cm);
        controller.getDataCollection().addModule(newCurriculumModule);

        return true;
    }

    /**
     * clears the form for reuse
     */
    private void clearData() {
        moduleIdTextField.setText("");
        moduleNaamTextField.setText("");
        moduleEcTextField.setText("");
        methodsCombo.setSelectedIndex(0);
        startQuarterCombo.setSelectedIndex(0);
        durationCombo.setSelectedIndex(0);
    }
}
