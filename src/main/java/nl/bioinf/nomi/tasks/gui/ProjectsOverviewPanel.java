package nl.bioinf.nomi.tasks.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.Project;
import nl.bioinf.nomi.tasks.datamodel.ProjectTask;

/**
 * This class shows the overview of all Projects
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class ProjectsOverviewPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;
    private TaskManagerController controller;
    private List<Project> projects;

    /**
     * construct with the controller
     *
     * @param controller
     */
    public ProjectsOverviewPanel(TaskManagerController controller) {
        this.controller = controller;
        this.projects = controller.getDataCollection().getProjects();
        initialize();
    }
    
    private void initialize() {
        GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);
        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        
        GridBagConstraints c = new GridBagConstraints();
        
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.ipadx = 4;
        c.ipady = 4;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        
        String title = "projecten overzicht van jaar " + controller.getCurrentYear() + " - " + (controller.getCurrentYear() + 1);
        this.add(GuiComponentFactory.getJLabel(title, GuiComponentStylingFactory.TITLE_COMPONENT), c);
        
        c.gridx = 0;
        c.gridy++;
        
        this.add(GuiComponentFactory.getJLabel("overzicht van lopende projecten", GuiComponentStylingFactory.DETAIL_COMPONENT), c);
        
        c.gridy++;
        
        this.add(getProjectsSummaryPane(), c);
        
        c.gridy++;
        c.gridwidth = 1;
        
        JButton delete = new JButton("verwijder projecten");
        delete.setBackground(Color.ORANGE);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProjectDeletionFrame pdf = new ProjectDeletionFrame(controller);
                pdf.setVisible(true);
            }
        });
        delete.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    ProjectDeletionFrame pdf = new ProjectDeletionFrame(controller);
                    pdf.setVisible(true);
                }
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
            }
            
            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        this.add(delete, c);

//		c.gridy++;
        c.gridx++;
        
        JButton export = new JButton("exporteer projecten naar excel");
        export.setBackground(new Color(10, 175, 50));
        export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.doAction(GuiAction.EXPORT_PROJECTS_OVERVIEW);
            }
        });
        export.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    controller.doAction(GuiAction.EXPORT_PROJECTS_OVERVIEW);
                }
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
            }
            
            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
        this.add(export, c);
        
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        
        this.add(GuiComponentFactory.getJLabel("projecttaken per docent; taakuren zijn hier niet te wijzigen", GuiComponentStylingFactory.DETAIL_COMPONENT), c);
        
        c.gridy++;
        
        this.add(getProjectsTasksPane(), c);
        
    }

    /**
     * returns a JTable of all the project tasks
     *
     * @return jTable
     */
    private JScrollPane getProjectsTasksPane() {
        JTable projectTasksTable = new JTable(new ProjectsTasksTable());
        projectTasksTable.setAutoCreateRowSorter(true);
        //projectTasksTable.setMinimumSize( new Dimension( (projects.size() * 100) , 700) );
        TableColumn column = null;
        for (int i = 0; i < projectTasksTable.getColumnCount(); i++) {
            column = projectTasksTable.getColumnModel().getColumn(i);
            if (i == 0) {//4-letter code
                column.setMinWidth(55);//setPreferredWidth(80);//
            } else if (i == 1) {//vrije uren
                column.setMinWidth(40);//setPreferredWidth(60);//
            } else {
                column.setPreferredWidth(60);//setMinWidth(50);//
            }
        }
        projectTasksTable.setColumnSelectionAllowed(true);
        projectTasksTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(projectTasksTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        pane.setPreferredSize(new Dimension(1300, 800));
        return pane;
    }

    /**
     * models the task list of the employee
     */
    private class ProjectsTasksTable extends AbstractTableModel {
        
        private static final long serialVersionUID = 1L;
        private ArrayList<String> columnNames;
        private List<Employee> employees;
        
        public ProjectsTasksTable() {
            columnNames = new ArrayList<String>();
            columnNames.add("Afkorting");
            columnNames.add("Uren over");
            
            for (Project p : projects) {
                columnNames.add(p.getName());
            }
            employees = controller.getDataCollection().getEmployees();
        }
        
        @Override
        public int getColumnCount() {
            return columnNames.size();
        }
        
        @Override
        public int getRowCount() {
            return employees.size();
        }
        
        @Override
        public Object getValueAt(int r, int c) {
            if (c == 0) {
                return employees.get(r).getEmployeeId();
            } else if (c == 1) {
                return employees.get(r).getVacantHours();
            } else {
                /*return the task hours for the employee for a particular project*/
                String hours = (employees.get(r).getProjectTaskHours(projects.get(c - 2).getName()) == 0 ? "" : (employees.get(r).getProjectTaskHours(projects.get(c - 2).getName()) + ""));
                return hours;
            }
        }
        
        public String getColumnName(int c) {
            return columnNames.get(c);
        }

        /*Don't need to implement this method unless your table's editable.*/
//        public boolean isCellEditable(int row, int col) {
//            /*All task hour fields are editable*/
//            if (col > 1) {
//                return true;
//            } else {
//                return false;
//            }
//        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            //System.out.println("changing value at r:c " + row + ":" + col);
            try {
                int taskHours = Integer.parseInt(value.toString());
                boolean update = false;
                Project project = projects.get(col - 2);
                
                List<ProjectTask> ptasks = employees.get(row).getProjectTasks(project.getName());

                //System.out.println( "tasks for project " + project.getName() + ": " + ptasks.size() );
                if (ptasks.size() == 0 && taskHours != 0) {
                    /*a new task was defined*/
                    ProjectTask pt = new ProjectTask(project.getNewTaskId(), taskHours, project);

                    /*register the new task in both parent objects*/
                    pt.setEmployee(employees.get(row));
                    pt.setName(project.getName() + " task ");
                    employees.get(row).addProjectTask(pt);
                    project.addTask(pt);
                    update = true;
                } else if (ptasks.size() == 1) {
                    /*there is one single task for this employee and it is changed*/
                    ProjectTask pt = ptasks.get(0);
                    if (taskHours == 0) {
                        /*remove the task*/
                        employees.get(row).removeProjectTask(pt);
                        project.removeTask(pt);
                        update = true;
                    } else {
                        /*change the task*/
                        pt.setTaskHours(taskHours);
                        update = true;
                    }
                } else {
                    /*there are more than one tasks for this employee in the project*/
                    if (taskHours == 0) {
                        /*remove all tasks*/
                        for (ProjectTask ptk : ptasks) {
                            /*remove the task*/
                            employees.get(row).removeProjectTask(ptk);
                            project.removeTask(ptk);
                        }
                        update = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "deze medewerker heeft meer dan 1 taak voor het project " + project.getName()
                                + "\nZe kunnen niet veranderd worden maar alleen volledig verwijderd.", "waarschuwing", JOptionPane.WARNING_MESSAGE);
                    }
                }
                if (update) {
                    fireTableCellUpdated(row, col);
                    controller.updateViews("projectsoverview");
                }
            } catch (Exception e) {
                /*do nothing*/
                e.printStackTrace();
            }
        }
    }

    /**
     * returns a JTable of the projects overview
     *
     * @return jTable
     */
    private JScrollPane getProjectsSummaryPane() {
        JTable projectsSummaryTable = new JTable(new ProjectsSummaryTable());
        projectsSummaryTable.setAutoCreateRowSorter(true);
        TableColumn column = null;
        for (int i = 0; i < projectsSummaryTable.getColumnCount(); i++) {
            column = projectsSummaryTable.getColumnModel().getColumn(i);
            if (i == 0) {//naam
                column.setPreferredWidth(120);
            } else if (i == 1) {//omschrijving
                column.setPreferredWidth(250);
            } else {
                column.setPreferredWidth(80);
            }
        }
        projectsSummaryTable.setColumnSelectionAllowed(true);
        projectsSummaryTable.setRowSelectionAllowed(true);
        JScrollPane pane = new JScrollPane(projectsSummaryTable);
        GuiComponentStylingFactory.stylizeComponent(pane.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
        pane.setPreferredSize(new Dimension(900, 300));
        return pane;
    }

    /**
     * models the task list of the employee
     */
    private class ProjectsSummaryTable extends AbstractTableModel {
        
        private static final long serialVersionUID = 1L;
        private String[] columnNames = {"Project", "Omschrijving", "Uren beschikbaar", "Uren uitgezet", "Uren over", "Kosten (geschat)"};
        
        public ProjectsSummaryTable() {
        }
        
        @Override
        public int getColumnCount() {
            return columnNames.length;
        }
        
        @Override
        public int getRowCount() {
            return projects.size() + 1;
        }
        
        @Override
        public Object getValueAt(int r, int c) {
            if (r == projects.size()) {
                switch (c) {
                    case 0:
                        return "TOTAAL";
                    case 1:
                        return controller.getDataCollection().getProjects().size();
                    case 2: {
                        int totalHours = 0;
                        for (int i = 0; i < projects.size(); i++) {
                            totalHours += projects.get(i).getTotalHours();
                        }
                        return totalHours + "";
                    }
                    case 3: {
                        int totalHours = 0;
                        for (int i = 0; i < projects.size(); i++) {
                            totalHours += projects.get(i).getAssignedHours();
                        }
                        return totalHours + "";
                    }
                    case 4: {
                        int totalHours = 0;
                        for (int i = 0; i < projects.size(); i++) {
                            totalHours += projects.get(i).getAssignableHours();
                        }
                        return totalHours + "";
                    }
                    case 5: {
                        double totalEuros = 0;
                        for (int i = 0; i < projects.size(); i++) {
                            totalEuros += projects.get(i).getAssignedHoursCost();
                        }
                        return (int) totalEuros + "";
                    }
                    default:
                        return "";
                }
            } else {
                switch (c) {
                    case 0:
                        return projects.get(r).getName();
                    case 1:
                        return projects.get(r).getDescription();
                    case 2:
                        return projects.get(r).getTotalHours();
                    case 3:
                        return projects.get(r).getAssignedHours();
                    case 4:
                        return projects.get(r).getAssignableHours();
                    case 5:
                        return (int) projects.get(r).getAssignedHoursCost();
                    default:
                        return "";
                }
            }
        }
        
        public String getColumnName(int c) {
            return columnNames[c];
        }

        /*Don't need to implement this method unless your table's editable.*/
//        public boolean isCellEditable(int row, int col) {
//            //Omschrijving en totaal uren zijn editable
//            if (col == 1 || col == 2) {
//                return true;
//            } else {
//                return false;
//            }
//        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            //System.out.println("changing value at r:c " + row + ":" + col);

            if (col == 1) {
                /*description was changed*/
                projects.get(row).setDescription(value.toString());
                fireTableCellUpdated(row, col);
            }
            if (col == 2) {
                /*total hours was changed*/
                int newHours = Integer.parseInt(value.toString());
                projects.get(row).setTotalHours(newHours);
                fireTableCellUpdated(row, 2);
                fireTableCellUpdated(row, 4);
            }
        }
    }
    
}
