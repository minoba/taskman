/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;

/**
 * @author Cellingo
 *
 */
public class DataCollectionCreationFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;

	private JTextField schoolTextField;
	private JComboBox<String> yearCombo;
	private JTextField plannerTextField;
	private JTextField fteSizeTextField;
	private ArrayList<JTextField> scaleTextFields;

	public DataCollectionCreationFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 750, 550);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Nieuwe dataset");
		/*creates the form/content*/
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);
		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		//this.setLayout( gbl );
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "maak een nieuwe dataset aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);
		
		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;
		
		formPanel.add(GuiComponentFactory.getJLabel( "een dataset is de verzameling van alle opleidingen, medewerkers, modules etc. van een planning", GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		c.gridwidth = 2;
		c.gridy++;
		c.gridx = 0;

		formPanel.add( GuiComponentFactory.getJLabel( "school (bv ILST)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridx = 2;
		c.gridwidth = 1;

		schoolTextField = new JTextField();
		schoolTextField.setColumns(6);
		formPanel.add(schoolTextField, c);

		c.gridwidth = 2;
		c.gridy++;
		c.gridx = 0;
		
		formPanel.add( GuiComponentFactory.getJLabel( "start schooljaar", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridwidth = 1;
		c.gridx = 2;

		Calendar cal = Calendar.getInstance();
		int year = cal.get( Calendar.YEAR );
		
		String[] years = new String[10];
		for( int i=0; i<10; i++ ){
			years[i] = ""+((year - 5)+i);
		}
		yearCombo = new JComboBox<String>( years );
		yearCombo.setSelectedIndex(5);
		formPanel.add( yearCombo, c );
		
		c.gridwidth = 2;
		c.gridy++;
		c.gridx = 0;

		formPanel.add( GuiComponentFactory.getJLabel( "planner (bv PLGR)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridwidth = 1;
		c.gridx = 2;

		plannerTextField = new JTextField();
		plannerTextField.setColumns(6);
		formPanel.add(plannerTextField, c);

		c.gridwidth = 2;
		c.gridy++;
		c.gridx = 0;

		formPanel.add( GuiComponentFactory.getJLabel( "grootte van 1 fte in uren (bv 1659)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridwidth = 1;
		c.gridx = 2;

		fteSizeTextField = new JTextField();
		fteSizeTextField.setColumns(6);
		fteSizeTextField.setText("1659");
		formPanel.add(fteSizeTextField, c);

		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 4;
		
		formPanel.add( GuiComponentFactory.getJLabel( "geef gemiddelde personeelslast per salarisschaal (laat leeg indien niet relevant)", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridwidth = 1;
		c.gridy++;
		c.ipadx = 1;
		c.ipady = 1;

		scaleTextFields = new ArrayList<JTextField>();
		for( int scale=5; scale<15; scale++){
			c.gridx = 0;
			formPanel.add( GuiComponentFactory.getJLabel( ("schaal " + scale), GuiComponentStylingFactory.FORM_COMPONENT ), c );
			c.gridx = 1;
			JTextField scaleField = new JTextField();
			scaleField.setColumns(4);
			scaleField.setName(""+scale);
			scaleTextFields.add(scaleField);
			formPanel.add(scaleField, c);
			c.gridy++;
			
		}
		
		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 2;		
		
		JButton ok = new JButton("Bewaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				
				if( dataOk ) dispose();
			}
		});
		formPanel.add( ok, c );
		
		c.gridx = 2;

		JButton cancel = new JButton("Annuleren");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		formPanel.add( cancel, c );
	}
	
	/**
	 * verifies the data and processes them if they are OK
	 * @return dataOk
	 */
	private boolean verifyAndProcess(){
		
		
		String yearStr = yearCombo.getSelectedItem().toString();
		int year = 0;
		if( yearStr == null || yearStr.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een jaar geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
			yearCombo.requestFocus();
			return false;
		}
		try{
			year = Integer.parseInt(yearStr);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "er moet een jaar geselecteerd worden", "Fout", JOptionPane.WARNING_MESSAGE);
			yearCombo.requestFocus();
			return false;
		}

		String schoolStr = schoolTextField.getText();
		if( schoolStr == null || schoolStr.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een school/instituut gegeven worden", "Fout", JOptionPane.WARNING_MESSAGE);
			schoolTextField.requestFocus();
			return false;
		}
		
		String plannerId = plannerTextField.getText();
		if( plannerId == null || plannerId.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een planner ID gegeven worden", "Fout", JOptionPane.WARNING_MESSAGE);
			plannerTextField.requestFocus();
			return false;
		}
		
		String fteSizeStr = fteSizeTextField.getText();
		int fteSize = 0;
		if( fteSizeStr == null || fteSizeStr.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "er moet een fte grootte (in uren) gegeven worden", "Fout", JOptionPane.WARNING_MESSAGE);
			fteSizeTextField.requestFocus();
			return false;
		}
		try{
			fteSize = Integer.parseInt(fteSizeStr);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "er moet een fte grootte (in hele uren) gegeven worden", "Fout", JOptionPane.WARNING_MESSAGE);
			fteSizeTextField.requestFocus();
			return false;
		}
		
		for( JTextField scaleTextField : scaleTextFields ){
			String scaleStr = scaleTextField.getName();
			String scaleSalaryStr = scaleTextField.getText();
			if( scaleSalaryStr != null && !scaleSalaryStr.equals("") ){
				int scale = 0;
				double scaleSalary = 0;
				//System.out.println( "scale=" + scaleStr + " rate=" + scaleSalaryStr );
				try{
					scale = Integer.parseInt( scaleStr );
					scaleSalary = Double.parseDouble( scaleTextField.getText() );
					Employee.addSalaryLevel(scale, scaleSalary);
				}catch (Exception e) {
					JOptionPane.showMessageDialog(getContentPane(), "er is een fout salarisniveau gegeven", "Fout", JOptionPane.WARNING_MESSAGE);
					scaleTextField.requestFocus();
					return false;
				}
			}
		}
		
		/*create and register both ways*/
		YearDataCollection ydc = new YearDataCollection();
		ydc.setPlanner(plannerId);
		ydc.setSchool(schoolStr);
		ydc.setSchoolYear(year);
		Employee.setFteSize(fteSize);
		/*create vacancy employee*/
		Employee vac = new Employee( "_VAC", 5);
		vac.setFamilyName("@Vacancy");
		vac.setFunctionScale(11);
		vac.setRemarks("virtual employee representing vacancy");
		vac.setTeam(schoolStr);
		Employee.setVacantEmployee( vac );
		ydc.addEmployee(vac);
		controller.addDataCollection(ydc);
		controller.showDataCollection(year);
		return true;
	}
	
}
