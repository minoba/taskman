/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * @author Cellingo
 *
 */
public class CurriculumCreationFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private TaskManagerController controller;
	private JTextField curriculumIdTextField;
	private JTextField curriculumNameTextField;
	private JTextField curriculumDescriptionTextField;
	private JComboBox<String> internshipStartYearCombo;
	private JComboBox<String> internshipStartQuarterCombo;
	private JComboBox<String> internshipDurationCombo;
	private JComboBox<String> graduationStartQuarterCombo;
	private JComboBox<String> graduationDurationCombo;
	private JComboBox<String> graduationStartYearCombo;

	public CurriculumCreationFrame(TaskManagerController controller) {
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 200, 750, 400);
		/*set frame icon and title*/
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.getImage("graphics/tasks_icon.gif");
		setIconImage(img);

		setTitle("Nieuwe opleiding");
		/*creates the form/content*/
		createContent();
		
		toFront();
		requestFocus();
		setAlwaysOnTop(true);
		//setVisible( true );
	}

	private void createContent(){
		GuiComponentStylingFactory.stylizeComponent(this.getContentPane(), GuiComponentStylingFactory.PANEL_COMPONENT);

		JPanel formPanel = GuiComponentFactory.getJPanel(GuiComponentStylingFactory.PANEL_COMPONENT);
		
		GridBagLayout gbl = new GridBagLayout();
		//this.setLayout( gbl );
		GridBagConstraints c = new GridBagConstraints();
		
		JScrollPane sp = new JScrollPane( formPanel );
		this.getContentPane().add( sp );
		formPanel.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 6;
		c.ipadx = 50;
		c.ipady = 50;
		c.anchor= GridBagConstraints.FIRST_LINE_START;

		formPanel.add(GuiComponentFactory.getJLabel( "maak een nieuwe opleiding aan", GuiComponentStylingFactory.TITLE_COMPONENT), c);
		
		c.ipadx = 10;
		c.ipady = 4;
		c.gridy++;
		
		formPanel.add(GuiComponentFactory.getJLabel( "een opleiding bestaat uit 1 of meerdere curricula die elk 1 of meerdere jaren omvatten", GuiComponentStylingFactory.DETAIL_COMPONENT), c);

		c.gridwidth = 1;
		c.gridy++;

		formPanel.add( GuiComponentFactory.getJLabel( "opleiding ID (bv BFV)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;

		curriculumIdTextField = new JTextField();
		curriculumIdTextField.setColumns(4);
		formPanel.add(curriculumIdTextField, c);
		
		c.gridy++;
		c.gridx = 0;

		formPanel.add( GuiComponentFactory.getJLabel( "opleidingsnaam", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridwidth = 2;
		c.gridx = 1;

		curriculumNameTextField = new JTextField();
		curriculumNameTextField.setColumns(12);
		formPanel.add(curriculumNameTextField, c);
		
		c.gridwidth = 2;
		c.gridy++;
		c.gridx = 0;

		formPanel.add( GuiComponentFactory.getJLabel( "omschrijving van de opleiding", GuiComponentStylingFactory.FORM_COMPONENT ), c );

		c.gridy++;
		c.gridwidth = 6;

		curriculumDescriptionTextField = new JTextField();
		curriculumDescriptionTextField.setColumns(50);
		formPanel.add(curriculumDescriptionTextField, c);

		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy++;

		formPanel.add( GuiComponentFactory.getJLabel( "start van de stage (jaar)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;
		
		String[] startDurations = { "kies", "1", "2", "3", "4" };
		internshipStartYearCombo = new JComboBox<String>( startDurations );
		formPanel.add( internshipStartYearCombo, c );
		
		c.gridx = 2;
		
		formPanel.add( GuiComponentFactory.getJLabel( "start van de stage (kwartaal)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 3;
		
		internshipStartQuarterCombo = new JComboBox<String>( startDurations );
		formPanel.add( internshipStartQuarterCombo, c );
		
		c.gridx = 4;
		
		formPanel.add( GuiComponentFactory.getJLabel( "duur van de stage (kwartalen)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 5;
		
		internshipDurationCombo = new JComboBox<String>( startDurations );
		formPanel.add( internshipDurationCombo, c );
		
		c.gridx = 0;
		c.gridy++;
		
		formPanel.add( GuiComponentFactory.getJLabel( "start van het afstuderen (jaar)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 1;
		
		graduationStartYearCombo = new JComboBox<String>( startDurations );
		formPanel.add( graduationStartYearCombo, c );
		
		c.gridx = 2;
		
		formPanel.add( GuiComponentFactory.getJLabel( "start van het afstuderen (kwartaal)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 3;
		
		graduationStartQuarterCombo = new JComboBox<String>( startDurations );
		formPanel.add( graduationStartQuarterCombo, c );
		
		c.gridx = 4;
		
		formPanel.add( GuiComponentFactory.getJLabel( "duur van het afstuderen (kwartalen)", GuiComponentStylingFactory.FORM_COMPONENT ), c );
		
		c.gridx = 5;
		
		graduationDurationCombo = new JComboBox<String>( startDurations );
		formPanel.add( graduationDurationCombo, c );
		
		
		c.gridx = 0;
		c.gridy++;

		JButton ok = new JButton("Bewaar");
		ok.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("curriculumcreation");
				if( dataOk ) dispose();
			}
		});
		ok.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("curriculumcreation");
					if( dataOk ) dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( ok, c );
		
		c.gridx = 1;
		c.gridwidth = 3;

		JButton okNew = new JButton("Bewaar en maak nog een opleiding");
		okNew.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean dataOk = verifyAndProcess();
				controller.updateViews("curriculumcreation");
				if( dataOk ) clearData();
			}
		});
		okNew.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					boolean dataOk = verifyAndProcess();
					controller.updateViews("curriculumcreation");
					if( dataOk ) clearData();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( okNew, c );
		
		c.gridx = 4;
		c.gridwidth = 1;
		
		JButton cancel = new JButton("Annuleren");
		cancel.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		cancel.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
					dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		formPanel.add( cancel, c );
	}
	
	/**
	 * verifies the data and processes them if they are OK
	 * @return dataOk
	 */
	private boolean verifyAndProcess(){
		
		String curId = curriculumIdTextField.getText();
		Curriculum curriculum = controller.getDataCollection().getCurriculum(curId);
		if( curriculum != null ){
			JOptionPane.showMessageDialog(getContentPane(), "een curriculum met deze ID bestaat al", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumIdTextField.requestFocus();
			return false;
		}

		String name = curriculumNameTextField.getText();
		if( name == null || name.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "curriculum moet een naam hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumNameTextField.requestFocus();
			return false;
		}
		
		String curDescr = curriculumDescriptionTextField.getText();
		if( curDescr == null || curDescr.equals("") ){
			JOptionPane.showMessageDialog(getContentPane(), "curriculum moet een omschrijving hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			curriculumDescriptionTextField.requestFocus();
			return false;
		}


		
		String istartY = internshipStartYearCombo.getSelectedItem().toString();
		int istartYear = 0;
		try{
			istartYear = Integer.parseInt(istartY);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "stage moet een startjaar hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			internshipStartYearCombo.requestFocus();
			return false;
		}

		String istartQ = internshipStartQuarterCombo.getSelectedItem().toString();
		int istartQuarter = 0;
		try{
			istartQuarter = Integer.parseInt(istartQ);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "stage moet een startkwartaal hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			internshipStartQuarterCombo.requestFocus();
			return false;
		}

		String idurationStr = internshipDurationCombo.getSelectedItem().toString();
		int iduration = 0;
		try{
			iduration = Integer.parseInt(idurationStr);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "stage moet een duur hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			internshipDurationCombo.requestFocus();
			return false;
		}
		if( istartQuarter + iduration > 5 ){
			JOptionPane.showMessageDialog(getContentPane(), "start en duur van stage zijn niet verenigbaar (gaan over het jaar heen)", "Fout", JOptionPane.WARNING_MESSAGE);
			internshipDurationCombo.requestFocus();
			return false;
		}

		
		
		String gstartY = graduationStartYearCombo.getSelectedItem().toString();
		int gstartYear = 0;
		try{
			gstartYear = Integer.parseInt(gstartY);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "afstuderen moet een startjaar hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			graduationStartYearCombo.requestFocus();
			return false;
		}

		String gstartQ = graduationStartQuarterCombo.getSelectedItem().toString();
		int gstartQuarter = 0;
		try{
			gstartQuarter = Integer.parseInt(gstartQ);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "afstuderen moet een startkwartaal hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			graduationStartQuarterCombo.requestFocus();
			return false;
		}

		String gdurationStr = graduationDurationCombo.getSelectedItem().toString();
		int gduration = 0;
		try{
			gduration = Integer.parseInt(gdurationStr);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(getContentPane(), "afstuderen moet een duur hebben", "Fout", JOptionPane.WARNING_MESSAGE);
			graduationDurationCombo.requestFocus();
			return false;
		}
		if( gstartQuarter + gduration > 5 ){
			JOptionPane.showMessageDialog(getContentPane(), "start en duur van afstuderen zijn niet verenigbaar (gaan over het jaar heen)", "Fout", JOptionPane.WARNING_MESSAGE);
			graduationDurationCombo.requestFocus();
			return false;
		}

		Curriculum newCurriculum = new Curriculum(curId, name);
		newCurriculum.setDescription(curDescr);
		newCurriculum.setInternship( new Timeframe(istartYear, istartQuarter, iduration) );
		newCurriculum.setGraduation( new Timeframe(gstartYear, gstartQuarter, gduration) );
		controller.getDataCollection().addCurriculum( newCurriculum );
		
		return true;
	}
	
	/**
	 * clears the form for reuse
	 */
	private void clearData(){
		curriculumIdTextField.setText("");
		curriculumNameTextField.setText("");
		curriculumDescriptionTextField.setText("");
		internshipStartYearCombo.setSelectedIndex(0);
		internshipStartQuarterCombo.setSelectedIndex(0);
		internshipDurationCombo.setSelectedIndex(0);
		graduationStartYearCombo.setSelectedIndex(0);
		graduationStartQuarterCombo.setSelectedIndex(0);
		graduationDurationCombo.setSelectedIndex(0);
	}
}
