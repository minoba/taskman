/**
 * 
 */
package nl.bioinf.nomi.tasks.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class CurriculumYearDetailPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private CurriculumYear curriculumYear;
	private int schoolYear;
	private TaskManagerController controller;
	private JComboBox<String> actionSelectionComboBox;
	private CurriculumQuarterEditorTable[] tables;
	
	public CurriculumYearDetailPanel( TaskManagerController taskManagerController, CurriculumYear curriculumYear, int schoolYear ){
		this.controller = taskManagerController;
		this.curriculumYear = curriculumYear;
		this.schoolYear = schoolYear;
		initialize();
	}
	
	private void initialize(){
		GuiComponentStylingFactory.stylizeComponent(this, GuiComponentStylingFactory.PANEL_COMPONENT);

		/*create action selection combo boxes*/
		String[] actions = {"annuleer", "verwijder"};
    	this.actionSelectionComboBox = new JComboBox<String>(actions);
    	tables = new CurriculumQuarterEditorTable[4];
    	
		/*laying out the panel*/
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout( gbl );

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 3;
		//c.weightx = 1;
		c.ipadx = 4;
		c.ipady = 4;
		c.anchor= GridBagConstraints.FIRST_LINE_START;
		
		
		String title = curriculumYear.getCurriculumVariant().getName() + " jaar " + curriculumYear.getYear(); 
		JLabel titleLabel = new JLabel( title + "          " + this.schoolYear + "-" + (this.schoolYear+1) );
		GuiComponentStylingFactory.stylizeComponent(titleLabel, GuiComponentStylingFactory.TITLE_COMPONENT);
		this.add( titleLabel, c );

		c.gridwidth = 1;
		c.gridx = 2;
		
		JButton delete = new JButton("verwijder klas uit curriculum");
        delete.setBackground(Color.ORANGE);
		delete.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
    			int n = JOptionPane.showConfirmDialog(null, "Weet u zeker dat u deze klas wilt verwijderen?", "Bevestig verwijderen", JOptionPane.OK_CANCEL_OPTION);
    			if( n == JOptionPane.OK_OPTION ) controller.deleteCurriculumYear( curriculumYear, schoolYear );
			}
		});
		delete.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
				if( e.getKeyCode() == 10 ){
	    			int n = JOptionPane.showConfirmDialog(null, "Weet u zeker dat u deze klas wilt verwijderen?", "Bevestig verwijderen", JOptionPane.OK_CANCEL_OPTION);
	    			if( n == JOptionPane.OK_OPTION ) controller.deleteCurriculumYear( curriculumYear, schoolYear );
				}
			}
			@Override
			public void keyReleased(KeyEvent e) { }
			@Override
			public void keyTyped(KeyEvent e) { }
		});
		this.add( delete, c );

		
		c.gridx = 0;
		c.gridy++;
		c.gridy++;

		/*fetching the quarter and half-year timeframes*/
		List<Timeframe> timeFrames = curriculumYear.getTimeframes();
		for( Timeframe tf : timeFrames ){
			//System.out.println( tf );

			if( tf.getDuration() == 1 ){
				c.gridwidth = 1;
				c.gridy--;
				this.add( getQuarterLabel( tf ), c );
				c.gridy++;
				//this.add( getQuarterLabel( tf ), c );
				this.add( getQuarterEditorTablePane( tf ) , c);
				c.gridx++;
			}
			else if( tf.getDuration() == 2 ){
				c.gridwidth = 2;
				c.gridy--;
				this.add( getQuarterLabel( tf ), c );
				c.gridy++;
				//this.add( getQuarterLabel( tf ), c );
				this.add( getQuarterEditorTablePane( tf ) , c);
				c.gridx += 2;
			}
		}
	}
	
	
	/**
	 * creates and styles a JLable for a timeframe table
	 * @param quarter
	 * @return
	 */
	private JLabel getQuarterLabel( Timeframe tf ){
		String t = "kwartaal ";
		if( tf.getDuration() == 1 ) t += tf.getOverallQuarterNumber();
		else t += (tf.getOverallQuarterNumber() + "-" + (tf.getOverallQuarterNumber() + tf.getDuration() - 1) );
		JLabel label = new JLabel( t );
		GuiComponentStylingFactory.stylizeComponent(label, GuiComponentStylingFactory.DETAIL_COMPONENT);
		return label;
	}
	
	/**
	 * returns a JTable of the requested quarter for the given curriculum
	 * @param c the curriculum
	 * @param year
	 * @param quarter
	 * @return jTable
	 */
	private JPanel getQuarterEditorTablePane( Timeframe timeframe ){
		CurriculumQuarterEditorTable cqet = new CurriculumQuarterEditorTable(  timeframe );
		tables[ timeframe.getQuarter()-1 ] = cqet;
		JTable table = new JTable( cqet );
		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(0).setCellEditor( new DefaultCellEditor( actionSelectionComboBox) );
//		table.getColumnModel().getColumn(0).setCellEditor( new ModuleSelectionCellEditor() );
		
		JPanel pane = GuiComponentFactory.getJPanel( GuiComponentStylingFactory.PANEL_COMPONENT );
		
    	JScrollPane sp = new JScrollPane( table );
    	GuiComponentStylingFactory.stylizeComponent(sp.getComponent(0), GuiComponentStylingFactory.PANEL_COMPONENT);
    	
    	pane.setLayout( new BorderLayout() );
    	pane.add(sp, BorderLayout.CENTER);
    	if( timeframe.getDuration() == 1) pane.setPreferredSize( new Dimension(200,160) );
    	else if( timeframe.getDuration() == 2) pane.setPreferredSize( new Dimension(400,160) );
    	
		JButton addModulesButton = new JButton( "voeg curriculumModules toe" );
		addModulesButton.addActionListener( new TableChangeActionListener( timeframe, cqet ) );
    	pane.add(addModulesButton, BorderLayout.SOUTH);
    	return pane;
	}
	
	/**
	 * ActionListener that listens to module selections in ModleSelectionFrame
	 * @author Cellingo
	 */
	public class TableChangeActionListener implements ActionListener{
		private CurriculumQuarterEditorTable table;
		private Timeframe timeframe;
		
		public TableChangeActionListener( Timeframe timeframe, CurriculumQuarterEditorTable table ){
			this.timeframe = timeframe;
			this.table = table;
		}
		
		public void modulesSelected( List<CurriculumModule> selectedCurriculumModules){
			/*process the selected curriculumModules*/
			for( CurriculumModule curriculumModule : selectedCurriculumModules){
				/*register both ways*/
				curriculumYear.addModule(timeframe.getQuarter(), curriculumModule);
				curriculumModule.addAttendingClass(curriculumYear);
				/*process necessary user actions*/
				controller.moduleAttendanceChanged(curriculumModule);
			}
    		controller.updateViews("curriculumyearoverview");
    		table.fireTableDataChanged();
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ModuleSelectionFrame msf = new ModuleSelectionFrame( controller, timeframe, this );
			msf.setVisible( true );
		}
	}
	
	/**
	 * models the list of curriculumModules for a single quarter of a curriculum variant
	 */
	public class CurriculumQuarterEditorTable extends AbstractTableModel{
		private static final long serialVersionUID = 1L;
		private String[] columnNames = { "CurriculumModule" };
		private List<CurriculumModule> curriculumModules;
		private Timeframe timeframe;
		
		public CurriculumQuarterEditorTable( Timeframe timeframe ){
			this.timeframe = timeframe;
			curriculumModules = curriculumYear.getModules(timeframe.getQuarter());
		}
		
		public void fireTableDataChanged(){
			curriculumModules = curriculumYear.getModules(timeframe.getQuarter());
		}
		
		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return curriculumModules.size()+1;
		}

		@Override
		public Object getValueAt(int r, int c) {
			if( r == getRowCount()-1 ) return "";
			else return (curriculumModules.get(r).getName() + " (" + curriculumModules.get(r).getModuleId() + ")" ) ;
		}
		
		public String getColumnName( int c ){
			return columnNames[c];
		}
		
        /*Don't need to implement this method unless your table's editable.*/
        public boolean isCellEditable(int row, int col) {
        	return true;
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
        	//"kies actie", "voeg toe", "verwijder"
        	String action = value.toString();
        	if( action.equals("verwijder") ){
            	//System.out.println( "removing module from quarter " + timeframe.getQuarter() );
            	if(row < curriculumModules.size()){
            		CurriculumModule remove = curriculumModules.get(row);
            		curriculumModules.remove(row);
            		curriculumYear.removeModule(timeframe.getQuarter(), remove);
            		controller.updateViews("curriculumyearoverview");
            	}
        	}
        	/*generate the new view of the table*/
            fireTableCellUpdated(row, col);
        }
	}
}
