package nl.bioinf.nomi.tasks.configuration;

import java.io.InputStream;
import java.text.DateFormat;

import org.apache.commons.configuration.XMLConfiguration;

public class TaskManConfig  {//extends XMLConfiguration

	public static XMLConfiguration configuration;
	
	public static final String USER_PROPERTIES_FILE = ".taskman.properties";
	public static final String USER_PROPERTY_LAST_FILE_LOCATION = "last_file_location";

	public static final String MAIN_TITLE = "main.title";
	public static final String TABS_ZERO_TITLE = "tabs.zero.title";
	public static final String TABS_ZERO_POPUP = "tabs.zero.popup";
	public static final String TABS_ONE_TITLE = "tabs.one.title";
	public static final String TABS_ONE_POPUP = "tabs.one.popup";
	public static final String TABS_TWO_TITLE = "tabs.two.title";
	public static final String TABS_TWO_POPUP = "tabs.two.popup";
	public static final String TABS_THREE_TITLE = "tabs.three.title";
	public static final String TABS_THREE_POPUP = "tabs.three.popup";
	public static final String TABS_FOUR_TITLE = "tabs.four.title";
	public static final String TABS_FOUR_POPUP = "tabs.four.popup";
	public static final String TABS_FIVE_TITLE = "tabs.five.title";
	public static final String TABS_FIVE_POPUP = "tabs.five.popup";
	public static final String TABS_SIX_TITLE = "tabs.six.title";
	public static final String TABS_SIX_POPUP = "tabs.six.popup";
	public static final String TABS_SEVEN_TITLE = "tabs.seven.title";
	public static final String TABS_SEVEN_POPUP = "tabs.seven.popup";
	public static final String TABS_EIGHT_TITLE = "tabs.eight.title";
	public static final String TABS_EIGHT_POPUP = "tabs.eight.popup";
	
	public static final String MENU_FILE = "menu.file";
	public static final String MENU_FILE_OPEN = "menu.file.open";
	public static final String MENU_FILE_SHOW = "menu.file.show";
	public static final String MENU_FILE_NEW = "menu.file.new";
	public static final String MENU_FILE_SAVE = "menu.file.save";
	public static final String MENU_FILE_SAVE_AS = "menu.file.save_as";
	public static final String MENU_FILE_EXIT = "menu.file.exit";
	
	public static final String MENU_EDIT = "menu.edit";
	public static final String MENU_EDIT_METADATA = "menu.edit.metadata";
	public static final String MENU_EDIT_COURSE_METHODS = "menu.edit.course_methods";
	public static final String MENU_EDIT_CURRICULUM_TYPES = "menu.edit.curriculum_types";
	
	public static final String MENU_ADD = "menu.add";
	public static final String MENU_ADD_EMPLOYEE = "menu.add.employee";
	public static final String MENU_ADD_REGULAR_TASK = "menu.add.regular_task";
	public static final String MENU_ADD_PROJECT = "menu.add.project";
	public static final String MENU_ADD_PROJECT_TASK = "menu.add.project_task";
	public static final String MENU_ADD_COURSE_METHOD = "menu.add.course_method";
	public static final String MENU_ADD_MODULE = "menu.add.module";
	public static final String MENU_ADD_EDUCATION = "menu.add.education";
	public static final String MENU_ADD_CURRICULUM = "menu.add.curriculum";
	public static final String MENU_ADD_CLASS = "menu.add.class";
	
	public static final String MENU_EXPORT = "menu.export";
	public static final String MENU_EXPORT_TASKLISTS = "menu.export.task_lists";
	public static final String MENU_EXPORT_ALL_TASKS = "menu.export.all_tasks";
	public static final String MENU_EXPORT_CURRICULUM_OVERVIEW = "menu.export.curriculum_overview";
	public static final String MENU_EXPORT_INTERNSHIP_OCCUPATION = "menu.export.internship_occupation";
	public static final String MENU_EXPORT_PROJECT_OVERVIEW = "menu.export.project_overview";
	public static final String MENU_EXPORT_PLANNING_RULES = "menu.export.planning_rules";

	public static final String MENU_HELP = "menu.help";
	public static final String MENU_HELP_ABOUT = "menu.help.about";
	public static final String MENU_HELP_ITEMS = "menu.help.items";

	public static final DateFormat DATE_FORMAT = DateFormat.getDateTimeInstance();
	/**
	 * convenience method
	 * @param key the xpath to the xml node
	 * @return value the value within the xml node
	 */
	public static String getString( String key ){
		return configuration.getString(key);
	}

	/**
     * The loadConfiguration method loads the configuration for the application from file.
     * If this configuration cannot be loaded, a ConfigurationException 
     * will be thrown.
     * It will also start the logger if it has yet been started
     * @throws Exception
     */
    public static void loadConfiguration( String configXmlFile ) throws Exception {
		// System.out.println("loading configuration file");
		ClassLoader loader = ClassLoader.getSystemClassLoader();
		InputStream in = null;
		try {
			in = loader.getResourceAsStream(configXmlFile);
			
			if (in != null) {
				configuration = new XMLConfiguration();
				configuration.load(in);
			}
			else{
				System.err.println("in is null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    
}
