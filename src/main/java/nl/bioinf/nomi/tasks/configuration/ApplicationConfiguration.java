/**
 * 
 */
package nl.bioinf.nomi.tasks.configuration;

import java.awt.Color;
import java.awt.Font;
import java.io.File;

import nl.bioinf.nomi.tasks.gui.GuiComponentStylingFactory;

import org.apache.commons.configuration.XMLConfiguration;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class ApplicationConfiguration {
	
    /**
     * The configuration settings for the database connection.
     */
    private static XMLConfiguration xmlConfiguration;
    
    
    /**
     * constructs with an xml configuration file
     * @param configXmlFile
     */
//    public ApplicationConfiguration( String configXmlFile ){
//    	try {
//			loadConfigurationXML( configXmlFile );
//			readGuiColors();
//			readGuiFonts();
//			//doClassInitialization();
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println( "unable to initialize configuration: " + e.getMessage() );
//		}
//    }
    
    
	public static void loadConfiguration(String configXmlFile) {
    	try {
			loadConfigurationXML( configXmlFile );
			readGuiColors();
			readGuiFonts();
			//doClassInitialization();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println( "unable to initialize configuration: " + e.getMessage() );
		}
	}

    /**
     * The loadConfiguration method loads the configuration for the application from file.
     * If this configuration cannot be loaded, a ConfigurationException 
     * will be thrown.
     * It will also start the logger if it has yet been started
     * @throws Exception
     */
    private static void loadConfigurationXML( String configXmlFile  ) throws Exception {
        try {
            File xmlConfigFile = new File( configXmlFile );
            if (! xmlConfigFile.canRead() ) {
                throw new Exception( "unable to read configuration file: " + configXmlFile);
            }
            xmlConfiguration = new XMLConfiguration( xmlConfigFile );
        } catch (Exception e) { // configuration could not be loaded
            throw new Exception("unable to load configuration file: " + configXmlFile + "; cause of exception:" + e.getCause() );
        }
    }

    /**
     * 
     * @return xmlConfiguration object
     */
    public static XMLConfiguration getXmlConfiguration(){
    	return xmlConfiguration;
    }
    
    private static void readGuiFonts() throws Exception{
    	NodeList fontNodes = xmlConfiguration.getDocument().getElementsByTagName("font");
    	for(int i=0; i<fontNodes.getLength(); i++){
    		Node fontNode = fontNodes.item(i);
    		
			if (fontNode.getNodeType() == Node.ELEMENT_NODE) {
				Element fontElement = (Element) fontNode;

				NodeList targetElmntLst = fontElement.getElementsByTagName("target");
				if( targetElmntLst.getLength() == 0 ) throw new Exception("target has no value");
				Element targetElmnt = (Element) targetElmntLst.item(0);				
				String target = targetElmnt.getTextContent();

				NodeList fontTypeElmntLst = fontElement.getElementsByTagName("font_type");
				if( fontTypeElmntLst.getLength() == 0 ) throw new Exception("fontType has no value");
				Element fontTypeElmnt = (Element) fontTypeElmntLst.item(0);				
				String fontType = fontTypeElmnt.getTextContent();

				NodeList styleElmntLst = fontElement.getElementsByTagName("style");
				if( styleElmntLst.getLength() == 0 ) throw new Exception("style has no value");
				Element styleElmnt = (Element) styleElmntLst.item(0);				
				String style = styleElmnt.getTextContent();
				int fontStyle = Font.PLAIN;
				if(style.equalsIgnoreCase("bold")) fontStyle = Font.BOLD; 
				else if(style.equalsIgnoreCase("italic")) fontStyle = Font.ITALIC; 

				NodeList sizeElmntLst = fontElement.getElementsByTagName("size");
				if( sizeElmntLst.getLength() == 0 ) throw new Exception("size has no value");
				Element sizeElmnt = (Element) sizeElmntLst.item(0);				
				int size = Integer.parseInt( sizeElmnt.getTextContent() );

				//System.out.println("bold=" + Font.BOLD + " italics=" + Font.ITALIC + " normal=" + Font.PLAIN);

				Font font = new Font(fontType, fontStyle, size);
				/*register the fonts in GuiComponentStylingFactory*/
				GuiComponentStylingFactory.addFont( processTarget(target), font );
				
			}
    	}
    }
    
    private static void readGuiColors() throws Exception{
    	NodeList colorNodes = xmlConfiguration.getDocument().getElementsByTagName("color");
    	for(int i=0; i<colorNodes.getLength(); i++){
    		Node colorNode = colorNodes.item(i);
    		
			if (colorNode.getNodeType() == Node.ELEMENT_NODE) {
				Element colorElement = (Element) colorNode;

				
				NodeList targetElmntLst = colorElement.getElementsByTagName("target");
				if( targetElmntLst.getLength() == 0 ) throw new Exception("target has no value");
				Element targetElmnt = (Element) targetElmntLst.item(0);				
				String target = targetElmnt.getTextContent();

				NodeList redElmntLst = colorElement.getElementsByTagName("red");
				if( redElmntLst.getLength() == 0 ) throw new Exception("red has no value");
				Element redElmnt = (Element) redElmntLst.item(0);				
				int redVal = Integer.parseInt( redElmnt.getTextContent() );

				NodeList greenElmntLst = colorElement.getElementsByTagName("green");
				if( greenElmntLst.getLength() == 0 ) throw new Exception("green has no value");
				Element greenElmnt = (Element) greenElmntLst.item(0);				
				int greenVal = Integer.parseInt( greenElmnt.getTextContent() );
    		
				NodeList blueElmntLst = colorElement.getElementsByTagName("blue");
				if( blueElmntLst.getLength() == 0 ) throw new Exception("blue has no value");
				Element blueElmnt = (Element) blueElmntLst.item(0);				
				int blueVal = Integer.parseInt( blueElmnt.getTextContent() );
				
				Color color = new Color(redVal, greenVal, blueVal);
				//System.out.println( color);
				
				
				/*register the colors in GuiComponentStylingFactory*/
				GuiComponentStylingFactory.addColor( processTarget(target), color);
				
			}
    	}
    	
    }
    
    private static int processTarget( String targetString ){
    	if(targetString.equalsIgnoreCase("menu") ) return GuiComponentStylingFactory.MENU_COMPONENT;
    	else if(targetString.equalsIgnoreCase("tab") ) return GuiComponentStylingFactory.TAB_COMPONENT;
    	else if(targetString.equalsIgnoreCase("panel") ) return GuiComponentStylingFactory.PANEL_COMPONENT;
    	else if(targetString.equalsIgnoreCase("form") ) return GuiComponentStylingFactory.FORM_COMPONENT;
    	else if(targetString.equalsIgnoreCase("list") ) return GuiComponentStylingFactory.LIST_COMPONENT;
    	else if(targetString.equalsIgnoreCase("tree") ) return GuiComponentStylingFactory.TREE_COMPONENT;
    	else return GuiComponentStylingFactory.GENERAL_COMPONENT;
    }

    
}
