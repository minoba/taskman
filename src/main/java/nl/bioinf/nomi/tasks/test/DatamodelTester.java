/**
 * 
 */
package nl.bioinf.nomi.tasks.test;

import nl.bioinf.nomi.tasks.configuration.ApplicationConfiguration;
import nl.bioinf.nomi.tasks.datamodel.Project;
import nl.bioinf.nomi.tasks.datamodel.ProjectTask;
import nl.bioinf.nomi.tasks.datamodel.Employee;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class DatamodelTester {
    
    public static ApplicationConfiguration applicationConfig;
    
    
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if( args.length != 1){
			System.out.println("no configuration file provided!");
			System.out.println("usage: java -jar TakenAdmin.jar <configuration file>");
			System.out.println("aborting");
			System.exit( 1 );
		}
		DatamodelTester dmt = new DatamodelTester();
		dmt.start( args[0] );

	}

	public void start( String configFile ){
		try {
			ApplicationConfiguration.loadConfiguration( configFile );
			/*test teacher methods*/
			//testTeacher();
			//testProject();
			//testProjectTask();
			//testCourseMethod();
			//testModuleCurriculum();
			
			//testClass();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
//	private void testClass(){
//		Curriculum bfv = new Curriculum("BFV", "bioinformatica");
//		StudentClass bfv1 = new StudentClass("BFV1_2009",1,1,bfv);
//		bfv1.setStudentNumber(21);
//		System.out.println(bfv1);
//	}
	
	@SuppressWarnings("unused")
	private void testModuleCurriculum(){
//		Curriculum bfv = new Curriculum("BFV", "bioinformatica");
//		
//		CourseMethod cm1 = new CourseMethod("COMP","computerpracticum",25,5,7,8,0.5);
//		CourseMethod cm2 = new CourseMethod("HCOL","hoorcollege",100,5,7,8,0.5);
//		
//		CurriculumModule m1 = new CurriculumModule("BFVTHM1",5,new Timeframe(-1,1,1),"praktijk thema 1",cm1);
//		CurriculumModule m2 = new CurriculumModule("BFV1BIO1",3,new Timeframe(-1,1,1),"biologie1",cm2);
//		CurriculumModule m3 = new CurriculumModule("BFV3HPC1",3,new Timeframe(-1,1,1),"high-performance computing",cm1);
//		
//		Employee t1 = new Employee( "NOMI", 0.9 );
//		StudentGroup g1 = new StudentGroup(1,t1,m1);
//		StudentGroup g2 = new StudentGroup(2,null,m1);
//		m1.addStudentGroup(g1);
//		m1.addStudentGroup(g2);
//		
//		System.out.println( bfv );
//		System.out.println( cm1 );
//		System.out.println( cm2 );
//		System.out.println( m1 );
//		System.out.println( m2 );
//		System.out.println( m3 );
//
//		System.out.println("student groups in " + m1 + ": " + m1.getStudentGroups().toString());
//		
//		System.out.println( m1 + " equals " + m1 + ": " + m1.equals(m1));
//		System.out.println( m1 + " equals " + m2 + ": " + m1.equals(m2));
		
//		bfv.addModule( m1 );
//		bfv.addModule( m2 );
//		bfv.addModule( m3 );
//		
//		System.out.println( "fetching BFVTHM1: " + bfv.getCurriculumModule("BFVTHM1") );
//		System.out.println( "fetching year 1, quarter 1 regular modules: " + bfv.getModules(1,1,ModuleCategory.REGULAR).toString() );
		
		
	}
	
	@SuppressWarnings("unused")
	private void testCourseMethod(){
//		CourseMethod cm1 = new CourseMethod("WCOL","werkcollege",25,5,7,8,0.5);
//		CourseMethod cm2 = new CourseMethod("HCOL","hoorcollege",100,5,7,8,0.5);
//		
//		System.out.println( cm1 );
//		System.out.println( cm2 );
//		System.out.println( cm1 + " equals " + cm1 + ": " + cm1.equals(cm1));
//		System.out.println( cm1 + " equals " + cm2 + ": " + cm1.equals(cm2));
//		
//		int students = 10;
//		int credits = 3;
//		System.out.println( "hours for werkcollege with " + students + " students and " + credits + " EC: " + cm1.getAssignableHours(students, credits) );
	}
	
	@SuppressWarnings("unused")
	private void testProjectTask(){
		Project p1 = new Project("SIRIUS", "Sirius");
		p1.setDescription("part of excellence program of HG");
		p1.setTotalHours(500);
		System.out.println( p1 );

		Employee t1 = new Employee( "NOMI", 0.9 );
		t1.setFirstName("Michiel");
		t1.setFamilyName("Noback");
		System.out.println( t1.toString() );

		ProjectTask pt1 = new ProjectTask(1, 70, p1);
		pt1.setName("Hounours program BFV");
		pt1.setEmployee(t1);
		ProjectTask pt2 = new ProjectTask(2, 90, p1);
		pt2.setName("Hounours program BFV");
		pt2.setEmployee(t1);
		ProjectTask pt3 = new ProjectTask(2, 90, p1);
		pt3.setName("Hounours program BFV");
		
		p1.addTask(pt1);
		p1.addTask(pt2);
		p1.addTask(pt3);
		
		System.out.println( pt1 );
		System.out.println( pt2 );
		System.out.println( pt1 + " equals " + pt2 + ": " + pt1.equals(pt2));
		System.out.println( pt1 + " equals " + pt1 + ": " + pt1.equals(pt1));
		System.out.println( pt2 + " equals " + pt3 + ": " + pt2.equals(pt3));
		
		System.out.println( p1 );
//		Iterator<ProjectTask> tasks = p1.getTasks();
//		while(tasks.hasNext()){
//			System.out.println( tasks.next() );
//		}
		
	}
	
	@SuppressWarnings("unused")
	private void testProject(){
		Project p1 = new Project("SIRIUS", "Sirius");
		p1.setDescription("part of excellence program of HG");
		p1.setTotalHours(500);
		Project p2 = new Project("TECHN", "Technasium");
		p2.setTotalHours(100);
		Project p3 = new Project("SIRIUS", "Sirius");
		p3.setDescription("part of excellence program of HG");
		p3.setTotalHours(500);
		
		System.out.println( p1.toString() );
		System.out.println( p2.toString() );
		System.out.println( p3.toString() );
		System.out.println( p1 + " equals " + p2 + ": " + p1.equals(p2) );
		System.out.println( p1 + " equals " + p1 + ": " + p1.equals(p1) );
		System.out.println( p1 + " equals " + p3 + ": " + p1.equals(p1) );

	}
	
	/**
	 * test teacher methods
	 */
	@SuppressWarnings("unused")
	private void testTeacher(){
		Employee teacherOne = new Employee( "NOMI", 0.9 );
		teacherOne.setFirstName("Michiel");
		teacherOne.setFamilyName("Noback");
		System.out.println( teacherOne.toString() );
		
		Employee teacherTwo = new Employee( "BBBB", 0.85 );
		teacherTwo.setFirstName("Jan");
		teacherTwo.setNameInfix("van");
		teacherTwo.setFamilyName("Veen");
		System.out.println( teacherTwo.toString() );

		Employee teacherThree = new Employee( "NOMI", 0.9 );
		teacherThree.setFirstName("Michiel");
		teacherThree.setFamilyName("Noback");
		System.out.println( teacherThree.toString() );

		System.out.println( teacherOne + " equals " + teacherTwo + ": " + teacherOne.equals(teacherTwo) );
		System.out.println( teacherOne + " equals " + teacherOne + ": " + teacherOne.equals(teacherOne) );
		System.out.println( teacherOne + " equals " + teacherThree + ": " + teacherOne.equals(teacherThree) );
		
	}
	
}
