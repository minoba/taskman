/**
 * 
 */
package nl.bioinf.nomi.tasks.test;

import java.io.File;

import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;
import nl.bioinf.nomi.tasks.io.XmlDataReader;

/**
 * @author Cellingo
 *
 */
public class IoTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		IoTester iot = new IoTester();
		iot.testXmlReading( args[0] );
		

	}
	
	private void testXmlReading( String inFile ){
		try {
			XmlDataReader xdr = new XmlDataReader(new File(inFile));
			YearDataCollection ydc = xdr.readDataCollection(  );
			
			System.out.println(ydc.toString());
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
	}

}
