/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package nl.bioinf.nomi.tasks.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michiel
 */
public class FileLockTester {

    public static void main(String[] args) {
        FileLockTester flt = new FileLockTester();
        flt.start();
    }

    private void start() {
        /*attempts to open the file*/
        String fileName = "/Users/michiel/Desktop/test_lock/TestText.xml";
        String lockFileName = "";
        File dataFile = new File(fileName);

        /*test for existence of a lock file*/
        if (fileName.endsWith(".xml")) {
            lockFileName = fileName.substring(0, fileName.length() - 4);
            System.out.println("lockFileName = " + lockFileName);

            File lockFile = new File(lockFileName + ".lock");//make hidden later on with "." +
            if (lockFile.exists()) {
                System.out.println("File " + fileName + " is being opened in read-only mode!");

            } else {
                try {
                    /*create the lock file*/
                    createLockFile(lockFile);

//do some work and save
                    int count = 0;
                    while (count++ < 2) {
                        try {
                            String lockContents = "__";
                            Thread.sleep(3000);
//checks lock file
                            boolean okToWrite = checkLockFile(lockFile);

                            if (okToWrite) {
                                try (BufferedWriter writer = Files.newBufferedWriter(
                                        Paths.get(dataFile.getAbsolutePath()),
                                        StandardOpenOption.APPEND)) {
                                    writer.write(System.getProperty("user.name") + " writes some data in iteration " + count);
                                }
                            } else {
                                throw new IllegalStateException("cannot write to given output file because lock has been corrupted by another user: " + lockContents);
                            }

                        } catch (InterruptedException ex) {
                            Logger.getLogger(FileLockTester.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

//all data saved, remove lock file for next use
                    boolean deleted = removeLockFile(lockFile);
                    if (!deleted) {
                        throw new IllegalStateException("could not delete the lock file: " + lockFile);
                    }

                } catch (IOException ex) {
                    System.out.println("Something went wrong while creating the lock file: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
        } else {
            throw new IllegalStateException("the file " + fileName + " should be an xml file");
        }

    }

    /**
     * method to create a lock file using the given lock file name.
     *     
* @param lockFile
     * @throws IOException
     */
    private void createLockFile(File lockFile) throws IOException {
        lockFile.createNewFile();
        try (BufferedWriter writer = Files.newBufferedWriter(
                Paths.get(lockFile.getAbsolutePath()),
                StandardOpenOption.WRITE)) {
            writer.write(System.getProperty("user.name"));
        }
    }

    /**
     * checks whether the lock file is uncorrupted since it was created. it should contain the System property user.name
     * as sole contents.
     *     
* @param lockFile
     * @return lockFileOK
     * @throws IOException
     */
    private boolean checkLockFile(File lockFile) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(
                Paths.get(lockFile.getAbsolutePath()))) {
            String lockContents = reader.readLine();
            return (lockContents.equals(System.getProperty("user.name")));
        }
    }

    /**
     * removes the lock file to release dataFile for next user.
     *     
* @param lockFile
     * @return deleted - the lock file was successfully deleted
     */
    private boolean removeLockFile(File lockFile) {
        return lockFile.delete();
    }

}
