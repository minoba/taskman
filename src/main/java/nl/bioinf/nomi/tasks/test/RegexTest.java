package nl.bioinf.nomi.tasks.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String re = "[^a-zA-Z_1-9]+";
		String id = "SIRIU-S_1";
		
		Pattern p = Pattern.compile(re);
		Matcher m = p.matcher(id);
		
		if(m.find()){
			System.out.println(re + " found in " + id);
		}
		else{
			System.out.println(re + " NOT found in " + id);
		}
	}

}
