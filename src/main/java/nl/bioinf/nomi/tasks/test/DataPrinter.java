/**
 * 
 */
package nl.bioinf.nomi.tasks.test;

import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.ProjectTask;
import nl.bioinf.nomi.tasks.datamodel.StudentGroup;

/**
 * @author Cellingo
 *
 */
public class DataPrinter {

	public static void printEmployeeData( Employee e ){
		System.out.println( e.getFullName() + " (" + e.getEmployeeId() + ")" );
		System.out.println( "fte=" + e.getFte() + " totals hours=" + (e.getFte() * Employee.getFteSize()) );
		System.out.println( "assigned hours=" + e.getAssignedHours() + " assignable hours=" + e.getAssignableHours() );
		System.out.println( "educational tasks: " );
		for( StudentGroup sg : e.getStudentGroups() ){
			System.out.println( "\tgroup=" + sg.getName() + "task hours=" + sg.getTaskHours() + " timeframe=" + sg.getTimeframe() );
		}
		System.out.println( "project tasks: " );
		for( ProjectTask pt : e.getProjectTasks() ){
			System.out.println( "\tproject=" + pt.getProject().getName() + " task=" + pt.getName() + "task hours=" + pt.getTaskHours() + " timeframe=" + pt.getTimeframe() );
		}
		
	}
	
}
