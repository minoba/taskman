/**
 * 
 */
package nl.bioinf.nomi.tasks.datamodel;

/**
 * Encapsulates a single lesson
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.0
 */
public class Lesson{
	public static final Double STANDARD_DURATION = 1.5;
	private String comments = "";
	/*duration in hours*/
	private double duration = 1.5; 
	private LessonType type = LessonType.THEORY;

	/**
	 * constructs with only lesson type
	 * @param type
	 */
	public Lesson( LessonType type ){
		super();
		this.type = type;
	}
	/**
	 * constructs with lesson type and duration
	 * @param type
	 * @param duration the duration in 45-minute sessions
	 */
	public Lesson( LessonType type, double duration ){
		super();
		this.type = type;
		if(duration < 0 || duration > 8) throw new IllegalArgumentException("lesson duration only allowed between 1 and 8 45 minute sessions");
		this.duration = duration;
	}
	
	/**
	 * @param type
	 * @param duration
	 * @param comments
	 */
	public Lesson( LessonType type, double duration,  String comments) {
		super();
		this.comments = comments;
		if(duration < 0 || duration > 8) throw new IllegalArgumentException("lesson duration only allowed between 1 and 8 45 minute sessions");
		this.duration = duration;
		this.type = type;
	}
	/**
	 * @return the classroomRequirement
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the classroomRequirement to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the duration
	 */
	public double getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(double duration) {
		this.duration = duration;
	}
	/**
	 * @return the type
	 */
	public LessonType getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(LessonType type) {
		this.type = type;
	}
	
	public String toString(){
		return this.getClass().getSimpleName() + "[type=" + type.name() + ", duration=" + duration + " hours, comments=" + comments + "]";
	}
	
	public Lesson clone(){
		Lesson clone = new Lesson( this.getType(), this.getDuration(), this.getComments());
		return clone;
	}
	/**
	 * definition of lesson types
	 * @author Michiel
	 *
	 */
	public enum LessonType{
		LABORATORY("P"),
		THEORY("IC"),
		SLB("S"),
		TUTORIAL("WC"),
		STUDENT_ASSISTED_TUTORIAL("SAWC"),
		COMPUTER_LAB("CP"),
		EXAMINATION("TOETS");
		
		private String type;

		private LessonType(String type){
			this.type = type;
		}
		public String toString(){
			return type;
		}
	}
}