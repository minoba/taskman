package nl.bioinf.nomi.tasks.datamodel;

/**
 * class represents a pair of values: 
 * 1: boolean for EC dependency
 * 2: hourly rate
 * 
 * @author Cellingo
 *
 */
public class TimeFactor implements Comparable<TimeFactor>{
	public static final int HOURS_PER_TEACHER = 0;
	public static final int HOURS_PER_GROUP = 1;
	public static final int HOURS_PER_STUDENT = 2;

	private int category;
	private String name;
	private boolean ecDependent = false;
	private double hours = 0;
	
	public TimeFactor(){}
	

	/**
	 * constructs with all members
	 * @param name
	 * @param ecDependent
	 * @param hours
	 */
	public TimeFactor(int category, String name, boolean ecDependent, double hours) {
		super();
		this.category = category;
		this.name = name;
		this.ecDependent = ecDependent;
		this.hours = hours;
	}
	
	/**
	 * @return the category
	 */
	public int getCategory() {
		return category;
	}


	/**
	 * @param category the category to set
	 */
	public void setCategory(int category) {
		this.category = category;
	}


	/**
	 * get the category represented b y the given string
	 * @param categoryString
	 * @return category
	 * @throws IllegalArgumentException
	 */
	public static int getCategory( String categoryString ) throws IllegalArgumentException{
		if( categoryString.equalsIgnoreCase("HOURS_PER_TEACHER") ) return HOURS_PER_TEACHER;
		else if( categoryString.equalsIgnoreCase("HOURS_PER_GROUP") ) return HOURS_PER_GROUP;
		else if( categoryString.equalsIgnoreCase("HOURS_PER_STUDENT") ) return HOURS_PER_STUDENT;
		else throw new IllegalArgumentException( "string does not represent a known time factor category" );
	}
	
	/**
	 * get a string representation of the category of this factor
	 * @return categoryString
	 */
	public String getCategoryString(){
		switch(this.category){
			case HOURS_PER_TEACHER: return "HOURS_PER_TEACHER";
			case HOURS_PER_GROUP: return "HOURS_PER_GROUP";
			case HOURS_PER_STUDENT: return "HOURS_PER_STUDENT";
		}
		return "";
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the ecDependent
	 */
	public boolean isEcDependent() {
		return ecDependent;
	}
	/**
	 * @param ecDependent the ecDependent to set
	 */
	public void setEcDependent(boolean ecDependent) {
		this.ecDependent = ecDependent;
	}
	/**
	 * @return the hours
	 */
	public double getHours() {
		return hours;
	}
	/**
	 * @param hours the hours to set
	 */
	public void setHours(double hours) {
		this.hours = hours;
	}

	/**
	 * returns a clone for safe editing purposes
	 * @return clone a clone of this object
	 */
	public TimeFactor clone(){
		TimeFactor clone = new TimeFactor(this.category, this.name, this.ecDependent, this.hours);
		return clone;
	}
	
	public String toString(){
		return (this.getClass().getSimpleName() + " category=" + this.getCategory() + " name=" + this.getName() + " hours=" + this.getHours() + " ec dependent=" + this.isEcDependent() );
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + category;
		result = prime * result + (ecDependent ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(hours);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeFactor other = (TimeFactor) obj;
		if (category != other.category)
			return false;
		if (ecDependent != other.ecDependent)
			return false;
		if (Double.doubleToLongBits(hours) != Double
				.doubleToLongBits(other.hours))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public int compareTo(TimeFactor other) {
		return (int)(Math.ceil( other.getHours() - this.getHours()) );
	}
	
	
}
