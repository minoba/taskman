package nl.bioinf.nomi.tasks.datamodel;

import java.io.Serializable;

/**
 * Specifies Task of type project.
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.3
 */
public class ProjectTask implements Serializable, Task, Comparable<ProjectTask> {
    private final int projectTaskId;
    private int taskHours;
    private String name;
    private Project project;
    private Employee employee;
    private Timeframe timeframe;
    private TaskLabel taskLabel;

    /**
     * construct with task ID and parent project.
     * The timeframe defaults to 1 year;
     * @param taskId the ID
     * @param hours the hours
     * @param project the project
     */
    public ProjectTask(final int taskId, final int hours, final Project project) {
        this.projectTaskId = taskId;
        this.taskHours = hours;
        this.project = project;
        this.timeframe = new Timeframe(-1, 1, 4);
        /*ProjectTask labels default to Internal Projects*/
        this.taskLabel = TaskLabel.INTERNAL_PROJECTS;
    }

    @Override
    public int getTaskHours() {
        return taskHours;
    }

    /**
     * @param taskHours the taskHours to set
     */
    public void setTaskHours(final int taskHours) {
        this.taskHours = taskHours;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the description to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(final Project project) {
        this.project = project;
    }

    /**
     * @return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * @param employee the teacher to set
     */
    public void setEmployee(final Employee employee) {
        this.employee = employee;
    }

    /**
     * @return the projectTaskId
     */
    public int getProjectTaskId() {
        return projectTaskId;
    }

    @Override
    public TaskCategory getTaskCategory() {
        return this.taskLabel.getTaskCategory();
    }

    @Override
    public void setTaskLabel(final TaskLabel taskLabel) {
        this.taskLabel = taskLabel;
    }

    @Override
    public TaskLabel getTaskLabel() {
        return this.taskLabel;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String t = (employee == null ? "none" : employee.getEmployeeId());
        String d = (name == null ? "none" : name);
        return (this.getClass().getSimpleName()
                + "[ID=" + getProjectTaskId()
                + " description=" + d
                + ", size=" + getTaskHours()
                + " hours, teacher=" + t + "]");
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + projectTaskId;
        result = prime * result + taskHours;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProjectTask other = (ProjectTask) obj;
        if (project == null) {
            if (other.project != null) {
                return false;
            }
        } else if (!project.equals(other.project)) {
            return false;
        }
        if (projectTaskId != other.projectTaskId) {
            return false;
        }
        return taskHours == other.taskHours;
    }

    @Override
    public Timeframe getTimeframe() {
        return this.timeframe;
    }

    /**
     * sets the timeframe of this task.
     * @param timeframe the time frame
     */
    public void setTimeframe(final Timeframe timeframe) {
        this.timeframe = timeframe;
    }

    @Override
    public boolean isCurriculumBound() {
        return this.taskLabel.getTaskCategory() == TaskCategory.CURRICULUM_BOUND;
    }

    @Override
    public int getQuarterHours(final int quarter) {
        return this.timeframe.getQuarterHours(quarter, taskHours);
    }

    @Override
    public double getQuarterHoursRaw(final int quarter) {
        return this.timeframe.getQuarterHoursRaw(quarter, taskHours);
    }

    @Override
    public int compareTo(final ProjectTask other) {
        /*HBO inclusive tasks have precedence*/
        if (this.isCurriculumBound() && (!other.isCurriculumBound())) {
            return -1;
        } else if ((!this.isCurriculumBound()) && other.isCurriculumBound()) {
            return 1;
        } else {
            /*secondary sort on name*/
            return (this.getName().compareTo(other.getName()));
        }
    }
}
