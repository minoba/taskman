/**
 *
 */
package nl.bioinf.nomi.tasks.datamodel;

import javax.print.attribute.standard.NumberUpSupported;
import java.util.*;
import java.util.stream.Stream;

/**
 * class encapsulates Teacher data.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.2
 */
public class Employee implements Comparable<Employee> {
    /**
     * storage Employee for new tasks without real employee.
     */
    private static Employee vacantEmployee;

    private static int fteSize;
    /**
     * the general salary scale.
     */
    public static final int GPL_SALARY_SCALE = -1;
    private static final HashMap<Integer, Double> SALARY_LEVELS = new HashMap<Integer, Double>();
    private static double gpl;
    /**
     * use GPL for all calculations?.
     */
    private static boolean useGpl;
    private final String employeeId;
    private String firstName;
    private String nameInfix;
    private String familyName;
    private double fte;
    /**
     * the function/salary scale.
     */
    private int functionScale;
    private String remarks;
    private String team;
    private String email;
    private final HashSet<ProjectTask> projectTasks;
    private final HashSet<StudentGroup> studentGroups;
    private final HashSet<RegularTask> regularTasks;

    /**
     * construct with 4-letter code and fte size.
     *
     * @param teacherId the employee ID
     * @param fte the fte
     */
    public Employee(final String teacherId, final double fte) {
        this.employeeId = teacherId;
        this.fte = fte;
        projectTasks = new HashSet<>();
        studentGroups = new HashSet<>();
        regularTasks = new HashSet<>();
    }

    /**
     * sets the vacancy.
     *
     * @param vacantEmployee the vacancy imaginary employee
     */
    public static void setVacantEmployee(final Employee vacantEmployee) {
        Employee.vacantEmployee = vacantEmployee;
    }

    /**
     * returns the vacancy.
     *
     * @return vacantEmployee
     */
    public static Employee getVacantEmployee() {
        return vacantEmployee;
    }

    /**
     * returns a list of valid salary scales.
     *
     * @return salaryScales
     */
    public static List<Integer> getSalaryScales() {
        List<Integer> scales = new ArrayList<>();
        scales.addAll(SALARY_LEVELS.keySet());
        Collections.sort(scales);
        return scales;
    }

    /**
     * adds a regular task to the collection.
     *
     * @param task the task to add
     */
    public void addRegularTask(final RegularTask task) {
        this.regularTasks.add(task);
    }

    /**
     * will delete the task with the given ID if it exists.
     *
     * @param task the task
     */
    public void removeRegularTask(final RegularTask task) {
        if (regularTasks.contains(task)) {
            regularTasks.remove(task);
        }
    }

    /**
     * returns a sorted list of regular task - first on category then on name.
     *
     * @return regularTasks
     */
    public List<RegularTask> getRegularTasks() {
        List<RegularTask> list = new ArrayList<RegularTask>();
        list.addAll(regularTasks);
        Collections.sort(list);
        return list;
    }

    public List<RegularTask> getRegularTasksWithCommunalProfessionalizationTask() {
        List<RegularTask> list = new ArrayList<RegularTask>();
        list.addAll(regularTasks);
        list.add(getCommunalProfessionalizationHoursAsTask());
        Collections.sort(list);
        return list;
    }

    /**
     * returns this Employees total for the given category.
     *
     * @param taskCategory the task category
     * @return taskHours
     */
    public int getRegularTaskHours(final TaskCategory taskCategory) {
        return this.regularTasks.stream()
                .filter(t -> t.getTaskCategory() == taskCategory)
                .mapToInt(t -> t.getTaskHours())
                .sum();
    }

    /**
     * adds a task to the teachers assignments.
     *
     * @param task the task
     */
    public void addProjectTask(final ProjectTask task) {
        projectTasks.add(task);
    }

    /**
     * removes the given task from this employee.
     *
     * @param task the task
     */
    public void removeProjectTask(final ProjectTask task) {
        projectTasks.remove(task);
    }

    /**
     * Returns a project tasks associated with the given project (as identified by its name). Returns null if the
     * project is not present
     *
     * @param projectName the project name
     * @return projectTask
     */
    public List<ProjectTask> getProjectTasks(final String projectName) {
        List<ProjectTask> rtasks = new ArrayList<>();
        for (ProjectTask pt : projectTasks) {
            //System.out.println( this.getClass().getSimpleName() + ": check against " + pt.getProject().getName() );
            if (pt.getProject().getName().equals(projectName)) {
                rtasks.add(pt);
            }
        }
        return rtasks;
    }

    /**
     * returns all project tasks of this employee.
     *
     * @return taskslist
     */
    public List<ProjectTask> getProjectTasks() {
        ArrayList<ProjectTask> list = new ArrayList<>();
        list.addAll(projectTasks);
        Collections.sort(list);
        return list;
    }

    /**
     * returns the task hours for the given project; 0 if this project is not associated with the employee.
     *
     * @param projectName the project name
     * @return taskHours
     */
    public int getProjectTaskHours(final String projectName) {
        int total = 0;
        for (ProjectTask pt : projectTasks) {
            if (pt.getProject().getName().equals(projectName)) {
                total += pt.getTaskHours();
            }
        }
        return total;
    }

    /**
     * adds a studentGroup to the teachers groups.
     *
     * @param studentGroup the student group
     */
    public void addStudentGroup(final StudentGroup studentGroup) {
        //TODO recalculate hours for the teacher
        studentGroups.add(studentGroup);
    }

    /**
     * returns all student groups of this employee.
     *
     * @return groupslist
     */
    public List<StudentGroup> getStudentGroups() {
        ArrayList<StudentGroup> list = new ArrayList<>();
        list.addAll(studentGroups);
        Collections.sort(list);
        return list;
    }

    /**
     * deletes a student group for this employee (if it exists in the first place).
     *
     * @param studentGroup the student group
     */
    public void removeStudentGroup(final StudentGroup studentGroup) {
        studentGroups.remove(studentGroup);
    }

    /**
     * returns a stream of all Tasks of this employee
     */
    public Stream<Task> getAllTasks() {
        return Stream.concat(Stream.concat(this.regularTasks.stream(), this.projectTasks.stream()), this.studentGroups.stream());
    }

    public int getFreelySpendableProfessionalizationHours() {
        int total = getRegularTasks().stream()
                .filter(t -> t.getTaskLabel()==TaskLabel.INDIVIDUAL_PROFESSIONAL_DEVELOPMENT)
                .mapToInt(Task::getTaskHours)
                .sum();
        return total;
    }

    public int getIndirectHours() {
        return getRegularTasks().stream()
                .filter(t -> t.getTaskLabel()==TaskLabel.INDIRECT_HOURS)
                .mapToInt(Task::getTaskHours)
                .sum();
    }

    public int getCummunalProfessionalizationHours() {
        //TODO klopt dit wel want wat als de aanstelling kleiner is dan 0.4??
        //44b = berekend as (CurrGebTaken / 1.1 * 0.15) - 80
        return (int)((getCurriculumboundHours() / 1.1 * 0.15) - getFreelySpendableProfessionalizationHours() - getIndirectHours());
    }

    public RegularTask getCommunalProfessionalizationHoursAsTask() {
        RegularTask commProfHoursTask = new RegularTask(-1, getCummunalProfessionalizationHours());
        commProfHoursTask.setEmployee(this);
        commProfHoursTask.setName("Schoolgestuurde professionalisering");
        commProfHoursTask.setTaskLabel(TaskLabel.COMMUNAL_PROFESSIONAL_DEVELOPMENT);
        commProfHoursTask.setTimeframe(new Timeframe(1, 4));
        return commProfHoursTask;
    }

    /**
     * Vrije professionaliseringsuren (40 uur / label 44a) + schoolgestuurde = 44b
     * @return
     */
    public int getProfessionalizationHours() {
        return getFreelySpendableProfessionalizationHours() + getCummunalProfessionalizationHours();
    }

    public int getCurriculumboundHours() {
        int total = getAllTasks()
                .filter(t -> t.getTaskCategory() == TaskCategory.CURRICULUM_BOUND)
                .mapToInt(t -> t.getTaskHours())
                .sum();
        return total;
    }

    public int getNonCurriculumboundHours() {
        int total = getAllTasks()
                .filter(t -> t.getTaskCategory() != TaskCategory.CURRICULUM_BOUND)
                .mapToInt(t -> t.getTaskHours())
                .sum();
        return total;
    }

    /**
     * returns the total number of hours according to the fte.
     *
     * @return assignableHours
     */
    public int getAssignableHours() {
        double total = getFte() * Employee.getFteSize();
        //total -= getHboHours();
        return (int) Math.round(total);
    }

    /**
     * returns the assigned hours of this teacher.
     *
     * @return assignedHours
     */
    public int getAssignedHours() {
        final int sum = getAllTasks().mapToInt(Task::getTaskHours).sum();
        return sum;
    }

    /**
     * returns the total number of vacant hours for this Employee.
     *
     * @return vacantHours
     */
    public int getVacantHours() {
        return (getAssignableHours() - getAssignedHours() - getCummunalProfessionalizationHours());
    }

    /**
     * returns the task totals for each quarter and as last element the overall totals of all tasks.
     *
     * @return quarterAndOverallTotals
     */
    public int[] getQuarterAndOverallTotals() {
        int[] quarterTotals = new int[5];
        List<ProjectTask> pts = getProjectTasks();
        for (Task t : pts) {
            for (int i = 0; i <= 3; i++) {
                quarterTotals[i] += t.getQuarterHours(i + 1);
            }
            quarterTotals[4] += t.getTaskHours();
        }
        List<StudentGroup> sgs = getStudentGroups();
        for (Task t : sgs) {
            for (int i = 0; i <= 3; i++) {
                quarterTotals[i] += t.getQuarterHours(i + 1);
            }
            quarterTotals[4] += t.getTaskHours();
        }

        List<RegularTask> rts = getRegularTasksWithCommunalProfessionalizationTask();
        for (Task t : rts) {
            for (int i = 0; i <= 3; i++) {
                quarterTotals[i] += t.getQuarterHours(i + 1);
            }
            quarterTotals[4] += t.getTaskHours();
        }

        return quarterTotals;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the nameInfix
     */
    public String getNameInfix() {
        return nameInfix;
    }

    /**
     * @param nameInfix the nameInfix to set
     */
    public void setNameInfix(final String nameInfix) {
        this.nameInfix = nameInfix;
    }

    /**
     * @return the familyName
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * @param familyName the familyName to set
     */
    public void setFamilyName(final String familyName) {
        this.familyName = familyName;
    }

    /**
     * @return full name of teacher
     */
    public String getFullName() {
        if (this.nameInfix == null) {
            return (firstName + " " + familyName);
        } else {
            return (firstName + " " + nameInfix + " " + familyName);
        }
    }

    /**
     * @return the fte
     */
    public double getFte() {
        return fte;
    }

    /**
     * @param fte the fte to set
     */
    public void setFte(final double fte) {
        this.fte = fte;
    }

    /**
     * @return the functionScale
     */
    public int getFunctionScale() {
        return functionScale;
    }

    /**
     * @param functionScale the functionScale to set
     */
    public void setFunctionScale(final int functionScale) {
        this.functionScale = functionScale;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(final String remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the fteSize
     */
    public static int getFteSize() {
        return fteSize;
    }

    /**
     * @return the teacherId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * sets the maximum fte size in hours.
     *
     * @param fteSize in hours
     */
    public static void setFteSize(final int fteSize) {
        Employee.fteSize = fteSize;
    }

    /**
     * sets the GPL; average employee costs, in euros.
     *
     * @param gpl the gpl
     */
    public static void setGpl(final double gpl) {
        Employee.gpl = gpl;
        SALARY_LEVELS.put(GPL_SALARY_SCALE, Employee.gpl);
    }

    /**
     * returns the GPL; average employee costs, in euros.
     *
     * @return gpl
     */
    public static double getGpl() {
        return Employee.gpl;
    }

    /**
     * sets whether to use the GPL for calculations instead of individual salary scales.
     *
     * @param useGpl use gpl
     */
    public static void setUseGpl(final boolean useGpl) {
        Employee.useGpl = useGpl;
    }

    /**
     * returns whether to use the GPL for calculations instead of individual salary scales.
     *
     * @return useGpl
     */
    public static boolean isUseGpl() {
        return Employee.useGpl;
    }

    /**
     * adds an average salary level for a function level.
     *
     * @param level the salary level (scale)
     * @param rate the hourly rate
     */
    public static void addSalaryLevel(final int level, final double rate) {
        SALARY_LEVELS.put(level, rate);
    }

    /**
     * returns whether the salary scale exists.
     *
     * @param level the salary level (scale)
     * @return exists
     */
    public static boolean containsSalaryLevel(final int level) {
        return SALARY_LEVELS.containsKey(level);
    }

    /**
     * Returns the salary level of the given function scale, or the level of GPL if this is selected. Do NOT make the
     * mistake using method getSalaryRate(functionScale), which gives the salary rate for a function scale instead of
     * getSalaryCosts(functionScale), which gives the actual costs, depending on the usage of GPL
     *
     * @param functionScale functiuon scale
     * @return salaryRate
     */
    public static double getSalaryRate(final int functionScale) {
        return SALARY_LEVELS.get(functionScale);
    }

    /**
     * Returns the salary level of the given function scale, or the level of GPL if this is selected. Do NOT make the
     * mistake using method getSalaryRate(functionScale), which gives the salary rate for a function scale instead of
     * getSalaryCosts(functionScale), which gives the actual costs, depending on the usage of GPL
     *
     * @param functionScale functiuon scale
     * @return salaryCosts
     */
    public static double getSalaryCosts(final int functionScale) {
        if (Employee.useGpl) {
            return SALARY_LEVELS.get(GPL_SALARY_SCALE);
        } else {
            return SALARY_LEVELS.get(functionScale);
        }
    }

    /**
     * @param team the team to set
     */
    public void setTeam(final String team) {
        this.team = team;
    }

    /**
     * @return the team
     */
    public String getTeam() {
        return team;
    }

    @Override
    public String toString() {
        return (this.getClass().getSimpleName() + "[" + getFullName() + " (" + getEmployeeId()
                + ") fte=" + getFte()
                + " team=" + getTeam()
                + "]");
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        Employee otherTeacher = (Employee) other;
        return this.employeeId.equals(otherTeacher.getEmployeeId());
    }

    @Override
    public int hashCode() {
        return employeeId.hashCode();
    }

    @Override
    public int compareTo(final Employee o) {
        return this.getFamilyName().compareTo(o.getFamilyName());
    }

}
