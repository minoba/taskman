package nl.bioinf.nomi.tasks.datamodel;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * contains all data related to a single school year.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class YearDataCollection {

    private String version;
    private int schoolYear;
    private String school;
    private String planner;
    private File dataFile;
    private final HashMap<String, Employee> employees;

    /**
     * a hashmap of team (eg. "BML") keys and set of employee as value.
     */
    private final HashMap<String, HashSet<Employee>> teams;

    /**
     * all the curricula as ID - curriculum map.
     */
    private final HashMap<String, Curriculum> curricula;
    /**
     * all the projects as ID - project map.
     */
    private final HashMap<String, Project> projects;
    /**
     * all the modules as ID - module map.
     */
    private final HashMap<String, CurriculumModule> modules;
    /**
     * all regular tasks that are not teaching tasks or project tasks.
     */
    private final HashMap<Integer, RegularTask> regularTasks;

    /**
     * constructor initializaes main data collections.
     */
    public YearDataCollection() {
        employees = new HashMap<String, Employee>();
        teams = new HashMap<String, HashSet<Employee>>();
        curricula = new HashMap<String, Curriculum>();
        projects = new HashMap<String, Project>();
        modules = new HashMap<String, CurriculumModule>();
        regularTasks = new HashMap<Integer, RegularTask>();
    }

    /**
     * Returns a new TaskId for a project: the first integer available.
     *
     * @return newTaskId new task ID
     */
    public int getNewRegularTaskId() {
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            if (!regularTasks.containsKey(i)) {
                return i;
            }
        }
        return Integer.MAX_VALUE;
    }

    /**
     * adds a regular task to the collection.
     *
     * @param task a task
     */
    public void addRegularTask(final RegularTask task) {
        //System.out.println( this.getClass().getSimpleName() + " adding " + task);
        this.regularTasks.put(task.getTaskId(), task);
    }

    /**
     * returns a task based on its ID or null if not existing.
     *
     * @param taskID task ID
     * @return RegularTask regular task
     */
    public Task getRegularTask(final int taskID) {
        if (regularTasks.containsKey(taskID)) {
            return regularTasks.get(taskID);
        } else {
            return null;
        }
    }

    /**
     * will delete the task with the given ID if it exists.
     *
     * @param taskID the task ID
     */
    public void removeRegularTask(final int taskID) {
        if (regularTasks.containsKey(taskID)) {
            regularTasks.remove(taskID);
        }
    }

    /**
     * returns a sorted list of regular task - first on category then on name.
     *
     * @return regularTasks all the regular tasks
     */
    public List<RegularTask> getRegularTasks() {
        List<RegularTask> list = new ArrayList<>();
        list.addAll(regularTasks.values());

        for (Employee employee : getEmployees()) {
            list.add(employee.getCommunalProfessionalizationHoursAsTask());
        }
        Collections.sort(list);
        return list;
    }

    /**
     * returns a sorted list of given category on name.
     *
     * @param cat the cat
     * @return regularTasks regular tasks of the cat
     */
    public List<RegularTask> getRegularTasks(final TaskCategory cat) {
        List<RegularTask> list = new ArrayList<>();
        for (RegularTask rt : getRegularTasks()) {
            if (rt.getTaskCategory() == cat) {
                list.add(rt);
                //System.out.println("(1) adding rt = " + rt);
            }
        }
        //System.out.println("(2) list before sort = " + list);
        Collections.sort(list);
        //System.out.println("(3) list after sort = " + list);
        return list;
    }

    /**
     * returns the total hours for the given category.
     *
     * @param cat the cat
     * @return totalHours total hours for the cat
     */
    public int getRegularTaskHours(final TaskCategory cat) {
        int total = 0;
        for (RegularTask rt : getRegularTasks()) {
            if (rt.getTaskCategory() == cat) {
                total += rt.getTaskHours();
            }
        }
        return total;
    }

    /**
     * returns the total hours for all categories.
     *
     * @return totalHours all hours for regular tasks
     */
    public int getRegularTaskHours() {
        int total = 0;
        for (RegularTask rt : getRegularTasks()) {
            total += rt.getTaskHours();
        }
        return total;
    }

    /**
     * returns the total cost for the given category.
     *
     * @param cat the cat
     * @return costs the costs
     */
    public double getRegularTaskCosts(final TaskCategory cat) {
        double total = 0;
        for (RegularTask rt : getRegularTasks()) {
            if (rt.getTaskCategory() == cat) {
                total += (Employee.getSalaryCosts(rt.getEmployee().getFunctionScale()) * rt.getTaskHours());
            }
        }
        return total;
    }

    /**
     * returns the total cost for all categories.
     *
     * @return totalHours the hours
     */
    public double getRegularTaskCosts() {
        double total = 0;
        for (RegularTask rt : getRegularTasks()) {
            total += (Employee.getSalaryCosts(rt.getEmployee().getFunctionScale()) * rt.getTaskHours());
        }
        return total;
    }

    /**
     * returns a module based on its id.
     *
     * @param id the module ID
     * @return module
     */
    public CurriculumModule getModule(final String id) {
        return modules.get(id);
    }

    /**
     * adds a curriculumModule to the year collection.
     *
     * @param curriculumModule the curriculumModule to add
     */
    public void addModule(final CurriculumModule curriculumModule) {
        modules.put(curriculumModule.getModuleId(), curriculumModule);
    }

    /**
     * removes an module from the collection.
     *
     * @param moduleId the module ID
     */
    public void removeModule(final String moduleId) {
        modules.remove(moduleId);
    }

    /**
     * returns a list of all modules.
     *
     * @return modulesList list
     */
    public List<CurriculumModule> getModules() {
        List<CurriculumModule> list = new ArrayList<CurriculumModule>();
        list.addAll(modules.values());
        Collections.sort(list);
        return list;
    }

    /**
     * returns a list of modules overlapping with a given timeframe.
     *
     * @param timeframe the timeframe
     * @return modules modules of the time frame
     */
    public List<CurriculumModule> getModules(final Timeframe timeframe) {
        List<CurriculumModule> list = new ArrayList<CurriculumModule>();
        for (CurriculumModule curriculumModule : modules.values()) {
            if (curriculumModule.getTimeframe().overlaps(timeframe)) {
                list.add(curriculumModule);
            }
        }
        Collections.sort(list);
        return list;
    }

    /**
     * returns whether a course method is in use in this data collection.
     *
     * @param method the method to check
     * @return courseMethodUsed
     */
    public boolean isCourseMethodUsed(final CourseMethod method) {
        for (CurriculumModule m : getModules()) {
            if (m.getCourseMethod().getId().equals(method.getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * returns a list of all tasks the given employee is involved in.
     *
     * @param employee the employee to fetch tasks for
     * @return projectTaskList
     */
    public List<ProjectTask> getProjectTasks(final Employee employee) {
        ArrayList<ProjectTask> emplTasks = new ArrayList<ProjectTask>();
        for (Project project : projects.values()) {
            for (ProjectTask task : project.getTasks()) {
                if (task.getEmployee().equals(employee)) {
                    emplTasks.add(task);
                }
            }
        }
        return emplTasks;
    }

    /**
     * returns an alphabetically sorted list of teams.
     *
     * @return teams
     */
    public List<String> getTeams() {
        List<String> teamIds = new ArrayList<String>();
        if (!teams.isEmpty()) {
            teamIds.addAll(teams.keySet());
        }
        Collections.sort(teamIds);
        return teamIds;
    }

    /**
     * returns a list of all teachers sorted on last name.
     *
     * @return teacherList
     */
    public List<Employee> getEmployees() {
        List<Employee> list = new ArrayList<Employee>();
        list.addAll(employees.values());
        Collections.sort(list);
        return list;
    }

    /**
     * returns a list of employees sorted using the given comparator.
     *
     * @param comparator the comparator
     * @return employeeslist
     */
    public List<Employee> getEmployees(final Comparator<Employee> comparator) {
        List<Employee> list = new ArrayList<Employee>();
        list.addAll(employees.values());
        Collections.sort(list, comparator);
        return list;
    }

    /**
     * returns a list of employees of given team sorted by family name.
     *
     * @param team the team to fetch employees for
     * @return employee list
     */
    public List<Employee> getEmployees(final String team) {
        return getEmployees(team, new Comparator<Employee>() {
            @Override
            public int compare(final Employee first, final Employee second) {
                return first.getFamilyName().compareTo(second.getFamilyName());
            }
        });
    }

    /**
     * returns a list of employees of given team sorted by given comparator.
     *
     * @param team the team
     * @param comparator the comparator
     * @return employees
     */
    public List<Employee> getEmployees(final String team, final Comparator<Employee> comparator) {
        List<Employee> emps = new ArrayList<Employee>();
        if (teams.containsKey(team)) {
            emps.addAll(teams.get(team));
        }
        Collections.sort(emps, comparator);
        return emps;
    }

    /**
     * returns a teacher based on the id.
     *
     * @param id employee ID
     * @return teacher
     */
    public Employee getEmployee(final String id) {
        return employees.get(id);
    }

    /**
     * adds an employee to the collection.
     *
     * @param employee the employee
     */
    public void addEmployee(final Employee employee) {
        employees.put(employee.getEmployeeId(), employee);
        if (teams.containsKey(employee.getTeam())) {
            teams.get(employee.getTeam()).add(employee);
        } else {
            HashSet<Employee> emps = new HashSet<Employee>();
            emps.add(employee);
            teams.put(employee.getTeam(), emps);
        }
    }

    /**
     * removes an employee from the collection.
     *
     * @param employeeId the employee ID
     */
    public void removeEmployee(final String employeeId) {
        Employee removed = employees.get(employeeId);
        employees.remove(employeeId);
        teams.get(removed.getTeam()).remove(removed);
    }

    /**
     * returns a list of all curricula sorted on ID.
     *
     * @return curriculaList
     */
    public List<Curriculum> getCurricula() {
        List<Curriculum> list = new ArrayList<Curriculum>();
        list.addAll(curricula.values());
        Collections.sort(list);
        return list;
    }

    /**
     * returns a curriculum based on id.
     *
     * @param id the curriculum ID
     * @return curriculum
     */
    public Curriculum getCurriculum(final String id) {
        return curricula.get(id);
    }

    /**
     * adds a curriculum to the collection.
     *
     * @param curriculum the curriculum to add
     */
    public void addCurriculum(final Curriculum curriculum) {
        curricula.put(curriculum.getCurriculumID(), curriculum);
    }

    /**
     * removes a curriculum from the collection.
     *
     * @param id the ID of the curriculum to remove
     */
    public void removeCurriculum(final String id) {
        curricula.remove(id);
    }

    /**
     * returns a list of all projects sorted on name.
     *
     * @return projectsList
     */
    public List<Project> getProjects() {
        List<Project> list = new ArrayList<Project>();
        list.addAll(projects.values());
        Collections.sort(list);
        return list;
    }

    /**
     * returns a project based on its name.
     *
     * @param name the project name
     * @return project
     */
    public Project getProjectByName(final String name) {
        for (Project p : projects.values()) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    /**
     * returns a project based on its name.
     *
     * @param id the project ID
     * @return project
     */
    public Project getProjectById(final String id) {
        return projects.get(id);
    }

    /**
     * adds a project to the collection.
     *
     * @param project the project to add
     */
    public void addProject(final Project project) {
        projects.put(project.getProjectId(), project);
    }

    /**
     * removes the given project from the collection.
     *
     * @param project the project to remove
     */
    public void removeProject(final Project project) {
        projects.remove(project.getProjectId());
    }

    /**
     * @return the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @return the schoolYear.
     */
    public int getSchoolYear() {
        return schoolYear;
    }

    /**
     * @param schoolYear the schoolYear to set.
     */
    public void setSchoolYear(final int schoolYear) {
        this.schoolYear = schoolYear;
    }

    /**
     * @return the school.
     */
    public String getSchool() {
        return school;
    }

    /**
     * @param school the school to set.
     */
    public void setSchool(final String school) {
        this.school = school;
    }

    /**
     * @return the userName.
     */
    public String getPlanner() {
        return planner;
    }

    /**
     * @param planner the userName to set.
     */
    public void setPlanner(final String planner) {
        this.planner = planner;
    }

    /**
     * @param dataFile the dataFile to set.
     */
    public void setDataFile(final File dataFile) {
        this.dataFile = dataFile;
    }

    /**
     * @return the dataFile.
     */
    public File getDataFile() {
        return dataFile;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return (this.getClass().getSimpleName() + "[version=" + getVersion()
                + ", year=" + getSchoolYear()
                + ", school=" + getSchool()
                + ", user=" + getPlanner()
                + "]");
    }

}
