/**
 * 
 */
package nl.bioinf.nomi.tasks.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Models a complete curriculum: its variants and their years
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.2
 */
public class Curriculum implements Serializable, Comparable<Curriculum>{
	private static final long serialVersionUID = -927916211464363796L;
	
	/**
	 * a static map to hold the available course methods
	 */
	private static HashMap<String, CourseMethod> courseMethods;
	/**
	 * a static map to hold the available curriculum types with their names
	 */
	private static HashMap<String, String> curriculumTypes;

	/**
	 * the ID of the curriculum
	 */
	private String curriculumID;
	/**
	 * the name of the curriculum
	 */
	private String name;
	/**
	 * the curriculum description
	 */
	private String description;
	/**
	 * the map of curriculum variants. The key (type) should be available in map curriculumTypes
	 */
	private HashMap<String, CurriculumVariant> curriculumVariants;
	/**
	 * internship start
	 */
	private Timeframe internship;
	/**
	 * graduation start
	 */
	private Timeframe graduation;

	/**
	 * static initializer creates the map of course methods as ID - method
	 */
	static{
		courseMethods = new HashMap<String, CourseMethod>();
		curriculumTypes = new HashMap<String, String>();
	}
	
	/**
	 * construct with ID and name
	 * @param curriculumID, eg BOV-R 
	 * @param name eg "biologisch-medisch laboratorium onderzoek"
	 */
	public Curriculum(String curriculumID, String name) {
		super();
		this.curriculumID = curriculumID;
		this.name = name;
		this.curriculumVariants = new HashMap<String, CurriculumVariant>();
	}
	
	/**
	 * sets the internship timeframe
	 * @param internship
	 */
	public void setInternship(Timeframe internship) {
		this.internship = internship;
	}

	/**
	 * returns the timeframe of the internship
	 * @return internship
	 */
	public Timeframe getInternship(){
		return internship;
	}
	
	/**
	 * sets the graduation timeframe
	 * @param graduation
	 */
	public void setGraduation(Timeframe graduation) {
		this.graduation = graduation;
	}

	/**
	 * returns the timeframe of the graduation
	 * @return graduation
	 */
	public Timeframe getGraduation(){
		return graduation;
	}
	
	/**
	 * returns a sorted list of all timeframes for this curriculum, including graduation and internship
	 * @return timeframes
	 */
	public List<Timeframe> getTimeframes(){
		//System.out.println( "internship=" + internship + " graduation=" + graduation );
		List<Timeframe> list = new ArrayList<Timeframe>();
		for( int i=1; i<=16; i++ ){
			Timeframe tf = new Timeframe(i);
			//System.out.println( "tf " + tf + " overlaps internship=" + internship + ": " + tf.overlaps(internship) );
			//System.out.println( "tf " + tf + " overlaps graduation=" + graduation + ": " + tf.overlaps(graduation) );
			if( ! (tf.overlaps(internship) || tf.overlaps(graduation) ) ) list.add(tf);
		}
		list.add(internship);
		list.add(graduation);
		Collections.sort(list);
		return list; 
	} 

	/**
	 * returns a list of timeframes for a particular year
	 * @param year
	 * @return timeframes
	 */
	public List<Timeframe> getTimeframes(int year) {
		List<Timeframe> list = new ArrayList<Timeframe>();
		boolean internshipAdded = false;
		boolean graduationAdded = false;
		
		for( int i=1; i<=4; i++ ){
			Timeframe tf = new Timeframe(year, i, 1);
			//System.out.println( "tf " + tf + " overlaps internship=" + internship + ": " + tf.overlaps(internship) );
			//System.out.println( "tf " + tf + " overlaps graduation=" + graduation + ": " + tf.overlaps(graduation) );
			if( tf.overlaps(internship) ){
				if(!internshipAdded ){
					list.add(internship);
					internshipAdded = true;
				}
			}
			else if( tf.overlaps(graduation) ){
				if(!graduationAdded ){
					list.add(graduation);
					graduationAdded = true;
				}
			}
			else list.add(tf);
		}
		Collections.sort(list);
		return list; 
	}



	/**
	 * adds a curriculum variant to the curriculum. Will use the type as key, so two variants of the same type will overwrite each other
	 * @param curriculumVariant
	 * @throws IllegalArgumentException
	 */
	public void addCurriculumVariant( CurriculumVariant curriculumVariant) throws IllegalArgumentException{
		String type = curriculumVariant.getType();
		if(! curriculumTypes.containsKey(type)) throw new IllegalArgumentException("trying to add an unknown type of curriculum variant");
		else curriculumVariants.put(type, curriculumVariant); 
	}

	/**
	 * returns whether this curriculum already contains a variant of a certain type
	 * @param curriculumType
	 * @return
	 */
	public boolean containsCurriculumType( String curriculumType ){
		if( curriculumVariants.containsKey(curriculumType)) return true;
		else return false;
	}
	
	/**
	 * returns a list of curriculum variants
	 * @return curriculumVariantsList
	 */
	public List<CurriculumVariant> getcurriculumVariants(  ){
		List<CurriculumVariant> list = new ArrayList<CurriculumVariant>();
		list.addAll(this.curriculumVariants.values());
		return list;
	}

	/**
	 * returns a curriculum variant of a given type or null if non-existing
	 * @param type
	 * @return curriculumVariant
	 */
	public CurriculumVariant getCurriculumVariant( String type ){
		return curriculumVariants.get(type);
	}
	
	
	/**
	 * returns a list of curriculum variants for a given year
	 * @param year
	 * @return curriculumYearList
	 */
	public List<CurriculumYear> getCurriculumYearVariants( int year ){
		List<CurriculumYear> list = new ArrayList<CurriculumYear>();
		for( CurriculumVariant cv : curriculumVariants.values() ){
			if(cv.containsCurriculumYear(year))	list.add(cv.getCurriculumYear(year));
		}
		return list;
	}
	
	/**
	 * adds a course method to the static collection
	 * @param method
	 */
	public static void addCourseMethod( CourseMethod method ){
		courseMethods.put(method.getId(), method);
	}
	
	/**
	 * returns a method based on its ID
	 * @param id
	 * @return courseMethod
	 */
	public static CourseMethod getCourseMethod( String id ){
		return courseMethods.get(id);
	}
	
	/**
	 * removes the given course method from the collection
	 * @param methodId the id of the method
	 */
	public static void removeCourseMethod( String methodId ){
		if(courseMethods.containsKey(methodId)) courseMethods.remove(methodId);
	}
	
	
	/**
	 * adds a legal type of curriculum (eg regular, VWO instroom etc)
	 * @param type
	 * @param description
	 */
	public static void addCurriculumType( String type, String description ){
		curriculumTypes.put(type, description);
	} 
	
	/**
	 * returns whether the given type exists as known curriculum type
	 * @param type
	 * @return type exists
	 */
	public static boolean isKnownCurriculumType(String type){
		return curriculumTypes.containsKey(type);
	}
	
	/**
	 * returns the description of the given curriculum type
	 * @param type
	 * @return description
	 */
	public static String getCurriculumTypeDescription( String type ){
		return curriculumTypes.get(type);
	}
	
	/**
	 * removes a type from the collection of valid curriculumvariant types
	 * @param variantType
	 */
	public static void removeCurriculumVariantType( String variantType ){
		curriculumTypes.remove(variantType);
	}
	
	/**
	 * removes a type from the collection of valid curriculumvariant types
	 * @param variantType
	 */
	public void removeCurriculumVariant( String variantType ){
		this.curriculumVariants.remove(variantType);
	}
	
	/**
	 * returns a list of available curriculum types
	 * @return curriculumTypes
	 */
	public static List<String> getCurriculumTypes(){
		List<String> list = new ArrayList<String>();
		list.addAll(curriculumTypes.keySet());
		Collections.sort(list);
		return list;
	}
	
	/**
	 * returns a list of available course methods sorted on name
	 * @return courseMethods
	 */
	public static List<CourseMethod> getCourseMethods(){
		List<CourseMethod> list = new ArrayList<CourseMethod>();
		list.addAll(courseMethods.values());
		Collections.sort(list);
		return list;
	}
	
	/**
	 * returns a list of lab-practicum related methods
	 * @return labMethods
	 */
	public static Set<CourseMethod> getLabMethods() {
		Set<CourseMethod> methods = new HashSet<CourseMethod>();
		for( CourseMethod method : courseMethods.values() ){
			if(method.getId().startsWith("LAB")) methods.add(method);
		}
//		methods.add( courseMethods.get("LABP") );
		return methods;
	}

	/**
	 * returns a list of theory related methods
	 * @return theoryMethods
	 */
	public static Set<CourseMethod> getTheoryMethods() {
		Set<CourseMethod> methods = new HashSet<CourseMethod>();
		for( String key : courseMethods.keySet() ){
			if( ! (key.startsWith("LAB") 
					|| key.equals("SPON")
					|| key.equals("STAG")
					|| key.equals("AFST")
					|| key.equals("STG1") ) ) methods.add( courseMethods.get(key) );
		}
		return methods;
	}
	
	/**
	 * returns a list of graduation related methods (stage & afstuderen)
	 * @return graduationMethods
	 */
	public static Set<CourseMethod> getGraduationMethods() {
		Set<CourseMethod> methods = new HashSet<CourseMethod>();
		for( String key : courseMethods.keySet() ){
			
			if( key.equals("STAG") || key.equals("STG1") || key.equals("AFST") ) methods.add( courseMethods.get(key) );
		}
		return methods;
	}
	
	/**
	 * returns a list of special educational methods (speciale ondersteuning)
	 * @return specialMethods
	 */
	public static Set<CourseMethod> getSpecialMethods() {
		Set<CourseMethod> methods = new HashSet<CourseMethod>();
		for( String key : courseMethods.keySet() ){
			if( key.equals("SPON") ) methods.add( courseMethods.get(key) );
		}
		return methods;
	}
	

	/**
	 * Convenience method to serve module selections, from all curriculum variants.
	 * Modules are sorted alphabetically according to module name,
	 * but modules starting with "praktijk" or "practicum" are given precedence
	 * @param timeframe
	 * @return list of modules of given timeframe
	 */
	public List<CurriculumModule> getModules(Timeframe tf ){
		HashSet<CurriculumModule> set = new HashSet<CurriculumModule>();
		for(CurriculumVariant variant : curriculumVariants.values() ){
			set.addAll( variant.getModules( tf ) );
		}
		List<CurriculumModule> list = new ArrayList<CurriculumModule>();
		list.addAll(set);
		Collections.sort(list, new Comparator<CurriculumModule>(){
			@Override
			public int compare(CurriculumModule arg0, CurriculumModule arg1 ) {
				if(arg0.getName().startsWith("praktijk") || arg0.getName().startsWith("practicum") ) return -1;
				else if(arg1.getName().startsWith("praktijk") || arg1.getName().startsWith("practicum") ) return 1;
				else return arg0.getName().compareTo( arg1.getName() );
			}
		});
		return list;
	}
	
	/**
	 * returns a list of all student groups attending the given timeframe. 
	 * Each studentgroup links to a module and a teacher 
	 * @param timeframe
	 * @return studentGrousList
	 */
	public List<StudentGroup> getStudentGroups( Timeframe timeframe ){
		HashSet<StudentGroup> sgSet = new HashSet<StudentGroup>();
		
		ArrayList<StudentGroup> sgList = new ArrayList<StudentGroup>();
		List<CurriculumModule>modList = getModules( timeframe );
		for( CurriculumModule m : modList ){
			sgSet.addAll( m.getStudentGroups() );
		}
		sgList.addAll(sgSet);
		Collections.sort(sgList);
		return sgList;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the curriculumID
	 */
	public String getCurriculumID() {
		return curriculumID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		for( CurriculumVariant cv : curriculumVariants.values()){
//			sb.append( "\n\t" + cv.toString() );
//		}
//		
//		return (this.getClass().getSimpleName() + "[ID=" + getCurriculumID() 
//						+ ", name="	+ getName()
//						+ ", variants= " + sb.toString()
//						+ "]" );
		return this.getCurriculumID();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((curriculumID == null) ? 0 : curriculumID.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curriculum other = (Curriculum) obj;
		if (curriculumID == null) {
			if (other.curriculumID != null)
				return false;
		} else if (!curriculumID.equals(other.curriculumID))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Curriculum o) {
		return this.getCurriculumID().compareTo(o.getCurriculumID());
	}

	
}
