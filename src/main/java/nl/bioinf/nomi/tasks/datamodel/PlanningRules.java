/**
 * 
 */
package nl.bioinf.nomi.tasks.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nl.bioinf.nomi.tasks.datamodel.Lesson.LessonType;

/**
 * This class encapsulates rules related to planning of lessons and resources.
 * 
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.0
 */
public class PlanningRules {
	/*some default rule sets*/
	private static final HashMap<String, PlanningRules> defaults = new HashMap<String, PlanningRules>();
	
	
	/*comments on this set of rules*/
	private String comments = "";
	/*the classroom requirement for the entire module*/
	private String requirements = "";
	/*a list of rules, one for each week of the TimeFrame*/
	private List<WeekRules> rules = new  ArrayList<WeekRules>();
	/*flag to indicate whether it is one of the default types*/
	private boolean defaultRules = false;


	private String name;

//	static{
//		defaults.put(arg0, arg1)
//	}
	
	public PlanningRules( int numberOfWeeks ){
		/*initialize rule set*/
		for( int i=0; i<numberOfWeeks; i++){
			rules.add(new WeekRules(i+1));
		}
	}
	public PlanningRules( int numberOfWeeks, boolean defaultPlanningRules, String name ){
		/*initialize rule set*/
		for( int i=0; i<numberOfWeeks; i++){
			rules.add(new WeekRules(i+1));
		}
		this.defaultRules = defaultPlanningRules;
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean isDefaultPlanningRules(){
		return defaultRules;
	}
	
	public static void addDefault(String defaultName, PlanningRules planningRules) {
		defaults.put(defaultName, planningRules);
	}

	public static String[] getDefaultNames() {
		return defaults.keySet().toArray(new String[defaults.size()]);
	}

	public static PlanningRules getDefault(String defaultName){
		if( defaults.containsKey(defaultName) ) return defaults.get(defaultName);
		else return null;
	}
	
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the classroomRequirement
	 */
	public String getRequirements() {
		return requirements;
	}
	/**
	 * @param requirements the classroomRequirement to set
	 */
	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}
	/**
	 * @return the rules
	 */
	public List<WeekRules> getRules() {
		return rules;
	}
	/**
	 * @param rules the rules to set
	 */
	public void setRules(List<WeekRules> rules) {
		this.rules = rules;
	}
	
	public void addLesson(int weekNumber, Lesson lesson){
		try{
			this.rules.get(weekNumber-1).addLesson(lesson);
		}catch (Exception e) {
			throw new IllegalArgumentException("error while adding lesson " + lesson + " to week " + weekNumber + "; cause=" + e.getMessage() );
		}
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		for( WeekRules r : rules){
			sb.append( "\t" + r.toString() + "\n");
		}
		return this.getClass().getSimpleName() 
			+ "[number of weeks=" + rules.size() 
			+ ", requirements=" + requirements
			+ ", comments=" + comments
			+ ", rules=\n" + sb.toString() + "]";
	}

	public PlanningRules clone(){
		PlanningRules clone = new PlanningRules(this.getRules().size());
		clone.setComments(this.getComments());
		clone.setRequirements(this.getRequirements());
		for( int wn=0; wn<this.getRules().size(); wn++){
			WeekRules wr = this.getRules().get(wn);
			for( Lesson l : wr.getLessons() ){
				clone.addLesson(wn+1, l.clone());
			}
		}
		return clone;
	}


	/**
	 * This class encapsulates a single week-planning rule
	 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
	 * @version 0.0
	 */
	public class WeekRules{
		private int weekNumber;

		private List<Lesson> lessons;
		
		/**
		 * constructs with the lesson week number
		 * @param weekNumber
		 */
		public WeekRules(int weekNumber){
			lessons = new ArrayList<Lesson>();
			this.weekNumber=weekNumber;
		}
		
		/**
		 * @return the lessons
		 */
		public List<Lesson> getLessons() {
			return lessons;
		}
		/**
		 * adds a single lesson to the WeekRules
		 * @param lesson
		 */
		public void addLesson(Lesson lesson){
			this.lessons.add(lesson);
		}
		/**
		 * @return the weekNumber
		 */
		public int getWeekNumber() {
			return weekNumber;
		}
		
		public String toString(){
			return this.getClass().getSimpleName() 
				+ "[weekNumber=" + weekNumber + ", lessons=" + lessons + "]";
		}
		
		/**
		 * for single-cell excel sheet reporting
		 * @return weekrulesString */
		public String getWeekRulesString(){
			StringBuilder sb = new StringBuilder();
			for(Lesson l : this.lessons){
				sb.append(l.getType());
				if( l.getDuration() != Lesson.STANDARD_DURATION ){
					sb.append("(");
					sb.append(l.getDuration());
					sb.append(")h");
				}
				if( l.getComments().length() != 0 ){
					sb.append("[");
					sb.append(l.getComments());
					sb.append("]");
				}
				sb.append(" | ");
			}
			if(sb.length()>3)sb.delete(sb.length()-3, sb.length());
			return sb.toString();
		}
	}
	public static void main(String[] args){
		System.out.println("testing PlanningRules");
		
		PlanningRules pr = new PlanningRules(10);
		Lesson l;
		l = new Lesson(LessonType.LABORATORY, 4);
		pr.addLesson( 1, l );
		System.out.println(pr);
	}

}
