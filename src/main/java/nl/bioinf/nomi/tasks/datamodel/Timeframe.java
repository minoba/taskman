package nl.bioinf.nomi.tasks.datamodel;

import java.util.Arrays;

/**
 * This class encapsulates a timeframe for a given task/activity: - year, duration and quarter - non-equal partitioning
 * of given hours, in percentages
 *
 * @author Cellingo
 */
public class Timeframe implements Comparable<Timeframe> {
    /*year - if relevant*/

    private int year = -1;
    /*the start quarter*/
    private int quarter;
    /*the duration in quarters*/
    private int duration = 1;
    /*the partitioning of the hours over the quarters */
    private double[] partitioning;

    /**
     * constructs with start quarter and duration value. If year is not set or set to -1, the timeframe is assumed to be
     * year-independent
     *
     * @param startQuarter
     * @param duration
     */
    public Timeframe(int startQuarter, int duration) {
        this.quarter = startQuarter;
        this.duration = duration;
    }

    /**
     * constructs with start quarter, duration, and partitioning of hours over the given quarters
     *
     * @param startQuarter
     * @param duration
     * @param partitioning
     */
    public Timeframe(int startQuarter, int duration, int[] partitioning) {
        this.quarter = startQuarter;
        this.duration = duration;
        assert partitioning.length == duration : "timeframe partitioning element number (" + Arrays.toString(partitioning) + ") does not equal the duration (" + duration + ")";
        assert sumOfPartitioningOk(partitioning) : "timeframe partitioning (" + Arrays.toString(partitioning) + ") does not add up to 100";
        setPartitioning(partitioning);
    }

    /**
     * sets the partitioning definition of the this timeframe in percentages. Will be converted to fractions here
     *
     * @param partitioningPercentages
     */
    public void setPartitioning(int[] partitioningPercentages) {
        //System.out.println( "[" + this.getClass().getSimpleName() + "] processing timeframe partitioning elements (" + Arrays.toString(partitioningPercentages) );
        this.partitioning = new double[4];

        /*sets quarters that are defined*/
        for (int i = 0; i < partitioningPercentages.length; i++) {
            int pp = partitioningPercentages[i];
            //System.out.println("quarter " + (quarter+i) + " has % " + pp );
            partitioning[quarter - 1 + i] = (double) pp / 100;
            //System.out.println("after: setting quarter " + (quarter+i) + " to " + partitioning[quarter-1+i]);
        }
        //System.out.println("[" + this.getClass().getSimpleName() + "] processed timeframe partitioning elements (" + Arrays.toString(partitioning) );
    }

    /**
     * returns the partitioning over the quarters
     *
     * @return partitioning
     */
    public double[] getPartitioning() {
        return this.partitioning;
    }

    /**
     * constructs with year and quarter value
     *
     * @param year
     * @param quarter
     * @param duration in quarters
     */
    public Timeframe(int year, int quarter, int duration) {
        this.year = year;
        this.quarter = quarter;
        this.duration = duration;
    }

    /**
     * constructs with overall quarter number (1-16). Duration defaults to 1
     *
     * @param overallQuarterNumber
     */
    public Timeframe(int overallQuarterNumber) {
        this.year = (int) Math.ceil((double) overallQuarterNumber / 4);
        this.quarter = (overallQuarterNumber % 4 == 0 ? 4 : overallQuarterNumber % 4);
    }

    /**
     * tests whether the parts add up to 100%
     *
     * @param partitioning
     * @return sum is 100
     */
    private boolean sumOfPartitioningOk(int[] partitioning) {
        int total = 0;
        for (int i : partitioning) {
            total += i;
        }
        return total == 100;
    }

    /**
     * the methods returns the number of hours (rounded) for
     *
     * @param quarter
     * @param taskHours
     * @return hours
     */
    public int getQuarterHours(int quarter, int taskHours) {
        if (this.quarter > quarter || (this.quarter + this.duration - 1) < quarter) {
            return 0;
        } else {
            if (this.partitioning == null) {
                return (int) (Math.round((double) taskHours / this.duration));
            } else {
                return (int) (Math.round((double) taskHours * partitioning[quarter - 1]));
            }
        }
    }

    /**
     * the methods returns the raw number of hours (unrounded) for a given quarter
     *
     * @param quarter
     * @param taskHours
     * @return hours
     */
    public double getQuarterHoursRaw(int quarter, int taskHours) {
        if (this.quarter > quarter || (this.quarter + this.duration - 1) < quarter) {
            return 0;
        } else {
            if (this.partitioning == null) {
                return ((double)taskHours) / (double)this.duration;
                //return (int) (Math.round((double) taskHours / this.duration));
            } else {
                return ((double)taskHours) * (double)partitioning[quarter - 1];
                //return (int) (Math.round((double) taskHours * partitioning[quarter - 1]));
            }
        }
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @return the quarter
     */
    public int getQuarter() {
        return quarter;
    }

    /**
     * returns the duration (in quarters) of this timeframe
     *
     * @return duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * returns the overall quarter number (1-16 if year is specified or 1-4 if not),
     *
     * @return overallQuarterNumber
     */
    public int getOverallQuarterNumber() {
        if (year == -1) {
            return getQuarter();
        } else {
            return ((getYear() - 1) * 4) + getQuarter();
        }
    }

    /**
     * returns true if these pairs overlap
     *
     * @param otherTimeframe
     * @return
     */
    public boolean overlaps(Timeframe otherTimeframe) {
        if (this.getYear() == -1 || otherTimeframe.getYear() == -1) {
            /*year-independent*/
            if (this.getQuarter() == otherTimeframe.getQuarter()) {
                return true;
            } else if (this.getQuarter() < otherTimeframe.getQuarter()) {
                return (this.getQuarter() + (getDuration() - 1) >= otherTimeframe.getQuarter());
            } else {
                return (otherTimeframe.getQuarter() + (otherTimeframe.getDuration() - 1) >= this.getQuarter());
            }
        } else {
            /*year-dependent*/
            if (this.getOverallQuarterNumber() == otherTimeframe.getOverallQuarterNumber()) {
                return true;
            } else if (this.getOverallQuarterNumber() < otherTimeframe.getOverallQuarterNumber()) {
                return (this.getOverallQuarterNumber() + (this.getDuration() - 1) >= otherTimeframe.getOverallQuarterNumber());
            } else {
                return (otherTimeframe.getOverallQuarterNumber() + (otherTimeframe.getDuration() - 1) >= this.getOverallQuarterNumber());
            }
        }
    }

    @Override
    public int compareTo(Timeframe other) {
        /*primary sort: year; secondary sort: quarter*/
        return (this.getOverallQuarterNumber() - other.getOverallQuarterNumber());
    }

    @Override
    public String toString() {
        return (this.getClass().getSimpleName()
                + "[year=" + this.getYear()
                + " quarter=" + this.getQuarter()
                + " quarter number=" + getOverallQuarterNumber()
                + " duration=" + getDuration() + "]");
    }

}
