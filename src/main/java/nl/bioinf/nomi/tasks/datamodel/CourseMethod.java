/**
 * 
 */
package nl.bioinf.nomi.tasks.datamodel;

import java.io.Serializable;

/**
 * This class encapsulates the course method (werkvorm): the way the contact hours are given.   
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.2
 */
public class CourseMethod  implements Serializable, Comparable<CourseMethod>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6360754817687469210L;
	
	/**
	 * the ID
	 */
	private String id;
	/**
	 * the name of this method
	 */
	private String name;
	/**
	 * the maximum number of students in a single group
	 */
	private int groupSize;
	/**
	 * all the time factors involved in the total
	 */
	private TimeAssignmentFactors timeFactors;
	
	/**
	 * construct with all necessary parameters
	 * @param id
	 * @param name
	 * @param groupSize
	 * @param timeFactors
	 */
	public CourseMethod( String id, String name, int groupSize, TimeAssignmentFactors timeFactors){
		this.id = id;
		this.name = name;
		this.groupSize = groupSize;
		this.timeFactors = timeFactors;
	}

	/**
	 * returns the number of hours te be assigned for a course with the given number of students and credits
	 * @param studentGroup
	 */
	public int getAssignableHours( StudentGroup studentGroup ){
		int teacherGroupNumber = 0;
		
		//System.out.print( this.getClass().getSimpleName() + " empl=" + studentGroup.getEmployee().getEmployeeId() + "; "); 
		
		for( StudentGroup sg : studentGroup.getCurriculumModule().getStudentGroups() ){
			//System.out.print( this.getClass().getSimpleName() + " test_empl=" + sg.getEmployee().getEmployeeId() ); 
			if( sg.getEmployee().getEmployeeId().equals(studentGroup.getEmployee().getEmployeeId() ) ) teacherGroupNumber++;
		}
		//System.out.print("\n");
		int newHours = (int)Math.round(timeFactors.getTotalTime(studentGroup.getStudentNumber(), studentGroup.getCurriculumModule().getCredits(), teacherGroupNumber));
		
//		System.out.println( this.getClass().getSimpleName() 
//				+ " getAssignableHours for module=" 
//				+ studentGroup.getCurriculumModule().getName()
//				+ " groep=" + studentGroup.getNumber() 
//				+ " werkvorm=" + studentGroup.getCurriculumModule().getCourseMethod().getId()
//				+ " teacher=" + studentGroup.getEmployee().getEmployeeId() 
//				+ " total groups=" + teacherGroupNumber 
//				+ " new hours=" + newHours);
		return newHours;
	}


	/**
	 * returns the time assignment factors object
	 * @return timeFactors
	 */
	public TimeAssignmentFactors getTimeAssignmentFactors(){
		return timeFactors;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the groupSize
	 */
	public int getGroupSize() {
		return groupSize;
	}

	/**
	 * @param groupSize the groupSize to set
	 */
	public void setGroupSize(int groupSize) {
		this.groupSize = groupSize;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (this.getClass().getSimpleName() + "[" + getId() + ", name=" + getName() + "]" );
	}

	@Override
	public int compareTo( CourseMethod other ) {
		return this.getName().compareTo( other.getName() );
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CourseMethod other = (CourseMethod) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	

}
