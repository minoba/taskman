/**
 *
 */
package nl.bioinf.nomi.tasks.datamodel;

/**
 * @author Cellingo
 *
 */
public class RegularTask implements Task, Comparable<RegularTask> {
    private final int taskId;
    private Employee employee;
    private int taskHours;
    private Timeframe timeframe;
    private String name = "UNNAMED TASK";
//    private boolean hboHoursInclusive = true;
    private TaskLabel taskLabel = TaskLabel.DIVERSE_NOT_CURRICULUM_BOUND;

    /**
     * construct with task ID and parent project. The timeframe defaults to 1 year;
     *
     * @param taskID the task ID
     * @param hours the hours
     */
    public RegularTask(final int taskID, final int hours) {
        this.taskId = taskID;
        this.taskHours = hours;
        this.timeframe = new Timeframe(-1, 1, 4);
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(final Employee employee) {
        this.employee = employee;
    }

    /**
     * @return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    @Override
    public TaskCategory getTaskCategory() {
        return this.taskLabel.getTaskCategory();
    }

    @Override
    public void setTaskLabel(final TaskLabel taskLabel) {
        this.taskLabel = taskLabel;
    }

    @Override
    public TaskLabel getTaskLabel() {
        return this.taskLabel;
    }


    /**
     * @return the taskId
     */
    public int getTaskId() {
        return taskId;
    }

    /**
     * @param taskHours the taskHours to set
     */
    public void setTaskHours(final int taskHours) {
        this.taskHours = taskHours;
    }

    /**
     * @param timeframe the timeframe to set
     */
    public void setTimeframe(final Timeframe timeframe) {
        this.timeframe = timeframe;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see nl.bioinf.nomi.tasks.datamodel.Task#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see nl.bioinf.nomi.tasks.datamodel.Task#getQuarterHours(int)
     */
    @Override
    public int getQuarterHours(final int quarter) {
        return getTimeframe().getQuarterHours(quarter, getTaskHours());
    }

    @Override
    public double getQuarterHoursRaw(final int quarter) {
        return this.timeframe.getQuarterHoursRaw(quarter, taskHours);
    }

    @Override
    public int getTaskHours() {
        return taskHours;
    }

    @Override
    public Timeframe getTimeframe() {
        return timeframe;
    }

    @Override
    public boolean isCurriculumBound() {
        return getTaskCategory() == TaskCategory.CURRICULUM_BOUND;
    }

//    /**
//     * @param hboHoursInclusive the hboHoursInclusive to set
//     */
//    public void setHboHoursInclusive(final boolean hboHoursInclusive) {
//        this.hboHoursInclusive = hboHoursInclusive;
//    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + " [id=" + this.taskId
                + " employee=" + employee.getEmployeeId()
                + " hours=" + taskHours
                + " cat=" + taskLabel + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + taskId;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RegularTask other = (RegularTask) obj;
        return taskId == other.taskId;
    }

    @Override
    public int compareTo(final RegularTask other) {
        if (this.taskLabel == other.getTaskLabel()) {
            return this.name.compareTo(other.getName());
        } else {
            return this.getName().compareTo(other.getName());
        }
    }
}
