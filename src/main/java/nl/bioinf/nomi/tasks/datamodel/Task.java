/**
 *
 */
package nl.bioinf.nomi.tasks.datamodel;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public interface Task {

    /**
     * returns task name.
     *
     * @return taskName the name
     */
    String getName();

    /**
     * returns assigned hours for task.
     *
     * @return taskHours the hours
     */
    int getTaskHours();

    /**
     * returns the number of hours per quarter for this task.
     *
     * @param quarter the quarter (1-4)
     * @return quarterHours the hours
     */
    int getQuarterHours(int quarter);

    /**
     * returns the raw number of hours per quarter for this task.
     *
     * @param quarter the quarter (1-4)
     * @return quarterHours the hours
     */
    double getQuarterHoursRaw(int quarter);

    /**
     * indicates whether the task is curriculum bound --> replaces isHboHoursInclusive
     * @return curriculumBound
     */
    boolean isCurriculumBound();


    /**
     * returns Timeframe of the task.
     *
     * @return timeframe the time frame
     */
    Timeframe getTimeframe();

    /**
     * returns the category of this task.
     * @return taskCategory the task category
     */
    TaskCategory getTaskCategory();

    /**
     * sets the task category.
     * @param taskLabel the task label
     */
    void setTaskLabel(TaskLabel taskLabel);

    /**
     * returns the label of this task.
     * @return taskLbel the task label
     */
    TaskLabel getTaskLabel();
}
