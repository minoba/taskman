package nl.bioinf.nomi.tasks.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Cellingo
 *
 */
public enum TaskCategory {

    PROFESSIONALIZATION("Professionalisering"),
    /**
     * Curriculum gebonden taken: onderwijs uitvoering, ontwikkeling & SLB.
     */
    CURRICULUM_BOUND("Curriculum gebonden"),
    /**
     * Niet curriculum gebonden taken: commissies, coordinatie etc., ten behoeve van onderwijs.
     */
    NOT_CURRICULUM_BOUND("Niet curriculum gebonden"),
    /**
     * Bedrijfsvoering en ondersteuning: management & administratie.
     */
    MANAGEMENT_AND_SUPPORT("Bedrijfsvoering en ondersteuning"),
    /**
     * Andere uren: seniorenuren, ziekte, zwangerschap etc.
     */
    OTHER("Andere uren"),
    /**
     * Projecten.
     */
    PROJECTS("Projecten"),
    /**
     * Inzet t.l.v. andere organisatieonderdelen: detachering.
     */
    OUTPLACEMENT("Inzet t.l.v. andere organisatieonderdelen");
//
//    ADMINISTRATIVE("administratief"),
//    ORGANISATIONAL("organisatorisch"),
//    PR("PR"),
//    EDUCATIONAL ("onderwijs gerelateerd"),
//    RESEARCH("onderzoek"),
//    TRAINING("scholing"),
//    OTHER("niet gespecificeerd");

    /**
     * this construct is needed for prioritized class loading.
     * Otherwise LABELS will be filled with null values for all TaskLabel ionstances.
     */
    private static class InitializerClass {

        /**
         * the labels mapping.
         */
        private static final Map<TaskCategory, List<TaskLabel>> LABELS = new HashMap<>();

        static {
            /*
             * fill map with lists of labels belonging to each category.
             */
            List<TaskLabel> l = new ArrayList<>();
            l.add(TaskLabel.CURRICULUM_DEVELOPMENT);
            l.add(TaskLabel.CURRICULUM_EXECUTION);
            l.add(TaskLabel.STUDY_GUIDANCE);
            LABELS.put(CURRICULUM_BOUND, l);

            l = new ArrayList<>();
            l.add(TaskLabel.COMMITTEES);
            l.add(TaskLabel.EDUCATION_COORDINATION);
            l.add(TaskLabel.INTERNATIONALIZATION);
            l.add(TaskLabel.ICT);
            l.add(TaskLabel.PR_MARKETING);
            l.add(TaskLabel.DIVERSE_NOT_CURRICULUM_BOUND);
            l.add(TaskLabel.ACCREDITATION_RELATED_ACTIVITIES);
            LABELS.put(NOT_CURRICULUM_BOUND, l);

            l = new ArrayList<>();
            l.add(TaskLabel.MANAGEMENT);
            l.add(TaskLabel.ADMIN_FRONT_OFFICE);
            l.add(TaskLabel.PLANNING_CONTROL);
            LABELS.put(MANAGEMENT_AND_SUPPORT, l);

            l = new ArrayList<>();
            l.add(TaskLabel.SUSTAINABLE_EMPLOYABILITY);
            l.add(TaskLabel.SICK_LEAVE_SUBSTITUTE);
            l.add(TaskLabel.INDIRECT_HOURS);
            l.add(TaskLabel.EXPERTISE_DEVELOPMENT);
            l.add(TaskLabel.PARENTAL_LEAVE);
            LABELS.put(OTHER, l);

            l = new ArrayList<>();
            l.add(TaskLabel.INTERNAL_PROJECTS_REGISTERED);
            l.add(TaskLabel.INTERNAL_PROJECTS);
            l.add(TaskLabel.KNOWLEDGE_CENTER);
            l.add(TaskLabel.OTHER_EXTERNAL_PROJECTS);
            l.add(TaskLabel.CONTRACT_ACTIVITIES);
            l.add(TaskLabel.QUALITY_ASSURANCE_FUNDS);
            LABELS.put(PROJECTS, l);

            l = new ArrayList<>();
            l.add(TaskLabel.MATERNITY_LEAVE);
            l.add(TaskLabel.HANZE_CONNECT);
            l.add(TaskLabel.OTHER_DEPARTMENTS);
            l.add(TaskLabel.EXTERNAL_OUTPLACEMENT);
            l.add(TaskLabel.UNPAID_LEAVE);
            LABELS.put(OUTPLACEMENT, l);

            l = new ArrayList<>();
            l.add(TaskLabel.INDIVIDUAL_PROFESSIONAL_DEVELOPMENT);
            l.add(TaskLabel.COMMUNAL_PROFESSIONAL_DEVELOPMENT);
            LABELS.put(PROFESSIONALIZATION, l);
        }
    }

    /**
     * the type as Dutch string.
     */
    private final String type;

    /**
     *
     * @param typeArg the type
     */
    TaskCategory(final String typeArg) {
        this.type = typeArg;
    }

    @Override
    public String toString() {
        return type;
    }

    /**
     * returns the TaskLabels associated with the Category.
     *
     * @return taskLabels the task labels of this category
     */
    public List<TaskLabel> getTaskLabels() {
        return InitializerClass.LABELS.get(this);
    }
}
