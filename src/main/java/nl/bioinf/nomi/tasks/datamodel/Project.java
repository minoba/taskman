/**
 * 
 */
package nl.bioinf.nomi.tasks.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.2
 */
public class Project implements Serializable, Comparable<Project> {

	/**
	 * project ID
	 */
	private String projectId;
	/**
	 * project name
	 */
	private String name;
	/**
	 * totals hours available for the project
	 */
	private int totalHours;
	/**
	 * project description
	 */
	private String description;
	/**
	 * task collection: map of taskID - ProjectTask objects
	 */
	private HashMap<Integer, ProjectTask> tasks;
	
	/**
	 * constructs with ID and name
	 * @param name
	 */
	public Project(String id, String name){
		tasks = new HashMap<Integer, ProjectTask>();
		this.projectId = id;
		this.name = name;
	}
	
	
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * returns whether this projetc contains the given task
	 * @return task is present
	 */
	public boolean containsTask( ProjectTask task ){
		if( tasks.containsKey( task.getProjectTaskId() ) ) return true;
		return false;
	}
	
	/**
	 * Returns a new TaskId for a project: the first integer available
	 * @return newTaskId
	 */
	public int getNewTaskId() {
		for( int i=1; i<Integer.MAX_VALUE; i++){
			if(! tasks.containsKey(i) ) return i;
		}
		return Integer.MAX_VALUE;
	}

	/**
	 * adds a ProjectTask to this project. If the task already exists, 
	 * it is replaced with the new one
	 * @param task
	 */
	public void addTask(ProjectTask task){
		//System.out.println( "Project " + this.toString() + " adding task " + task.toString()  );
		//if(tasks.containsKey(task.getProjectTaskId() ) ) System.out.println( "Projecttask exists already"  );
		tasks.put( task.getProjectTaskId(), task);
	}
	
	/**
	 * removes the given task from the project
	 * @param task
	 */
	public void removeTask(ProjectTask task){
		tasks.remove(task.getProjectTaskId() );
	}

	/**
	 * returns a list of the tasks in this Project
	 * @return taskList
	 */
	public List<ProjectTask> getTasks(){
		List<ProjectTask> list = new ArrayList<ProjectTask>();
		list.addAll(tasks.values());
		Collections.sort(list);
		return list;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the total number of hours available for this project
	 * @return the totalHours
	 */
	public int getTotalHours() {
		return totalHours;
	}

	/**
	 * Sets the total number of hours available for this project
	 * @param totalHours the totalHours to set
	 */
	public void setTotalHours(int totalHours) {
		this.totalHours = totalHours;
	}

	/**
	 * @return the assignedHours
	 */
	public int getAssignedHours() {
		int assignedHours = 0;
		for( ProjectTask task : tasks.values() ){
			assignedHours += task.getTaskHours();
		}
		return assignedHours;
	}

	/**
	 * returns the cost of the assigned hours calculated depending on the employee's gpl
	 * @return assignedHoursCost
	 */
	public double getAssignedHoursCost(){
		double total = 0;
		for( ProjectTask t : this.tasks.values()){
			total += ( t.getTaskHours() * Employee.getSalaryCosts( t.getEmployee().getFunctionScale() ) );
		}
		return total;
	}
	
	/**
	 * returns the remaining hours left for this project
	 * @return remainingHours
	 */
	public int getAssignableHours(){
		return (this.totalHours - getAssignedHours() );
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (this.getClass().getSimpleName() + "[name=" + getName() 
				+ ", size=" + getTotalHours()
				+ " hours, remaining=" + getAssignableHours() 
				+ " hours, task count=" + this.tasks.size() 
				+ "]" );
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Project o) {
		return this.getName().compareTo(o.getName());
	}


}
