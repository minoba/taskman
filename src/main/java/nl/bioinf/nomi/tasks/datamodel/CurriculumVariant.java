package nl.bioinf.nomi.tasks.datamodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class CurriculumVariant {
	/**
	 * the type of this variant (eg regulier)
	 */
	private String type;
	/**
	 * the id of this variant
	 */
//	private String id;
	/**
	 * the name of this variant
	 */
//	private String name;
	/**
	 * the containing curriculum
	 */
	private Curriculum curriculum;
	
	/**
	 * an array holding all curriculum years
	 */
	private HashMap<Integer, CurriculumYear> years;
	
	/**
	 * construct with relevant data
	 * @param curriculum the parent curriculum
	 * @param type the type of variant
	 */
	public CurriculumVariant(Curriculum curriculum, String type ) {
		super();
		this.curriculum = curriculum;
		this.type = type;
//		this.id = id;
//		this.name = name;
		this.years = new HashMap<Integer, CurriculumYear>();
	}

	/**
	 * @return the curriculum
	 */
	public Curriculum getCurriculum() {
		return curriculum;
	}

	/**
	 * adds a curriculum year to the curriculum variant. 
	 * @param curriculumYear
	 */
	public void addCurriculumYear( CurriculumYear curriculumYear ){
		//System.out.println( "CurriculumVariant: adding a curriculum year " + curriculumYear.toString() );
		this.years.put(curriculumYear.getYear(), curriculumYear);
	}

	/**
	 * removes the given curriculum year from the variant
	 * @param curriculumYear
	 */
	public void removeCurriculumYear(CurriculumYear curriculumYear) {
		years.remove(curriculumYear.getYear() );
	}

	/**
	 * returns the CurriculumYear of a given year
	 * @param year
	 * @return curriculumYear
	 */
	public CurriculumYear getCurriculumYear( int year ){
		return years.get(year);
	}
	
	/**
	 * returns a list of curriculum years for this variant, sorted on year
	 * @return curriculumYears
	 */
	public List<CurriculumYear> getCurriculumYears(){
		List<CurriculumYear> list = new ArrayList<CurriculumYear>();
		for( int y=1; y<=4; y++ ){
			if( years.containsKey(y) ) list.add(years.get(y) );
		}
		return list;
	}
	
	/**
	 * returns whether this variant has the given year defined
	 * @param year
	 * @return year is defined
	 */
	public boolean containsCurriculumYear( int year ){
		return years.containsKey(year);
	}
	
	/**
	 * Convenience method to serve module selections, from all curriculum variants.
	 * Modules are sorted alphabetically according to module name,
	 * but modules starting with "praktijk" or "practicum" are given precedence
	 * @param timeframe
	 * @return list of modules of given timeframe
	 */
	public List<CurriculumModule> getModules(Timeframe timeframe ){
		List<CurriculumModule> list = new ArrayList<CurriculumModule>();
		if( timeframe.getYear() < 0 ){
			/*year-independent*/
			//System.out.println(this.getClass().getSimpleName() + " CV: " + this.getName() + "; retrieving modules year-independent");
			for( int y=1; y<5; y++ ){
				//System.out.println(this.getClass().getSimpleName() + " retrieving modules for year " + y);
				if(years.containsKey(y)){
					//System.out.println(this.getClass().getSimpleName() + " variant contains modules for year " + y);
					for( int i=timeframe.getQuarter(); i<(timeframe.getQuarter()+timeframe.getDuration()); i++ ){
						list.addAll( years.get(y).getModules(i) );
					}
				}
			}
		}
		if(years.containsKey(timeframe.getYear())){
			for( int i=timeframe.getQuarter(); i<(timeframe.getQuarter()+timeframe.getDuration()); i++ ){
				list.addAll( years.get(timeframe.getYear()).getModules(i) );
			}
		}
		Collections.sort(list, new Comparator<CurriculumModule>(){
			@Override
			public int compare(CurriculumModule arg0, CurriculumModule arg1 ) {
				if(arg0.getName().startsWith("praktijk") || arg0.getName().startsWith("practicum") ) return -1;
				else if(arg1.getName().startsWith("praktijk") || arg1.getName().startsWith("practicum") ) return 1;
				else return arg0.getName().compareTo( arg1.getName() );
			}
		});
		return list;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the name
	 */
	public String getName(){
		return curriculum.getCurriculumID() + " " + Curriculum.getCurriculumTypeDescription( this.type );
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		for( CurriculumYear cy : years.values() ){
//			sb.append( "\n\t\t" + cy.toString() );
//		}
//		
//		return (this.getClass().getSimpleName() + "[ID=" + getId() 
//				+ ", name="	+ getName()
//				+ ", type="	+ getType()
//				+ ", years= " + sb.toString()
//				+ "]" );
		
		return ( getCurriculum().getCurriculumID() + "-" + getType() );  //this.getName();
	}

	
	
}
