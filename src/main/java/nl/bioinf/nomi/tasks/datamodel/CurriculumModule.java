package nl.bioinf.nomi.tasks.datamodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.2
 */
public class CurriculumModule implements Comparable<CurriculumModule> {

    /**
     * the ID of the module (progress code).
     */
    private final String moduleId;
    /**
     * the timeframe of this module (quarter, duration).
     */
    private Timeframe timeframe;
    /**
     * the number of EC credits of for this module.
     */
    private double credits;
    /**
     * the description of this module.
     */
    private String name;
    /**
     * the course method.
     */
    private CourseMethod method;
    /**
     * flag to indicate wether the number of groups is locked.
     */
    private boolean groupNumberLocked = false;
    /**
     * the locked number of groups to use.
     */
    private int lockedGroupNumber;
    /**
     * the collection of groups associated with this module.
     */
    private final HashMap<Integer, StudentGroup> groups;
    /**
     * lists the attending classes for this module.
     */
    private final HashSet<CurriculumYear> attendingClasses;
    /**
     * the planning rules.
     */
    private PlanningRules planningRules;
    /**
     * the module is part of the SLB program.
     */
    private boolean slb;

    /**
     *
     * @param moduleId the progress code
     * @param credits the number of credits assigned for the module
     * @param timeframe the timeframe in which the module runs (starts)
     * @param name the name of the module
     * @param method the CourseMethod used for this module
     */
    public CurriculumModule(final String moduleId,
                            final double credits,
                            final Timeframe timeframe,
                            final String name,
                            final CourseMethod method) {
        super();
        this.moduleId = moduleId;
        this.credits = credits;
        this.timeframe = timeframe;
        this.name = name;
        this.method = method;
        this.groups = new HashMap<Integer, StudentGroup>();
        this.attendingClasses = new HashSet<CurriculumYear>();
    }

    /**
     *
     * @param moduleId the module ID
     * @param credits the credits
     * @param timeframe the timeframe
     * @param name the name
     * @param method the method
     * @param groupNumberLocked is group number locked
     * @param lockedGroupNumber groupnumber in lockled status
     */
    public CurriculumModule(final String moduleId,
                            final double credits,
                            final Timeframe timeframe,
                            final String name,
                            final CourseMethod method,
                            final boolean groupNumberLocked,
                            final int lockedGroupNumber) {
        super();
        this.moduleId = moduleId;
        this.credits = credits;
        this.timeframe = timeframe;
        this.name = name;
        this.method = method;
        this.groups = new HashMap<Integer, StudentGroup>();
        this.attendingClasses = new HashSet<CurriculumYear>();
        this.groupNumberLocked = groupNumberLocked;
        this.lockedGroupNumber = lockedGroupNumber;
    }

    /**
     * adds an attending class to this module.
     *
     * @param curriculumYear the curriculum year
     */
    public void addAttendingClass(final CurriculumYear curriculumYear) {
        attendingClasses.add(curriculumYear);
    }

    /**
     * removes an attending class from this module.
     *
     * @param curriculumYear the curriculum year to remove from module
     */
    public void removeAttendingClass(final CurriculumYear curriculumYear) {
        attendingClasses.remove(curriculumYear);
    }

    /**
     * Has to be called whenever the size of an attending class has changed. Will cause a redistribution of students
     * over groups and the creation of new Vacancy student groups if necessary. If groups have to be removed,
     * RequiredAction.REMOVE_GROUPS will be returned
     *
     * @return requiredAction
     */
    public RequiredAction classSizeChanged() {
        if (this.getAttendingStudents() == 0) {
            for (StudentGroup sg : this.groups.values()) {
                sg.getEmployee().removeStudentGroup(sg);
            }
            groups.clear();
            return RequiredAction.NONE;
        }
        /*hours recalculation is necessary and possibly groups recalculation*/
        if ((this.groups.size() > 1)
                && (this.getAttendingStudents()
                <= (this.getCourseMethod().getGroupSize() * (this.groups.size() - 1)))) {
            /*there are too many groups for the new method*/
            return RequiredAction.REMOVE_GROUPS;
        } else if (this.getAttendingStudents() > (this.getCourseMethod().getGroupSize() * this.groups.size())) {
            this.addVacantStudentGroups();
            return RequiredAction.NONE;
        } else {
            redistributeAttendingStudents();
            return RequiredAction.NONE;
        }
    }

    /**
     * returns a list of attending classes for this module.
     *
     * @return classList
     */
    public List<CurriculumYear> getAttendingClasses() {
        ArrayList<CurriculumYear> list = new ArrayList<CurriculumYear>();
        list.addAll(attendingClasses);
        return list;
    }

    /**
     * .
     * returns a String representation of the attending classes of this module
     *
     * @return attendingClassesString
     */
    public String getAttendingClassesString() {
        String cl = "";
        List<CurriculumYear> classes = getAttendingClasses();
        for (CurriculumYear cy : classes) {
            cl += (cy.getCurriculumVariant().getCurriculum().getCurriculumID()
                    + cy.getCurriculumVariant().getType() + cy.getYear() + ", ");
        }
        if (cl.length() > 2) {
            cl = cl.substring(0, cl.length() - 2);
        }
        return cl;
    }

    /**
     *
     * @return lecturingEmployees as string representation
     */
    public String getLecturingEmployeesString() {
        HashMap<String, Integer> employeeGroupCount = new HashMap<String, Integer>();
        for (StudentGroup sg : getStudentGroups()) {
            if (employeeGroupCount.containsKey(sg.getEmployee().getEmployeeId())) {
                employeeGroupCount.put(
                        sg.getEmployee().getEmployeeId(),
                        employeeGroupCount.get(sg.getEmployee().getEmployeeId()) + 1);
            } else {
                employeeGroupCount.put(sg.getEmployee().getEmployeeId(), 1);
            }
        }
        StringBuilder sb = new StringBuilder();
        List<String> empIDs = new ArrayList<String>();
        empIDs.addAll(employeeGroupCount.keySet());
        Collections.sort(empIDs);
        for (String id : empIDs) {
            if (employeeGroupCount.get(id) == 1) {
                sb.append(id);
            } else {
                sb.append(id);
                sb.append("(");
                sb.append(employeeGroupCount.get(id));
                sb.append(")");
            }
            sb.append(",");
        }
        if (sb.length() > 1) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * returns the total of students over all attending classes.
     *
     * @return attendingStudents
     */
    public int getAttendingStudents() {
        int total = 0;
        for (CurriculumYear cy : attendingClasses) {
            total += cy.getStudentNumber(this.getTimeframe().getQuarter());
        }
        return total;
    }

    /**
     * adds a student group to this module.
     *
     * @param group the student group to add
     */
    public void addStudentGroup(final StudentGroup group) {
        groups.put(group.getGroupId(), group);
        //System.out.println(getClass().getSimpleName() + ": groups size=" + groups.size());
        recalculateEmployeeHours();
    }

    /**
     * @return the groupNumberLocked
     */
    public boolean isGroupNumberLocked() {
        return groupNumberLocked;
    }

    /**
     * @param groupNumberLocked the groupNumberLocked to set
     */
    public void setGroupNumberLocked(final boolean groupNumberLocked) {
        this.groupNumberLocked = groupNumberLocked;
    }

    /**
     * @return the lockedGroupNumber
     */
    public int getLockedGroupNumber() {
        return lockedGroupNumber;
    }

    /**
     * @param lockedGroupNumber the lockedGroupNumber to set
     * @return data changed - view should be updated
     */
    public RequiredAction setLockedGroupNumber(final int lockedGroupNumber) {
        //System.out.println("setting locked group number to " + lockedGroupNumber);
        this.lockedGroupNumber = lockedGroupNumber;
        if (this.groupNumberLocked && this.lockedGroupNumber != this.getGroupNumber()) {

            if (this.getGroupNumber() < this.lockedGroupNumber) {
                //addVacantStudentGroups();
                return RequiredAction.ADD_GROUPS;
            } else {
                return RequiredAction.REMOVE_GROUPS;
            }
        }
        return RequiredAction.NONE;
    }

    /**
     * a change has occurred in the groups or other attributes that makes a recalculation necessary.
     */
    public void recalculateEmployeeHours() {
        for (StudentGroup sg : this.groups.values()) {
            sg.setEmployeeHours(this.getCourseMethod().getAssignableHours(sg));
        }
    }

    /**
     * returns the number of groups in this module.
     *
     * @return groupnumber
     */
    public int getGroupNumber() {
        return groups.size();
    }

    /**
     * returns a sorted list (on number) of student groups.
     *
     * @return groupslist
     */
    public List<StudentGroup> getStudentGroups() {
        List<StudentGroup> list = new ArrayList<StudentGroup>();
        list.addAll(this.groups.values());
        Collections.sort(list);
        return list;
    }

    /**
     * returns the number of groups assigned to the given employee.
     *
     * @param employee the employee
     * @return groupcount
     */
    public int getGroupCount(final Employee employee) {
        int count = 0;
        for (StudentGroup sg : this.groups.values()) {
            if (sg.getEmployee().equals(employee)) {
                count++;
            }
        }
        return count;
    }

    /**
     * returns a -naturally sorted- list of employees associated with this module.
     *
     * @return employeeList
     */
    public List<Employee> getAssociatedEmployees() {
        Set<Employee> set = new HashSet<Employee>();
        for (StudentGroup sg : this.groups.values()) {
            set.add(sg.getEmployee());
        }
        List<Employee> list = new ArrayList<Employee>();
        list.addAll(set);
        Collections.sort(list);
        return list;
    }

    /**
     * Removes a student group from this module. Causes a redistribution of students over remaining groups.
     *
     * @param groupNumber the group number
     */
    public void removeStudentGroup(final int groupNumber) {
        if (groups.containsKey(groupNumber)) {
            //System.out.println("removing student group " + groups.get( groupNumber ));
            /*deregister from employee*/
            StudentGroup removed = groups.get(groupNumber);
            removed.getEmployee().removeStudentGroup(removed);
            /*remove from this module*/
            groups.remove(groupNumber);

            /*redistribute group numbers*/
            ArrayList<StudentGroup> sgs = new ArrayList<StudentGroup>();
            sgs.addAll(groups.values());
            groups.clear();

            Collections.sort(sgs);

            for (int i = 0; i < sgs.size(); i++) {
                sgs.get(i).setNumber(i + 1);
                addStudentGroup(sgs.get(i));
            }

            this.redistributeAttendingStudents();
        } else {
            assert false : "the group with number " + groupNumber + " does not exist";
        }
    }

    /**
     * set the timeframe of this module.
     *
     * @param timeframe the timeframe to set
     */
    public void setTimeframe(final Timeframe timeframe) {
        this.timeframe = timeframe;
    }

    /**
     * returns the timeframe of this module.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe() {
        return this.timeframe;
    }

    /**
     * @return the credits
     */
    public double getCredits() {
        return credits;
    }

    /**
     * Sets a new number of credits for this module. Will cause a recalculation of assigned hours for
     * studentgroups/employees based on this modules' course method
     *
     * @param credits the credits to set
     */
    public void setCredits(final double credits) {
        if (this.credits != credits) {
            /*recalculation is necessary*/
            this.credits = credits;
            recalculateEmployeeHours();
        }
    }

    /**
     * @return the description
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * SLB flag is inferred from the CourseMethod member.
     * returns true is the method starts with "SLB"
     * @return slb
     */
    public boolean isSLB() {
        return this.method.getName().startsWith("SLB");
    }

    /**
     * adds planning rules.
     *
     * @param planningRules the planning rules to set
     */
    public void setPlanningRules(final PlanningRules planningRules) {
        this.planningRules = planningRules;
    }

    /**
     * returns planning rules.
     *
     * @return planningRules
     */
    public PlanningRules getPlanningRules() {
        return this.planningRules;
    }

    /**
     * @return the method
     */
    public CourseMethod getCourseMethod() {
        return method;
    }

    /**
     * adds student groups to the module with employee vacancy as long as the group size is above required.
     */
    public void addVacantStudentGroups() {
        /*group number is the ID*/
        int maxGroupNumber = 0;
        for (StudentGroup s : this.groups.values()) {
            maxGroupNumber = (maxGroupNumber >= s.getNumber() ? maxGroupNumber : s.getNumber());
        }

        if (this.groupNumberLocked) {
            int difference = this.lockedGroupNumber - this.getGroupNumber();
            for (int i = 0; i < difference; i++) {
                //System.out.println("adding student group");
                maxGroupNumber++;
                StudentGroup sg = new StudentGroup(maxGroupNumber, Employee.getVacantEmployee(), this);
                Employee.getVacantEmployee().addStudentGroup(sg);
                sg.setRemarks("automatically generated student group");
                this.addStudentGroup(sg);
            }
        } else {
            while (this.getAttendingStudents() > (this.getCourseMethod().getGroupSize() * this.groups.size())) {
                /*keep adding student groups as long as necessary*/
                maxGroupNumber++;
                StudentGroup sg = new StudentGroup(maxGroupNumber, Employee.getVacantEmployee(), this);
                Employee.getVacantEmployee().addStudentGroup(sg);
                sg.setRemarks("automatically generated student group");
                this.addStudentGroup(sg);
                //System.out.println("CurriculumModule: adding student group " + sg );
            }
        }

        /*redistribute the attending students over the groups*/
        redistributeAttendingStudents();

    }

    /**
     * Sets a new course method for this module. Will cause a recalculation of assigned hours for student
     * groups/employees based on this new course method. If any student group is now too large for the new course
     * method, a redistribution of students into groups will be triggered
     *
     * @param method the method to set
     * @return a RequiredAction (REMOVE_GROUP, ADD_GROUP, NO_ACTION)
     */
    public RequiredAction setMethod(final CourseMethod method) {
        //TODO check this method!!!
        if (!this.method.equals(method)) {
            this.method = method;

            //recalculate the hours for all groups, regardless of whether the student groups will change in number
            recalculateEmployeeHours();

            return courseMethodChanged();
        }
        return RequiredAction.NONE;
    }

    /**
     * method that should be called whenever the course method changes. (i.e. it is another method or the method is
     * edited)
     *
     * @return requiredAction
     */
    public RequiredAction courseMethodChanged() {
        /*hours recalculation is necessary and possibly groups recalculation*/
        if ((this.groups.size() > 1)
                && (this.getAttendingStudents()
                <= (this.getCourseMethod().getGroupSize() * (this.groups.size() - 1)))) {
            /*there are too many groups for the new method*/
            return RequiredAction.REMOVE_GROUPS;
        } else if (this.getAttendingStudents() > (this.getCourseMethod().getGroupSize() * this.groups.size())) {
            this.addVacantStudentGroups();
            return RequiredAction.NONE;
        } else {
            return RequiredAction.NONE;
        }

    }

    /**
     * returns a number of groups that should be removed to regain legal group sizes.
     *
     * @return groupsNumberToRemove
     */
    public int getGroupsNumberToRemove() {

        if (groupNumberLocked && this.lockedGroupNumber < this.getGroupNumber()) {
            return this.getGroupNumber() - this.lockedGroupNumber;
        }

        if (this.groups.size() > 1) {
            /*there are too many groups for the new method*/
            int legal = (int) Math.ceil((double) this.getAttendingStudents() / this.getCourseMethod().getGroupSize());
            int remove = this.groups.size() - legal;
//            System.out.println( this.getClass().getSimpleName()
//                    + ".getGroupsNumberToRemove: module="
//                    + getModuleId()
//                    + "; group size=" + getCourseMethod().getGroupSize()
//                    + "; requiredgroups=" + legal
//                    + "; current groups=" + this.groups.size()
//                    + "; remove=" + remove );
            return (remove > 0 ? remove : 0);
        } else {
            return 0;
        }
    }

    /**
     * will redistribute the attending students over the given groups in this module. will ignore possible violations of
     * group size
     */
    public void redistributeAttendingStudents() {
        //System.out.println( "CurriculumModule: redistributing student groups " );
        if (this.getAttendingStudents() == 0) {
            for (StudentGroup sg : this.groups.values()) {
                sg.setStudentNumber(0);
            }
        } else if (this.getGroupNumber() == 0) {
            addVacantStudentGroups();
        } else {
            int groupSizeLower = this.getAttendingStudents() / this.getGroupNumber();
            int remainder = this.getAttendingStudents() % this.getGroupNumber();
            int count = 0;
            for (StudentGroup sg : this.groups.values()) {
                if (count < remainder) {
                    sg.setStudentNumber(groupSizeLower + 1);
                } else {
                    sg.setStudentNumber(groupSizeLower);
                }
                count++;
            }
        }
    }

    /**
     * @return the moduleId
     */
    public String getModuleId() {
        return moduleId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
//        return (this.getClass().getSimpleName() + "[" + getName()
//                        + ", ID=" + getModuleId()
//                        + ", credits="    + getCredits()
//                        + "]" );
        return this.getName() + " (" + this.getModuleId() + ")";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((moduleId == null) ? 0 : moduleId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CurriculumModule other = (CurriculumModule) obj;
        if (moduleId == null) {
            if (other.moduleId != null) {
                return false;
            }
        } else if (!moduleId.equals(other.moduleId)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(final CurriculumModule o) {
        return this.getModuleId().compareTo(o.getModuleId());
    }

}
