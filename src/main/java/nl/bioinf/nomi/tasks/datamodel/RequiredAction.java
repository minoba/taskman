package nl.bioinf.nomi.tasks.datamodel;

/**
 * enum specifying module actions for GUI events.
 * @author Michiel Noback [michiel.noback@gmail.com]
 */
public enum RequiredAction {

    /**
     * no action required with groups.
     */
    NONE("no action required with groups"),
    /**
     * only an update of views required.
     */
    UPDATE_VIEWS("only an update of views required"),
    /**
     * a student group should be removed.
     */
    REMOVE_GROUPS("a student group should be removed"),
    /**
     * a student group should be added.
     */
    ADD_GROUPS("a student group should be added");

    /**
     * the type.
     */
    private final String type;

    /**
     * çonstructs with type string.
     *
     * @param type the type
     */
    private RequiredAction(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
