/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *    - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *    - Neither the name of the author nor the names of its contributors may
 *      be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package nl.bioinf.nomi.tasks.datamodel;

/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 */
public enum TaskLabel {

    CURRICULUM_DEVELOPMENT("11 onderwijsontwikkeling vanuit initieel budget", TaskCategory.CURRICULUM_BOUND),
    CURRICULUM_EXECUTION("12 onderwijsuitvoering", TaskCategory.CURRICULUM_BOUND),
    STUDY_GUIDANCE("13 studiebegeleiding", TaskCategory.CURRICULUM_BOUND),

    COMMITTEES("21 deelname aan commissies", TaskCategory.NOT_CURRICULUM_BOUND),
    EDUCATION_COORDINATION("22 onderwijscoördinatie", TaskCategory.NOT_CURRICULUM_BOUND),
    INTERNATIONALIZATION("23 internationalisering", TaskCategory.NOT_CURRICULUM_BOUND),
    ICT("24 ICT", TaskCategory.NOT_CURRICULUM_BOUND),
    PR_MARKETING("25 PR, marketing en voorlichting", TaskCategory.NOT_CURRICULUM_BOUND),
    DIVERSE_NOT_CURRICULUM_BOUND("26 diversen", TaskCategory.NOT_CURRICULUM_BOUND),
    ACCREDITATION_RELATED_ACTIVITIES("27 accreditatie, midterm reviews", TaskCategory.NOT_CURRICULUM_BOUND),

    MANAGEMENT("31 management", TaskCategory.MANAGEMENT_AND_SUPPORT),
    ADMIN_FRONT_OFFICE("32 administratie en secretariaat (OBP)", TaskCategory.MANAGEMENT_AND_SUPPORT),
    PLANNING_CONTROL("33 planning en beheer (staf)", TaskCategory.MANAGEMENT_AND_SUPPORT),

    /**
     * SENIORITY_HOURS was changed to SUSTAINABLE_EMPLOYABILITY
     * */
    SUSTAINABLE_EMPLOYABILITY("41 duurzame inzetbaarheidsuren", TaskCategory.OTHER),
    SICK_LEAVE_SUBSTITUTE("42 ziektevervanging ", TaskCategory.OTHER),
    INDIRECT_HOURS("43 indirecte uren", TaskCategory.OTHER),

    /**
     * Dit zijn nu taken zoals BDB, Afronden stage master, Opleiding master die vielen onder
     * EXPERTISE_DEVELOPMENT("deskundigheidsbevordering (5%  incl 59 uren)", TaskCategory.OTHER).
     *
     * --> Deze moeten gewoon blijven staan
     */
    EXPERTISE_DEVELOPMENT("44 professionaliseringsuren", TaskCategory.OTHER),
    INDIVIDUAL_PROFESSIONAL_DEVELOPMENT("44a (individuele professionaliserings -- 40 uur)", TaskCategory.PROFESSIONALIZATION),
    COMMUNAL_PROFESSIONAL_DEVELOPMENT("44b (schoolgestuurde professionalisering)", TaskCategory.PROFESSIONALIZATION),

    PARENTAL_LEAVE("45 ouderschapsverlof", TaskCategory.OTHER),

    INTERNAL_PROJECTS_REGISTERED("50 interne projecten (SSBR, CSBR, etc.) wel Timetell", TaskCategory.PROJECTS),
    INTERNAL_PROJECTS("51 interne projecten (SSBR, CSBR, etc.) niet Timetell", TaskCategory.PROJECTS),
    KNOWLEDGE_CENTER("52 lectoraat en kenniskring", TaskCategory.PROJECTS),
    OTHER_EXTERNAL_PROJECTS("53 extern gefinancierde projecten Timetell", TaskCategory.PROJECTS),
    CONTRACT_ACTIVITIES("54 contractactiviteiten Timetell", TaskCategory.PROJECTS),

    QUALITY_ASSURANCE_FUNDS("14 kwaliteitsgelden", TaskCategory.PROJECTS),

    MATERNITY_LEAVE("60 Zwangerschapsverlof", TaskCategory.OUTPLACEMENT),
    HANZE_CONNECT("61 HG brede vergoedingsregelingen", TaskCategory.OUTPLACEMENT),
    OTHER_DEPARTMENTS("62 detachering binnen HG", TaskCategory.OUTPLACEMENT),
    EXTERNAL_OUTPLACEMENT("63 detachering buiten HG", TaskCategory.OUTPLACEMENT),
    UNPAID_LEAVE("64 onbetaald verlof", TaskCategory.OUTPLACEMENT);

    /**
     * the type as Dutch string.
     */
    private final String type;
    /**
     * the category.
     */
    private final TaskCategory taskCategory;

    /**
     * @param dutchLabelDescription the type
     * @param taskCategoryArg the task category
     */
    private TaskLabel(final String dutchLabelDescription, final TaskCategory taskCategoryArg) {
        this.type = dutchLabelDescription;
        this.taskCategory = taskCategoryArg;
    }

    public TaskCategory getTaskCategory() {
        return this.taskCategory;
    }

    @Override
    public String toString() {
        return type;
    }
}
