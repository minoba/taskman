/**
 *
 */
package nl.bioinf.nomi.tasks.datamodel;

import java.io.Serializable;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.2
 */
public class StudentGroup implements Task, Comparable<StudentGroup> {
    private static int groupNumber = 0;
    private final int groupId;
    private int number;
    private Employee employee;
    private int employeeHours;
    private final CurriculumModule curriculumModule;
    private int studentNumber;
    private String remarks;
    //private TaskLabel taskLabel;

    /**
     * construct with some relevant fields.
     *
     * @param number the group number
     * @param employee the associated teacher
     * @param curriculumModule the curriculumModule
     */
    public StudentGroup(final int number, final Employee employee, final CurriculumModule curriculumModule) {
        super();
        groupNumber++;
        this.groupId = groupNumber;
        this.number = number;
        this.employee = employee;
        this.curriculumModule = curriculumModule;
        /*student group task labels default to education execution*/
    }

    /**
     * @return the groupId
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * @return the teacher
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * @param employee the teacher to set
     */
    public void setEmployee(final Employee employee) {
        this.employee = employee;
    }

    /**
     * @return the employeeHours
     */
    public int getEmployeeHours() {
        return employeeHours;
    }

    /**
     * @param employeeHours the employeeHours to set
     */
    public void setEmployeeHours(final int employeeHours) {
        this.employeeHours = employeeHours;
    }

    /**
     * @return the studentNumber
     */
    public int getStudentNumber() {
        return studentNumber;
    }

    /**
     * sets the student number for this student group. Will cause a recalculation of employee hours if the number
     * changes
     *
     * @param studentNumber the studentNumber to set
     */
    public void setStudentNumber(final int studentNumber) {
        if (this.studentNumber != studentNumber) {
            this.studentNumber = studentNumber;
            getCurriculumModule().recalculateEmployeeHours();
        }
    }

    @Override
    public TaskCategory getTaskCategory() {
        return getTaskLabel().getTaskCategory();
    }

    @Override
    public void setTaskLabel(final TaskLabel taskLabel) {
        throw new UnsupportedOperationException("TaskLabels cannot be set on StudentGroups"
                + " -- they are inferred from the enclosing Modules' isSLB field.");
    }

    @Override
    public TaskLabel getTaskLabel() {
        TaskLabel tl = curriculumModule.isSLB() ? TaskLabel.STUDY_GUIDANCE : TaskLabel.CURRICULUM_EXECUTION;
        return tl;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(final String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the group number
     */
    public void setNumber(final int number) {
        this.number = number;
    }

    /**
     * @return the curriculumModule
     */
    public CurriculumModule getCurriculumModule() {
        return curriculumModule;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return (this.getClass().getSimpleName() + "[number=" + getNumber()
                + ", curriculumModule=" + getCurriculumModule().getName()
                + ", student number=" + getStudentNumber()
                + ", employee=" + getEmployee().getEmployeeId()
                + "]");
    }

    @Override
    public int compareTo(final StudentGroup other) {
        /*primary sort: curriculumModule; secondary sort: group number*/
        if (this.getCurriculumModule().getName() == null
                ? other.getCurriculumModule().getName() == null
                : this.getCurriculumModule().getName().equals(other.getCurriculumModule().getName())) {
            return this.getNumber() - other.getNumber();
        } else {
            if (this.getName().startsWith("praktijk") && (!other.getName().startsWith("praktijk"))) {
                return -1;
            } else if ((!this.getName().startsWith("praktijk")) && other.getName().startsWith("praktijk")) {
                return 1;
            } else {
                return (this.getCurriculumModule().getName().compareTo(other.getCurriculumModule().getName()));
            }
        }
    }

    @Override
    public String getName() {
        return getCurriculumModule().getName() + " [" + getCurriculumModule().getAttendingClassesString() + "] gr " + this.getNumber();
    }

    @Override
    public Timeframe getTimeframe() {
        //TODO the timeframe of a student group is not connected to a curriculum year
        //--> this is probably correct but should be checked
        return curriculumModule.getTimeframe();
    }

    @Override
    public int getTaskHours() {
        return getEmployeeHours();
    }

    @Override
    public boolean isCurriculumBound() {
        /*is always true for lecturing tasks*/
        assert getTaskCategory() == TaskCategory.CURRICULUM_BOUND;

        return getTaskCategory() == TaskCategory.CURRICULUM_BOUND;
    }

    @Override
    public int getQuarterHours(final int quarter) {
        if (getTimeframe().getQuarter() > quarter
                || (getTimeframe().getQuarter() + (getTimeframe().getDuration() - 1) < quarter)) {
            return 0;
        } else {
            return (int) (Math.round((double) getTaskHours() / getTimeframe().getDuration()));
        }
    }

    @Override
    public double getQuarterHoursRaw(final int quarter) {
        if (getTimeframe().getQuarter() > quarter
                || (getTimeframe().getQuarter() + (getTimeframe().getDuration() - 1) < quarter)) {
            return 0;
        } else {
            return (double) getTaskHours() / getTimeframe().getDuration();
        }
    }
}
