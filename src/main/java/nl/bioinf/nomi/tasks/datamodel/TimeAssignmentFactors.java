package nl.bioinf.nomi.tasks.datamodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * class representing all factors contributing to the number of hours for a student group
 * @author Cellingo
 *
 */
public class TimeAssignmentFactors{
	
	/**
	 * uren  
	 */
	private HashMap<Integer, List<TimeFactor>> timeFactors;
	
	/**
	 * default constructor
	 */
	public TimeAssignmentFactors(){
		timeFactors = new HashMap<Integer, List<TimeFactor>>();
		for(int i=0; i<3; i++) timeFactors.put(i, new ArrayList<TimeFactor>() );
	}

	/**
	 * clears all data
	 */
	public void clear(){
		timeFactors = new HashMap<Integer, List<TimeFactor>>();
		for(int i=0; i<3; i++) timeFactors.put(i, new ArrayList<TimeFactor>() );
	}
	
	/**
	 * adds a time factor; if a key with this name exists in the given category, it is overwritten
	 * timeFactor the timeFactor
	 */
	public void addTimeFactor( TimeFactor timeFactor){
		//System.out.println( "TAF: adding timefactor: " + timeFactor.toString() );
		//if( timeFactors.get(timeFactor.getCategory()) == null ) timeFactors.put(timeFactor.getCategory(), new ArrayList<TimeFactor>() );
		timeFactors.get(timeFactor.getCategory()).add( timeFactor );
	}
	
	/**
	 * returns a list of time factors of the given category
	 * @param category
	 * @return list of factors
	 */
	public List<TimeFactor> getTimeFactors( int category ){
		//int number = ( timeFactors.get(category) != null ? 0 : timeFactors.get(category).size() );
		//System.out.println( "TAF: timefactors of category " + category + " requested; " + number + " available" );
		ArrayList<TimeFactor> list = new ArrayList<TimeFactor>();
		if( timeFactors.get(category) != null )	list.addAll(timeFactors.get(category));
		Collections.sort( list );
		return list;
	}
	
	/**
	 * returns a list of all time factors for all three categories
	 * @return list of factors
	 */
	public List<TimeFactor> getTimeFactors( ){
		ArrayList<TimeFactor> list = new ArrayList<TimeFactor>();
		for( int i=0; i<timeFactors.size(); i++ ){
			if( getTimeFactors(i) != null ){
				//System.out.println("\t" + this.getClass().getSimpleName() + ": " + "cat=" + i + " has " + getTimeFactors(i).size() + " factors"); 
				//for( TimeFactor tf : getTimeFactors(i) ) System.out.println( "\t" + tf );
				list.addAll( getTimeFactors(i) );
				//System.out.println("\t" +this.getClass().getSimpleName() + ": " + "list size=" + list.size() );
			}
		}
		//System.out.println("\t" +this.getClass().getSimpleName() + ": return list size=" + list.size() );
		return list;
	}

	
	/**
	 * returns the total time to be assigned for a group given this set of factors. 
	 * Requires as variables the number of ECs involved and the total number of groups of this 
	 * module assigned to the teacher that is also responsible for this one  
	 * @param studentNumber the number of students in the group
	 * @param ec the number of EC's involved
	 * @param teacherGroupNumber the number of groups assigned to the teacher
	 * @return total time for the student group
	 */
	public double getTotalTime( int studentNumber, double ec, int teacherGroupNumber ){
		//System.out.println(this.getClass().getSimpleName() + " calculating total time with " + studentNumber + " students, " + ec + " ec's and " + teacherGroupNumber + " groups for the teacher");
		double total = 0;
		/*first process hours per teacher*/
		if( timeFactors.get(TimeFactor.HOURS_PER_TEACHER) != null ){
			for( TimeFactor f : timeFactors.get(TimeFactor.HOURS_PER_TEACHER) ){
				if( f.isEcDependent() ) total += ( (f.getHours() / teacherGroupNumber ) * ec );
				else total += (f.getHours() / teacherGroupNumber);
			}
			//System.out.println(this.getClass().getSimpleName() + " HOURS_PER_TEACHER total time=" + total);
		}
		if( timeFactors.get(TimeFactor.HOURS_PER_GROUP) != null ){
			for( TimeFactor f : timeFactors.get(TimeFactor.HOURS_PER_GROUP) ){
				if( f.isEcDependent() ) total += ( f.getHours() * ec );
				else total += f.getHours();
			}
			//System.out.println(this.getClass().getSimpleName() + " HOURS_PER_GROUP total time=" + total);
		}
		if( timeFactors.get(TimeFactor.HOURS_PER_STUDENT) != null ){
			for( TimeFactor f : timeFactors.get(TimeFactor.HOURS_PER_STUDENT) ){
				if( f.isEcDependent() ) total += ( f.getHours() * ec * studentNumber );
				else total += f.getHours() * studentNumber;
			}
			//System.out.println(this.getClass().getSimpleName() + " HOURS_PER_STUDENT total time=" + total);
		}
		//System.out.println(this.getClass().getSimpleName() + " total time=" + total);

		//the HBO hours 10% are added here directly to the calculated hours
		return (total * 1.1);
	}

}