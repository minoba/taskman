package nl.bioinf.nomi.tasks.datamodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Models a single curriculum variant and its constituting years.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class CurriculumYear {

    /**
     * the year of the curriculum that this object defines (1-4).
     */
    private final int year;
    /**
     * the numbers of students attending this curriculum year, per quarter.
     */
    private final int[] studentNumbers;
    /**
     * the containing curriculum variant.
     */
    private final CurriculumVariant curriculumVariant;
    /**
     * a map with the quarter number as key and a list of modules as value.
     */
    private final HashMap<Integer, ArrayList<CurriculumModule>> quarters;

    /**
     * construct.
     *
     * @param curriculumVariant the containing curriculum variant
     * @param year the curriculum year
     */
    public CurriculumYear(final CurriculumVariant curriculumVariant, final int year) {
        super();
        this.curriculumVariant = curriculumVariant;
        this.year = year;
        this.studentNumbers = new int[4];
        this.quarters = new HashMap<Integer, ArrayList<CurriculumModule>>();
        quarters.put(1, new ArrayList<CurriculumModule>());
        quarters.put(2, new ArrayList<CurriculumModule>());
        quarters.put(3, new ArrayList<CurriculumModule>());
        quarters.put(4, new ArrayList<CurriculumModule>());
    }

    /**
     * construct.
     *
     * @param curriculumVariant the containing curriculum variant
     * @param year the curriculum year
     * @param studentNumbers the student numbers
     */
    public CurriculumYear(final CurriculumVariant curriculumVariant, final int year, final int[] studentNumbers) {
        super();
        this.curriculumVariant = curriculumVariant;
        this.year = year;

        assert studentNumbers.length == 4 : "studentnumbers array should have four elements";

        this.studentNumbers = studentNumbers;
        this.quarters = new HashMap<>();
        quarters.put(1, new ArrayList<CurriculumModule>());
        quarters.put(2, new ArrayList<CurriculumModule>());
        quarters.put(3, new ArrayList<CurriculumModule>());
        quarters.put(4, new ArrayList<CurriculumModule>());
    }

    /**
     * add a curriculumModule in a given quarter of this year.
     *
     * @param quarter the quarter
     * @param curriculumModule the curriculumModule
     */
    public void addModule(final int quarter, final CurriculumModule curriculumModule) {
        quarters.get(quarter).add(curriculumModule);
    }

    /**
     * returns a list of modules for a given quarter.
     *
     * @param quarter the quarter
     * @return moduleList
     */
    public List<CurriculumModule> getModules(final int quarter) {
        return quarters.get(quarter);
    }

    /**
     * returns a list of modules for this curriculum year (all quarters), sorted on quarter number.
     *
     * @return moduleList
     */
    public List<CurriculumModule> getModules() {
        List<CurriculumModule> curriculumModules = new ArrayList<>();
        List<Integer> qs = new ArrayList<>();
        qs.addAll(quarters.keySet());
        Collections.sort(qs);

        for (int q : qs) {
            curriculumModules.addAll(quarters.get(q));
        }
        return curriculumModules;
    }

    /**
     * removes a curriculumModule from the given quarter. If not existing, nothing happens
     *
     * @param quarter the quarter
     * @param curriculumModule the curriculumModule
     */
    public void removeModule(final int quarter, final CurriculumModule curriculumModule) {
        quarters.get(quarter).remove(curriculumModule);
    }

    /**
     * @param quarter the quarter
     * @return the studentNumber
     */
    public int getStudentNumber(final int quarter) {
        assert (quarter > 0 && quarter < 5) : "quarter should be 1,2,3 or 4";
        return studentNumbers[quarter - 1];
    }

    /**
     * @param quarter the quarter
     * @param studentNumber the studentNumber to set for the given quarter
     */
    public void setStudentNumber(final int quarter, final int studentNumber) {
        if (!(quarter > 0 && quarter < 5)) {
            throw new IllegalArgumentException("quarter should be 1, 2, 3 or 4");
        }
        this.studentNumbers[quarter - 1] = studentNumber;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * returns the containing curriculum variant.
     *
     * @return curriculumVariant
     */
    public CurriculumVariant getCurriculumVariant() {
        return curriculumVariant;
    }

    /**
     * returns a list of relevant timeframes for this year.
     *
     * @return timeFrames
     */
    public List<Timeframe> getTimeframes() {
        return curriculumVariant.getCurriculum().getTimeframes(this.getYear());
    }

    /**
     * returns the number of invested hours given the function scale. Will -if relevant with multiple classes- calculate
     * the number of hours for a module as a fraction of the total for that module
     *
     * @param functionScales the function scales to fetch the hours for
     * @param methods the set of course methods to fetch the hours for
     * @return investedHours
     */
    public int getInvestedHours(final Set<Integer> functionScales, final Set<CourseMethod> methods) {
        int total = 0;
        for (int quarter : quarters.keySet()) {
            for (CurriculumModule curriculumModule : quarters.get(quarter)) {
                if (methods.contains(curriculumModule.getCourseMethod())) {
                    /*it is the relevant method*/
                    List<CurriculumYear> classes = curriculumModule.getAttendingClasses();
                    List<StudentGroup> groups = curriculumModule.getStudentGroups();
                    if (classes.size() == 1) {
                        /*there is only one class; therefore that must be this one*/
                        for (StudentGroup sg : groups) {
                            if (functionScales.contains(sg.getEmployee().getFunctionScale())) {
                                total += sg.getEmployeeHours();
                            }
                        }
                    } else {
                        /*there are multiple classes; the relative contribution has to be calculated*/
                        double fraction = (double) this.getStudentNumber(quarter) / curriculumModule.getAttendingStudents();
                        //System.out.println( "class " + this.toString() + "; fraction=" + fraction);
                        int subTotal = 0;
                        for (StudentGroup sg : groups) {
                            if (functionScales.contains(sg.getEmployee().getFunctionScale())) {
                                subTotal += sg.getEmployeeHours();
                            }
                        }
                        total += (int) (Math.round(subTotal * fraction));
                    }
                }
            }
        }
        return total;
    }

    /**
     * Returns the total cost, in euros, of this student class.
     * Will -if relevant with multiple classes- calculate the
     * number of euros for a module as a fraction of the total for that module
     *
     * @param functionScales the function scales to fetch the hours for
     * @param methods the set of course methods to fetch the hours for
     * @return totalCost
     */
    public int getInvestedEuros(final Set<Integer> functionScales, final Set<CourseMethod> methods) {
        int total = 0;
        for (int quarter : quarters.keySet()) {
            for (CurriculumModule curriculumModule : quarters.get(quarter)) {

                if (methods.contains(curriculumModule.getCourseMethod())) {
                    /*it is the relevant method*/
                    List<CurriculumYear> classes = curriculumModule.getAttendingClasses();
                    List<StudentGroup> groups = curriculumModule.getStudentGroups();
                    if (classes.size() == 1) {
                        /*there is only one class; therefore that must be this one*/
                        for (StudentGroup sg : groups) {
                            if (functionScales.contains(sg.getEmployee().getFunctionScale())) {
                                total += sg.getEmployeeHours()
                                        * (Employee.getSalaryCosts(sg.getEmployee().getFunctionScale()));
                            } else {
                                throw new IllegalArgumentException(
                                        "(1: getInvestedEuros) The functionscale "
                                                + sg.getEmployee().getFunctionScale()
                                                + "was not found for emloyee " + sg.getEmployee());
                            }

                        }
                    } else {
                        /*there are multiple classes; the relative contribution has to be calculated*/
                        double fraction = (double) this.getStudentNumber(quarter) / curriculumModule.getAttendingStudents();
                        int subTotal = 0;
                        for (StudentGroup sg : groups) {
                            if (functionScales.contains(sg.getEmployee().getFunctionScale())) {
                                subTotal += sg.getEmployeeHours()
                                        * (Employee.getSalaryCosts(sg.getEmployee().getFunctionScale()));
                            } else {
                                throw new IllegalArgumentException(
                                        "(2: getInvestedEuros) The functionscale "
                                                + sg.getEmployee().getFunctionScale()
                                                + "was not found for emloyee " + sg.getEmployee());
                            }
                        }
                        total += (int) (Math.round(subTotal * fraction));
                    }
                } else {
                    System.err.println("The course method " + curriculumModule.getCourseMethod() + "was not found!!");

                }
            }
        }
        return total;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        //DO NOT CHANGE THIS IMPLEMENTATION
        return (curriculumVariant.getCurriculum().getCurriculumID()
                + "-" + curriculumVariant.getType() + "-" + getYear());
    }

}
