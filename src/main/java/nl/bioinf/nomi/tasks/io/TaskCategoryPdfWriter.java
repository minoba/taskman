/**
 *
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import nl.bioinf.nomi.tasks.configuration.TaskManConfig;
import nl.bioinf.nomi.tasks.datamodel.RegularTask;
import nl.bioinf.nomi.tasks.datamodel.TaskCategory;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author Michiel
 *
 */
public class TaskCategoryPdfWriter {

    private int year;
    private File file;
    private Font titleFont;
    private Font subtitleFont;
    private Font bodyFont;
    private Font tableFontLarge;
    private Font tableFontSmall;
    private TaskCategory category;
    private YearDataCollection dataCollection;

    public TaskCategoryPdfWriter(YearDataCollection dataCollection, TaskCategory category, File file) {
        this.category = category;
        this.dataCollection = dataCollection;
        this.year = dataCollection.getSchoolYear();
        this.file = file;
        this.titleFont = FontFactory.getFont(FontFactory.TIMES, 16.0f, Font.BOLD, new BaseColor(0, 0, 0));
        this.subtitleFont = FontFactory.getFont(FontFactory.TIMES, 13.0f, Font.BOLD, new BaseColor(20, 20, 20));
        this.bodyFont = FontFactory.getFont(FontFactory.TIMES, 12.0f, Font.NORMAL, new BaseColor(0, 0, 0));
        this.tableFontLarge = FontFactory.getFont(FontFactory.TIMES, 11.0f, Font.BOLD, new BaseColor(10, 10, 10));
        this.tableFontSmall = FontFactory.getFont(FontFactory.TIMES, 11.0f, Font.NORMAL, new BaseColor(10, 10, 10));
    }

    public void write() throws Exception {
		//System.out.println( "start writing to pdf; file=" + file.getAbsolutePath() + ": " + employee.toString() );

        List<RegularTask> tasks = dataCollection.getRegularTasks(category);
        int totalHours = 0;
        for (RegularTask t : tasks) {
            totalHours += t.getTaskHours();
        }

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        document.add(new Paragraph("Taken categorie " + category.toString() + "  " + year + "-" + (year + 1), titleFont));

        document.add(new Paragraph("\n\n", bodyFont));

        document.add(new Paragraph("Algemeen overzicht:", subtitleFont));

        document.add(new Paragraph(" ", bodyFont));

        float[] colsWidthOverview = {5f, 2f};
        PdfPTable taskCatOverviewTable = new PdfPTable(colsWidthOverview);
        taskCatOverviewTable.setWidthPercentage(60);
        taskCatOverviewTable.setHorizontalAlignment(Element.ALIGN_LEFT);

        taskCatOverviewTable.addCell(createPdfCell("Totaal uren", tableFontLarge));
        taskCatOverviewTable.addCell(createPdfCell(totalHours + "", tableFontLarge));

        taskCatOverviewTable.addCell(createPdfCell("Aantal taken", tableFontLarge));
        taskCatOverviewTable.addCell(createPdfCell(tasks.size() + "", tableFontLarge));

        document.add(taskCatOverviewTable);

        document.add(new Paragraph(" ", bodyFont));

        document.add(new Paragraph("Takenoverzicht:", subtitleFont));

        document.add(new Paragraph(" ", bodyFont));

        float[] colsWidthTasks = {4f, 1.8f, 5f, 1f, 1f, 1f, 1f, 1.5f};
        PdfPTable tasksTable = new PdfPTable(colsWidthTasks);
        tasksTable.setWidthPercentage(100);
        tasksTable.setHorizontalAlignment(Element.ALIGN_LEFT);

        tasksTable.addCell(createPdfCell("Naam", tableFontLarge));
        tasksTable.addCell(createPdfCell("Afkorting", tableFontLarge));
        tasksTable.addCell(createPdfCell("Taak", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 1", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 2", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 3", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 4", tableFontLarge));
        tasksTable.addCell(createPdfCell("Totaal", tableFontLarge));

        /*add the project tasks*/
        int[] qTotals = new int[5];
        for (RegularTask t : tasks) {

            tasksTable.addCell(createPdfCell(t.getEmployee().getFullName(), tableFontSmall));

            tasksTable.addCell(createPdfCell(t.getEmployee().getEmployeeId(), tableFontSmall));

            String name = (t.isCurriculumBound()) ? "* " + t.getName() : t.getName();
            tasksTable.addCell(createPdfCell(name, tableFontSmall));
            int qOneHours = t.getQuarterHours(1);
            qTotals[0] += qOneHours;
            String qh1 = (qOneHours == 0) ? "" : qOneHours + "";
            tasksTable.addCell(createPdfCell(qh1, tableFontSmall));

            int qTwoHours = t.getQuarterHours(2);
            qTotals[1] += qTwoHours;
            String qh2 = (qTwoHours == 0) ? "" : qTwoHours + "";
            tasksTable.addCell(createPdfCell(qh2, tableFontSmall));

            int qThreeHours = t.getQuarterHours(3);
            qTotals[2] += qThreeHours;
            String qh3 = (qThreeHours == 0) ? "" : qThreeHours + "";
            tasksTable.addCell(createPdfCell(qh3, tableFontSmall));

            int qFourHours = t.getQuarterHours(4);
            qTotals[3] += qFourHours;
            String qh4 = (qFourHours == 0) ? "" : qFourHours + "";
            tasksTable.addCell(createPdfCell(qh4, tableFontSmall));

            qTotals[4] += t.getTaskHours();
            tasksTable.addCell(createPdfCell(t.getTaskHours() + "", tableFontSmall));
        }

        tasksTable.addCell(createPdfCell("Totalen", tableFontLarge));

        /*add the quarter and overall totals*/
        for (int qt : qTotals) {
            tasksTable.addCell(createPdfCell(qt + "", tableFontSmall));
        }

        document.add(tasksTable);

        Date now = new Date();
        document.add(new Paragraph("Gegenereerd op: " + TaskManConfig.DATE_FORMAT.format(now), bodyFont));

        document.close();
        writer.flush();
        writer.close();

    }

    private PdfPCell createPdfCell(String text, Font font) {
        Chunk chunk = new Chunk(text, font);
        Phrase phrase = new Phrase(chunk);
        PdfPCell cell = new PdfPCell(phrase);
        return cell;
    }

}
