package nl.bioinf.nomi.tasks.io;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.PlanningRules;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;
import nl.bioinf.nomi.tasks.datamodel.PlanningRules.WeekRules;

/**
 * exports all planningrules to excel
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class PlanningRulesExcelWriter {

	private ExcelUtils excelUtils;
	private YearDataCollection dataCollection;
	private File baseDir;

	/**
	 * 
	 */
	public PlanningRulesExcelWriter( File baseDir, YearDataCollection dataCollection ) {
		this.baseDir = baseDir;
		this.dataCollection = dataCollection;
	}

	/**
	 * writes the planning rules to files, one file per curriculum
	 * @throws IOException
	 */
	public void write() throws IOException {
		//System.out.println("writing internship tasks to file " + file );

		for( Curriculum c : dataCollection.getCurricula() ){
			createPlanningRulesExcelBook(c);
			//premature break for development purposes 
			//break;
		}
	}

	/**
	 * creates a single excel workbook with planning rules for the given curriculum
	 * @param curriculum
	 * @throws IOException
	 */
	public void createPlanningRulesExcelBook(Curriculum curriculum) throws IOException{
		String fileName = "roosterregels_" + dataCollection.getSchool() + "_" + curriculum.getCurriculumID() + ".xlsx";
		String base = this.baseDir.getAbsolutePath();
		if( base.endsWith(File.separator) ){
			base = base.substring(0, base.length()-2);
		}
		
		//System.out.println("writing to " + base + " file " + (this.baseDir.getAbsolutePath() + File.separator + fileName));

		FileOutputStream fileOut = new FileOutputStream(this.baseDir.getAbsolutePath() + File.separator + fileName);
		
		Workbook wb = new XSSFWorkbook();
		this.excelUtils = new ExcelUtils( wb ); 
		
		for( Timeframe tf : curriculum.getTimeframes() ){
			//System.out.println("timeframe: " + tf + ": " + tf.getYear() + "_" + tf.getQuarter() );
			Sheet currSheet = wb.createSheet( tf.getYear() + "_" + tf.getQuarter() );
			createQuarterSheet(currSheet, tf, curriculum);
		}

		wb.write(fileOut);
		fileOut.close();
	}
	
	private void createQuarterSheet(Sheet sheet, Timeframe tf, Curriculum curriculum){
		int colWidth = 3000;
		sheet.setColumnWidth(0, 6000);
		sheet.setColumnWidth(1, 6000);
		sheet.setColumnWidth(2, 2000);
		sheet.setColumnWidth(3, 4500);
		sheet.setColumnWidth(4, 2000);
		for( int i=5; i<5+(tf.getDuration()*10); i++ ){
			sheet.setColumnWidth(i, colWidth);
		}
		sheet.setColumnWidth(5+(tf.getDuration()*10), 15000);
		sheet.setColumnWidth(6+(tf.getDuration()*10), 15000);

		short rowNum = 0;
		
		
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, "Roosterregels gegenereerd door TaskManager op "+new Date(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		
		
		
		List<CurriculumYear> cyl = curriculum.getCurriculumYearVariants(tf.getYear());
		StringBuilder classesDescr = new StringBuilder();
		int students = 0;
		for(CurriculumYear cy : cyl){
			classesDescr.append(cy.toString() );
			classesDescr.append(" & ");
			students += cy.getStudentNumber(tf.getQuarter());
		}
		if( classesDescr.length() > 3 ) classesDescr.delete(classesDescr.length()-3, classesDescr.length());
		
		//System.out.println(classesDescr + ": " + students);
		
		rowNum+=2;
		row = sheet.createRow( rowNum );
		
		excelUtils.createExcelCell(row, 0, "Klassen", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, classesDescr.toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		
		rowNum++;
		row = sheet.createRow( rowNum );
		
		excelUtils.createExcelCell(row, 0, "Studenten", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, students+"", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );

		/*create header row*/
		rowNum+=2;
		row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, "Vak", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, "Docenten", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, "Groepen", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 3, "Klassen", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 4, "Studenten", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		
		for( int i=0; i<(tf.getDuration()*10); i++ ){
			String week = ("week " + (i+1));
			excelUtils.createExcelCell(row, (5 + i), week, ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		}
		excelUtils.createExcelCell(row, 5+(tf.getDuration()*10), "Modulevereisten", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 6+(tf.getDuration()*10), "Modulecommentaal", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		

		/*create module rows*/
		for( CurriculumModule m: curriculum.getModules(tf) ){
			rowNum++;
			row = sheet.createRow( rowNum );
			excelUtils.createExcelCell(row, 0, m.getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 1, m.getLecturingEmployeesString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 2, m.getGroupNumber()+"", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			excelUtils.createExcelCell(row, 3, m.getAttendingClassesString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 4, m.getAttendingStudents()+"", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			
			PlanningRules pr = m.getPlanningRules();
			if( pr != null ){
				List<WeekRules> rules = pr.getRules();
				for(int i=0; i<rules.size(); i++){
					WeekRules wr = rules.get(i);
					excelUtils.createExcelCell(row, (5+i), wr.getWeekRulesString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
				}
				if( pr.getRequirements().length() != 0 ){
					excelUtils.createExcelCell(row, (rules.size()+5), pr.getRequirements(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
				}
				if( pr.getComments().length() != 0 ){
					excelUtils.createExcelCell(row, (rules.size()+6), pr.getComments(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
				}
			}
			else{
				for(int i=0; i<10; i++){
					excelUtils.createExcelCell(row, (5+i), "NA", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
				}
			}
			
		}
	}


	public static void main(String[] args){
		File file = new File("C:\\Users\\Michiel\\projects\\taken_admin\\data\\planning2011-2012-kw2_roosterregels.xml");
		XmlDataReader xdr = new XmlDataReader(file);
		try {
			/*read and add the collection*/
			YearDataCollection ydc = xdr.readDataCollection();
			PlanningRulesExcelWriter prew = new PlanningRulesExcelWriter(new File("C:\\Users\\Michiel\\Desktop"), ydc);
			prew.write();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
