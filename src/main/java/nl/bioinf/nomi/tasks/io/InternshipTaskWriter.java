/**
 * 
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


import nl.bioinf.nomi.tasks.datamodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;

/**
 * Can be used to export internship and graduation occupation to Excel file 
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class InternshipTaskWriter {

	private File file;
	private YearDataCollection dataCollection;
	private ExcelUtils excelUtils;
	

	/**
	 * constructs with the file to export to and the data collection from which 
	 * the internship- and graduation overview should be created
	 * @param file
	 * @param dataCollection
	 */
	public InternshipTaskWriter(File file, YearDataCollection dataCollection) {
		this.file = file;
		this.dataCollection = dataCollection;
	}

	/**
	 * writes the overview to file
	 * @throws IOException
	 */
	public void write() throws IOException {
		//System.out.println("writing internship tasks to file " + file );

		Workbook wb = new XSSFWorkbook();
		this.excelUtils = new ExcelUtils( wb ); 
		
		FileOutputStream fileOut = new FileOutputStream(file);

		List<Curriculum> curricula = dataCollection.getCurricula();
		//dataCollection.getTeams();
		
		for( Curriculum curriculum : curricula ){
			HashMap<String, EmployeeInternshipStats> stats = new HashMap<String, EmployeeInternshipStats>();
			
			Sheet currSheet = wb.createSheet(curriculum.getCurriculumID() );
			currSheet.setColumnWidth(0, 2500);
			currSheet.setColumnWidth(1, 10000);
			currSheet.setColumnWidth(2, 3500);
			currSheet.setColumnWidth(3, 3500);
			
			createHeaderRow( currSheet );
			
			/*traverse internships*/
			for( CurriculumModule mod : curriculum.getModules( curriculum.getInternship() ) ){
				//System.out.println( curriculum.getCurriculumID() + " internships period module: " + mod);
				if( mod.getModuleId().startsWith("STAGE")){
					//System.out.println( curriculum.getCurriculumID() + " internship: " + mod);
					for(StudentGroup sg : mod.getStudentGroups() ){
						Employee emp = sg.getEmployee();
						//System.out.println( "INT sg: " + sg );
						if( stats.containsKey(emp.getEmployeeId() ) ){
							stats.get(emp.getEmployeeId()).internships++;
						}
						else{
							EmployeeInternshipStats eis = new EmployeeInternshipStats(emp);
							eis.internships = 1;
							stats.put(emp.getEmployeeId(), eis);
						}
					}
				}
			}
			/*traverse graduations*/
			for( CurriculumModule mod : curriculum.getModules( curriculum.getGraduation() ) ){
				//System.out.println( curriculum.getCurriculumID() + " graduation period module: " + mod);
				if( mod.getModuleId().startsWith("AFST")){
					//System.out.println( curriculum.getCurriculumID() + " graduation: " + mod);
					for(StudentGroup sg : mod.getStudentGroups() ){
						//System.out.println( "AFST sg: " + sg );
						Employee emp = sg.getEmployee();
						if( stats.containsKey(emp.getEmployeeId() ) ){
							stats.get(emp.getEmployeeId()).graduations++;
						}
						else{
							EmployeeInternshipStats eis = new EmployeeInternshipStats(emp);
							eis.graduations = 1;
							stats.put(emp.getEmployeeId(), eis);
						}
					}
				}
			}
			
			/*write stats to file*/
			ArrayList<EmployeeInternshipStats> statsList = new ArrayList<EmployeeInternshipStats>();
			statsList.addAll(stats.values());
			Collections.sort(statsList);
			
			short rowNum = 1;
			int internshipTotal = 0;
			int graduationTotal = 0;
			//System.out.println( "statsList size: " + statsList.size() );
			for( EmployeeInternshipStats eis : statsList ){
				//System.out.println( " emp: " + eis.employee.getEmployeeId() );
				createEmployeeRow(eis, currSheet, rowNum );
				internshipTotal += eis.internships;
				graduationTotal += eis.graduations;
				rowNum++;
			}
			
			Row row = currSheet.createRow( rowNum );
			excelUtils.createExcelCell(row, 0, "TOTALEN", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 1, "", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 2, ""+internshipTotal, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			excelUtils.createExcelCell(row, 3, ""+graduationTotal, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );

		}

		wb.write(fileOut);
		fileOut.close();
	}

	
	/**
	 * creates an entry for an employee with the number of internship and graduation students
	 * @param employeeInternshipStats
	 * @param sheet
	 */
	private void createEmployeeRow(EmployeeInternshipStats employeeInternshipStats, Sheet sheet, short rowNum ) {
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, employeeInternshipStats.employee.getEmployeeId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, employeeInternshipStats.employee.getFullName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, ""+employeeInternshipStats.internships, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		excelUtils.createExcelCell(row, 3, ""+employeeInternshipStats.graduations, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
	}

	/**
	 * creates a header row on each workbook sheet
	 * @param sheet
	 */
	private void createHeaderRow(Sheet sheet) {
		Row row = sheet.createRow( (short)0 );
		excelUtils.createExcelCell(row, 0, "afkorting", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, "naam", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, "stages", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 3, "afstudeerders", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
	}

	private class EmployeeInternshipStats implements Comparable<EmployeeInternshipStats>{
		Employee employee;
		int internships = 0;
		int graduations = 0;

		public EmployeeInternshipStats( Employee employee ){
			this.employee = employee;
		}

		@Override
		public int compareTo(EmployeeInternshipStats o) {
			return employee.getFamilyName().compareTo(o.employee.getFamilyName());
		}
	}
	
}
