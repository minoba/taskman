/**
 * 
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import nl.bioinf.nomi.tasks.datamodel.Project;
import nl.bioinf.nomi.tasks.datamodel.ProjectTask;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;

/**
 * exports all projects to excel
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class ProjectsExcelWriter {

	private ExcelUtils excelUtils;
	private File file;
	private YearDataCollection dataCollection;

	/**
	 * 
	 */
	public ProjectsExcelWriter( File file, YearDataCollection dataCollection ) {
		this.file = file;
		this.dataCollection = dataCollection;
	}

	/**
	 * writes the overview to file
	 * @throws IOException
	 */
	public void write() throws IOException {
		//System.out.println("writing internship tasks to file " + file );

		Workbook wb = new XSSFWorkbook();
		this.excelUtils = new ExcelUtils( wb ); 
		
		FileOutputStream fileOut = new FileOutputStream(file);

		Sheet currSheet = wb.createSheet( "overzicht" );
		
		createOverviewSheet(currSheet);
		
		
		for( Project p : dataCollection.getProjects() ){
			String pName = p.getProjectId();
			//pName = URLEncoder.encode(pName, "UTF-8");
			//int end = pName.length() > 8 ? 8 : pName.length(); 
			//pName = pName.substring(0,end);
			//System.out.println(pName);
			currSheet = wb.createSheet(pName);
			currSheet.setColumnWidth(0, 4500); //4-letter
			currSheet.setColumnWidth(1, 8000); //naam
			currSheet.setColumnWidth(2, 9000); //taak
			currSheet.setColumnWidth(3, 2500); //uren
			currSheet.setColumnWidth(4, 4000); //start kw
			currSheet.setColumnWidth(5, 4000); //eind kw

			createProjectHeader(currSheet, p);
			
			short rowNum = 9;
			
			for( ProjectTask pt : p.getTasks() ){
				createProjectTaskRow(currSheet, pt, rowNum);
				
				rowNum++;
			}
		}

		wb.write(fileOut);
		fileOut.close();
	}


	/**
	 * creates a single row (entry) for the given project task
	 * @param sheet
	 * @param pt
	 * @param rowNum
	 */
	private void createProjectTaskRow(Sheet sheet, ProjectTask pt, short rowNum) {
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, pt.getEmployee().getEmployeeId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, pt.getEmployee().getFullName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, pt.getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 3, ""+pt.getTaskHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		excelUtils.createExcelCell(row, 4, ""+pt.getTimeframe().getQuarter(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		excelUtils.createExcelCell(row, 5, ""+pt.getTimeframe().getDuration(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
	}

	/**
	 * creates a header section for the given project
	 * @param sheet
	 * @param p the project
	 */
	private void createProjectHeader(Sheet sheet, Project p){
		short rowNum = 0;
		
		/*create title row*/
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, p.getProjectId(), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );

		rowNum++;
		row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, p.getName(), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		
		rowNum++;
		row = sheet.createRow( rowNum );
		
		excelUtils.createExcelCell(row, 0, p.getDescription(), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		
		rowNum++;
		row = sheet.createRow( rowNum );
		
		excelUtils.createExcelCell(row, 0, "Uren beschikbaar", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, ""+p.getTotalHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );

		rowNum++;
		row = sheet.createRow( rowNum );

		excelUtils.createExcelCell(row, 0, "Uren uitgezet", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, ""+p.getAssignedHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );

		rowNum++;
		row = sheet.createRow( rowNum );

		excelUtils.createExcelCell(row, 0, "Uren over", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, ""+p.getAssignableHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		
		rowNum++;
		row = sheet.createRow( rowNum );

		excelUtils.createExcelCell(row, 0, "Kosten", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, ""+(int)(Math.round(p.getAssignedHoursCost())), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );

		rowNum += 3;
	
		/*create header row for taskl table*/
		//code	naam	omschrijving	uren	start	kwartalen
		row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, "4-Letter code", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, "Naam", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, "Taak", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 3, "Uren", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 4, "Start (kwartaal)", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 5, "Duur (kwartalen)", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		rowNum++;
		
		
	}

	/**
	 * creates a an overview sheet of all projects
	 * @param sheet
	 */
	private void createOverviewSheet(Sheet sheet) {
		//Taak | Omschrijving| Uren beschikbaar | Uren uitgezet | Uren over | Kosten
		
		sheet.setColumnWidth(0, 5000);
		sheet.setColumnWidth(1, 10000);
		sheet.setColumnWidth(2, 3500);
		sheet.setColumnWidth(3, 3500);
		sheet.setColumnWidth(4, 3500);
		sheet.setColumnWidth(5, 3500);

		
		short rowNum = 0;
		
		/*create header row*/
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, "Project", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, "Omschrijving", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, "Uren beschikbaar", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 3, "Uren uitgezet", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 4, "Uren over", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 5, "Kosten", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
		rowNum++;

		int totalAvailableHours = 0;
		int totalAssignedHours = 0;
		int totalRemainingHours = 0;
		double totalCosts = 0;
		
		/*create a row per project*/
		for( Project p : dataCollection.getProjects() ){
			row = sheet.createRow( rowNum );
			totalAvailableHours += p.getTotalHours();
			totalAssignedHours += p.getAssignedHours();
			totalRemainingHours += p.getAssignableHours();
			totalCosts += p.getAssignedHoursCost();
			
			excelUtils.createExcelCell(row, 0, p.getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 1, p.getDescription(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
			excelUtils.createExcelCell(row, 2, ""+p.getTotalHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			excelUtils.createExcelCell(row, 3, ""+p.getAssignedHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			excelUtils.createExcelCell(row, 4, ""+p.getAssignableHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			excelUtils.createExcelCell(row, 5, ""+(int)(Math.round(p.getAssignedHoursCost())), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
			rowNum++;
		}
		
		/*create the totals row*/
		excelUtils.createExcelCell(row, 0, "TOTALEN", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, "", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 2, ""+totalAvailableHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		excelUtils.createExcelCell(row, 3, ""+totalAssignedHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		excelUtils.createExcelCell(row, 4, ""+totalRemainingHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		excelUtils.createExcelCell(row, 5, ""+(int)(Math.round(totalCosts)), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		rowNum++;
	}

	
}
