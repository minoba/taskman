/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package nl.bioinf.nomi.tasks.io;

import nl.bioinf.nomi.tasks.configuration.TaskManConfig;
import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class IOcontroller {

    /**
     * key = datafile name value = lockFile File.
     */
    private final Map<String, File> lockedFiles = new HashMap<>();

    /**
     * singleton implementation.
     */
    private static class InstanceProvider {

        /**
         * INSTANCE.
         */
        private static final IOcontroller INSTANCE = new IOcontroller();

        /**
         * instance serving method.
         *
         * @return instance
         */
        protected static IOcontroller getInstance() {
            return INSTANCE;
        }
    }

    /**
     * returns the INSTANCE of the IOcontroller.
     *
     * @return instance the instance
     */
    public static IOcontroller getInstance() {
        return InstanceProvider.getInstance();
    }

    /**
     * saves the test file.
     */
    public void saveTestFile() {
        File file = new File("C:\\Users\\Gebruiker\\Dropbox\\projects\\taskman\\2015-planning1516-nov.xml");
//        File file = new File("C:\\Users\\Michiel\\projects\\taken_admin\\data\\planning2011-2012-kw2X.xml");
        IOcontroller.getInstance().writeToSelectedFile(file, TaskManagerController.getInstance().getDataCollection());
        //writeToSelectedFile(file, getDataCollection());
    }

    /**
     * opens the test file.
     */
    public void openTestFile() {
//      File file = new File("/homes/michiel/Desktop/2015-planning1516-aug.xml");
//        File file = new File("C:\\Users\\Gebruiker\\Dropbox\\projects\\taskman\\2015-planning1516-nov.xml");
//        File file = new File("/Users/michiel/Dropbox/projects/taskman/2015-planning1516-nov.xml");
//        File file = new File("/Users/michiel/tmp/Taskman/2016-planning1617-0%.xml");
        File file = new File("/Users/michiel/onedrive_hg/projects/taskman/Planning1819_dec.xml");
        //File file = new File("/homes/michiel/Dropbox/projects/taskman/_2015-planning1516_MIGRATE_WITH_LABELS.xml");

        XmlDataReader xdr = new XmlDataReader(file);
        try {
            /*read and add the collection*/
            YearDataCollection ydc = xdr.readDataCollection();
            ydc.setDataFile(file);
            TaskManagerController.getInstance().addDataCollection(ydc);
            TaskManagerController.getInstance().setCurrentYear(ydc.getSchoolYear());
//            currentYear = ydc.getSchoolYear();
            /*process the collection*/
            TaskManagerController.getInstance().showDataCollection(ydc.getSchoolYear());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * loads and returns the user properties.
     *
     * @return userProperties the user properties
     */
    public Properties loadUserProperties() {
        Properties userProperties;
        try {
            try (FileInputStream fis = new FileInputStream(new File(TaskManConfig.USER_PROPERTIES_FILE))) {
                userProperties = new Properties();
                userProperties.load(fis);
                fis.close();
//            System.out.println("user prop last_file_location:"
//                    + userProperties.getProperty(TaskManConfig.USER_PROPERTY_LAST_FILE_LOCATION));
            }
        } catch (IOException e) {
            //e.printStackTrace();
            userProperties = new Properties();
        }
        return userProperties;
    }

    /**
     * Loads a data file as specified by the file and creates a lock on this file for the active user.
     */
    public void openFile() {
        /*try to get the last opened directory and file*/
        TaskManagerController controllerInstance = TaskManagerController.getInstance();
        Properties userProperties = controllerInstance.getUserProperties();
        File lastOpenedFileLocation = new File(userProperties.getProperty(
                TaskManConfig.USER_PROPERTY_LAST_FILE_LOCATION, System.getProperty("user.home")));

        JFileChooser fileChooser = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("XML file", "xml");
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setSelectedFile(lastOpenedFileLocation);
        int returnVal = fileChooser.showOpenDialog(null); //frame.getContentPane()
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fileChooser.getSelectedFile();
                File lockFile = getLockFile(file);
                if (lockFile == null) {
                    JOptionPane.showMessageDialog(
                            null,
                            "you did not select a valid file format: XML",
                            "IO error",
                            JOptionPane.WARNING_MESSAGE);
                    //abort current file open action
                    return;
                }
                if (lockFile.exists() && !checkLockFile(lockFile)) {
                    /*the file is locked so register this datafile as protected*/
                    this.lockedFiles.put(file.getAbsolutePath(), lockFile);
                    JOptionPane.showMessageDialog(
                            null,
                            "User " + getLockFileUser(lockFile)
                                    + " has locked this datafile: you cannot save it under the current name.",
                            "Datafile " + file + " locked by other user",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    /*create a lock for this datafile for current user and register its lock*/
                    this.lockedFiles.put(file.getAbsolutePath(), lockFile);
                    try {
                        this.createLockFile(lockFile);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Lockfile " + lockFile
                                        + " for this datafile could not be created; multi-user mode is now not safe!",
                                "Lockfile creation error",
                                JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(IOcontroller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                /*store last opened file location*/
                userProperties.setProperty(TaskManConfig.USER_PROPERTY_LAST_FILE_LOCATION, file.getAbsolutePath());
                try {
                    userProperties.store(
                            new FileOutputStream(new File(TaskManConfig.USER_PROPERTIES_FILE)), "last file location");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                XmlDataReader xdr = new XmlDataReader(file);
                try {
                    /*read and add the collection*/
                    YearDataCollection ydc = xdr.readDataCollection();
                    ydc.setDataFile(file);
                    controllerInstance.addDataCollection(ydc);
                    controllerInstance.setCurrentYear(ydc.getSchoolYear()); //currentYear = ydc.getSchoolYear();

                    /*process the collection*/
                    controllerInstance.showDataCollection(ydc.getSchoolYear());
                } catch (Exception e) {
                    //e.printStackTrace();
                    JOptionPane.showMessageDialog(
                            null,
                            "fout tijdens lezen van datafile; verkeerd format geselecteerd?",
                            "data fout",
                            JOptionPane.ERROR_MESSAGE);
                }
            } catch (IOException ex) {
                Logger.getLogger(IOcontroller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * loads a data file as specified by the file.
     *
     * @param year the year to save data for
     */
    public void saveFile(final int year) {
        if (year == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "er is geen dataset om te bewaren",
                    "fout",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }

        YearDataCollection ydc = TaskManagerController.getInstance().getDataCollection(year);
        File dataFile = ydc.getDataFile();
        processSaveRequest(dataFile, ydc, false);
    }

    /**
     * Supports the "save as" action.
     *
     * @param year the year to save data for
     */
    public void saveAsFile(final int year) {
        if (year == 0) {
            JOptionPane.showMessageDialog(null,
                    "er is geen dataset om te bewaren",
                    "fout",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }
        YearDataCollection ydc = TaskManagerController.getInstance().getDataCollection(year);
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("bewaar " + year + " dataset");
        fileChooser.setApproveButtonText("Bewaar");
        FileFilter filter = new FileNameExtensionFilter("XML file", "xml");
        fileChooser.addChoosableFileFilter(filter);
        boolean selectedFileOK = false;
        while (!selectedFileOK) {
            int returnVal = fileChooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                try {
                    File file = fileChooser.getSelectedFile();
                    File lockFile = getLockFile(file);
                    //there should be no lock or the lock should be this users' lock
                    if (!lockFile.exists() || checkLockFile(lockFile)) {
                        selectedFileOK = true;
                        if (file.exists()) {
                            int n = JOptionPane.showConfirmDialog(
                                    null,
                                    "Bestand bestaat al. Overschrijven?",
                                    "Bevestig overschrijven",
                                    JOptionPane.OK_CANCEL_OPTION);
                            if (n != JOptionPane.OK_OPTION) {
                                continue;
                            }
                        }
                        if (this.lockedFiles.containsKey(file.getAbsolutePath())) {
                            processSaveRequest(file, ydc, false);
                        } else {
                            processSaveRequest(file, ydc, true);
                        }
                    } else {
                        JOptionPane.showConfirmDialog(
                                null,
                                "Gekozen bestand is al gelocked door "
                                + getLockFileUser(lockFile),
                                "kies andere naam",
                                JOptionPane.WARNING_MESSAGE);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(IOcontroller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * processes the save request for current datafile and year data collection, checking for lock on file.
     *
     * @param dataFile the data file
     * @param ydc the year data collection
     * @param newName boolean to indicate whether the indicated file already is in use
     */
    private void processSaveRequest(final File dataFile, final YearDataCollection ydc, final boolean newName) {
        if (!newName) {
            if (this.lockedFiles.containsKey(dataFile.getAbsolutePath())) {
                try {
                    //there is a lock file -- check the user name against current user
                    File lockFile = this.lockedFiles.get(dataFile.getAbsolutePath());
                    boolean lockFileCheck = checkLockFile(lockFile);
                    if (lockFileCheck) {
                        writeToSelectedFile(ydc.getDataFile(), ydc);
                        //overwrite datafile field for this datacvollection
                        ydc.setDataFile(dataFile);
                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Datafile " + dataFile.getAbsolutePath()
                                        + " is locked! Please save under different name.",
                                "Lockfile error",
                                JOptionPane.WARNING_MESSAGE);
                    }
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(
                            null,
                            "The lock for datafile " + dataFile.getAbsolutePath()
                                    + " is has been removed! Please save under different name.",
                            "Lockfile error",
                            JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(
                        null,
                        "Lockfile for " + dataFile.getAbsolutePath()
                        + " has been removed; multi-user mode is now not safe! "
                        + "Please save file under different name.",
                        "Lockfile error",
                        JOptionPane.ERROR_MESSAGE);
                //abort save action
            }
        } else {
            File oldDataFile = ydc.getDataFile();
            try {
                //process saving under new name
                File lockFile = getLockFile(dataFile);

                //overwrite datafile field for this data collection
                ydc.setDataFile(dataFile);
                writeToSelectedFile(ydc.getDataFile(), ydc);

                /*store last opened file location*/
                TaskManagerController controllerInstance = TaskManagerController.getInstance();
                Properties userProperties = controllerInstance.getUserProperties();
                userProperties.setProperty(TaskManConfig.USER_PROPERTY_LAST_FILE_LOCATION, dataFile.getAbsolutePath());
                userProperties.store(
                        new FileOutputStream(new File(TaskManConfig.USER_PROPERTIES_FILE)), "last file location");
                controllerInstance.updateFrameTitle(ydc.getSchoolYear());

                /*create a lock for this datafile for current user and register its lock*/
                this.lockedFiles.put(dataFile.getAbsolutePath(), lockFile);
                try {
                    this.createLockFile(lockFile);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Lockfile " + lockFile
                            + " for this datafile could not be created; multi-user mode is now not safe!",
                            "Lockfile creation error",
                            JOptionPane.WARNING_MESSAGE);
                    Logger.getLogger(IOcontroller.class.getName()).log(Level.SEVERE, null, ex);
                }

                //remove lock for old data file
                this.lockedFiles.remove(oldDataFile.getAbsolutePath());
            } catch (IOException ex) {
                Logger.getLogger(IOcontroller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * writes the given dataset to the given file.
     *
     * @param file the output file
     * @param ydc the year data collection
     */
    public void writeToSelectedFile(final File file, final YearDataCollection ydc) {
        try {
            XmlDataWriter xdw = new XmlDataWriter(ydc, file);
            xdw.writeDataCollection();
        } catch (Exception e) {
            //e.printStackTrace();
            JOptionPane.showMessageDialog(
                    null,
                    "an error occurred writing to file: " + e.getMessage(),
                    "data procesing error",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * will return the lock file belonging to the given data file. e.g. file "some_file.xml" will give the lock file
     * name ".some_file.lock"
     *
     * @param dataFile the data file
     * @return lockFile Name
     */
    private File getLockFile(final File dataFile) {
        String dataFileName = dataFile.getName();
        File lockFile = null;
        /*test for existence of a lock file*/
        if (dataFileName.endsWith(".xml")) {
            //make hidden later on with "." +
            String lockFileName = dataFileName.substring(0, dataFileName.length() - 4) + ".lock";
            lockFile = new File(dataFile.getParentFile() + File.separator + lockFileName);
        }
        return lockFile;
    }

    /**
     * method to create a lock file using the given lock file name. Will write as -only- content the system property
     * "user.name" to the file.
     *
     * @param lockFile the lock file
     * @throws IOException ex
     */
    private void createLockFile(final File lockFile) throws IOException {
        lockFile.createNewFile();
        try (BufferedWriter writer = Files.newBufferedWriter(
                Paths.get(lockFile.getAbsolutePath()),
                StandardOpenOption.WRITE)) {
            writer.write(System.getProperty("user.name"));
        }
    }

    /**
     * checks whether the lock file is uncorrupted since it was created. it should contain the System property user.name
     * as sole contents.
     *
     * @param lockFile the lock file
     * @return lockFileUser the user owning the lock
     * @throws IOException ex
     */
    private String getLockFileUser(final File lockFile) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(
                Paths.get(lockFile.getAbsolutePath()))) {
            String lockContents = reader.readLine();
            return lockContents;
        }
    }

    /**
     * checks whether the lock file is uncorrupted since it was created. it should contain the System property user.name
     * as sole contents.
     *
     * @param lockFile the lock file
     * @return lockFileOK
     * @throws IOException ex
     */
    private boolean checkLockFile(final File lockFile) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(
                Paths.get(lockFile.getAbsolutePath()))) {
            String lockContents = reader.readLine();
            return (lockContents.equals(System.getProperty("user.name")));
        }
    }

    /**
     * removes all locks associated with this user.
     */
    public void removeLocksForUser() {
        for (String dataFile : this.lockedFiles.keySet()) {
            try {
                File lockFile = this.lockedFiles.get(dataFile);
                //only delete own locks
                if (checkLockFile(lockFile)) {
                    boolean deleted = lockFile.delete();
                    if (!deleted) {
                        throw new IOException("lockfile " + lockFile + "could not be deleted!");
                    }
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(
                        null,
                        "Error during lockfile cleaning! Please check the datafolder manually",
                        "Lockfile error",
                        JOptionPane.ERROR_MESSAGE);

                Logger.getLogger(IOcontroller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
