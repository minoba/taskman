/**
 *
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.RegularTask;
import nl.bioinf.nomi.tasks.datamodel.TaskCategory;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * exports all regular tasks to excel
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class RegularTasksExcelWriter {

    private ExcelUtils excelUtils;
    private File file;
    private YearDataCollection dataCollection;
    private TaskManagerController controller;

    /**
     *
     * @param file
     * @param file
     * @param controller
     */
    public RegularTasksExcelWriter(File file, TaskManagerController controller) {
        this.file = file;
        this.controller = controller;
        this.dataCollection = controller.getDataCollection();
    }

    /**
     * writes the overview to file
     *
     * @throws IOException
     */
    public void write() throws IOException {
        //System.out.println("writing internship tasks to file " + file );

        Workbook wb = new XSSFWorkbook();
        this.excelUtils = new ExcelUtils(wb);

        FileOutputStream fileOut = new FileOutputStream(file);

        Sheet currSheet = wb.createSheet("overzicht");

        createOverviewSheet(currSheet);

        for (TaskCategory tc : TaskCategory.values()) {
            currSheet = wb.createSheet(tc.name());
            currSheet.setColumnWidth(0, 4500); //4-letter
            currSheet.setColumnWidth(1, 8000); //naam
            currSheet.setColumnWidth(2, 9000); //taak
            currSheet.setColumnWidth(3, 2500); //uren
            currSheet.setColumnWidth(4, 4000); //start kw
            currSheet.setColumnWidth(5, 4000); //eind kw

            createTaskCategoryHeader(currSheet, tc);

            short rowNum = 6;

            for (RegularTask rt : dataCollection.getRegularTasks(tc)) {
                createRegularTaskRow(currSheet, rt, rowNum);

                rowNum++;
            }
        }

        wb.write(fileOut);
        fileOut.close();
    }

    /**
     * creates a single row (entry) for the given project task
     *
     * @param sheet
     * @param pt
     * @param rowNum
     */
    private void createRegularTaskRow(Sheet sheet, RegularTask rt, short rowNum) {
        Row row = sheet.createRow(rowNum);
        excelUtils.createExcelCell(row, 0, rt.getEmployee().getEmployeeId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, rt.getEmployee().getFullName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 2, rt.getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 3, "" + rt.getTaskHours(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
        excelUtils.createExcelCell(row, 4, "" + rt.getTimeframe().getQuarter(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
        excelUtils.createExcelCell(row, 5, "" + rt.getTimeframe().getDuration(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
    }

    /**
     * creates a header section for the given project
     *
     * @param sheet
     * @param p the project
     */
    private void createTaskCategoryHeader(Sheet sheet, TaskCategory tc) {
        short rowNum = 0;
        int taskNumber = controller.getDataCollection().getRegularTasks(tc).size();
        int tcHours = controller.getDataCollection().getRegularTaskHours(tc);
        double tcCosts = controller.getDataCollection().getRegularTaskCosts(tc);

        /*create title row*/
        Row row = sheet.createRow(rowNum);
        excelUtils.createExcelCell(row, 0, tc.toString(), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING);

        rowNum++;
        row = sheet.createRow(rowNum);

        excelUtils.createExcelCell(row, 0, "Aantal taken", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "" + taskNumber, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);

        rowNum++;
        row = sheet.createRow(rowNum);

        excelUtils.createExcelCell(row, 0, "Uren uitgezet", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "" + tcHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);

        rowNum++;
        row = sheet.createRow(rowNum);

        excelUtils.createExcelCell(row, 0, "Kosten", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "" + (int) (Math.round(tcCosts)), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);

        rowNum += 2;

        /*create header row for taskl table*/
        //code	naam	omschrijving	uren	start	kwartalen
        row = sheet.createRow(rowNum);
        excelUtils.createExcelCell(row, 0, "4-Letter code", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "Naam", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 2, "Taak", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 3, "Uren", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 4, "Start (kwartaal)", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 5, "Duur (kwartalen)", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        rowNum++;

    }

    /**
     * creates a an overview sheet of all projects
     *
     * @param sheet
     */
    private void createOverviewSheet(Sheet sheet) {
		//Taak | Uren uitgezet | Kosten

        sheet.setColumnWidth(0, 5000);
        sheet.setColumnWidth(1, 3500);
        sheet.setColumnWidth(2, 3500);

        short rowNum = 0;

        /*create header row*/
        Row row = sheet.createRow(rowNum);
        excelUtils.createExcelCell(row, 0, "Taak Categorie", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "Aantal taken", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 2, "Uren uitgezet", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 3, "Kosten", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        rowNum++;

        int totalTasks = controller.getDataCollection().getRegularTasks().size();
        int totalAssignedHours = controller.getDataCollection().getRegularTaskHours();
        double totalCosts = controller.getDataCollection().getRegularTaskCosts();

        /*create a row per task category*/
        for (TaskCategory tc : TaskCategory.values()) {
            row = sheet.createRow(rowNum);

            /*get category totals*/
            int taskNumber = controller.getDataCollection().getRegularTasks(tc).size();
            int tcHours = controller.getDataCollection().getRegularTaskHours(tc);
            double tcCosts = controller.getDataCollection().getRegularTaskCosts(tc);

            excelUtils.createExcelCell(row, 0, tc.toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
            excelUtils.createExcelCell(row, 1, "" + taskNumber, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
            excelUtils.createExcelCell(row, 2, "" + tcHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
            excelUtils.createExcelCell(row, 3, "" + (int) (Math.round(tcCosts)), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
            rowNum++;
        }

        /*create the totals row*/
        row = sheet.createRow(rowNum);
        excelUtils.createExcelCell(row, 0, "TOTALEN", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "" + totalTasks, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
        excelUtils.createExcelCell(row, 2, "" + totalAssignedHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
        excelUtils.createExcelCell(row, 3, "" + (int) (Math.round(totalCosts)), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
        rowNum++;
    }

}
