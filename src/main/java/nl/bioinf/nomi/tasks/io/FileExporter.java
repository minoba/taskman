package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.Project;
import nl.bioinf.nomi.tasks.datamodel.TaskCategory;
import nl.bioinf.nomi.tasks.gui.TaskManagerFrame;

/**
 * provides several static file (pdf, excel) export functionalities
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class FileExporter {

    /**
     * exports all internship (stage) and graduation (afstuderen) tasks of the current year to an excel file
     *
     * @param controller
     * @param frame
     */
    public static void exportInternshipTaskOverview(TaskManagerController controller, JFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer stage- en afstudeerbezetting naar excel");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File("stage_en_afstudeer_bezetting_" + controller.getCurrentYear() + "_" + (controller.getCurrentYear() + 1) + ".xlsx"));
        FileFilter filter = new FileNameExtensionFilter("Excel file", "xls", "xlsx");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    InternshipTaskWriter itw = new InternshipTaskWriter(file, controller.getDataCollection());
                    itw.write();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * exports all task of all employees to Excel. A file name has to be selected
     *
     * @param controller
     * @param frame
     */
    public static void exportAllTasks(TaskManagerController controller, JFrame frame) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer alle taken naar excel");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File("takenoverzicht_" + controller.getCurrentYear() + "_" + (controller.getCurrentYear() + 1) + ".xlsx"));
        FileFilter filter = new FileNameExtensionFilter("Excel file", "xls", "xlsx");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    AllTasksExcelWriter atew = new AllTasksExcelWriter(file, controller);
                    atew.write();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }

    }

    /**
     * exports all task lists to PDF. Only a directory has to be selected
     *
     * @param controller
     * @param frame
     */
    public static void exportCurriculaOverviews(TaskManagerController controller, JFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer alle curricula overzichten als pdf");
        fileChooser.setApproveButtonText("Save");

        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);

        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File dir = fileChooser.getSelectedFile();
            if (!dir.isDirectory()) {
                JOptionPane.showMessageDialog(frame, "you did not select a directory", "Error", JOptionPane.WARNING_MESSAGE);
            }
            try {
                for (Curriculum curriculum : controller.getDataCollection().getCurricula()) {
                    File f = new File(dir, "Curriculum_" + curriculum.getCurriculumID() + "_" + controller.getCurrentYear() + ".pdf");
                    f.createNewFile();
                    if (!f.canWrite()) {
                        JOptionPane.showMessageDialog(frame, "could not write to selected file: " + f.getAbsolutePath(), "Error", JOptionPane.WARNING_MESSAGE);
                        return;
                    } else {
                        CurriculumPdfWriter emplWriter = new CurriculumPdfWriter(curriculum, controller.getCurrentYear(), f);
                        emplWriter.write();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(frame, ("an error occurred while writing curriculum data" + e.getMessage()), "Error", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * writes a single project to pdf
     *
     * @param project
     * @param year
     * @param frame
     */
    public static void writeProjectToPdf(Project project, int year, JFrame frame) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Bewaar project als pdf");
        fileChooser.setApproveButtonText("Bewaar");
        String projectFileName;
        try {
            projectFileName = java.net.URLEncoder.encode(project.getName(), "UTF-8");
            fileChooser.setSelectedFile(new File(projectFileName + "_" + year + "-" + (year + 1) + ".pdf"));
            FileFilter filter = new FileNameExtensionFilter("PDF file", "pdf");
            fileChooser.addChoosableFileFilter(filter);
            int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                if (testSelectedFile(frame, file)) {
                    try {
                        ProjectPdfWriter projectWriter = new ProjectPdfWriter(project, year, file);
                        projectWriter.write();
                    } catch (Exception e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

    }

    public static void writeRegularTaskCatToPdf(TaskManagerController controller, TaskCategory taskCategory, int year, JFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Bewaar taakcategorie als pdf");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File(taskCategory + "_" + year + "-" + (year + 1) + ".pdf"));
        FileFilter filter = new FileNameExtensionFilter("PDF file", "pdf");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    TaskCategoryPdfWriter taskCatWriter = new TaskCategoryPdfWriter(controller.getDataCollection(), taskCategory, file);
                    taskCatWriter.write();
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * writes a curriculum to pdf overview
     *
     * @param curriculum
     * @param year
     * @param frame
     */
    public static void writeCurriculumToPdf(Curriculum curriculum, int year, JFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Bewaar opleiding als pdf");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File(curriculum.getCurriculumID() + "_" + year + "-" + (year + 1) + ".pdf"));
        FileFilter filter = new FileNameExtensionFilter("PDF file", "pdf");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    CurriculumPdfWriter emplWriter = new CurriculumPdfWriter(curriculum, year, file);
                    emplWriter.write();
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * writes a single employee to pdf
     *
     * @param employee
     * @param year
     * @param frame
     */
    public static void writeEmployeeToPdf(Employee employee, int year, JFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Bewaar medewerker takenlijst als pdf");
        fileChooser.setApproveButtonText("Save");
        fileChooser.setSelectedFile(new File(employee.getEmployeeId() + ".pdf"));
        FileFilter filter = new FileNameExtensionFilter("PDF file", "pdf");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    EmployeePdfWriter emplWriter = new EmployeePdfWriter(employee, year, file);
                    emplWriter.write();
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * exports the task overview of the given list of employees to pdf
     *
     * @param employees
     * @param year
     * @param frame
     */
    public static void exportEmployeeTaskoverviews(List<Employee> employees, int year, JFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Selecteer een folder voor PDF export van medewerker takenlijsten");
        fileChooser.setApproveButtonText("Save");

        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);

        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File dir = fileChooser.getSelectedFile();
            if (!dir.isDirectory()) {
                JOptionPane.showMessageDialog(frame, "you did not select a directory", "Error", JOptionPane.WARNING_MESSAGE);
            }
            try {
                for (Employee e : employees) {
                    File f = new File(dir, e.getTeam() + "_" + e.getEmployeeId() + "_" + year + ".pdf");
                    f.createNewFile();
                    if (!f.canWrite()) {
                        JOptionPane.showMessageDialog(frame, "could not write to selected file: " + f.getAbsolutePath(), "Error", JOptionPane.WARNING_MESSAGE);
                        return;
                    } else {
                        //System.out.println("you selected file: " + file.getAbsolutePath());
                        EmployeePdfWriter emplWriter = new EmployeePdfWriter(e, year, f);
                        emplWriter.write();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(frame, ("an error occurred while writing employee data" + e.getMessage()), "Error", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * exports all task categories of the current year to excel; one sheet per category and a general overview sheet
     *
     * @param controller
     * @param frame
     */
    public static void exportRegularTasksOverview(TaskManagerController controller, TaskManagerFrame frame) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer reguliere taken naar excel");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File("reguliere_taken_" + controller.getCurrentYear() + "_" + (controller.getCurrentYear() + 1) + ".xlsx"));
        FileFilter filter = new FileNameExtensionFilter("Excel file", "xls", "xlsx");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    RegularTasksExcelWriter rtew = new RegularTasksExcelWriter(file, controller);
                    rtew.write();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * exports all projects of the current year to excel; one sheet per project and a general overview sheet
     *
     * @param controller
     * @param frame
     */
    public static void exportProjectsOverview(TaskManagerController controller, TaskManagerFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer projecten naar excel");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File("projecten_" + controller.getCurrentYear() + "_" + (controller.getCurrentYear() + 1) + ".xlsx"));
        FileFilter filter = new FileNameExtensionFilter("Excel file", "xls", "xlsx");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    ProjectsExcelWriter pew = new ProjectsExcelWriter(file, controller.getDataCollection());
                    pew.write();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * exports all projects of the current year to excel; one sheet per project and a general overview sheet
     *
     * @param controller
     * @param frame
     */
    public static void exportPlanningRules(TaskManagerController controller, TaskManagerFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer roosterregels voor alle opleidingen naar excel");
        fileChooser.setApproveButtonText("Kies folder");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
//		fileChooser.setSelectedFile( new File( "projecten_" + controller.getCurrentYear() + "_" + (controller.getCurrentYear()+1) + ".xlsx" ) );
//		FileFilter filter = new FileNameExtensionFilter("Excel file","xls", "xlsx"); 
//		fileChooser.addChoosableFileFilter(filter);

        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    PlanningRulesExcelWriter pew = new PlanningRulesExcelWriter(file, controller.getDataCollection());
                    pew.write();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * exports the financial overview for the curricula to excel
     *
     * @param controller
     * @param frame
     */
    public static void exportGeneralOverview(TaskManagerController controller, TaskManagerFrame frame) {
        /**/
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Exporteer financieel overzicht curricula naar excel");
        fileChooser.setApproveButtonText("Bewaar");
        fileChooser.setSelectedFile(new File("algemeen_overzicht_" + controller.getCurrentYear() + "_" + (controller.getCurrentYear() + 1) + ".xlsx"));
        FileFilter filter = new FileNameExtensionFilter("Excel file", "xls", "xlsx");
        fileChooser.addChoosableFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(frame.getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (testSelectedFile(frame, file)) {
                try {
                    CurriculaResourceUsageExcelWriter oew = new CurriculaResourceUsageExcelWriter(file, controller.getDataCollection());
                    oew.write();
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ("Er is een fout opgetreden tijdens het schrijven" + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * tests whether a selected file can be opened and written and gives an error message if not
     *
     * @param frame
     * @param file
     * @return
     */
    private static boolean testSelectedFile(JFrame frame, File file) {
        if (file.getName().equals("")) {
            JOptionPane.showMessageDialog(frame, "Er is geen bestand geselecteerd", "Fout", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        try {
            if (!file.exists() && !file.createNewFile()) {
                JOptionPane.showMessageDialog(frame, "Het geselecteerde bestand " + file.getAbsolutePath()
                        + " kon niet gemaakt worden", "Fout", JOptionPane.WARNING_MESSAGE);
                return false;
            } else if (file.exists() && !file.canWrite()) {
                JOptionPane.showMessageDialog(frame, "Er kon niet geschreven worden naar het bestand " + file.getAbsolutePath(), "Fout", JOptionPane.WARNING_MESSAGE);
                return false;
            } else {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, ("File kon niet gemaakt worden: " + e.getMessage()), "Fout", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
}
