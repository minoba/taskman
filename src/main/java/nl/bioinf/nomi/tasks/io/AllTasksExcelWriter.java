/**
 *
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

import nl.bioinf.nomi.tasks.control.TaskManagerController;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.ProjectTask;
import nl.bioinf.nomi.tasks.datamodel.RegularTask;
import nl.bioinf.nomi.tasks.datamodel.StudentGroup;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * exports all regular tasks to excel.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class AllTasksExcelWriter {

    private ExcelUtils excelUtils;
    private final File file;
    private final YearDataCollection dataCollection;
    private final TaskManagerController controller;

    /**
     *
     * @param file the file
     * @param controller the controller
     */
    public AllTasksExcelWriter(final File file, final TaskManagerController controller) {
        this.file = file;
        this.controller = controller;
        this.dataCollection = controller.getDataCollection();
    }

    /**
     * writes the overview to file.
     *
     * @throws IOException ex
     */
    public void write() throws IOException {
        Workbook wb = new XSSFWorkbook();
        this.excelUtils = new ExcelUtils(wb);

        try (FileOutputStream fileOut = new FileOutputStream(file)) {
            Sheet currSheet = wb.createSheet("taken");
            createOverviewSheet(currSheet);
            wb.write(fileOut);
        }
    }

    /**
     * creates a an overview sheet of all tasks.
     *
     * @param sheet the excel sheet
     */
    private void createOverviewSheet(final Sheet sheet) {
        //create number formatter
        Locale locale = new Locale("en", "US");
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        numberFormat.setMaximumFractionDigits(3);

        sheet.setColumnWidth(0, 2500); //emp ID
        sheet.setColumnWidth(1, 7000); //naam
        sheet.setColumnWidth(2, 1500); //fte
        sheet.setColumnWidth(3, 1500); //team EXTRA
        sheet.setColumnWidth(4, 4000); //type
        sheet.setColumnWidth(5, 10000); //Klas/Jaar
        sheet.setColumnWidth(6, 4500); //vakcode
        sheet.setColumnWidth(7, 12000); //omschr
        sheet.setColumnWidth(8, 10000); //Categorie
        sheet.setColumnWidth(9, 10000); //Label
        sheet.setColumnWidth(10, 1500); //HBO incl
        sheet.setColumnWidth(11, 1500); //kwartaal EXTRA
        sheet.setColumnWidth(12, 2000); //uren

        short rowNum = 0;

        /*create header row*/
        Row row = sheet.createRow(rowNum);
        excelUtils.createExcelCell(row, 0, "Code", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 1, "Naam", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 2, "fte", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 3, "Team", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 4, "Type", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 5, "Klas/Jaar/ProjectID", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 6, "Vak code", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 7, "Omschrijving", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 8, "Categorie", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 9, "Label", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 10, "HBO incl", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 11, "kwartaal", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        excelUtils.createExcelCell(row, 12, "Uren", ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING);
        rowNum++;

        for (Employee empl : dataCollection.getEmployees()) {
            /*writing all project tasks*/
            for (ProjectTask pt : empl.getProjectTasks()) {
                //iterate quarters
                int[] quarters = {1, 2, 3, 4};
                for (int q : quarters) {

                    double quarterHours = pt.getQuarterHoursRaw(q);
                    //if (quarterHours > 0.0) { //corrected minus hours included again in request of Grietinus
                        row = sheet.createRow(rowNum);
                        String quarterHoursStr = numberFormat.format(quarterHours);
                        excelUtils.createExcelCell(
                                row, 0, empl.getEmployeeId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 1, empl.getFullName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 2, Double.toString(empl.getFte()), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        excelUtils.createExcelCell(
                                row, 3, empl.getTeam(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 4, "project", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 5, pt.getProject().getProjectId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 6, "", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 7, pt.getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 8, pt.getTaskCategory().toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 9, pt.getTaskLabel().toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 10, "nvt",
                                ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 11, Integer.toString(q), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        excelUtils.createExcelCell(
                                row, 12, quarterHoursStr, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        rowNum++;
                    //}
                }
            }
            for (RegularTask rt : empl.getRegularTasksWithCommunalProfessionalizationTask()) {
                //iterate quarters
                int[] quarters = {1, 2, 3, 4};
                for (int q : quarters) {
                    double quarterHours = rt.getQuarterHoursRaw(q);
                    //if (quarterHours > 0.0) { //TODO removed on request of Jeroen Jager
                        //int quarterHoursInt = pt.getQuarterHours(q);
                        String quarterHoursStr = numberFormat.format(quarterHours);
                        row = sheet.createRow(rowNum);
                        excelUtils.createExcelCell(
                                row, 0, empl.getEmployeeId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 1, empl.getFullName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 2, Double.toString(empl.getFte()), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        excelUtils.createExcelCell(
                                row, 3, empl.getTeam(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 4, "regulier", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 5, "", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 6, "", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 7, rt.getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 8, rt.getTaskCategory().toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 9, rt.getTaskLabel().toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 10, "nvt",
                                ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 11, Integer.toString(q), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        excelUtils.createExcelCell(
                                row, 12, quarterHoursStr, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        rowNum++;
                    //}
                }
            }
            for (StudentGroup sg : empl.getStudentGroups()) {
                //iterate quarters
                int[] quarters = {1, 2, 3, 4};
                for (int q : quarters) {
                    double quarterHours = sg.getQuarterHoursRaw(q);
                    //if (quarterHours > 0.0) { //TODO removed on request of Jeroen Jager
                        //int quarterHoursInt = pt.getQuarterHours(q);
                        String quarterHoursStr = numberFormat.format(quarterHours);

                        row = sheet.createRow(rowNum);
                        excelUtils.createExcelCell(
                                row, 0, empl.getEmployeeId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 1, empl.getFullName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 2, Double.toString(empl.getFte()), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        excelUtils.createExcelCell(
                                row, 3, empl.getTeam(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 4, "onderwijs", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        StringBuilder classesSb = new StringBuilder();
                        for (CurriculumYear cy : sg.getCurriculumModule().getAttendingClasses()) {
                            classesSb.append(cy.getCurriculumVariant().getName());
                            classesSb.append(" jaar ");
                            classesSb.append(cy.getYear());
                            classesSb.append(", ");
                        }
                        if (classesSb.length() > 2) {

                            classesSb.delete(classesSb.length() - 2, classesSb.length() - 1);
                        } else {
                            System.out.println("CurriculumModule: " + sg.getCurriculumModule().getModuleId());
                        }
                        excelUtils.createExcelCell(
                                row, 5, classesSb.toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 6, sg.getCurriculumModule().getModuleId(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 7, sg.getCurriculumModule().getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 8, sg.getTaskCategory().toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 9, sg.getTaskLabel().toString(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 10, "nvt",
                                ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING);
                        excelUtils.createExcelCell(
                                row, 11, Integer.toString(q), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        excelUtils.createExcelCell(
                                row, 12, quarterHoursStr, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC);
                        rowNum++;
                    //}
                }
            }
        }
    }
}
