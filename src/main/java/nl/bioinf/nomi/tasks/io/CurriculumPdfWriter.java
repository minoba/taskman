/**
 * 
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.Timeframe;

/**
 * @author Cellingo
 *
 */
public class CurriculumPdfWriter {

	private Curriculum curriculum;
	private int year;
	private File file;
	private Font titleFont;
	private Font bodyFont;
	private Font tableFontMedium;
	private Font tableFontSmall;

	/**
	 * 
	 * @param employee
	 * @param year
	 * @param file
	 */
	public CurriculumPdfWriter(Curriculum curriculum, int year, File file) {
		this.curriculum = curriculum;
		this.year = year;
		this.file = file;
		this.titleFont = FontFactory.getFont( FontFactory.TIMES, 16.0f, Font.BOLD, new BaseColor(0, 0, 0) );
		this.bodyFont = FontFactory.getFont( FontFactory.TIMES, 12.0f, Font.NORMAL, new BaseColor(0, 0, 0) );
		this.tableFontMedium = FontFactory.getFont( FontFactory.TIMES, 10.0f, Font.BOLD, new BaseColor(0, 0, 0) );
		this.tableFontSmall = FontFactory.getFont( FontFactory.TIMES, 9.0f, Font.NORMAL, new BaseColor(0, 0, 0) );
	}
	
	public void write() throws Exception{
		Document document = new Document( PageSize.A4.rotate(), 30, 30, 30, 30);
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
		document.open();
		
		document.add( new Paragraph( "Bezettingsoverzicht " + year + "-" + (year+1) + " opleiding " + curriculum.getName() + " (" + curriculum.getCurriculumID() + ")",	titleFont ) );

		/*traverse years*/
		for( int y=1; y<=4; y++ ){
			List<Timeframe> timeframes = curriculum.getTimeframes(y);
			float[] yearColsWidthOverview = new float[ timeframes.size() ];//{1f, 1f, 1f, 1f};
			/*first determine column widths*/
			for( int i=0; i<timeframes.size(); i++ ){
				yearColsWidthOverview[ i ] = (float)timeframes.get(i).getDuration();
			}
			PdfPTable yearTable = new PdfPTable(yearColsWidthOverview);
			yearTable.setWidthPercentage(100);
			yearTable.setSpacingBefore(2f);
			yearTable.setSpacingAfter(2f);
			yearTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			for( Timeframe tf : timeframes ){
				PdfPCell cell = createPdfCell( "kwartaal " + tf.getOverallQuarterNumber(), tableFontMedium );
				cell.setBorderWidth(0);
				cell.setPadding(2f);
				yearTable.addCell(cell );
			}
			for( Timeframe tf : timeframes ){
				PdfPCell cell = new PdfPCell( getTimeframeTable(tf) );
				cell.setBorderWidth(0);
				cell.setPadding(2f);
				yearTable.addCell(cell );
			}
			document.add(yearTable);
		}
		
		Date now = new Date();
		document.add( new Paragraph( "Gegenereerd op: " + now , bodyFont ) );

		document.close();
		writer.flush();
		writer.close();
	}
	
	/**
	 * creates and returns a table for one timeframe
	 * @param tf
	 * @return
	 */
	private PdfPTable getTimeframeTable( Timeframe tf ){
		List<CurriculumModule> curriculumModules = curriculum.getModules(tf);
		PdfPTable quarterTable = null;
		
		if( tf.getDuration() == 2 && (curriculum.getInternship().overlaps(tf) || curriculum.getGraduation().overlaps(tf) ) ){
			float[] colsWidthOverview = {5f, 1f};
			quarterTable = new PdfPTable(colsWidthOverview);
			quarterTable.setWidthPercentage( (int)(100 * ((double)tf.getDuration() / 4 ) ) );
			quarterTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			quarterTable.addCell( createPdfCell("CurriculumModule", tableFontMedium) );
			//quarterTable.addCell( createPdfCell("Klassen", tableFontMedium) );
			quarterTable.addCell( createPdfCell("Aantal", tableFontMedium) );
			//quarterTable.addCell( createPdfCell("Docent", tableFontMedium) );

			for( CurriculumModule curriculumModule : curriculumModules){
				quarterTable.addCell( createPdfCell( curriculumModule.getName(), tableFontSmall ) );
//				String cl = "";
//				List<CurriculumYear> classes = curriculumModule.getAttendingClasses();
//				for( CurriculumYear cy : classes ){
//					cl += (curriculum.getCurriculumID() + cy.getCurriculumVariant().getType() + cy.getYear() + ", ");
//				}
//				cl = cl.substring(0, cl.length()-2);
//				quarterTable.addCell( createPdfCell( cl, tableFontSmall ) );
				quarterTable.addCell( createPdfCell( curriculumModule.getAttendingStudents()+"", tableFontSmall ) );
				//quarterTable.addCell( createPdfCell( curriculumModule.getStudentGroups().size()+"", tableFontSmall ) );
			}
		}
		else{
			float[] colsWidthOverview = {7f, 2f, 1f};
			quarterTable = new PdfPTable(colsWidthOverview);
			quarterTable.setWidthPercentage( (int)(100 * ((double)tf.getDuration() / 4 ) ) );
			quarterTable.setHorizontalAlignment(Element.ALIGN_LEFT);

			quarterTable.addCell( createPdfCell("CurriculumModule", tableFontMedium) );
			//quarterTable.addCell( createPdfCell("Klassen", tableFontMedium) );
			quarterTable.addCell( createPdfCell("Docent", tableFontMedium) );
			quarterTable.addCell( createPdfCell("Gr", tableFontMedium) );

			for( CurriculumModule curriculumModule : curriculumModules){
				int empCount = 0;
				for( Employee employee : curriculumModule.getAssociatedEmployees() ){
					empCount++;
				//System.out.println( curriculumModule.toString() );
//				for( StudentGroup sg : curriculumModule.getStudentGroups() ){
					if(empCount == 1) quarterTable.addCell( createPdfCell(curriculumModule.getName(), tableFontSmall) );
					else quarterTable.addCell( createPdfCell(" ", tableFontSmall) );
					quarterTable.addCell( createPdfCell( employee.getEmployeeId(), tableFontSmall) );
					quarterTable.addCell( createPdfCell( (curriculumModule.getGroupCount(employee)+""), tableFontSmall) );
					
//					quarterTable.addCell( createPdfCell(cl, tableFontSmall) );
//					quarterTable.addCell( createPdfCell( (sg.getNumber()+""), tableFontSmall) );
				}
			}
		}
		return quarterTable;
	}
	
	
	/**
	 * creates a single table cell
	 * @param text
	 * @param font
	 * @return tableCell
	 */
	private PdfPCell createPdfCell( String text, Font font ){
		Chunk chunk = new Chunk(text, font);
		Phrase phrase = new Phrase( chunk );
		PdfPCell cell = new PdfPCell( phrase );
		cell.setPadding(1f);
		return cell;
	}

}
