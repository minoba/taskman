package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import nl.bioinf.nomi.tasks.datamodel.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import nl.bioinf.nomi.tasks.datamodel.Lesson.LessonType;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;

/**
 * Main data reader.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.2
 */
public class XmlDataReader {

    private YearDataCollection ydc;
    private Document doc;
    private final File in;

    /**
     * constructor.
     *
     * @param in the infile
     */
    public XmlDataReader(final File in) {
        this.in = in;
    }

    /**
     * read from a file with given filename.
     *
     * @return ydc the year data collection
     * @throws Exception ex
     */
    public YearDataCollection readDataCollection() throws Exception {
        if (!in.exists()) {
            throw new Exception("input file: " + in.getAbsolutePath() + " does not exist");
        }
        if (!in.canRead()) {
            throw new Exception("could not read from input file: " + in.getAbsolutePath());
        }
        /*init a data collection*/
        ydc = new YearDataCollection();

        try {
            //File file = new File(fileName);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(in);
            doc.getDocumentElement().normalize();

            parseHeader();
            parseEmployeeMetadata();
            parsePlanningRulesDefaults();
            parseEmployees();
            parseProjects();
            parseRegularTasks();
            parseCourseMethods();
            parseCurriculumTypes();
            parseModules();
            parseCurricula();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("error reading data file " + e.getMessage());
        }
        return ydc;
    }

    /**
     * parse the metadata section.
     *
     * @throws Exception ex
     */
    private void parseHeader() throws Exception {
        NodeList metaElmntLst = doc.getElementsByTagName("header");
        Element metaElmnt = (Element) metaElmntLst.item(0);
        NodeList versionElmntLst = metaElmnt.getElementsByTagName("version");
        Element versionElmnt = (Element) versionElmntLst.item(0);
        //System.out.println("version: " + versionElmnt.getTextContent());
        ydc.setVersion(versionElmnt.getTextContent());

        NodeList schoolElmntLst = metaElmnt.getElementsByTagName("school_id");
        Element schoolElmnt = (Element) schoolElmntLst.item(0);
        ydc.setSchool(schoolElmnt.getTextContent());

        NodeList yearElmntLst = metaElmnt.getElementsByTagName("school_year");
        Element yearElmnt = (Element) yearElmntLst.item(0);
        ydc.setSchoolYear(Integer.parseInt(yearElmnt.getTextContent()));

        NodeList plannerElmntLst = metaElmnt.getElementsByTagName("planner");
        Element plannerElmnt = (Element) plannerElmntLst.item(0);
        ydc.setPlanner(plannerElmnt.getTextContent());
    }

    /**
     * parses the planning rules default sets.
     *
     * @throws Exception ex
     */
    private void parsePlanningRulesDefaults() throws Exception {
        NodeList metaElmntLst = doc.getElementsByTagName("planningrules_defaults");

        /*no defaults are specified*/
        if (metaElmntLst.getLength() == 0) {
            return;
        }
        /*process defaults*/
        Element metaElmnt = (Element) metaElmntLst.item(0);
        NodeList defaultsElmntLst = metaElmnt.getElementsByTagName("planningrules_default");
        for (int i = 0; i < defaultsElmntLst.getLength(); i++) {
            Node defaultNode = defaultsElmntLst.item(i);
            if (defaultNode.getNodeType() == Node.ELEMENT_NODE) {
                Element defaultElement = (Element) defaultNode;

                String defaultName = defaultElement.getAttribute("name");
                if (defaultName.length() == 0) {
                    throw new Exception("planning rule default should have a name attribute");
                }

                int weeks = 10;
                String defaultWeeks = defaultElement.getAttribute("weeks");
                if (defaultWeeks.length() > 0) {
                    weeks = Integer.parseInt(defaultWeeks);
                }

                PlanningRules planningRules = new PlanningRules(weeks, true, defaultName);
                PlanningRules.addDefault(defaultName, planningRules);

                String requirements = "";
                NodeList requirementsElmntLst = defaultElement.getElementsByTagName("requirements");
                if (requirementsElmntLst.getLength() > 0) {
                    Element requirementsElmnt = (Element) requirementsElmntLst.item(0);
                    requirements = requirementsElmnt.getTextContent();
                }
                planningRules.setRequirements(requirements);

                String comments = "";
                NodeList commentsElmntLst = defaultElement.getElementsByTagName("comments");
                if (commentsElmntLst.getLength() > 0) {
                    Element commentsElmnt = (Element) commentsElmntLst.item(0);
                    comments = commentsElmnt.getTextContent();
                }
                planningRules.setComments(comments);

                /*parse week rules*/
                NodeList weekrulesElmntLst = defaultElement.getElementsByTagName("weekrule");
                for (int j = 0; j < weekrulesElmntLst.getLength(); j++) {
                    Node weekruleNode = weekrulesElmntLst.item(j);
                    if (weekruleNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element weekruleElement = (Element) weekruleNode;

                        int weeknumber = 0;
                        String weeknumberStr = weekruleElement.getAttribute("weeknumber");
                        if (weeknumberStr.length() > 0) {
                            weeknumber = Integer.parseInt(weeknumberStr);
                        } else {
                            throw new Exception("weekrule should have a weeknumber");
                        }

                        /*parse lessons*/
                        NodeList lessonsElmntLst = weekruleElement.getElementsByTagName("lesson");
                        for (int k = 0; k < lessonsElmntLst.getLength(); k++) {
                            Node lessonNode = lessonsElmntLst.item(k);
                            if (lessonNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element lessonElement = (Element) lessonNode;

                                String type = lessonElement.getAttribute("type");
                                if (type.length() == 0) {
                                    throw new Exception("lesson should have a type");
                                }
                                /*defaults to 1.5 hours*/
                                double duration = 1.5;

                                String durationStr = lessonElement.getAttribute("duration");
                                if (durationStr.length() > 0) {
                                    duration = Double.parseDouble(durationStr);
                                }

                                planningRules.addLesson(weeknumber, new Lesson(LessonType.valueOf(type), duration));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * parses the employee metadata.
     *
     * @throws Exception ex
     */
    private void parseEmployeeMetadata() throws Exception {
        NodeList metaElmntLst = doc.getElementsByTagName("employee_metadata");
        Element metaElmnt = (Element) metaElmntLst.item(0);

        NodeList fteSizeElmntLst = metaElmnt.getElementsByTagName("fte_size");
        Element fteSizeElmnt = (Element) fteSizeElmntLst.item(0);
        /*class init for fte size*/
        Employee.setFteSize(Integer.parseInt(fteSizeElmnt.getTextContent()));

        NodeList ogplElmntLst = metaElmnt.getElementsByTagName("gpl_euros");
        Element ogplElmnt = (Element) ogplElmntLst.item(0);
        Employee.setGpl(Double.parseDouble(ogplElmnt.getTextContent()));

        NodeList ugplElmntLst = metaElmnt.getElementsByTagName("use_gpl");
        Element ugplElmnt = (Element) ugplElmntLst.item(0);
        Employee.setUseGpl(Boolean.parseBoolean(ugplElmnt.getTextContent()));

        /*class init for function level salary*/
        NodeList gplElmntLst = metaElmnt.getElementsByTagName("function_scale");
        for (int i = 0; i < gplElmntLst.getLength(); i++) {
            Node gplNode = gplElmntLst.item(i);
            if (gplNode.getNodeType() == Node.ELEMENT_NODE) {
                Element gplElement = (Element) gplNode;
                String levelStr = gplElement.getAttribute("level");
                int level = Integer.parseInt(levelStr);
                String valueStr = gplElement.getAttribute("value");
                double value = Double.parseDouble(valueStr);
                Employee.addSalaryLevel(level, value);
//                System.out.println("level = " + level);
//                System.out.println("value = " + value);
            }
        }
    }

    /**
     * parse the employee elements.
     *
     * @throws Exception ex
     */
    private void parseEmployees() throws Exception {
        NodeList employeesBlockElmntLst = doc.getElementsByTagName("employees");
        Element employeesBlockElmnt = (Element) employeesBlockElmntLst.item(0);
        NodeList employeesElmntLst = employeesBlockElmnt.getElementsByTagName("employee");
        /*traverse employees*/
        for (int i = 0; i < employeesElmntLst.getLength(); i++) {
            Node employeeNode = employeesElmntLst.item(i);
            String empId;
            double fte;

            if (employeeNode.getNodeType() == Node.ELEMENT_NODE) {
                Element employeeElement = (Element) employeeNode;
                int scale = 0;
                try {
                    String levelStr = employeeElement.getAttribute("function_scale");
                    scale = Integer.parseInt(levelStr);
                } catch (Exception e) {
                    Logger.getLogger(XmlDataReader.class.getName())
                            .log(Level.OFF, "silently passed parsing salary scale", e);
                }
                empId = employeeElement.getAttribute("id");
                if (empId.equals("")) {
                    throw new Exception("employee has no id");
                }

                NodeList fteElmntLst = employeeElement.getElementsByTagName("fte");
                if (fteElmntLst.getLength() == 0) {
                    throw new Exception("employee has no fte");
                }
                Element fteElmnt = (Element) fteElmntLst.item(0);
                fte = Double.parseDouble(fteElmnt.getTextContent());

                /*create the Teacher object*/
                Employee employee = new Employee(empId, fte);
                employee.setFunctionScale(scale);

                NodeList fNameElmntLst = employeeElement.getElementsByTagName("first_name");
                if (fNameElmntLst.getLength() == 0) {
                    throw new Exception("employee has no first name");
                }
                Element fNameElmnt = (Element) fNameElmntLst.item(0);
                employee.setFirstName(fNameElmnt.getTextContent());
                NodeList iNameElmntLst = employeeElement.getElementsByTagName("infix");
                if (iNameElmntLst.getLength() == 1) {
                    Element iNameElmnt = (Element) iNameElmntLst.item(0);
                    employee.setNameInfix(iNameElmnt.getTextContent());
                }

                NodeList lNameElmntLst = employeeElement.getElementsByTagName("family_name");
                if (lNameElmntLst.getLength() == 0) {
                    throw new Exception("employee has no family name");
                }
                Element lNameElmnt = (Element) lNameElmntLst.item(0);

                employee.setFamilyName(lNameElmnt.getTextContent());

                NodeList teamElmntLst = employeeElement.getElementsByTagName("team");
                if (teamElmntLst.getLength() == 0) {
                    throw new Exception("employee has no team");
                }
                Element teamElmnt = (Element) teamElmntLst.item(0);

                employee.setTeam(teamElmnt.getTextContent());

                NodeList remarksElmntLst = employeeElement.getElementsByTagName("remarks");
                if (remarksElmntLst.getLength() > 0) {
                    Element remarksElmnt = (Element) remarksElmntLst.item(0);
                    employee.setRemarks(remarksElmnt.getTextContent());
                }

                NodeList emailElmntLst = employeeElement.getElementsByTagName("email");
                if (emailElmntLst.getLength() > 0) {
                    Element emailElmnt = (Element) emailElmntLst.item(0);
                    employee.setEmail(emailElmnt.getTextContent());
                }

                if (empId.equals("_VAC")) {
                    Employee.setVacantEmployee(employee);
                }

                /*add the employee to the collection*/
                ydc.addEmployee(employee);
            }
        }
    }

    /**
     * parse the project elements.
     *
     * @throws Exception ex
     */
    private void parseProjects() throws Exception {
        NodeList projectsBlockElmntLst = doc.getElementsByTagName("projects");
        Element projectsBlockElmnt = (Element) projectsBlockElmntLst.item(0);
        NodeList projectsElmntLst = projectsBlockElmnt.getElementsByTagName("project");

        /*traverse projects*/
        for (int i = 0; i < projectsElmntLst.getLength(); i++) {
            Node projectNode = projectsElmntLst.item(i);

            if (projectNode.getNodeType() == Node.ELEMENT_NODE) {
                Element projectElement = (Element) projectNode;

                String pId = projectElement.getAttribute("id");
                if (pId.equals("")) {
                    throw new Exception("project has no id");
                }

                NodeList projectNameElmntLst = projectElement.getElementsByTagName("name");
                if (projectNameElmntLst.getLength() == 0) {
                    throw new Exception("project has no name");
                }
                Element projectNameElmnt = (Element) projectNameElmntLst.item(0);
                String projectName = projectNameElmnt.getTextContent();

                /*create project object*/
                Project project = new Project(pId, projectName);

                NodeList projectDescrElmntLst = projectElement.getElementsByTagName("description");
                if (projectNameElmntLst.getLength() > 0) {
                    Element projectDescrElmnt = (Element) projectDescrElmntLst.item(0);
                    String projectDescr = projectDescrElmnt.getTextContent();
                    project.setDescription(projectDescr);
                }

                NodeList projectHoursElmntLst = projectElement.getElementsByTagName("total_hours");
                if (projectHoursElmntLst.getLength() == 0) {
                    throw new Exception("project has no total hours");
                }
                Element projectHoursElmnt = (Element) projectHoursElmntLst.item(0);
                String projectHours = projectHoursElmnt.getTextContent();
                project.setTotalHours(Integer.parseInt(projectHours));

//                NodeList projectSpendHoursElmntLst = projectElement.getElementsByTagName("spend_hours");
//                if( projectSpendHoursElmntLst.getLength() == 0 ) throw new Exception("project has no spend hours");
//                Element projectSpendHoursElmnt = (Element) projectSpendHoursElmntLst.item(0);
//                String projectSpendHours = projectSpendHoursElmnt.getTextContent();
//                project.setSpendHours( Integer.parseInt(projectSpendHours ) );
//                boolean hboHoursInclusive = false;
//                NodeList hboHoursInclElmntLst = projectElement.getElementsByTagName("hbo_hours_inclusive");
//                if (hboHoursInclElmntLst.getLength() == 1) {
//                    Element hboHoursInclElmnt = (Element) hboHoursInclElmntLst.item(0);
//                    String hboHoursInclStr = hboHoursInclElmnt.getTextContent();
//                    hboHoursInclusive = Boolean.parseBoolean(hboHoursInclStr);
//                }
//                project.setHboHoursInclusive(hboHoursInclusive);

                /*traverse tasks*/
                NodeList tasksElmntLst = projectElement.getElementsByTagName("project_task");
                for (int j = 0; j < tasksElmntLst.getLength(); j++) {
                    Node taskNode = tasksElmntLst.item(j);

                    if (taskNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element taskElement = (Element) taskNode;

                        String taskIdStr = taskElement.getAttribute("id");
                        if (taskIdStr.equals("")) {
                            throw new Exception("project task has no id");
                        }
                        int taskId = Integer.parseInt(taskIdStr);
                        /*defaults to internal projects*/
                        TaskLabel tl = TaskLabel.INTERNAL_PROJECTS;
                        String catStr = taskElement.getAttribute("cat");
                        if (!catStr.equals("")) {
                            try {
                                tl = TaskLabel.valueOf(catStr);
                            } catch (IllegalArgumentException ex) {
                                throw new Exception("could not parse TaskLabel from " + catStr);
                            }
                        }
                        NodeList taskHoursElmntLst = taskElement.getElementsByTagName("task_hours");
                        if (taskHoursElmntLst.getLength() == 0) {
                            throw new Exception("project task has no id");
                        }
                        Element taskHoursElmnt = (Element) taskHoursElmntLst.item(0);
                        int taskHours = Integer.parseInt(taskHoursElmnt.getTextContent());

                        NodeList taskNameElmntLst = taskElement.getElementsByTagName("name");
                        if (taskNameElmntLst.getLength() == 0) {
                            throw new Exception("project task has no name");
                        }
                        Element taskNameElmnt = (Element) taskNameElmntLst.item(0);
                        String taskName = taskNameElmnt.getTextContent();

                        NodeList employeeIdElmntLst = taskElement.getElementsByTagName("employee");
                        if (employeeIdElmntLst.getLength() == 0) {
                            throw new Exception("project task has no id");
                        }
                        Element employeeIdElmnt = (Element) employeeIdElmntLst.item(0);
                        String employeeId = employeeIdElmnt.getTextContent();

                        Employee employee = ydc.getEmployee(employeeId);
                        if (employee == null) {
                            throw new Exception("unknown employee coupled to project task");
                        }

                        /*create the task object and add it to the project*/
                        ProjectTask task = new ProjectTask(taskId, taskHours, project);
                        project.addTask(task);
                        task.setName(taskName);
                        task.setEmployee(employee);
                        task.setTaskLabel(tl);

                        /*process the timeframe if existing*/
                        NodeList timeframeElmntLst = taskElement.getElementsByTagName("timeframe");
                        task.setTimeframe(parseTimeframeNode(timeframeElmntLst));
//                        if( timeframeElmntLst.getLength() > 0 ){
//                            Element timeframeElmnt = (Element) timeframeElmntLst.item(0);
//                            String startStr = timeframeElmnt.getAttribute("start");
//                            if( startStr.equals("") ) throw new Exception("project timeframe has no start");
//                            String durationStr = timeframeElmnt.getAttribute("duration");
//                            if( durationStr.equals("") ) throw new Exception("project timeframe has no duration");
//                            int start = Integer.parseInt(startStr);
//                            int duration = Integer.parseInt(durationStr);
//
//                            //task.setTimeframe( new Timeframe(-1, start, duration) );
//                        }

                        /*register the task with the teacher*/
                        this.ydc.getEmployee(employeeId).addProjectTask(task);

                        /*test print*/
//                        System.out.println("\t" + task);
                    }
                }
                /*add the project to the collection*/
                this.ydc.addProject(project);

                /*test print*/
//                System.out.println( project );
            }
        }
    }

    /**
     * parses a timeframe node.
     * @param timeframeElmntLst the Timeframe element list
     * @return timeframe the Timeframe
     * @throws Exception ex
     */
    private Timeframe parseTimeframeNode(final NodeList timeframeElmntLst) throws Exception {
        //<timeframe start="1" duration="4" partition="20,20,40,20" />

        if (timeframeElmntLst.getLength() > 0) {
            if (timeframeElmntLst.getLength() > 1) {
                throw new Exception("timeframe should not be null");
            }
            Element timeframeElmnt = (Element) timeframeElmntLst.item(0);
            String startStr = timeframeElmnt.getAttribute("start");
            if (startStr.equals("")) {
                throw new Exception("project timeframe has no start");
            }
            String durationStr = timeframeElmnt.getAttribute("duration");
            if (durationStr.equals("")) {
                throw new Exception("project timeframe has no duration");
            }
            int start = Integer.parseInt(startStr);
            int duration = Integer.parseInt(durationStr);
            String partitionStr = timeframeElmnt.getAttribute("partition");
            if (partitionStr == null || "".equals(partitionStr)) {
                return new Timeframe(-1, start, duration);
            } else {
                //System.out.println("partitioning info present: " + partitionStr);
                String[] elmnts = partitionStr.split(",");
                if (elmnts.length != duration) {
                    throw new Exception("partitioning does not match duration: " + partitionStr);
                }
                int[] percentages = new int[elmnts.length];
                for (int i = 0; i < elmnts.length; i++) {
                    percentages[i] = Integer.parseInt(elmnts[i]);
                }
                return new Timeframe(start, duration, percentages);
            }
        }
        /*no Timeframe parsed*/
        throw new Exception("timeframe should not be null");
    }

    /**
     * parse all regular tasks.
     *
     * @throws Exception ex
     */
    private void parseRegularTasks() throws Exception {
        /*
         <regular_tasks>
         <regular_task id="1" cat="ORGANISATIONAL" employee="NOMI" hours="24" >
         <name>stagecoordinatie BFV</name>
         <timeframe start="1" duration="4" />
         </regular_task>
         <regular_task id="2" cat="EDUCATIONAL" employee="NOMI"  hours="24" >
         <name>externe cursus Python</name>
         <timeframe start="4" duration="1" />
         <hbo_hours_inclusive>true</hbo_hours_inclusive>
         <description>Python cursus is onderdeel van cursusportefeuille ILST</description>
         </regular_task>
         </regular_tasks>
         */
        NodeList rTasksBlockElmntLst = doc.getElementsByTagName("regular_tasks");
        if (rTasksBlockElmntLst.getLength() == 0) {
            return;
        }

        Element rTasksBlockElmnt = (Element) rTasksBlockElmntLst.item(0);

        NodeList rTasksElmntLst = rTasksBlockElmnt.getElementsByTagName("regular_task");

        for (int i = 0; i < rTasksElmntLst.getLength(); i++) {
            Node rTaskNode = rTasksElmntLst.item(i);

            if (rTaskNode.getNodeType() == Node.ELEMENT_NODE) {
                Element rTaskElement = (Element) rTaskNode;

                String tIdStr = rTaskElement.getAttribute("id");
                if (tIdStr.equals("")) {
                    throw new Exception("regular_task has no id attribute");
                }
                int tId = Integer.parseInt(tIdStr);

                String tCat = rTaskElement.getAttribute("cat");
                if (tCat.equals("")) {
                    throw new Exception("regular_task has no category attribute");
                }
                TaskLabel tc = TaskLabel.valueOf(tCat);

                String empId = rTaskElement.getAttribute("employee");
                if (empId.equals("")) {
                    throw new Exception("regular_task has no employee attribute");
                }
                Employee employee = ydc.getEmployee(empId);
                if (employee == null) {
                    throw new Exception("unknown employee coupled to regular_task");
                }

                String tHoursStr = rTaskElement.getAttribute("hours");
                if (tHoursStr.equals("")) {
                    throw new Exception("regular_task has no task hours attribute");
                }
                int tHours = Integer.parseInt(tHoursStr);

                NodeList nameElmntLst = rTaskElement.getElementsByTagName("name");
                if (nameElmntLst.getLength() == 0) {
                    throw new Exception("regular_task has no name");
                }
                Element nameElmnt = (Element) nameElmntLst.item(0);
                String name = nameElmnt.getTextContent();

                /*process the timeframe if existing*/
                Timeframe tf;
                NodeList timeframeElmntLst = rTaskElement.getElementsByTagName("timeframe");
                tf = parseTimeframeNode(timeframeElmntLst);

//                if( timeframeElmntLst.getLength() > 0 ){
//                    Element timeframeElmnt = (Element) timeframeElmntLst.item(0);
//                    String startStr = timeframeElmnt.getAttribute("start");
//                    if( startStr.equals("") ) throw new Exception("regular_task timeframe has no start");
//                    String durationStr = timeframeElmnt.getAttribute("duration");
//                    if( durationStr.equals("") ) throw new Exception("regular_task timeframe has no duration");
//                    int start = Integer.parseInt(startStr);
//                    int duration = Integer.parseInt(durationStr);
//
//                    tf = ( new Timeframe(-1, start, duration) );
//                }else throw new Exception("regular_task has no timeframe");
                //descripion removed from model
//                NodeList descElmntLst = rTaskElement.getElementsByTagName("description");
//                String description = null;
//                if( descElmntLst.getLength() > 0 ){
//                    Element descElmnt = (Element) descElmntLst.item(0);
//                    description = descElmnt.getTextContent();
//                }
                NodeList hboIncElmntLst = rTaskElement.getElementsByTagName("hbo_hours_inclusive");
                boolean hboInc = false;
                if (hboIncElmntLst.getLength() > 0) {
                    Element hboIncElmnt = (Element) hboIncElmntLst.item(0);
                    hboInc = Boolean.parseBoolean(hboIncElmnt.getTextContent());
                }

                RegularTask rt = new RegularTask(tId, tHours);
                rt.setEmployee(employee);
                rt.setTaskLabel(tc);
                rt.setName(name);
                rt.setTimeframe(tf);
                ydc.addRegularTask(rt);

                /*register inversely*/
                employee.addRegularTask(rt);
            }
        }

    }

    /**
     * parse the course methods.
     *
     * @throws Exception ex
     */
    private void parseCourseMethods() throws Exception {
        NodeList methodsBlockElmntLst = doc.getElementsByTagName("course_methods");
        Element methodsBlockElmnt = (Element) methodsBlockElmntLst.item(0);
        NodeList methodsElmntLst = methodsBlockElmnt.getElementsByTagName("course_method");

        /*traverse methods*/
        for (int i = 0; i < methodsElmntLst.getLength(); i++) {
            Node methodNode = methodsElmntLst.item(i);

            if (methodNode.getNodeType() == Node.ELEMENT_NODE) {
                Element methodElement = (Element) methodNode;

                String mId = methodElement.getAttribute("id");
                if (mId.equals("")) {
                    throw new Exception("course method has no id");
                }

                NodeList nameElmntLst = methodElement.getElementsByTagName("name");
                if (nameElmntLst.getLength() == 0) {
                    throw new Exception("course method has no name");
                }
                Element nameElmnt = (Element) nameElmntLst.item(0);
                String name = nameElmnt.getTextContent();

                NodeList groupSizeElmntLst = methodElement.getElementsByTagName("group_size");
                if (groupSizeElmntLst.getLength() == 0) {
                    throw new Exception("course method has no group size");
                }
                Element groupSizeElmnt = (Element) groupSizeElmntLst.item(0);
                int groupSize = Integer.parseInt(groupSizeElmnt.getTextContent());

                /*create the CourseMethod object*/
                TimeAssignmentFactors taf = new TimeAssignmentFactors();
                CourseMethod method = new CourseMethod(mId, name, groupSize, taf);
                //System.out.println(this.getClass().getSimpleName() + ": method created " + method.toString());

//                <timefactor category="HOURS_PER_TEACHER" name="voorbereidingsuren" value="1.5" per_ec="true" />
                NodeList factorElmntLst = methodElement.getElementsByTagName("timefactor");
                if (factorElmntLst.getLength() == 0) {
                    throw new Exception("course method has no time factors");
                }
                /*traverse methods*/
                for (int j = 0; j < factorElmntLst.getLength(); j++) {
                    Node factorNode = factorElmntLst.item(j);
                    if (factorNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element factorElement = (Element) factorNode;

                        int category = -1;
                        String fCat = factorElement.getAttribute("category");

                        //System.out.println(fCat);
                        if (fCat.equals("")) {
                            throw new Exception("time factor has no category");
                        }
                        try {
                            category = TimeFactor.getCategory(fCat);
                        } catch (Exception e) {
                            throw new Exception("time factor has no known category");
                        }

                        String fName = factorElement.getAttribute("name");
                        if (fName.equals("")) {
                            throw new Exception("time factor has no name");
                        }

                        double value = 0;
                        String fValue = factorElement.getAttribute("value");
                        if (fValue.equals("")) {
                            throw new Exception("time factor has no value");
                        }
                        try {
                            value = Double.parseDouble(fValue);
                        } catch (Exception e) {
                            throw new Exception("time factor time value is not a decimal number");
                        }

                        boolean perEc = false;
                        String perEcStr = factorElement.getAttribute("per_ec");
                        if (perEcStr.equals("")) {
                            throw new Exception("time factor has no per_ec value");
                        }
                        try {
                            perEc = Boolean.parseBoolean(perEcStr);
                        } catch (Exception e) {
                            throw new Exception("time factor per_ec aspect is not a boolean");
                        }
                        TimeFactor tf = new TimeFactor(category, fName, perEc, value);
                        taf.addTimeFactor(tf);
                    }
                }
                /*add it to class Curriculum*/
                Curriculum.addCourseMethod(method);
            }
        }
    }

    /**
     * parse the accepted curriculum types.
     *
     * @throws Exception ex
     */
    private void parseCurriculumTypes() throws Exception {
        NodeList currTypeBlockElmntLst = doc.getElementsByTagName("curriculum_types");
        Element currTypeBlockElmnt = (Element) currTypeBlockElmntLst.item(0);
        NodeList currTypesElmntLst = currTypeBlockElmnt.getElementsByTagName("type");

        /*traverse curricula*/
        for (int i = 0; i < currTypesElmntLst.getLength(); i++) {
            Node currTypeNode = currTypesElmntLst.item(i);

            if (currTypeNode.getNodeType() == Node.ELEMENT_NODE) {
                Element currTypeElement = (Element) currTypeNode;

                String typeId = currTypeElement.getAttribute("id");
                if (typeId.equals("")) {
                    throw new Exception("curriculum type has no id");
                }

                String typeDesc = currTypeElement.getAttribute("description");
                if (typeDesc.equals("")) {
                    throw new Exception("curriculum type has no description");
                }

                //System.out.println( "type=" + typeId + " desc=" + typeDesc);
                Curriculum.addCurriculumType(typeId, typeDesc);
            }
        }

    }

    /**
     * parse all modules.
     *
     * @throws Exception ex
     */
    private void parseModules() throws Exception {
        /*parse modules*/
        NodeList modulesElmntLst = doc.getElementsByTagName("modules");
        if (modulesElmntLst.getLength() == 0) {
            return;
        }
        Node modulesNode = modulesElmntLst.item(0);
        if (modulesNode.getNodeType() == Node.ELEMENT_NODE) {
            Element modulesElement = (Element) modulesNode;
            NodeList moduleElmntLst = modulesElement.getElementsByTagName("module");
            for (int j = 0; j < moduleElmntLst.getLength(); j++) {
                Node moduleNode = moduleElmntLst.item(j);
                if (moduleNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element moduleElement = (Element) moduleNode;
                    String modId = moduleElement.getAttribute("id");
                    if (modId.equals("")) {
                        throw new Exception("curriculumModule has no id");
                    }
                    String cs = moduleElement.getAttribute("credits");
                    if (cs.equals("")) {
                        throw new Exception("curriculumModule has no credits");
                    }
                    double credits = Double.parseDouble(cs);
                    String qs = moduleElement.getAttribute("quarter");
                    if (qs.equals("")) {
                        throw new Exception("curriculumModule has no quarter");
                    }
                    int quarter = Integer.parseInt(qs);
                    /*set default duration for curriculumModule*/
                    int duration = 1;
                    String ds = moduleElement.getAttribute("duration");
                    if (!ds.equals("")) {
                        duration = Integer.parseInt(ds);
                    }
                    String method = moduleElement.getAttribute("course_method");
                    if (method.equals("")) {
                        throw new Exception("curriculumModule has no course method");
                    }
                    String fixedGroupsString = moduleElement.getAttribute("fixed_groups");
                    boolean fixedGroups = false;
                    if (fixedGroupsString.length() > 0) {
                        fixedGroups = Boolean.parseBoolean(fixedGroupsString);
                    }
                    int lockedGroupNumber = 0;
                    if (fixedGroups) {
                        String lockedGroupNumberStr = moduleElement.getAttribute("group_number");
                        if (lockedGroupNumberStr.length() > 0) {
                            lockedGroupNumber = Integer.parseInt(lockedGroupNumberStr);
                        } else {
                            throw new Exception("error parsing curriculumModule node: "
                                    + "no group number provided for curriculumModule with locked number of groups");
                        }
                    }
                    NodeList nameElmntLst = moduleElement.getElementsByTagName("name");
                    if (nameElmntLst.getLength() == 0) {
                        throw new Exception("curriculumModule has no name");
                    }
                    Element nameElmnt = (Element) nameElmntLst.item(0);
                    String name = nameElmnt.getTextContent();
                    CurriculumModule curriculumModule;
                    if (fixedGroups) {
                        curriculumModule = new CurriculumModule(modId, credits, new Timeframe(-1, quarter, duration),
                                name, Curriculum.getCourseMethod(method), fixedGroups, lockedGroupNumber);
                    } else {
                        curriculumModule = new CurriculumModule(modId, credits, new Timeframe(-1, quarter, duration),
                                name, Curriculum.getCourseMethod(method));
                    }
                    ydc.addModule(curriculumModule);
                    /*traverse student groups*/
                    NodeList sgElmntLst = moduleElement.getElementsByTagName("student_group");
                    for (int i = 0; i < sgElmntLst.getLength(); i++) {
                        Node sgNode = sgElmntLst.item(i);
                        if (sgNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element sgElement = (Element) sgNode;
                            String gn = sgElement.getAttribute("number");
                            if (gn.equals("")) {
                                throw new Exception("student group has no group number");
                            }
                            int groupNumber = Integer.parseInt(gn);
                            String sns = sgElement.getAttribute("student_number");
                            if (sns.equals("")) {
                                throw new Exception("student group has no student number");
                            }
                            int studentNumber = Integer.parseInt(sns);
                            String empId = sgElement.getAttribute("employee");
                            if (sns.equals("")) {
                                throw new Exception("student group has no employee");
                            }
                            //TaskLabel tl = TaskLabel.CURRICULUM_EXECUTION;
                            //String catStr = sgElement.getAttribute("cat");
//                            if (!catStr.equals("")) {
//                                try {
//                                    tl = TaskLabel.valueOf(catStr);
//                                } catch (IllegalArgumentException ex) {
//                                    throw new Exception("could not parse TaskLabel from " + catStr);
//                                }
//                            }

                            /*create the student group and register it with the curriculumModule and the teacher*/
                            Employee empl = ydc.getEmployee(empId);
                            StudentGroup sg = new StudentGroup(groupNumber, empl, curriculumModule);
                            sg.setStudentNumber(studentNumber);
//                            sg.setTaskLabel(tl);
                            empl.addStudentGroup(sg);
                            curriculumModule.addStudentGroup(sg);
                        }
                    }
                    /*traverse planning rules*/
                    NodeList prElmntLst = moduleElement.getElementsByTagName("planning_rules");
                    for (int i = 0; i < prElmntLst.getLength(); i++) {
                        Node prNode = prElmntLst.item(i);
                        if (i == 1) {
                            assert false : "only one planning rule element expected";
                        }
                        if (prNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element prElement = (Element) prNode;
                            String defaultRulesName = prElement.getAttribute("default_name");
                            if (defaultRulesName.length() > 0) {
                                assert PlanningRules.getDefault(defaultRulesName) != null
                                        : "this default type planning rules is not registered: " + defaultRulesName;
                                PlanningRules planningRules = PlanningRules.getDefault(defaultRulesName);
                                curriculumModule.setPlanningRules(planningRules);
                                continue;
                            }
                            String req = prElement.getAttribute("requirements");
                            String comments = prElement.getAttribute("comments");
                            PlanningRules planningRules = new PlanningRules(curriculumModule.getTimeframe().getDuration() * 10);
                            planningRules.setRequirements(req);
                            planningRules.setComments(comments);
                            curriculumModule.setPlanningRules(planningRules);
                            /*traverse weekrules*/
                            NodeList wrElmntLst = prElement.getElementsByTagName("weekrule");
                            for (int k = 0; k < wrElmntLst.getLength(); k++) {
                                Node wrNode = wrElmntLst.item(k);
                                if (wrNode.getNodeType() == Node.ELEMENT_NODE) {
                                    Element wrElement = (Element) wrNode;
                                    String wn = wrElement.getAttribute("weeknumber");
                                    if (wn.equals("")) {
                                        throw new Exception("weeknumber has no number");
                                    }
                                    int weekNumber = Integer.parseInt(wn);
                                    /*traverse lessons*/
                                    NodeList lessonsElmntLst = wrElement.getElementsByTagName("lesson");
                                    for (int l = 0; l < lessonsElmntLst.getLength(); l++) {
                                        Node lessonNode = lessonsElmntLst.item(l);
                                        if (lessonNode.getNodeType() == Node.ELEMENT_NODE) {
                                            Element lessonElement = (Element) lessonNode;
                                            String lessonTypeStr = lessonElement.getAttribute("type");
                                            if (lessonTypeStr.equals("")) {
                                                throw new Exception("lesson has no lessonType");
                                            }
                                            LessonType lessonType = LessonType.valueOf(lessonTypeStr);
                                            String lessonCommentsStr = lessonElement.getAttribute("comments");
                                            if (lessonCommentsStr.equals("")) {
                                                lessonCommentsStr = "";
                                            }
                                            String lessonDurationStr = lessonElement.getAttribute("duration");
                                            Lesson lesson;
                                            if (lessonDurationStr.equals("")) {
                                                lesson = new Lesson(lessonType);
                                            } else {
                                                double lessonDuration = Double.parseDouble(lessonDurationStr);
                                                lesson = new Lesson(lessonType, lessonDuration);
                                            }
                                            lesson.setComments(lessonCommentsStr);
                                            planningRules.addLesson(weekNumber, lesson);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * parse the curricula.
     *
     * @throws Exception ex
     */
    private void parseCurricula() throws Exception {
        NodeList curriculaBlockElmntLst = doc.getElementsByTagName("curricula");
        if (curriculaBlockElmntLst.getLength() == 0) {
            return;
        }

        Element curriculaBlockElmnt = (Element) curriculaBlockElmntLst.item(0);
        NodeList curriculaElmntLst = curriculaBlockElmnt.getElementsByTagName("curriculum");
        /*traverse curricula*/
        for (int i = 0; i < curriculaElmntLst.getLength(); i++) {
            Node curriculumNode = curriculaElmntLst.item(i);

            if (curriculumNode.getNodeType() == Node.ELEMENT_NODE) {
                Element curriculumElement = (Element) curriculumNode;

                NodeList abbreviationElmntLst = curriculumElement.getElementsByTagName("id");
                if (abbreviationElmntLst.getLength() == 0) {
                    throw new Exception("course curriculum has no id");
                }
                Element abbreviationElmnt = (Element) abbreviationElmntLst.item(0);
                String abbreviation = abbreviationElmnt.getTextContent();

                NodeList nameElmntLst = curriculumElement.getElementsByTagName("name");
                if (nameElmntLst.getLength() == 0) {
                    throw new Exception("course curriculum has no name");
                }
                Element nameElmnt = (Element) nameElmntLst.item(0);
                String name = nameElmnt.getTextContent();

                /*create the curriculum object*/
                Curriculum curriculum = new Curriculum(abbreviation, name);
                ydc.addCurriculum(curriculum);

                NodeList intElmntLst = curriculumElement.getElementsByTagName("internship");
                if (intElmntLst.getLength() == 0) {
                    throw new Exception("course curriculum has no internship");
                }
                Element intElmnt = (Element) intElmntLst.item(0);

                try {
                    String intYearStr = intElmnt.getAttribute("year");
                    String intQuarterStr = intElmnt.getAttribute("quarter");
                    String intDurationStr = intElmnt.getAttribute("duration");
                    int intYear = Integer.parseInt(intYearStr);
                    int intQuarter = Integer.parseInt(intQuarterStr);
                    int intDuration = Integer.parseInt(intDurationStr);
                    curriculum.setInternship(new Timeframe(intYear, intQuarter, intDuration));
                } catch (Exception e) {
                    throw new Exception("parsing curriculum internship attributes failed");
                }

                NodeList gradElmntLst = curriculumElement.getElementsByTagName("graduation");
                if (gradElmntLst.getLength() == 0) {
                    throw new Exception("course curriculum has no graduation ");
                }
                Element gradElmnt = (Element) gradElmntLst.item(0);

                try {
                    String gradYearStr = gradElmnt.getAttribute("year");
                    String gradQuarterStr = gradElmnt.getAttribute("quarter");
                    String gradDurationStr = gradElmnt.getAttribute("duration");
                    int gradYear = Integer.parseInt(gradYearStr);
                    int gradQuarter = Integer.parseInt(gradQuarterStr);
                    int gradDuration = Integer.parseInt(gradDurationStr);
                    curriculum.setGraduation(new Timeframe(gradYear, gradQuarter, gradDuration));
                } catch (Exception e) {
                    throw new Exception("parsing curriculum graduation attributes failed");
                }

                /*parse curriculum variants*/
                NodeList currVarsElmntLst = curriculumElement.getElementsByTagName("curriculum_variant");
                parseCurriculumVariants(currVarsElmntLst, curriculum);

                /*test print*/
                //System.out.println( curriculum );
            }
        }
    }

    /**
     * parse curriculum variants.
     * @param currVarsElmntLst the curriculum variants list
     * @param curriculum the curriculum
     * @throws Exception ex
     */
    private void parseCurriculumVariants(
            final NodeList currVarsElmntLst,
            final Curriculum curriculum) throws Exception {
        for (int j = 0; j < currVarsElmntLst.getLength(); j++) {
            Node curVarNode = currVarsElmntLst.item(j);
            if (curVarNode.getNodeType() == Node.ELEMENT_NODE) {
                Element curVarElement = (Element) curVarNode;
                String typeStr = curVarElement.getAttribute("type");
                if (typeStr.equals("")) {
                    throw new Exception("curriculum variant has no type");
                }
                /*create*/
                CurriculumVariant cv = new CurriculumVariant(curriculum, typeStr);
                curriculum.addCurriculumVariant(cv);
                /*parse curriculum years*/
                NodeList currYearsElmntLst = curVarElement.getElementsByTagName("year");
                parseCurriculumYears(currYearsElmntLst, cv);
            }
        }
    }

    /**
     * parse the years of a curriculum variant.
     *
     * @param currYearsElmntLst list
     * @param cv curriculum variant
     * @throws Exception ex
     */
    private void parseCurriculumYears(final NodeList currYearsElmntLst, final CurriculumVariant cv) throws Exception {
        for (int j = 0; j < currYearsElmntLst.getLength(); j++) {
            Node curYearNode = currYearsElmntLst.item(j);
            if (curYearNode.getNodeType() == Node.ELEMENT_NODE) {
                Element curYearElement = (Element) curYearNode;

                String yearStr = curYearElement.getAttribute("number");
                if (yearStr.equals("")) {
                    throw new Exception("curriculum variant year has no number");
                }
                int year = Integer.parseInt(yearStr);
                /*create and process the curriculum year*/
                CurriculumYear cy = new CurriculumYear(cv, year);
                cv.addCurriculumYear(cy);
                /*parse quarters*/
                NodeList quartersElmntLst = curYearElement.getElementsByTagName("quarter");
                parseQuarters(quartersElmntLst, cy);
            }
        }
    }

    /**
     * parse quarter data.
     *
     * @param quartersElmntLst list
     * @param cy curriculum  year
     * @throws Exception ex
     */
    private void parseQuarters(final NodeList quartersElmntLst, final CurriculumYear cy) throws Exception {
        for (int j = 0; j < quartersElmntLst.getLength(); j++) {
            Node quarterNode = quartersElmntLst.item(j);
            if (quarterNode.getNodeType() == Node.ELEMENT_NODE) {
                Element quarterElement = (Element) quarterNode;

                String qnStr = quarterElement.getAttribute("number");
                if (qnStr.equals("")) {
                    throw new Exception("quarter number is not present");
                }
                int quarter = Integer.parseInt(qnStr);

                String csStr = quarterElement.getAttribute("class_size");
                if (csStr.equals("")) {
                    throw new Exception("class_size is not present in class " + cy);
                }
                int cs = Integer.parseInt(csStr);
                cy.setStudentNumber(quarter, cs);

                NodeList modulesElmntLst = quarterElement.getElementsByTagName("module");
                for (int k = 0; k < modulesElmntLst.getLength(); k++) {
                    Node moduleNode = modulesElmntLst.item(k);
                    if (moduleNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element moduleElement = (Element) moduleNode;

                        String moduleId = moduleElement.getAttribute("id");
                        if (moduleId.equals("")) {
                            throw new Exception("curriculumModule has no ID");
                        }
                        /*retrieve the curriculumModule*/
                        CurriculumModule curriculumModule = ydc.getModule(moduleId);
                        if (curriculumModule == null) {
                            throw new Exception("no curriculumModule found with this ID: " + moduleId);
                        }
                        /*add the curriculumModule to the curriculum year*/
                        cy.addModule(quarter, curriculumModule);
                        /*register the class with the curriculumModule*/
                        curriculumModule.addAttendingClass(cy);
                    }
                }
            }
        }
    }
}
