/**
 * 
 */
package nl.bioinf.nomi.tasks.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import nl.bioinf.nomi.tasks.datamodel.CourseMethod;
import nl.bioinf.nomi.tasks.datamodel.Curriculum;
import nl.bioinf.nomi.tasks.datamodel.CurriculumYear;
import nl.bioinf.nomi.tasks.datamodel.Employee;
import nl.bioinf.nomi.tasks.datamodel.YearDataCollection;


/**
 * is responsible for exporting resource usage overview (hours, euros) of curricula to excel
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class CurriculaResourceUsageExcelWriter {
	private ExcelUtils excelUtils;
	private File file;
	private YearDataCollection dataCollection;
	private HashMap<String, List<CurriculumYear>> curriculumYears;
	private HashMap<String, int[]> totals;
	private final String[] columnNames = { "Curriculumvariant", "Jaar", "Lab / praktijkdocent (uren)", "Lab / theoriedocent (uren)",
			"Theorie (uren)", "Speciale onderwijstaken (uren)", "Stage/Afstuderen (uren)","Totaal (uren)", "Totaal (euro)", "Studenten", "Euro/student" };
	private HashSet<Integer> practicumScales;
	private HashSet<Integer> theoryScales;
	private HashSet<Integer> allScales;
	private HashSet<CourseMethod> allMethods;
	private NumberFormat numberFormat;
	private Sheet sheet;
	private int rowNum;
	

	public CurriculaResourceUsageExcelWriter(File file, YearDataCollection dataCollection) {
		this.curriculumYears = new HashMap<String, List<CurriculumYear>>();
		this.totals = new HashMap<String, int[]>();
		this.file = file;
		this.dataCollection = dataCollection;
		this.practicumScales = new HashSet<Integer>();
		practicumScales.add(8);
		practicumScales.add(9);
		this.theoryScales = new HashSet<Integer>();
		theoryScales.add(11);
		theoryScales.add(12);
		this.allScales = new HashSet<Integer>();
		allScales.add(9);
		allScales.add(10);
		allScales.add(11);
		allScales.add(12);
		allScales.add(13);
		this.allMethods = new HashSet<CourseMethod>();
		allMethods.addAll(Curriculum.getCourseMethods());
		
		numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(0);
	}

	public void write() throws IOException {
		/*fetch the collection of classes*/
		List<Curriculum> curricula = dataCollection.getCurricula();
		for( Curriculum curriculum : curricula ){
			curriculumYears.put( curriculum.getCurriculumID(), new ArrayList<CurriculumYear>() );
			for( int y=1; y<=4; y++){
				List<CurriculumYear> varYears = curriculum.getCurriculumYearVariants(y);
				curriculumYears.get( curriculum.getCurriculumID() ).addAll(varYears);
			}
		}
		List<String> currIds = new ArrayList<String>(); 
		currIds.addAll( curriculumYears.keySet() );

		Workbook wb = new XSSFWorkbook();
		this.excelUtils = new ExcelUtils( wb ); 
		
		FileOutputStream fileOut = new FileOutputStream(file);

		int stWidth = 6000;
		sheet = wb.createSheet( );
		sheet.setColumnWidth(0, 5000); //Curriculumvariant
		sheet.setColumnWidth(1, 5000); //jaar
		sheet.setColumnWidth(2, stWidth); //lab/pd
		sheet.setColumnWidth(3, stWidth); //lab/td
		sheet.setColumnWidth(4, stWidth); //spec
		sheet.setColumnWidth(5, stWidth); //st/afst
		sheet.setColumnWidth(6, stWidth);
		sheet.setColumnWidth(7, stWidth);
		sheet.setColumnWidth(8, stWidth);
		sheet.setColumnWidth(9, stWidth);
		sheet.setColumnWidth(10, stWidth);

		rowNum = 0;
		
		/*create title row*/
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, "Overzicht financien en uren van de curricula", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		
		rowNum++;
		row = sheet.createRow( rowNum );		
		excelUtils.createExcelCell(row, 0, "", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );

		rowNum = 8;

		for( String currId : currIds ){
			int[] currTotals = new int[10];
			totals.put(currId, currTotals);
			addCurriculumOverview(currId);
		}
		
		Row totalsRow = sheet.createRow( 3 );
		int totalHours=0;
		int totalEuros=0;
		int totalStudents =0;
		for( int[] tots : totals.values()){
			totalHours += tots[7];
			totalEuros += tots[8];
			totalStudents += tots[9];
		}
		excelUtils.createExcelCell(totalsRow, 0, "Totaal uren=", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(totalsRow, 1, numberFormat.format(totalHours), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_NUMERIC );
		
		//System.out.println("total euros=" + numberFormat.format(totalEuros));
		excelUtils.createExcelCell(totalsRow, 2, "Totaal euros=", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(totalsRow, 3, numberFormat.format(totalEuros), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		
		excelUtils.createExcelCell(totalsRow, 4, "Totaal fte=", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(totalsRow, 5, numberFormat.format((double)totalHours/Employee.getFteSize()), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_NUMERIC );
		
		excelUtils.createExcelCell(totalsRow, 6, "Totaal studenten=", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(totalsRow, 7, numberFormat.format(totalStudents), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_NUMERIC );

		excelUtils.createExcelCell(totalsRow, 8, "Euros per student=", ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(totalsRow, 9, numberFormat.format(totalEuros/totalStudents), ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_NUMERIC );

		wb.write(fileOut);
		fileOut.close();		
	}
	
	private void addCurriculumOverview( String curriculumId ){
		
		Row row = sheet.createRow( rowNum );
		
		excelUtils.createExcelCell(row, 0, curriculumId, ExcelUtils.TITLE_STYLE, Cell.CELL_TYPE_STRING );
		
		createHeaderRow();
		rowNum++;
		
		List<CurriculumYear> curYears = curriculumYears.get(curriculumId);
		for( CurriculumYear cy : curYears ){
			createCurriculumYearRow( cy);
			rowNum++;
		}
		
		int[] curTot = totals.get(curriculumId);
		createCurriculumTotalsRow(curTot);
		
		rowNum++;
	}
	
	private void createCurriculumTotalsRow(int[] curTot) {
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, "Totaal", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, "Alle jaren", ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		
		for( int i=2; i<10; i++){
			excelUtils.createExcelCell(row, i, ""+numberFormat.format(curTot[i]), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		}
		int sNum = curTot[9];
		String eps = "";
		if( sNum == 0 ) eps = "";
		else eps = numberFormat.format( curTot[8] / sNum ); 
		excelUtils.createExcelCell(row, 10, eps, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
	}

	private void createHeaderRow(){
		Row row = sheet.createRow( rowNum );
		int cellNum = 0;
		for( String colName : columnNames ){
			excelUtils.createExcelCell(row, cellNum, colName, ExcelUtils.SUBTITLE_STYLE, Cell.CELL_TYPE_STRING );
			cellNum++;
		}
	}

	
	private void createCurriculumYearRow(CurriculumYear curriculumYear){
		/*
		 *			case 0: return curriculumYears.get(curriculumId).get(r).getCurriculumVariant().getName();
					case 1: return curriculumYears.get(curriculumId).get(r).getYear();
					case 2: return curriculumYears.get(curriculumId).get(r).getInvestedHours( practicumScales, Curriculum.getLabMethods() );
					case 3: return curriculumYears.get(curriculumId).get(r).getInvestedHours( theoryScales, Curriculum.getLabMethods() );
					case 4: return curriculumYears.get(curriculumId).get(r).getInvestedHours( theoryScales, Curriculum.getTheoryMethods() );
					case 5: return curriculumYears.get(curriculumId).get(r).getInvestedHours( allScales, Curriculum.getSpecialMethods() );
					case 6: return curriculumYears.get(curriculumId).get(r).getInvestedHours( allScales, Curriculum.getGraduationMethods() );
					case 7: return curriculumYears.get(curriculumId).get(r).getInvestedHours( allScales, allMethods );
					case 8: return curriculumYears.get(curriculumId).get(r).getInvestedEuros( allScales, allMethods );
					case 9: return curriculumYears.get(curriculumId).get(r).getStudentNumber( 1 );
					case 10:{
						Integer studentNumber = Integer.parseInt( getValueAt(r, 9).toString() );
						if( studentNumber == 0 ) return "NA";
						else return numberFormat.format( Double.parseDouble(getValueAt(r, 8).toString()) / studentNumber ); 
					}
					default: return "";
*/
		int[] curTot = totals.get(curriculumYear.getCurriculumVariant().getCurriculum().getCurriculumID());
		Row row = sheet.createRow( rowNum );
		excelUtils.createExcelCell(row, 0, curriculumYear.getCurriculumVariant().getName(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_STRING );
		excelUtils.createExcelCell(row, 1, ""+curriculumYear.getYear(), ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		
		int plHours = curriculumYear.getInvestedHours( practicumScales, Curriculum.getLabMethods() );
		excelUtils.createExcelCell(row, 2, ""+plHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[2] += plHours;
		
		int tlHours = curriculumYear.getInvestedHours( theoryScales, Curriculum.getLabMethods() );
		excelUtils.createExcelCell(row, 3, ""+tlHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[3] += tlHours;
		
		int tHours = curriculumYear.getInvestedHours( theoryScales, Curriculum.getTheoryMethods() );
		excelUtils.createExcelCell(row, 4, ""+tHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[4] += tHours;
		
		int sHours = curriculumYear.getInvestedHours( allScales, Curriculum.getSpecialMethods() );
		excelUtils.createExcelCell(row, 5, ""+sHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[5] += sHours;
		
		int gHours = curriculumYear.getInvestedHours( allScales, Curriculum.getGraduationMethods() );
		excelUtils.createExcelCell(row, 6, ""+gHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[6] += gHours;
		
		int aHours = curriculumYear.getInvestedHours( allScales, allMethods );
		excelUtils.createExcelCell(row, 7, ""+aHours, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[7] += aHours;
		
		int aEuros = curriculumYear.getInvestedEuros( allScales, allMethods );
		excelUtils.createExcelCell(row, 8, ""+aEuros, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[8] += aEuros;
		
		int sNum = curriculumYear.getStudentNumber( 1 );
		excelUtils.createExcelCell(row, 9, ""+sNum, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );
		curTot[9] += sNum;
		
		String eps = "";
		if( sNum == 0 ) eps = "";
		else eps = numberFormat.format( aEuros / sNum ); 
		excelUtils.createExcelCell(row, 10, eps, ExcelUtils.DATA_STYLE, Cell.CELL_TYPE_NUMERIC );

	}
	
}
