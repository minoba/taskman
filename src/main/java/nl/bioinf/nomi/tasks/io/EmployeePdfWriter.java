/**
 *
 */
package nl.bioinf.nomi.tasks.io;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import nl.bioinf.nomi.tasks.configuration.TaskManConfig;
import nl.bioinf.nomi.tasks.datamodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Writes a tasklist and general info of a single Employee to pdf.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 *
 */
public class EmployeePdfWriter {

    private final Employee employee;
    private final int year;
    private final File file;
    private final Font titleFont;
    private final Font subtitleFont;
    private final Font bodyFont;
    private final Font tableFontLarge;
    private final Font tableFontSmall;
//    private final int[] quarterSubTotals = new int[5];

    //TODO refactor these out
//    private final int[] quarterTotalsHBOhours = new int[5];
    /**
     * quarter totals HBO hours.
     */
    private final int[] quarterTotals = new int[5];
    /**
     * relatieve fte belasting.
     */
    private final String[] quarterFteSizes = new String[5];

    private NumberFormat nf = NumberFormat.getNumberInstance();
    /**
     * the tasks list is the model.
     */
    private final List<Task> tasks;

    /**
     *
     * @param employee the employee
     * @param year the school year
     * @param file the file to write to
     */
    public EmployeePdfWriter(final Employee employee, final int year, final File file) {
        this.employee = employee;
        this.year = year;
        this.file = file;
        this.titleFont = FontFactory.getFont(FontFactory.TIMES, 16.0f, Font.BOLD, new BaseColor(0, 0, 0));
        this.subtitleFont = FontFactory.getFont(FontFactory.TIMES, 13.0f, Font.BOLD, new BaseColor(20, 20, 20));
        this.bodyFont = FontFactory.getFont(FontFactory.TIMES, 12.0f, Font.NORMAL, new BaseColor(0, 0, 0));
        this.tableFontLarge = FontFactory.getFont(FontFactory.TIMES, 11.0f, Font.BOLD, new BaseColor(10, 10, 10));
        this.tableFontSmall = FontFactory.getFont(FontFactory.TIMES, 10.0f, Font.NORMAL, new BaseColor(10, 10, 10));
        this.tasks = new ArrayList<Task>();
        tasks.addAll(employee.getProjectTasks());
        tasks.addAll(employee.getRegularTasksWithCommunalProfessionalizationTask());
        tasks.addAll(employee.getStudentGroups());
        calculateTotals();
    }

    private void calculateTotals() {
//            double[] qHoursHBO = new double[4];

        /**
         * q1 / q2 / q3 / q4 / year
         */
        double[] qHours = new double[5];
        for (Task t : tasks) {
//                if (t.getTaskCategory() == TaskCategory.PROFESSIONALIZATION) {
//                    continue;
//                }
            //System.out.println(t.toString());
            for (int i = 0; i <= 3; i++) {
                double taskHours = t.getQuarterHoursRaw(i + 1);
                qHours[i] += taskHours;
//                    if (!t.isHboHoursInclusive()) {
//                        qHoursHBO[i] += (t.getQuarterHoursRaw(i + 1) * 0.25); //NOT 0.20 BECAUSE REVERSED FRACTIONATION!
//                    }
            }
            qHours[4] += t.getTaskHours();
        }
        for (int q = 0; q < qHours.length; q++) {
//                quarterSubTotals[q] = (int) Math.round(qHours[q]);
            quarterTotals[q] = (int) Math.round(qHours[q]);
            quarterFteSizes[q] = nf.format(quarterTotals[q] / ((double) Employee.getFteSize() / 4));
        }
//            for (int q = 0; q < qHoursHBO.length; q++) {
//                quarterTotalsHBOhours[q] = (int) Math.round(qHoursHBO[q]);
//                quarterTotals[q] += (int) Math.round(qHoursHBO[q]);
//            }
//            quarterTotalsHBOhours[4] = employee.getHboHours();
//            quarterTotals[4] += employee.getHboHours();
        quarterFteSizes[4] = nf.format(quarterTotals[4] / (double) Employee.getFteSize());
    }

    /**
     *
     * @throws Exception ex
     */
    public void write() throws Exception {
        //HboHoursPartition hboHoursPartition = employee.getHboHoursPartition();

        Document document = new Document(PageSize.A4, 40, 40, 40, 40);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();
        document.add(new Paragraph("Takenoverzicht " + year + "-" + (year + 1)
                + " voor " + employee.getFullName() + " (" + employee.getEmployeeId() + ")", titleFont));
        document.add(new Paragraph("Algemeen", subtitleFont));
//        document.add(new Paragraph(" ", bodyFont));
        float[] colsWidthOverview = {12f, 2f};

        PdfPTable emplOverviewTable = new PdfPTable(colsWidthOverview);
        emplOverviewTable.setWidthPercentage(80);

        emplOverviewTable.setHorizontalAlignment(Element.ALIGN_LEFT);

        emplOverviewTable.addCell(createPdfCell("Team", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getTeam(), tableFontLarge));

        emplOverviewTable.addCell(createPdfCell("Aanstelling (fte)", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getFte() + "", tableFontLarge));

        int totalHours = (int) Math.round(employee.getFte() * Employee.getFteSize());
        emplOverviewTable.addCell(createPdfCell("1: Aanstelling (uren)", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(totalHours + "", tableFontLarge));

        emplOverviewTable.addCell(createPdfCell("2: Totaal van niet curriculumgebonden taken", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getNonCurriculumboundHours() + "", tableFontLarge));

        //int subtotal = (totalHours - employee.getHboInclusiveTaskHours());
        emplOverviewTable.addCell(createPdfCell("3: Totaal van curriculumgebonden taken (waarin 10% HBO uren)", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getCurriculumboundHours() + "", tableFontLarge));

        int professionalizationHours = employee.getProfessionalizationHours();
        emplOverviewTable.addCell(createPdfCell("4: Professionaliseringsuren (4a + 4b)", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(professionalizationHours + "", tableFontLarge));

        emplOverviewTable.addCell(createPdfCell("    4a: Vrije professionaliseringsuren (40 uur bij >0,4 fte) ", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getFreelySpendableProfessionalizationHours() + "", tableFontLarge));

        emplOverviewTable.addCell(createPdfCell("    4b: Schoolgestuurde professionalisering", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getCummunalProfessionalizationHours() + "", tableFontLarge));

        emplOverviewTable.addCell(createPdfCell("5: Resturen (1 - 2 - 3 - 4b); negatief is overwerk", tableFontLarge));
        emplOverviewTable.addCell(createPdfCell(employee.getVacantHours() + "", tableFontLarge));

        document.add(emplOverviewTable);
        document.add(new Paragraph(" ", bodyFont));
//        document.add(new Paragraph("Taken", subtitleFont));

        float[] colsWidthTasks = {6f, 1f, 1f, 1f, 1f, 1f};
        PdfPTable tasksTable = new PdfPTable(colsWidthTasks);
        tasksTable.setWidthPercentage(100);
        tasksTable.setHorizontalAlignment(Element.ALIGN_LEFT);
        tasksTable.addCell(createPdfCell("Taak", tableFontLarge));
        //tasksTable.addCell(createPdfCell("Label", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 1", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 2", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 3", tableFontLarge));
        tasksTable.addCell(createPdfCell("Kw 4", tableFontLarge));
        tasksTable.addCell(createPdfCell("Totaal", tableFontLarge));

        /*add the project tasks*/
        for (ProjectTask t : employee.getProjectTasks()) {
            String name = (! t.isCurriculumBound()) ? "* " + t.getName() : t.getName();
            tasksTable.addCell(createPdfCell(name, tableFontSmall));
            //tasksTable.addCell(createPdfCell(t.getTaskLabel().toString(), tableFontSmall));
            String qh1 = (t.getQuarterHours(1) == 0) ? "" : t.getQuarterHours(1) + "";
            tasksTable.addCell(createPdfCell(qh1, tableFontSmall));
            String qh2 = (t.getQuarterHours(2) == 0) ? "" : t.getQuarterHours(2) + "";
            tasksTable.addCell(createPdfCell(qh2, tableFontSmall));
            String qh3 = (t.getQuarterHours(3) == 0) ? "" : t.getQuarterHours(3) + "";
            tasksTable.addCell(createPdfCell(qh3, tableFontSmall));
            String qh4 = (t.getQuarterHours(4) == 0) ? "" : t.getQuarterHours(4) + "";
            tasksTable.addCell(createPdfCell(qh4, tableFontSmall));
            tasksTable.addCell(createPdfCell(t.getTaskHours() + "", tableFontSmall));
        }

        /*add the regular tasks*/
        for (RegularTask t : employee.getRegularTasksWithCommunalProfessionalizationTask()) {

            String name = (! t.isCurriculumBound()) ? "* " + t.getName() : t.getName();
            tasksTable.addCell(createPdfCell(name, tableFontSmall));
            //tasksTable.addCell(createPdfCell(t.getTaskLabel().toString(), tableFontSmall));
            String qh1 = (t.getQuarterHours(1) == 0) ? "" : t.getQuarterHours(1) + "";
            tasksTable.addCell(createPdfCell(qh1, tableFontSmall));
            String qh2 = (t.getQuarterHours(2) == 0) ? "" : t.getQuarterHours(2) + "";
            tasksTable.addCell(createPdfCell(qh2, tableFontSmall));
            String qh3 = (t.getQuarterHours(3) == 0) ? "" : t.getQuarterHours(3) + "";
            tasksTable.addCell(createPdfCell(qh3, tableFontSmall));
            String qh4 = (t.getQuarterHours(4) == 0) ? "" : t.getQuarterHours(4) + "";
            tasksTable.addCell(createPdfCell(qh4, tableFontSmall));
            tasksTable.addCell(createPdfCell(t.getTaskHours() + "", tableFontSmall));
        }

        /*add the eductational tasks*/
        int internshipCount = 0;
        StudentGroup firstInternship = null;
        int[] internshipQuarterHours = new int[5];
        int graduationCount = 0;
        StudentGroup firstGraduation = null;
        int[] graduationQuarterHours = new int[5];
        for (StudentGroup sg : employee.getStudentGroups()) {
            if (sg.getCurriculumModule().getName().contains("stage")
                    || sg.getCurriculumModule().getName().contains("Stage")) {
                if (firstInternship == null) {
                    firstInternship = sg;
                }
                internshipCount++;
                internshipQuarterHours[0] += sg.getQuarterHours(1);
                internshipQuarterHours[1] += sg.getQuarterHours(2);
                internshipQuarterHours[2] += sg.getQuarterHours(3);
                internshipQuarterHours[3] += sg.getQuarterHours(4);
                internshipQuarterHours[4] += sg.getTaskHours();
            } else if (sg.getCurriculumModule().getName().contains("afstuderen")
                    || sg.getCurriculumModule().getName().contains("Afstuderen")) {
                if (firstGraduation == null) {
                    firstGraduation = sg;
                }
                graduationCount++;
                graduationQuarterHours[0] += sg.getQuarterHours(1);
                graduationQuarterHours[1] += sg.getQuarterHours(2);
                graduationQuarterHours[2] += sg.getQuarterHours(3);
                graduationQuarterHours[3] += sg.getQuarterHours(4);
                graduationQuarterHours[4] += sg.getTaskHours();
            } else {
                tasksTable.addCell(createPdfCell(sg.getName(), tableFontSmall));
                //tasksTable.addCell(createPdfCell(sg.getTaskLabel().toString(), tableFontSmall));
                String qh1 = (sg.getQuarterHours(1) == 0) ? "" : sg.getQuarterHours(1) + "";
                tasksTable.addCell(createPdfCell(qh1, tableFontSmall));
                String qh2 = (sg.getQuarterHours(2) == 0) ? "" : sg.getQuarterHours(2) + "";
                tasksTable.addCell(createPdfCell(qh2, tableFontSmall));
                String qh3 = (sg.getQuarterHours(3) == 0) ? "" : sg.getQuarterHours(3) + "";
                tasksTable.addCell(createPdfCell(qh3, tableFontSmall));
                String qh4 = (sg.getQuarterHours(4) == 0) ? "" : sg.getQuarterHours(4) + "";
                tasksTable.addCell(createPdfCell(qh4, tableFontSmall));
                tasksTable.addCell(createPdfCell(sg.getTaskHours() + "", tableFontSmall));
            }
        }
        if (firstInternship != null) {
            tasksTable.addCell(createPdfCell(
                    firstInternship.getCurriculumModule().getName() + " " + internshipCount + " groepen", tableFontSmall));
            //tasksTable.addCell(createPdfCell(TaskLabel.CURRICULUM_EXECUTION.toString(), tableFontSmall));

            tasksTable.addCell(createPdfCell(
                    internshipQuarterHours[0] == 0 ? "" : internshipQuarterHours[0] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    internshipQuarterHours[1] == 0 ? "" : internshipQuarterHours[1] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    internshipQuarterHours[2] == 0 ? "" : internshipQuarterHours[2] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    internshipQuarterHours[3] == 0 ? "" : internshipQuarterHours[3] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    internshipQuarterHours[4] + "", tableFontSmall));
        }
        if (firstGraduation != null) {
            tasksTable.addCell(createPdfCell(
                    firstGraduation.getCurriculumModule().getName() + " " + graduationCount + " groepen", tableFontSmall));
            //tasksTable.addCell(createPdfCell(TaskLabel.CURRICULUM_EXECUTION.toString(), tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    graduationQuarterHours[0] == 0 ? "" : graduationQuarterHours[0] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    graduationQuarterHours[1] == 0 ? "" : graduationQuarterHours[1] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    graduationQuarterHours[2] == 0 ? "" : graduationQuarterHours[2] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    graduationQuarterHours[3] == 0 ? "" : graduationQuarterHours[3] + "", tableFontSmall));
            tasksTable.addCell(createPdfCell(
                    graduationQuarterHours[4] + "", tableFontSmall));
        }

//        tasksTable.addCell(createPdfCell("Subtotalen", tableFontLarge));
//        /*add the quarter and overall totals*/
//        for (int qt : this.quarterSubTotals) { //employee.getQuarterAndOverallTotals()
//            tasksTable.addCell(createPdfCell(qt + "", tableFontSmall));
//        }
//        tasksTable.addCell(createPdfCell("HBO uren", tableFontLarge));
//        /*add the quarter and overall totals*/
//        for (int qt : this.quarterTotalsHBOhours) {
//            tasksTable.addCell(createPdfCell(qt + "", tableFontSmall));
//        }
        tasksTable.addCell(createPdfCell("Totalen", tableFontLarge));
        /*add the quarter and overall totals*/
        for (int qt : this.quarterTotals) {
            tasksTable.addCell(createPdfCell(qt + "", tableFontSmall));
        }
        tasksTable.addCell(createPdfCell("Taakbelasting (fte)", tableFontLarge));
        /*add the quarter and overall totals*/
        for (String qt : this.quarterFteSizes) {
            tasksTable.addCell(createPdfCell(qt + "", tableFontSmall));
        }

        
        document.add(tasksTable);

        document.add(new Paragraph("*: niet curriculumgebonden taken", bodyFont));

        Date now = new Date();
        document.add(new Paragraph("Opmerkingen:", subtitleFont));
        document.add(new Paragraph(employee.getRemarks(), bodyFont));
        document.add(new Paragraph("Gegenereerd op: " + TaskManConfig.DATE_FORMAT.format(now), bodyFont));

        document.close();
        writer.flush();
        writer.close();
    }

    /**
     * creates a pdf table cell.
     *
     * @param text the text to place
     * @param font the font to use
     * @return pdfCell
     */
    private PdfPCell createPdfCell(final String text, final Font font) {
        Chunk chunk = new Chunk(text, font);
        Phrase phrase = new Phrase(chunk);
        PdfPCell cell = new PdfPCell(phrase);
        return cell;
    }
}
