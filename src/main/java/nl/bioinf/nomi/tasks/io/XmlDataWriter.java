/**
 *
 */
package nl.bioinf.nomi.tasks.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.List;

import nl.bioinf.nomi.tasks.datamodel.*;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.datamodel.PlanningRules.WeekRules;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public class XmlDataWriter {
    /**
     * the data collection to export.
     */
    private final YearDataCollection dataCollection;
    /**
     * the file to write to.
     */
    private final File file;
    /**
     * the bufferedwriter used for writing.
     */
    private BufferedWriter bw;

    /**
     *
     * @param dataCollection the data collection
     * @param file the file
     */
    public XmlDataWriter(final YearDataCollection dataCollection, final File file) {
        this.dataCollection = dataCollection;
        this.file = file;
    }

    /**
     * writes the data collection.
     * @throws Exception ex
     */
    public void writeDataCollection() throws Exception {
        if (!file.exists() && !file.createNewFile()) {
            throw new Exception("could not create the output file: " + file.getAbsolutePath());
        }
        if (!file.canWrite()) {
            throw new Exception("could not write to output file: " + file.getAbsolutePath());
        }

        //System.out.println( "writing data to file " + file.getName() );
        bw = new BufferedWriter(new FileWriter(file));

        writeHeader();

        writeEmployeeMetadata();

        writePlanningRulesDefaults();

        writeEmployees();

        writeRegularTasks();

        writeProjects();

        writeCourseMethods();

        writeCurriculumTypes();

        writeModules();

        writeCurricula();

        writeFooter();

        bw.close();
    }

    /**
     * writes the curricula.
     * @throws Exception ex
     */
    private void writeCurricula() throws Exception {
        bw.write("\t<curricula>");
        bw.newLine();

        for (Curriculum curriculum : dataCollection.getCurricula()) {
            bw.write("\t\t<curriculum>");
            bw.newLine();

            bw.write("\t\t\t<id>" + curriculum.getCurriculumID() + "</id>");
            bw.newLine();
            bw.write("\t\t\t<name>" + curriculum.getName() + "</name>");
            bw.newLine();
            bw.write("\t\t\t<internship year=\"" + curriculum.getInternship().getYear()
                    + "\" quarter=\"" + curriculum.getInternship().getQuarter()
                    + "\" duration=\"" + curriculum.getInternship().getDuration()
                    + "\" />");
            bw.newLine();
            bw.write("\t\t\t<graduation year=\"" + curriculum.getGraduation().getYear()
                    + "\" quarter=\"" + curriculum.getGraduation().getQuarter()
                    + "\" duration=\"" + curriculum.getGraduation().getDuration()
                    + "\" />");
            bw.newLine();

            for (CurriculumVariant cv : curriculum.getcurriculumVariants()) {
                bw.write("\t\t\t<curriculum_variant type=\"" + cv.getType() + "\" >");
                bw.newLine();

                for (int y = 1; y <= 4; y++) {
                    if (cv.containsCurriculumYear(y)) {
                        CurriculumYear cy = cv.getCurriculumYear(y);
                        bw.write("\t\t\t\t<year number=\"" + cy.getYear() + "\" >");
                        bw.newLine();
                        for (int q = 1; q <= 4; q++) {
                            if (cy.getModules(q).size() > 0) {
                                bw.write("\t\t\t\t\t<quarter number=\"" + q
                                        + "\" class_size=\"" + cy.getStudentNumber(q) + "\">");
                                bw.newLine();

                                for (CurriculumModule curriculumModule : cy.getModules(q)) {
                                    bw.write("\t\t\t\t\t\t<module id=\"" + curriculumModule.getModuleId() + "\" />");
                                    bw.newLine();
                                }

                                bw.write("\t\t\t\t\t</quarter>");
                                bw.newLine();
                            }
                        }
                        bw.write("\t\t\t\t</year>");
                        bw.newLine();
                    }
                }
                bw.write("\t\t\t</curriculum_variant>");
                bw.newLine();
            }
            bw.write("\t\t</curriculum>");
            bw.newLine();
        }
        bw.write("\t</curricula>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes the modules.
     * @throws Exception ex
     */
    private void writeModules() throws Exception {
        bw.write("\t<modules>");
        bw.newLine();

        for (CurriculumModule curriculumModule : dataCollection.getModules()) {

            bw.write("\t\t<module id=\"" + curriculumModule.getModuleId()
                    + "\" credits=\"" + curriculumModule.getCredits()
                    + "\" quarter=\"" + curriculumModule.getTimeframe().getQuarter() + "\" ");
            if (curriculumModule.getTimeframe().getDuration() != 1) {
                bw.write("duration=\"" + curriculumModule.getTimeframe().getDuration() + "\" ");
            }
            bw.write("course_method=\"" + curriculumModule.getCourseMethod().getId() + "\"");
            if (curriculumModule.isGroupNumberLocked()) {
                bw.write(" fixed_groups=\"true\" group_number=\"" + curriculumModule.getLockedGroupNumber() + "\"");
            }
            bw.write(" >");
            bw.newLine();
            bw.write("\t\t\t<name>" + curriculumModule.getName() + "</name>");
            bw.newLine();

            for (StudentGroup sg : curriculumModule.getStudentGroups()) {
                bw.write("\t\t\t<student_group number=\"" + sg.getNumber()
                        + "\" student_number=\"" + sg.getStudentNumber()
                        + "\" employee=\"" + sg.getEmployee().getEmployeeId()
//                        + "\" cat=\"" + sg.getTaskLabel().name()
                        + "\" />");
                bw.newLine();
            }

            if (curriculumModule.getPlanningRules() != null) {
                PlanningRules pr = curriculumModule.getPlanningRules();
                /*process default type*/
                if (pr.isDefaultPlanningRules()) {
                    /*<planning_rules default_name="propedeuse hoorcollege" />*/
                    bw.write("\t\t\t<planning_rules default_name=\"" + pr.getName() + "\" />");
                    bw.newLine();
                } /*process custom types*/ else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\t\t\t<planning_rules");
                    if (pr.getRequirements().length() > 0) {
                        sb.append(" requirements=\"");
                        sb.append(pr.getRequirements());
                        sb.append("\"");
                    }
                    if (pr.getComments().length() > 0) {
                        sb.append(" comments=\"");
                        sb.append(pr.getComments());
                        sb.append("\"");
                    }

                    sb.append(" >");

                    bw.write(sb.toString());
                    bw.newLine();

                    for (WeekRules wr : pr.getRules()) {
                        if (wr.getLessons().isEmpty()) {
                            continue;
                        }
                        bw.write("\t\t\t\t<weekrule weeknumber=\"" + wr.getWeekNumber() + "\">");
                        bw.newLine();

                        for (Lesson l : wr.getLessons()) {
                            sb = new StringBuilder("\t\t\t\t\t<lesson type=\"");
                            sb.append(l.getType().name());
                            sb.append("\"");
                            if (l.getDuration() != Lesson.STANDARD_DURATION) {
                                sb.append(" duration=\"");
                                sb.append(l.getDuration());
                                sb.append("\"");
                            }
                            if (l.getComments().length() != 0) {
                                sb.append(" comments=\"");
                                sb.append(l.getComments());
                                sb.append("\"");
                            }
                            sb.append(" />");

                            bw.write(sb.toString());
                            bw.newLine();
                        }
                        bw.write("\t\t\t\t</weekrule>");
                        bw.newLine();
                    }
                    bw.write("\t\t\t</planning_rules>");
                    bw.newLine();
                }
            }
            bw.write("\t\t</module>");
            bw.newLine();
        }
        bw.write("\t</modules>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes curriculum types.
     * @throws Exception ex
     */
    private void writeCurriculumTypes() throws Exception {
        bw.write("\t<curriculum_types>");
        bw.newLine();

        for (String type : Curriculum.getCurriculumTypes()) {
            bw.write("\t\t<type id=\"" + type
                    + "\" description=\"" + Curriculum.getCurriculumTypeDescription(type) + "\" />");
            bw.newLine();
        }
        bw.write("\t</curriculum_types>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes course methods.
     * @throws Exception ex
     */
    private void writeCourseMethods() throws Exception {
        bw.write("\t<course_methods>");
        bw.newLine();

        for (CourseMethod method : Curriculum.getCourseMethods()) {
            bw.write("\t\t<course_method id=\"" + method.getId() + "\" >");
            bw.newLine();

            bw.write("\t\t\t<name>" + method.getName() + "</name>");
            bw.newLine();
            bw.write("\t\t\t<group_size>" + method.getGroupSize() + "</group_size>");
            bw.newLine();
            //<timefactor category="HOURS_PER_TEACHER" name="voorbereidingsuren" value="3.0" per_ec="true" />

            TimeAssignmentFactors taf = method.getTimeAssignmentFactors();
            List<TimeFactor> tfs = taf.getTimeFactors();
            for (TimeFactor tf : tfs) {
                bw.write("\t\t\t<timefactor category=\"" + tf.getCategoryString()
                        + "\" name=\"" + tf.getName()
                        + "\" value=\"" + tf.getHours()
                        + "\" per_ec=\"" + tf.isEcDependent() + "\" />");
                bw.newLine();
            }
            bw.write("\t\t</course_method>");
            bw.newLine();
        }
        bw.write("\t</course_methods>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes the projects.
     * @throws Exception ex
     */
    private void writeProjects() throws Exception {
        bw.write("\t<projects>");
        bw.newLine();

        for (Project project : dataCollection.getProjects()) {
            bw.write("\t\t<project id=\"" + project.getProjectId() + "\">");
            bw.newLine();

            bw.write("\t\t\t<name>" + project.getName() + "</name>");
            bw.newLine();
            bw.write("\t\t\t<description>" + project.getDescription() + "</description>");
            bw.newLine();
            bw.write("\t\t\t<total_hours>" + project.getTotalHours() + "</total_hours>");
            bw.newLine();

            for (ProjectTask task : project.getTasks()) {
                bw.write("\t\t\t<project_task id=\"" + task.getProjectTaskId()
                        + "\" cat=\"" + task.getTaskLabel().name() + "\" >");
                bw.newLine();
                String partString = getPartitionString(task);
                bw.write("\t\t\t\t<timeframe start=\"" + task.getTimeframe().getQuarter()
                        + "\" duration=\"" + task.getTimeframe().getDuration() + "\""
                        + partString + " />");
                bw.newLine();
                bw.write("\t\t\t\t<name>" + task.getName() + "</name>");
                bw.newLine();
                bw.write("\t\t\t\t<task_hours>" + task.getTaskHours() + "</task_hours>");
                bw.newLine();
                bw.write("\t\t\t\t<employee>" + task.getEmployee().getEmployeeId() + "</employee>");
                bw.newLine();
                bw.write("\t\t\t</project_task>");
                bw.newLine();
            }
            bw.write("\t\t</project>");
            bw.newLine();
            bw.newLine();
        }
        bw.write("\t</projects>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes all regular tasks.
     * @throws Exception ex
     */
    private void writeRegularTasks() throws Exception {
        /*
         *     <regular_tasks>
            <regular_task id="1" cat="ORGANISATIONAL" employee="NOMI" hours="24" >
                <name>stagecoordinatie BFV</name>
                <timeframe start="1" duration="4" partition="10,20,30,40" />
                </regular_task>
                <regular_task id="2" cat="EDUCATIONAL" employee="NOMI"  hours="24" >
                <name>externe cursus Python</name>
                <timeframe start="4" duration="1" />
                <hbo_hours_inclusive>true</hbo_hours_inclusive>
                <description>Python cursus is onderdeel van cursusportefeuille ILST</description>
            </regular_task>
         </regular_tasks>*/

        bw.write("\t<regular_tasks>");
        bw.newLine();

        for (RegularTask rt : dataCollection.getRegularTasks()) {
            if (rt.getTaskLabel() == TaskLabel.COMMUNAL_PROFESSIONAL_DEVELOPMENT) {
                continue;
            }
            String partString = getPartitionString(rt);

            bw.write("\t\t<regular_task id=\"" + rt.getTaskId()
                    + "\" cat=\"" + rt.getTaskLabel().name()
                    + "\" employee=\"" + rt.getEmployee().getEmployeeId()
                    + "\" hours=\"" + rt.getTaskHours() + "\" >");
            bw.newLine();
            bw.write("\t\t\t<name>" + rt.getName() + "</name>");
            bw.newLine();
            bw.write("\t\t\t<timeframe start=\"" + rt.getTimeframe().getQuarter()
                    + "\" duration=\"" + rt.getTimeframe().getDuration() + "\""
                    + partString + " />");
            bw.newLine();
            bw.write("\t\t</regular_task>");
            bw.newLine();
        }

        bw.write("\t</regular_tasks>");
        bw.newLine();
        bw.newLine();

    }

    /**
     * generates the partition string attribute for timeframe element.
     *
     * @param t the task
     * @return partitionstring
     */
    private String getPartitionString(final Task t) {
        double[] part = t.getTimeframe().getPartitioning();
        StringBuilder partStrB = new StringBuilder("");
        if (part != null) {
            partStrB.append(" partition=\"");
            int start = t.getTimeframe().getQuarter();
            int duration = t.getTimeframe().getDuration();
            for (int i = start; i < (start + duration); i++) {
                double quarterPart = part[i - 1];
                int ip = (int) (quarterPart * 100);
                partStrB.append(ip);
                partStrB.append(",");
            }
            partStrB.deleteCharAt(partStrB.length() - 1);
            partStrB.append("\"");
        }
        return partStrB.toString();
    }

    /**
     * writes the employees.
     * @throws Exception ex
     */
    private void writeEmployees() throws Exception {
        bw.write("\t<employees>");
        bw.newLine();

        for (Employee emp : dataCollection.getEmployees()) {
            bw.write("\t\t<employee id=\"" + emp.getEmployeeId()
                    + "\" function_scale=\"" + emp.getFunctionScale() + "\" >");
            bw.newLine();

            bw.write("\t\t\t<first_name>");
            String fn = (emp.getFirstName() == null ? "" : emp.getFirstName());
            bw.write(fn);
            bw.write("</first_name>");
            bw.newLine();

            if (emp.getNameInfix() != null) {
                bw.write("\t\t\t<infix>");
                bw.write(emp.getNameInfix());
                bw.write("</infix>");
                bw.newLine();
            }

            bw.write("\t\t\t<family_name>");
            bw.write(emp.getFamilyName());
            bw.write("</family_name>");
            bw.newLine();

            bw.write("\t\t\t<fte>");
            bw.write(emp.getFte() + "");
            bw.write("</fte>");
            bw.newLine();

            bw.write("\t\t\t<team>");
            bw.write(emp.getTeam());
            bw.write("</team>");
            bw.newLine();
            if (emp.getEmail() != null) {
                bw.write("\t\t\t<email>");
                bw.write(emp.getEmail());
                bw.write("</email>");
                bw.newLine();
            }

            if (emp.getRemarks() != null) {
                bw.write("\t\t\t<remarks>");
                bw.write(emp.getRemarks());
                bw.write("</remarks>");
                bw.newLine();
            }
            bw.write("\t\t</employee>");
            bw.newLine();
        }
        bw.write("\t</employees>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes the planning rules defaults.
     * @throws Exception ex
     */
    private void writePlanningRulesDefaults() throws Exception {
        /*    <planningrules_defaults>
         <planningrules_default name="propedeuse hoorcollege" weeks="10">
         <requirements>Hoorcollegezaal</requirements>
         <comments>nvt</comments>
         <weekrule weeknumber="1">
         <lesson type="THEORY" duration="1.5" />
         <lesson type="THEORY" duration="1.5" />
         </weekrule>
         <weekrule weeknumber="2">
         <lesson type="THEORY" />
         <lesson type="THEORY" />
         </weekrule>
         */
        bw.write("\t<planningrules_defaults>");
        bw.newLine();

        for (String name : PlanningRules.getDefaultNames()) {
            PlanningRules defaultPr = PlanningRules.getDefault(name);
            bw.write("\t\t<planningrules_default name=\"" + name
                    + "\" weeks=\"" + defaultPr.getRules().size() + "\" >");
            bw.newLine();

            if (defaultPr.getRequirements().length() > 0) {
                bw.write("\t\t\t<requirements>" + defaultPr.getRequirements() + "</requirements>");
                bw.newLine();
            }

            if (defaultPr.getComments().length() > 0) {
                bw.write("\t\t\t<comments>" + defaultPr.getComments() + "</comments>");
                bw.newLine();
            }

            for (WeekRules wr : defaultPr.getRules()) {
                if (wr.getLessons().isEmpty()) {
                    continue;
                }
                bw.write("\t\t\t<weekrule weeknumber=\"" + wr.getWeekNumber() + "\">");
                bw.newLine();

                for (Lesson l : wr.getLessons()) {
                    StringBuilder sb = new StringBuilder("\t\t\t\t<lesson type=\"");
                    sb.append(l.getType().name());
                    sb.append("\"");
                    if (l.getDuration() != Lesson.STANDARD_DURATION) {
                        sb.append(" duration=\"");
                        sb.append(l.getDuration());
                        sb.append("\"");
                    }
                    if (l.getComments().length() != 0) {
                        sb.append(" comments=\"");
                        sb.append(l.getComments());
                        sb.append("\"");
                    }
                    sb.append(" />");
                    bw.write(sb.toString());
                    bw.newLine();
                }
                bw.write("\t\t\t</weekrule>");
                bw.newLine();
            }
            bw.write("\t\t</planningrules_default>");
            bw.newLine();
        }
        bw.write("\t</planningrules_defaults>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes employee metadata.
     * @throws Exception ex
     */
    private void writeEmployeeMetadata() throws Exception {
        bw.write("\t<employee_metadata>");
        bw.newLine();
        /*        <fte_size>1659</fte_size>
         <gpl_euros>42.24</gpl_euros>
         <use_gpl>true</use_gpl>
         <function_scale level="9" value="35.0" />
         <function_scale level="11" value="40.0" />
         <function_scale level="12" value="45.0" />
         */
        bw.write("\t\t<fte_size>");
        bw.write(Employee.getFteSize() + "");
        bw.write("</fte_size>");
        bw.newLine();

        bw.write("\t\t<gpl_euros>");
        bw.write(Employee.getGpl() + "");
        bw.write("</gpl_euros>");
        bw.newLine();

        bw.write("\t\t<use_gpl>");
        bw.write(Employee.isUseGpl() + "");
        bw.write("</use_gpl>");
        bw.newLine();

        for (int scale : Employee.getSalaryScales()) {
            bw.write("\t\t<function_scale level=\"" + scale + "\" value=\"" + Employee.getSalaryRate(scale) + "\" />");
            bw.newLine();
        }

        bw.write("\t</employee_metadata>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes the header data to file.
     *
     * @throws Exception ex
     */
    private void writeHeader() throws Exception {
        bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        bw.newLine();
        bw.write("<school>");
        bw.newLine();
        bw.write("\t<header>");
        bw.newLine();

        bw.write("\t\t<version>");
        Date now = new Date();
        bw.write(now.toString());
        bw.write("</version>");
        bw.newLine();

        bw.write("\t\t<school_year>");
        bw.write(dataCollection.getSchoolYear() + "");
        bw.write("</school_year>");
        bw.newLine();

        bw.write("\t\t<school_id>");
        bw.write(dataCollection.getSchool());
        bw.write("</school_id>");
        bw.newLine();

        bw.write("\t\t<planner>");
        bw.write(dataCollection.getPlanner());
        bw.write("</planner>");
        bw.newLine();

        bw.write("\t</header>");
        bw.newLine();
        bw.newLine();
    }

    /**
     * writes the header data to file.
     *
     * @throws Exception ex
     */
    private void writeFooter() throws Exception {
        bw.write("</school>");
        bw.newLine();
    }
}
