/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package nl.bioinf.nomi.tasks.io;

import nl.bioinf.nomi.tasks.datamodel.Employee;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * this class is responsible for sending task lists as pdf attachment to email recipients.
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class TaskListEmailSender {

    /**
     * this class should not be instantiated.
     */
    private TaskListEmailSender() { }

    /**
     * will send the task list of the given employee through email.
     * @param employee the employee
     * @param year the school year
     * @param messageBody the message body
     * @throws Exception ex
     */
    public static void sendTaskListPDF(
            final Employee employee,
            final int year,
            final String messageBody) throws Exception {
        //create temp file
        SimpleDateFormat format = new SimpleDateFormat("yyyy_MM_dd");
        String dateStr = format.format(new Date());

        File tempFile = File.createTempFile(employee.getEmployeeId() + "_" + dateStr + "_", ".pdf");

        //create pdf
        EmployeePdfWriter emplWriter = new EmployeePdfWriter(employee, year, tempFile);
        emplWriter.write();

        // message info
        String mailTo = employee.getEmail();
        String subject = "Nieuwe concept takenlijst (via " + System.getProperty("user.name", "planner") + ")";

        // attachments
        String[] attachFiles = new String[1];
        attachFiles[0] = tempFile.getAbsolutePath();

        sendEmailWithAttachments(mailTo, subject, messageBody, attachFiles);
        //System.out.println("Email sent.");

        /*delete temp file*/
        tempFile.delete();
    }

    /**
     * sends an email with attachment.
     * @param toAddress the address to send to
     * @param subject the email subject
     * @param message the email message
     * @param attachFiles the attached files
     * @throws MessagingException ex
     * @throws IOException ex
     */
    private static void sendEmailWithAttachments(final String toAddress,
            final String subject, final String message, final String[] attachFiles)
            throws MessagingException, IOException {
        // SMTP info
        final String host = "smtp.gmail.com";
        final String port = "587";
        final String mailFrom = "sils.taskman@gmail.com";
        final String password = "L2feSc2ence&Teknology";

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", mailFrom);
        properties.put("mail.password", password);

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailFrom, password);
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(mailFrom));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");

        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        // adds attachments
        if (attachFiles != null && attachFiles.length > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();

                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                multipart.addBodyPart(attachPart);
            }
        }

        // sets the multi-part as e-mail's content
        msg.setContent(multipart);

        // sends the e-mail
        Transport.send(msg);
    }
}
