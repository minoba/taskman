/**
 *
 */
package nl.bioinf.nomi.tasks.control;

import nl.bioinf.nomi.tasks.configuration.TaskManConfig;
import nl.bioinf.nomi.tasks.datamodel.*;
import nl.bioinf.nomi.tasks.datamodel.CurriculumModule;
import nl.bioinf.nomi.tasks.gui.*;
import nl.bioinf.nomi.tasks.io.FileExporter;
import nl.bioinf.nomi.tasks.io.IOcontroller;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * the main controller of the application.
 *
 * @author M.A. Noback (m.a.noback@pl.hanze.nl)
 * @version 0.1
 */
public final class TaskManagerController implements Runnable {
    /**
     * the main frame of the gui.
     */
    private TaskManagerFrame frame;
    /**
     * user properties stored in local file.
     */
    private Properties userProperties;
    /**
     * the datacollection of one or more years.
     */
    private final HashMap<Integer, YearDataCollection> dataCollection;
    /**
     * the year that is currently being shown.
     */
    private int currentYear;
    /**
     * the GUI components factory.
     */
    private final GuiComponentFactory guiComponentFactory;
    /**
     * the currently viewed panels: curriculum.
     */
    private Curriculum viewedCurriculum;
    /**
     * the currently viewed panels: Employee.
     */
    private Employee viewedEmployee;
    /**
     * the currently viewed panels: CurriculumModule.
     */
    private CurriculumModule viewedCurriculumModule;
    /**
     * the currently viewed panels: CurriculumYear.
     */
    private CurriculumYear viewedCurriculumYear;
    /**
     * the currently viewed panels: CurriculumVariant.
     */
    private CurriculumVariant viewedCurriculumVariant;
    /**
     * the currently viewed panels: Project.
     */
    private Project viewedProject;
    /**
     * the currently viewed panels: TaskCategory.
     */
    private TaskCategory viewedTaskCategory;

    /**
     * singleton implementation.
     */
    private static class InstanceProvider {
        /**
         * single INSTANCE.
         */
        private static final TaskManagerController INSTANCE = new TaskManagerController();
        /**
         * static INSTANCE provider.
         * @return INSTANCE
         */
        protected static TaskManagerController getInstance() {
            return INSTANCE;
        }
    }

    /**
     * returns the INSTANCE of the TaskManagerController.
     * @return controller INSTANCE
     */
    public static TaskManagerController getInstance() {
        return InstanceProvider.getInstance();
    }

    /**
     * default constructor.
     */
    private TaskManagerController() {
        this.dataCollection = new HashMap<Integer, YearDataCollection>();
        this.guiComponentFactory = GuiComponentFactory.getInstance();
    }

    /**
     * this method runs as long as the program is active.
     */
    public void go() {
        /*set the look and feel*/
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException
                | InstantiationException
                | IllegalAccessException
                | UnsupportedLookAndFeelException e) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (final ClassNotFoundException
                    | InstantiationException
                    | IllegalAccessException
                    | UnsupportedLookAndFeelException e1) {
                e1.printStackTrace();
            }
        }
        EventQueue.invokeLater(this);
    }

    @Override
    public void run() {
        try {
            TaskManConfig.loadConfiguration("configuration_nl.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        userProperties = IOcontroller.getInstance().loadUserProperties();
        frame = new TaskManagerFrame(this);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new TaskManagerWindowListener());
        frame.setVisible(true);

//        IOcontroller.getInstance().openTestFile();
        //IOcontroller.getInstance().saveTestFile( );
    }

    /**
     * returns the user properties.
     * @return userProperties
     */
    public Properties getUserProperties() {
        return userProperties;
    }

    /**
     * sets the current school year.
     * @param currentSchoolYear the school year
     */
    public void setCurrentYear(final int currentSchoolYear) {
        this.currentYear = currentSchoolYear;
    }

    /**
     * Returns the main JFrame of the application.
     * @return frame the main frame
     */
    public JFrame getMainFrame() {
        return frame;
    }

    /**
     * exit code.
     */
    private void exit() {
        //TODO support multiple datafiles
        if (currentYear == 0) {
            frame.dispose();
        } else {
            int response = JOptionPane.showConfirmDialog(
                    frame,
                    "afsluiten zonder data bewaren?",
                    "beeindig sessie zonder bewaren",
                    JOptionPane.OK_CANCEL_OPTION);
            if (response == JOptionPane.OK_OPTION) {
                frame.dispose();
            } else {
                if (dataCollection.get(currentYear).getDataFile() == null) {
                    IOcontroller.getInstance().saveAsFile(currentYear);
                } else {
                    IOcontroller.getInstance().saveFile(currentYear);
                }
            }
        }
        //remove all file locks
        IOcontroller.getInstance().removeLocksForUser();
        //superfluous exit??
        System.exit(0);
    }

    /**
     * adds a year data collection to the overall collection.
     * @param ydc the year data collection
     */
    public void addDataCollection(final YearDataCollection ydc) {
        dataCollection.put(ydc.getSchoolYear(), ydc);
    }

    /**
     * removes a data collection (if present) with the given year.
     * @param year the school year
     */
    public void removeDataCollection(final int year) {
        dataCollection.remove(year);
    }

    /**
     * returns the data collection for the given year.
     * @param year the school year
     * @return yearDataCollection the data collection
     */
    public YearDataCollection getDataCollection(final int year) {
        return dataCollection.get(year);
    }

    /**
     * returns the data collection for the current year.
     * @return yearDataCollection ydc
     */
    public YearDataCollection getDataCollection() {
        return dataCollection.get(currentYear);
    }

    /**
     * returns the current, active year collection.
     * @return currentYear
     */
    public int getCurrentYear() {
        return currentYear;
    }

    /**
     * swaps the removedEmployee for the newEmployee in the given studentGroup
     * and processes all relevant changes.
     *
     * @param studentGroup the student group
     * @param removedEmployee the removed employee
     * @param newEmployee the new employee
     */
    public void swapStudentGroupEmployee(final StudentGroup studentGroup,
            final Employee removedEmployee,
            final Employee newEmployee) {
        /*only process if anything changed*/
        if (!newEmployee.equals(removedEmployee)) {
            removedEmployee.removeStudentGroup(studentGroup);
            /*sets the changed employee as new teacher for the student group*/
            studentGroup.setEmployee(newEmployee);
            /*add the student group to the other employee*/
            newEmployee.addStudentGroup(studentGroup);
            /*a recalculation may be required*/
            studentGroup.getCurriculumModule().recalculateEmployeeHours();
            /*show the changed data*/
            this.viewedEmployee = newEmployee;
            updateViews("curriculumoverview");
        }
    }

    /**
     * swaps the regular task between two employees.
     * @param changedTask the task to swap
     * @param removedEmpl the removed employee
     * @param newEmpl the new employee
     */
    public void swapRegularTaskEmployee(final RegularTask changedTask,
            final Employee removedEmpl,
            final Employee newEmpl) {
        if (!newEmpl.equals(removedEmpl)) {
            removedEmpl.removeRegularTask(changedTask);
            changedTask.setEmployee(newEmpl);
            newEmpl.addRegularTask(changedTask);
            this.viewedEmployee = newEmpl;
            updateViews("regularTaskDetailPanel");
        }
    }

    /**
     * swaps the project task between two employees.
     * @param changedTask the changed project task
     * @param removedEmpl the removed employee
     * @param newEmpl the new employee
     */
    public void swapProjectTaskEmployee(
            final ProjectTask changedTask,
            final Employee removedEmpl,
            final Employee newEmpl) {
        if (!newEmpl.equals(removedEmpl)) {
            removedEmpl.removeProjectTask(changedTask);
            changedTask.setEmployee(newEmpl);
            newEmpl.addProjectTask(changedTask);
            this.viewedEmployee = newEmpl;
            updateViews("projectDetailPanel");
        }
    }

    /**
     * entry point for various GUI actions.
     *
     * @param action the GUI action/event
     */
    public void doAction(final GuiAction action) {
        switch (action) {
            case CREATE_EMPLOYEE: {
                EmployeeCreationFrame ecf = new EmployeeCreationFrame(this);
                ecf.setVisible(true);
                break;
            }
            case CREATE_DATACOLLECTION: {
                DataCollectionCreationFrame dccf = new DataCollectionCreationFrame(this);
                dccf.setVisible(true);
                break;
            }
            case CREATE_REGULAR_TASK: {
                RegularTaskCreationFrame rtcf = new RegularTaskCreationFrame(this);
                rtcf.setVisible(true);
                break;
            }
            case CREATE_PROJECT: {
                ProjectCreationFrame pcf = new ProjectCreationFrame(this);
                pcf.setVisible(true);
                break;
            }
            case CREATE_PROJECT_TASK: {
                ProjectTaskCreationFrame pcf = new ProjectTaskCreationFrame(this);
                pcf.setVisible(true);
                break;
            }
            case EDIT_COURSE_METHODS: {
                CourseMethodsEditingFrame mef = new CourseMethodsEditingFrame(this);
                mef.setVisible(true);
                break;
            }
            case CREATE_MODULE: {
                ModuleCreationFrame mcf = new ModuleCreationFrame(this);
                mcf.setVisible(true);
                break;
            }
            case CREATE_CURRICULUM: {
                CurriculumCreationFrame ccf = new CurriculumCreationFrame(this);
                ccf.setVisible(true);
                break;
            }
            case CREATE_CURRICULUMVARIANT: {
                CurriculumvariantCreationFrame cvcf = new CurriculumvariantCreationFrame(this);
                cvcf.setVisible(true);
                break;
            }
            case CREATE_CURRICULUMYEAR: {
                CurriculumyearCreationFrame cycf = new CurriculumyearCreationFrame(this);
                cycf.setVisible(true);
                break;
            }
            case EDIT_DATACOLLECTION: {
                DataCollectionEditingFrame mdef = new DataCollectionEditingFrame(this);
                mdef.setVisible(true);
                break;
            }
            case EDIT_CURRICULUM_TYPES: {
                CurriculumTypesEditingFrame ctef = new CurriculumTypesEditingFrame(this);
                ctef.setVisible(true);
                break;
            }
            case EXPORT_TASKOVERVIEWS: {
                selectEmployeesForExport(getCurrentYear());
                break;
            }
            case EXPORT_ALL_TASKS: {
                FileExporter.exportAllTasks(this, frame);
                break;
            }
            case EXPORT_CURRICULA: {
                FileExporter.exportCurriculaOverviews(this, frame);
                break;
            }
            case EXPORT_INTERNSHIP_OVERVIEW: {
                FileExporter.exportInternshipTaskOverview(this, frame);
                break;
            }
            case EXPORT_REGULAR_TASKS_OVERVIEW: {
                FileExporter.exportRegularTasksOverview(this, frame);
                break;
            }
            case EXPORT_PROJECTS_OVERVIEW: {
                FileExporter.exportProjectsOverview(this, frame);
                break;
            }
            case EXPORT_GENERAL_OVERVIEW: {
                FileExporter.exportGeneralOverview(this, frame);
                break;
            }
            case EXPORT_PLANNING_RULES: {
                FileExporter.exportPlanningRules(this, frame);
                break;
            }
            case FILE_OPEN: {
                IOcontroller.getInstance().openFile();
                //openFile();
                break;
            }
            case FILE_SHOW: {
                IOcontroller.getInstance().openFile();
                //openFile();
                break;
            }
            case SAVE_FILE: {
                IOcontroller.getInstance().saveFile(currentYear);
                break;
            }
            case SAVE_AS_FILE: {
                IOcontroller.getInstance().saveAsFile(currentYear);
                break;
            }
            case EXIT: {
                exit();
                break;
            }
            case SHOW_HELP_ABOUT: {
                showGeneralHelp();
                break;
            }
            case SHOW_HELP_ITEMS: {
                showHelpItems();
                break;
            }
            default:
                throw new IllegalArgumentException("Unsupported action: " + action);
        }
    }

    /**
     * carry out a required action on the curriculumModule.
     *
     * @param ra required action
     * @param curriculumModule the curriculumModule that it concerns
     */
    public void doModuleAction(final RequiredAction ra, final CurriculumModule curriculumModule) {
        this.viewedCurriculumModule = curriculumModule;
        switch (ra) {
            case REMOVE_GROUPS: {
                GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
                gsf.setAlwaysOnTop(true);
                gsf.setVisible(true);
                break;
            }
            case ADD_GROUPS: {
                /*this will probably never be called*/
                curriculumModule.addVacantStudentGroups();
                break;
            }
            case NONE: break;
            default:
                throw new IllegalArgumentException("Unsupported action: " + ra);
        }
        if (ra != RequiredAction.NONE) {
            updateViews("");
        }
    }

    /**
     * Process a change in class size. This will influence all modules that this
     * class attends.
     *
     * @param curriculumYear the curriculum year
     * @param quarter the quarter with a changed class size
     */
    public void classSizeChanged(final CurriculumYear curriculumYear, final int quarter) {
        List<CurriculumModule> curriculumModules = curriculumYear.getModules(quarter);
        Collections.reverse(curriculumModules);
        for (CurriculumModule curriculumModule : curriculumModules) {
            RequiredAction reqAction = curriculumModule.classSizeChanged();

            if (reqAction == RequiredAction.REMOVE_GROUPS) {
                GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
                gsf.setAlwaysOnTop(true);
                gsf.setVisible(true);
            }
        }
        updateViews("classesoverview");
    }

    /**
     * Process changes in attendance of a curriculumModule.
     *
     * @param curriculumModule the curriculumModule
     */
    public void moduleAttendanceChanged(final CurriculumModule curriculumModule) {
        RequiredAction requiredAction = curriculumModule.classSizeChanged();
        if (requiredAction == RequiredAction.REMOVE_GROUPS) {
            GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
            gsf.setAlwaysOnTop(true);
            gsf.setVisible(true);
        }
        updateViews(""); //TODO maybe this call should be removed
    }

    /**
     * a course method has changed; this has repercussions for modules,
     * tasklists etc.
     *
     * @param changedCourseMethod the changed course method
     */
    public void courseMethodChanged(final CourseMethod changedCourseMethod) {
        for (CurriculumModule curriculumModule : dataCollection.get(currentYear).getModules()) {
            if (curriculumModule.getCourseMethod().getId().equals(changedCourseMethod.getId())) {
                RequiredAction requiredAction = curriculumModule.courseMethodChanged();
                if (requiredAction == RequiredAction.REMOVE_GROUPS) {
                    GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
                    gsf.setAlwaysOnTop(true);
                    gsf.setVisible(true);
                }

                for (StudentGroup sg : curriculumModule.getStudentGroups()) {
                    sg.setEmployeeHours(changedCourseMethod.getAssignableHours(sg));
                }
            }
        }
        updateViews("");
    }

    /**
     * deletes the given curriculum from the year datacollection.
     *
     * @param curriculum the curriculum
     * @param year the school year
     */
    public void deleteCurriculum(final Curriculum curriculum, final int year) {
        dataCollection.get(year).removeCurriculum(curriculum.getCurriculumID());

        for (CurriculumVariant cv : curriculum.getcurriculumVariants()) {
            for (CurriculumYear cy : cv.getCurriculumYears()) {
                for (CurriculumModule curriculumModule : cy.getModules()) {
                    curriculumModule.removeAttendingClass(cy);
                    RequiredAction reqAction = curriculumModule.classSizeChanged();
                    if (reqAction == RequiredAction.REMOVE_GROUPS) {
                        GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
                        gsf.setAlwaysOnTop(true);
                        gsf.setVisible(true);
                    }
                }
            }
        }
        viewedCurriculum = null;
        viewedCurriculumVariant = null;
        viewedCurriculumYear = null;
        frame.getTabPanel(TaskManagerFrame.CURRICULA_TAB).setDetailComponent(new JPanel());
        frame.getTabPanel(TaskManagerFrame.CURRICULUM_VARIANT_TAB).setDetailComponent(new JPanel());
        updateViews("curriculumdeletion");
        frame.repaint();
    }

    /**
     * deletes the given curriculum variant from the collection.
     *
     * @param curriculumVariant the curriculum variant
     * @param year the school year
     */
    public void deleteCurriculumVariant(final CurriculumVariant curriculumVariant, final int year) {
        for (CurriculumYear cy : curriculumVariant.getCurriculumYears()) {
            for (CurriculumModule curriculumModule : cy.getModules()) {
                curriculumModule.removeAttendingClass(cy);
                RequiredAction reqAction = curriculumModule.classSizeChanged();
                if (reqAction == RequiredAction.REMOVE_GROUPS) {
                    GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
                    gsf.setAlwaysOnTop(true);
                    gsf.setVisible(true);
                }
            }
        }
        curriculumVariant.getCurriculum().removeCurriculumVariant(curriculumVariant.getType());
        viewedCurriculumVariant = null;
        viewedCurriculumYear = null;
        //frame.getTabPanel(TaskManagerFrame.CURRICULA_TAB).setDetailComponent( new JPanel() );
        frame.getTabPanel(TaskManagerFrame.CURRICULUM_VARIANT_TAB).setDetailComponent(new JPanel());
        updateViews("curriculumdeletion");
        frame.repaint();
    }

    /**
     * deletes the given curriculum year from parent curriculum.
     *
     * @param curriculumYear the curriculum year
     * @param year the school year
     */
    public void deleteCurriculumYear(final CurriculumYear curriculumYear, final int year) {
        CurriculumVariant cv = curriculumYear.getCurriculumVariant();

        for (CurriculumModule curriculumModule : curriculumYear.getModules()) {
            curriculumModule.removeAttendingClass(curriculumYear);
            RequiredAction reqAction = curriculumModule.classSizeChanged();
            if (reqAction == RequiredAction.REMOVE_GROUPS) {
                GroupSelectionFrame gsf = new GroupSelectionFrame(this, curriculumModule);
                gsf.setAlwaysOnTop(true);
                gsf.setVisible(true);
            }
        }
        cv.removeCurriculumYear(curriculumYear);

        viewedCurriculumYear = null;
        frame.getTabPanel(TaskManagerFrame.CURRICULUM_VARIANT_TAB).setDetailComponent(new JPanel());
        updateViews("curriculumdeletion");
        frame.repaint();
    }

    /**
     * deletes an employee from the collection.
     *
     * @param employee the employee to delete
     * @param year the school year
     */
    public void deleteEmployee(final Employee employee, final int year) {
        /*remove project tasks*/
        for (ProjectTask pt : employee.getProjectTasks()) {
            pt.setEmployee(Employee.getVacantEmployee());
            Employee.getVacantEmployee().addProjectTask(pt);
        }

        /*add studentgroups to _VAC*/
        for (StudentGroup sg : employee.getStudentGroups()) {
            sg.setEmployee(Employee.getVacantEmployee());
            Employee.getVacantEmployee().addStudentGroup(sg);
        }

        /*set regular tasks to _VAC*/
        for (RegularTask rt : employee.getRegularTasks()) {
            rt.setEmployee(Employee.getVacantEmployee());
            Employee.getVacantEmployee().addRegularTask(rt);
        }

        dataCollection.get(year).removeEmployee(employee.getEmployeeId());
        viewedEmployee = null;
        frame.getTabPanel(TaskManagerFrame.EMPLOYEE_TAB).setDetailComponent(new JPanel());
        updateViews("employeedeletion");
    }

    /**
     * deletes a curriculumModule from the datacollection.
     *
     * @param curriculumModule the curriculumModule to delete
     * @param year the school year
     */
    public void deleteModule(final CurriculumModule curriculumModule, final int year) {
        for (CurriculumYear cy : curriculumModule.getAttendingClasses()) {
            cy.removeModule(curriculumModule.getTimeframe().getQuarter(), curriculumModule);
        }
        for (StudentGroup sg : curriculumModule.getStudentGroups()) {
            sg.getEmployee().removeStudentGroup(sg);
        }
        dataCollection.get(year).removeModule(curriculumModule.getModuleId());
        viewedCurriculumModule = null;
        frame.getTabPanel(TaskManagerFrame.MODULES_TAB).setDetailComponent(new JPanel());
        updateViews("moduledeletion");
    }

    /**
     * deletes the given list of projects.
     *
     * @param projects the selected projects to delete
     */
    public void deleteProjects(final List<Project> projects) {
        for (Project project : projects) {
            deleteProject(project);
        }
        updateViews("projectsdeletion");
    }

    /**
     * deletes a single project from the current year.
     *
     * @param project the project to delete
     */
    public void deleteProject(final Project project) {
        for (ProjectTask pt : project.getTasks()) {
            pt.getEmployee().removeProjectTask(pt);
            project.removeTask(pt);
        }
        dataCollection.get(currentYear).removeProject(project);
        updateViews("projectsdeletion");
    }

    /**
     * updates all required panels based on the source of the change; to be
     * called when the data change.
     * //TODO refactor source away
     *
     * @param source the source of the change
     */
    public void updateViews(final String source) {
        showCurriculaList();
        showCurriculumDetails();
        showCurriculumYearDetails(viewedCurriculumYear);
        showCurriculumVariantsList();
        showCurriculumVariantDetails(viewedCurriculumVariant);
        showModulesList();
        showModuleDetails();
        showTeachersList();
        showEmployeeDetails();
        showRegularTasksOverview();
        showRegularTasksCategoriesList();
        showProjectsList();
        showProjectsOverview();
        showClassesOverview();
        showGeneralOverview();
        showFinancialOverview();
    }

    /**
     * updates the frame title.
     * Will set the given year as the active school year
     * @param schoolYear the school year
     */
    public void updateFrameTitle(final int schoolYear) {
        this.currentYear = schoolYear;
        frame.setTitle(TaskManConfig.getString(TaskManConfig.MAIN_TITLE)
                + "     BEWERK DATA VOOR Jaar: " + currentYear
                + " [" + dataCollection.get(currentYear).getDataFile() + "]");
    }

    /**
     * refreshes all data to show the data associated with the given school year.
     *
     * @param schoolYear the school year
     */
    public void showDataCollection(final int schoolYear) {
        this.currentYear = schoolYear;
        //System.out.println("new datacollection for year: " + schoolYear);
        frame.setTitle(TaskManConfig.getString(TaskManConfig.MAIN_TITLE)
                + "     BEWERK DATA VOOR Jaar: " + currentYear
                + " [" + dataCollection.get(currentYear).getDataFile() + "]");

        viewedCurriculum = null;
        viewedEmployee = null;
        viewedCurriculumModule = null;
        viewedCurriculumYear = null;
        viewedCurriculumVariant = null;
        viewedProject = null;
        viewedTaskCategory = null;

        /*show the teachers list*/
        showTeachersList();
        showEmployeeDetails();

        /*show the curricula list*/
        showCurriculaList();
        showCurriculumDetails();
        showCurriculumVariantDetails(viewedCurriculumVariant);

        /*show the curriculum variants list*/
        showCurriculumVariantsList();

        /*show the module list*/
        showModulesList();
        showModuleDetails();

        /*show the regular tasks*/
        showRegularTasksCategoriesList();
        showRegularTasksOverview();

        /*show the projects overview*/
        showProjectsList();
        showProjectsOverview();

        /*show the classes overview*/
        showClassesOverview();

        /*show the general overview of employee usage and costs per curriculum*/
        showGeneralOverview();

        /*shows the finances tab*/
        showFinancialOverview();
    }

    /**
     * shows general help and info on TaskMan.
     */
    private void showGeneralHelp() {
        //System.out.println("show general help");
        @SuppressWarnings("unused")
        GeneralHelpFrame ghf = new GeneralHelpFrame();
    }

    /**
     * shows the help items.
     */
    private void showHelpItems() {
        //System.out.println("show help items");
        @SuppressWarnings("unused")
        HelpItemsFrame ghf = new HelpItemsFrame();
    }

    /**
     * for selecting and editing curriculum variants.
     */
    private void showCurriculumVariantsList() {
        JComponent curriculumVariantTree = guiComponentFactory.getCurriculumVariantTreePanel(
                SelectedType.CURRICULUMVARIANT.name(),
                dataCollection.get(this.currentYear),
                new TreeSelectionListenerImpl());
        frame.getTabPanel(TaskManagerFrame.CURRICULUM_VARIANT_TAB).setListComponent(curriculumVariantTree);

    }

    /**
     * shows the curricula list.
     */
    private void showCurriculaList() {
        List<Curriculum> curricula = dataCollection.get(currentYear).getCurricula();
        //for(Curriculum curr : curricula ){ System.out.println(curr);}
        String[] curriculaIdArr = new String[curricula.size()];
        int selectedIndex = -1;
        for (int i = 0; i < curricula.size(); i++) {
            curriculaIdArr[i] = curricula.get(i).getCurriculumID();
            if (viewedCurriculum != null
                    && curricula.get(i).getCurriculumID().equals(viewedCurriculum.getCurriculumID())) {
                selectedIndex = i;
            }
        }

        /*get the curricula selectable list and register as listener*/
        JComponent currList = guiComponentFactory.getListPanel(
                SelectedType.CURRICULUM.name(),
                curriculaIdArr,
                new ExtListSelectionListener(curriculaIdArr), selectedIndex);
        frame.getTabPanel(TaskManagerFrame.CURRICULA_TAB).setListComponent(currList);
    }

    /**
     * shows the modules list.
     */
    private void showModulesList() {
        JComponent moduleTree = guiComponentFactory.getModuleTreePanel(
                SelectedType.MODULE.name(),
                dataCollection.get(this.currentYear),
                new TreeSelectionListenerImpl());
        frame.getTabPanel(TaskManagerFrame.MODULES_TAB).setListComponent(moduleTree);
    }

    /**
     * shows the teachers list.
     */
    private void showTeachersList() {
        /*show the teachers list*/
        List<Employee> teachers = dataCollection.get(currentYear).getEmployees();
        String[] teachersNameArr = new String[teachers.size() + 1];
        String[] teachersIdArr = new String[teachers.size() + 1];
        int selectedIndex = 0;
        teachersNameArr[0] = "TOON OVERZICHT";
        teachersIdArr[0] = "OVERVIEW";

        for (int i = 0; i < teachers.size(); i++) {
            Employee current = teachers.get(i);
            teachersNameArr[i + 1] = current.getFullName();
            teachersIdArr[i + 1] = current.getEmployeeId();
            if (viewedEmployee != null && current.getEmployeeId().equals(viewedEmployee.getEmployeeId())) {
                selectedIndex = i + 1;
            }
        }
        /*get the teachers selectable list and register as listener*/
        JComponent teacherList = guiComponentFactory.getListPanel(
                SelectedType.TEACHER.name(),
                teachersNameArr,
                new ExtListSelectionListener(teachersIdArr),
                selectedIndex);
        frame.getTabPanel(TaskManagerFrame.EMPLOYEE_TAB).setListComponent(teacherList);
    }

    /**
     * takes care of displaying the selectable projects list.
     */
    private void showProjectsList() {
        List<Project> projects = dataCollection.get(currentYear).getProjects();
        String[] projectsNameArr = new String[projects.size() + 1];
        String[] projectsIdArr = new String[projects.size() + 1];
        projectsNameArr[0] = "TOON OVERZICHT";
        projectsIdArr[0] = "OVERVIEW";
        int selectedIndex = 0;
        for (int i = 0; i < projects.size(); i++) {
            Project pr = projects.get(i);
            projectsNameArr[i + 1] = pr.getName();
            projectsIdArr[i + 1] = pr.getProjectId();
            if (viewedProject != null && pr.getName().equals(viewedProject.getName())) {
                selectedIndex = i + 1;
            }
        }
        /*get the projects selectable list and register as listener*/
        JComponent projectsList = guiComponentFactory.getListPanel(
                SelectedType.PROJECT.name(),
                projectsNameArr,
                new ExtListSelectionListener(projectsIdArr),
                selectedIndex);
        frame.getTabPanel(TaskManagerFrame.PROJECTS_TAB).setListComponent(projectsList);
    }

    /**
     * takes care of displaying the selectable regular task categories list.
     */
    private void showRegularTasksCategoriesList() {
        TaskCategory[] categories = TaskCategory.values();
        String[] catNameArr = new String[categories.length + 1];
        String[] catIdArr = new String[categories.length + 1];
        catNameArr[0] = "TOON OVERZICHT";
        catIdArr[0] = "OVERVIEW";
        int selectedIndex = 0;
        for (int i = 0; i < categories.length; i++) {
            TaskCategory cat = categories[i];
            catNameArr[i + 1] = cat.toString();
            catIdArr[i + 1] = cat.name();
            if (viewedTaskCategory != null && cat.name().equals(viewedTaskCategory.name())) {
                //System.out.println("viewed cat=" + cat.name());
                selectedIndex = i + 1;
            }
        }
        /*get the projects selectable list and register as listener*/
        JComponent regTasksList = guiComponentFactory.getListPanel(
                SelectedType.REGULAR_TASK.name(),
                catNameArr,
                new ExtListSelectionListener(catIdArr),
                selectedIndex);
        frame.getTabPanel(TaskManagerFrame.REGULAR_TASKS_TAB).setListComponent(regTasksList);
    }

    /**
     * decides which type of detail panel should be shown.
     *
     * @param type the selected type
     * @param id the id of the selected type
     */
    private void showSelectionDetails(final SelectedType type, final String id) {
        switch (type) {
            case TEACHER: {
                this.viewedEmployee = dataCollection.get(currentYear).getEmployee(id);
                if (id.equals("OVERVIEW")) {
                    //System.out.println( this.getClass().getSimpleName() + ": showing employees overview pane" );
                    viewedEmployee = null;
                } else {
                    this.viewedEmployee = dataCollection.get(currentYear).getEmployee(id);
                }
                showEmployeeDetails();
                break;
            }
            case CURRICULUM: {
                this.viewedCurriculum = dataCollection.get(currentYear).getCurriculum(id);
                showCurriculumDetails();
                break;
            }
            case CURRICULUMVARIANT: {
                //bv id = BFV-REG-1 or BFV-REG
                String[] elements = id.split("-");
                Curriculum c = dataCollection.get(currentYear).getCurriculum(elements[0]);
                CurriculumVariant cv = c.getCurriculumVariant(elements[1]);
                if (elements.length == 2) {
                    this.viewedCurriculumVariant = cv;
                    showCurriculumVariantDetails(cv);
                } else if (elements.length == 3) {
                    int year = Integer.parseInt(elements[2]);
                    CurriculumYear cy = cv.getCurriculumYear(year);
                    this.viewedCurriculumYear = cy;
                    showCurriculumYearDetails(cy);
                }
                break;
            }
            case MODULE: {
                this.viewedCurriculumModule = dataCollection.get(currentYear).getModule(id);
                showModuleDetails();
            }
            case PROJECT: {
                if (id.equals("OVERVIEW")) {
                    //System.out.println( this.getClass().getSimpleName() + ": showing projects overview pane" );
                    viewedProject = null;
                } else {
                    this.viewedProject = dataCollection.get(currentYear).getProjectById(id);
                }
                showProjectsOverview();
                break;
            }
            case REGULAR_TASK: {
                if (id.equals("OVERVIEW")) {
                    viewedTaskCategory = null;
                } else {
                    this.viewedTaskCategory = TaskCategory.valueOf(id);
                }
                showRegularTasksOverview();
                break;
            }
            default:
                throw new IllegalArgumentException("unknown type requested: " + type);
        }
    }

    /**
     * shows the details of a curriculum year also for editing.
     *
     * @param cv the curriculum variant
     */
    private void showCurriculumVariantDetails(final CurriculumVariant cv) {
        CurriculumVariantDetailPanel cvdp = null;
        if (viewedCurriculumVariant != null) {
            cvdp = new CurriculumVariantDetailPanel(this, viewedCurriculumVariant, currentYear);
        }
        frame.getTabPanel(TaskManagerFrame.CURRICULUM_VARIANT_TAB).setDetailComponent(cvdp);
        frame.repaint();
    }

    /**
     * shows the details of a curriculum year also for editing.
     *
     * @param cy the curriculum year
     */
    private void showCurriculumYearDetails(final CurriculumYear cy) {
        CurriculumYearDetailPanel cydp = null;
        if (viewedCurriculumYear != null) {
            cydp = new CurriculumYearDetailPanel(this, viewedCurriculumYear, currentYear);
        }
        frame.getTabPanel(TaskManagerFrame.CURRICULUM_VARIANT_TAB).setDetailComponent(cydp);
        frame.repaint();
    }

    /**
     * shows a general overview of employee usage and costs.
     */
    private void showGeneralOverview() {
        JComponent generalOverview = new GeneralOverviewPanel(this);
        frame.getTabPanel(TaskManagerFrame.OVERVIEW_TAB).setDetailComponent(generalOverview);
    }

    /**
     * shows the financial overview tab.
     */
    private void showFinancialOverview() {
        JComponent financialOverview = new FinancialOverviewPanel();
        frame.getTabPanel(TaskManagerFrame.FINANCES_TAB).setDetailComponent(financialOverview);
    }

    /**
     * shows an overview of all student classes.
     */
    private void showClassesOverview() {
        JComponent classesOverview = new ClassesOverviewPanel(this);
        frame.getTabPanel(TaskManagerFrame.CLASSES_TAB).setDetailComponent(classesOverview);
    }

    /**
     * shows an overview of all projects or overview.
     */
    private void showProjectsOverview() {
        //System.out.println( this.getClass().getSimpleName() + " showing viewed project: " + viewedProject);
        if (this.viewedProject == null) {
            JComponent projectsOverview = new ProjectsOverviewPanel(this);
            frame.getTabPanel(TaskManagerFrame.PROJECTS_TAB).setDetailComponent(projectsOverview);
        } else {
            JComponent projectDetailPanel = new ProjectDetailPanel(this, viewedProject);
            frame.getTabPanel(TaskManagerFrame.PROJECTS_TAB).setDetailComponent(projectDetailPanel);
        }
        frame.repaint();
    }

    /**
     * shows selected regular task category or overview.
     */
    private void showRegularTasksOverview() {
        if (this.viewedTaskCategory == null) {
            JComponent regularTasksOverview = new RegularTaskOverviewPanel(this);
            frame.getTabPanel(TaskManagerFrame.REGULAR_TASKS_TAB).setDetailComponent(regularTasksOverview);
        } else {
            JComponent regularTaskDetailPanel = new RegularTaskDetailPanel(this, viewedTaskCategory);
            frame.getTabPanel(TaskManagerFrame.REGULAR_TASKS_TAB).setDetailComponent(regularTaskDetailPanel);
        }
        frame.repaint();
    }

    /**
     * shows the curriculum details.
     */
    private void showCurriculumDetails() {
        CurriculumDetailPanel cdp = null;
        if (viewedCurriculum != null) {
            cdp = new CurriculumDetailPanel(this, viewedCurriculum, currentYear);
        }
        frame.getTabPanel(TaskManagerFrame.CURRICULA_TAB).setDetailComponent(cdp);
        frame.repaint();
    }

    /**
     * shows the teacher details of currently active Employee.
     */
    private void showEmployeeDetails() {
        if (this.viewedEmployee == null) {
            JComponent employeesOverview = new EmployeesOverviewPanel(this);
            frame.getTabPanel(TaskManagerFrame.EMPLOYEE_TAB).setDetailComponent(employeesOverview);
        } else {
            EmployeeDetailPanel edp = new EmployeeDetailPanel(this, viewedEmployee, currentYear);
            frame.getTabPanel(TaskManagerFrame.EMPLOYEE_TAB).setDetailComponent(edp);
        }
        frame.repaint();
    }

    /**
     * shows the details of a module.
     */
    private void showModuleDetails() {
        ModuleDetailPanel mdp = null;
        if (viewedCurriculumModule != null) {
            mdp = new ModuleDetailPanel(viewedCurriculumModule, this, currentYear);
        }
        frame.getTabPanel(TaskManagerFrame.MODULES_TAB).setDetailComponent(mdp);
        frame.repaint();
    }

    /**
     * sets the current active employee.
     *
     * @param employee the employee to set active/viewed
     */
    public void setViewedEmployee(final Employee employee) {
        this.viewedEmployee = employee;
    }

    /**
     * sets the current active TaskCategory.
     *
     * @param tc the task category
     */
    public void setViewedTaskCategory(final TaskCategory tc) {
        this.viewedTaskCategory = tc;
    }

    /**
     * exports the task overview of the given list of employees to pdf.
     *
     * @param employees the employees to export
     */
    public void exportEmployeeTaskoverviews(final List<Employee> employees) {
        FileExporter.exportEmployeeTaskoverviews(employees, getCurrentYear(), frame);
    }

    /**
     * exports all task lists to PDF. Only a directory has to be selected.
     * @param year the school year
     */
    private void selectEmployeesForExport(final int year) {
        /*first get list of employees to be exported*/
        EmployeeSelectionFrame esf = new EmployeeSelectionFrame(this);
        esf.setVisible(true);
    }

    /**
     * writes a single employee to pdf.
     *
     * @param employee the employee
     * @param year the school year
     */
//    public void writeEmployeeToPdf(final Employee employee, final int year) {
//        FileExporter.writeEmployeeToPdf(employee, year, frame);
//    }

    /**
     * writes a single project to pdf.
     *
     * @param project the project
     * @param year the school year
     */
    public void writeProjectToPdf(final Project project, final int year) {
        FileExporter.writeProjectToPdf(project, year, frame);
    }

    /**
     *
     * @param taskCategory the task category
     * @param year the school year
     */
    public void writeRegularTaskCatToPdf(final TaskCategory taskCategory, final int year) {
        FileExporter.writeRegularTaskCatToPdf(this, taskCategory, year, frame);
    }

    /**
     * writes a curriculum employee usage to pdf.
     *
     * @param curriculum the curriculum
     * @param year the school year
     */
    public void writeCurriculumToPdf(final Curriculum curriculum, final int year) {
        FileExporter.writeCurriculumToPdf(curriculum, year, frame);
    }

    /**
     * listens to list selection events.
     *
     * @author M.A. Noback (m.a.noback@pl.hanze.nl)
     * @version 0.1
     */
    public class ExtListSelectionListener implements ListSelectionListener {
        /**
         * the list elements.
         */
        private final String[] listElements;
        /**
         * the jList being listened to.
         */
        private JList<String> jList;

        /**
         *
         * @param listElements the list elements
         */
        public ExtListSelectionListener(final String[] listElements) {
            this.listElements = listElements;
        }

        /**
         *
         * @param jlist the jlist
         */
        public void setJList(final JList<String> jlist) {
            this.jList = jlist;
        }

        @Override
        public void valueChanged(final ListSelectionEvent e) {
            if (jList != null && !e.getValueIsAdjusting()) {
                int index = jList.getSelectedIndex();
                /*array at index will hold ID to relevant element*/
                showSelectionDetails(SelectedType.valueOf(jList.getName()), listElements[index]);
            }
        }
    }

    /**
     * tree selection listener.
     */
    public class TreeSelectionListenerImpl implements TreeSelectionListener {
        /**
         * the jTree being listened to.
         */
        private JTree tree;

        /**
         * @param tree the tree
         */
        public void setTree(final JTree tree) {
            this.tree = tree;
        }

        @Override
        public void valueChanged(final TreeSelectionEvent e) {
            //Returns the last path element of the selection.
            //This method is useful only when the selection model allows a single selection.
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

            //Nothing is selected.
            if (node == null) {
                return;
            }

            Object nodeInfo = node.getUserObject();
            if (node.isLeaf()) {
                try {
                    if (nodeInfo instanceof CurriculumModule) {
                        CurriculumModule curriculumModule = (CurriculumModule) nodeInfo;
                        showSelectionDetails(SelectedType.valueOf(tree.getName()), curriculumModule.getModuleId());
                    } else if (nodeInfo instanceof CurriculumYear) {
                        CurriculumYear cy = (CurriculumYear) nodeInfo;
                        showSelectionDetails(SelectedType.valueOf(tree.getName()), cy.toString());
                    } else if (nodeInfo instanceof CurriculumVariant) {
                        CurriculumVariant cv = (CurriculumVariant) nodeInfo;
                        showSelectionDetails(SelectedType.valueOf(tree.getName()), cv.toString());
                    }

                } catch (Exception ex) {
                    Logger.getLogger(TaskManagerController.class.getName()).log(
                            Level.INFO,
                            "exception in tree value changed event " + e,
                            ex);
                }
            } else {
                if (nodeInfo instanceof CurriculumVariant) {
                    @SuppressWarnings("unused")
                    CurriculumVariant cv = (CurriculumVariant) nodeInfo;
                    //TODO implement showing of selected curriculum variant or curriculum
                    //showSelectionDetails( SelectedType.valueOf( tree.getName() ), cv.toString() );
                }
            }
        }
    }

    /**
     * handle window events. Only window closing event will be dealt with
     *
     * @author M.A. Noback (m.a.noback@pl.hanze.nl)
     * @version 0.1
     */
    private class TaskManagerWindowListener implements WindowListener {

        @Override
        public void windowClosing(final WindowEvent arg0) {
            //TODO restore
            exit();
            //System.exit(0);
            //System.out.println("window is closing");
        }

        @Override
        public void windowActivated(final WindowEvent arg0) { }

        @Override
        public void windowClosed(final WindowEvent arg0) { }

        @Override
        public void windowDeactivated(final WindowEvent arg0) { }

        @Override
        public void windowDeiconified(final WindowEvent arg0) { }

        @Override
        public void windowIconified(final WindowEvent arg0) { }

        @Override
        public void windowOpened(final WindowEvent arg0) { }
    }
}
