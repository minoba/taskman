/**
 * 
 */
package nl.bioinf.nomi.tasks.control;


/**
 * the main class of the GUI application
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 0.1
 */
public class TaskManager {
	
	public static void main(String[] args){
		TaskManagerController.getInstance().go();
	}
	
}
